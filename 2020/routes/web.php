<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

Auth::routes();
Route::group(['middleware' => ['web','auth']], function() {
    Route::get('/bantuan', 'HomeController@bantuan')->name('bantuan');

    //SKPD
    Route::get('/skpd/skpd_export', 'SkpdController@skpd_export')->name('skpd_export');
    
    //KINERJA ANGGARAN
    Route::get('kinerja_anggaran/export_excel', 'KinerjaAnggaranController@export_excel')->name('kinerja_anggaran.export_excel');
    Route::get('kinerja_anggaran/create/{provinsi_id?}/{user_id?}', 'KinerjaAnggaranController@create')->name('kinerja_anggaran.create');
    Route::get('kinerja_anggaran/edit/{kinerja_anggaran_id}/{provinsi_id?}', 'KinerjaAnggaranController@edit')->name('kinerja_anggaran.edit');
    Route::get('kinerja_anggaran/{kinerja_anggaran_id}/{tahap}', 'KinerjaAnggaranController@download_file')->name('kinerja_anggaran.download_file');

    // PESERTA
    Route::get('/peserta/{pelatihan_id}', 'PesertaController@index')->name('peserta.index');
    Route::get('/peserta/{peserta_id}/show', 'PesertaController@show')->name('peserta.show');
    Route::get('working_paper/{user_id?}', 'DashboardController@working_paper')->name('working_paper');
    
    // Route::get('/peserta/{code}/lihat_peserta', 'PesertaController@lihat_peserta')->name('peserta.lihat_peserta');
    Route::get('/peserta/{pelatihan_id}/format_laporan', 'PesertaController@format_laporan')->name('peserta.format_laporan');
    Route::get('/peserta/{peserta_id}/peserta_export', 'PesertaController@peserta_export')->name('peserta.peserta_export');
    
    // PENDAMPING
    // Route::resource('/peserta', 'PesertaController');
    Route::get('/pendamping/pendamping_export/{user_id?}', 'PendampingController@pendamping_export')->name('pendamping_export');
    Route::resource('/pendamping', 'PendampingController');
    Route::group(['middleware' => ['ifNotDinas']], function() {
        Route::get('/dashboard', 'HomeController@index')->name('dashboard');

        // SKPD
        Route::resource('/skpd', 'SkpdController');

        // KINERJA ANGGARAN
        Route::resource('/kinerja_anggaran', 'KinerjaAnggaranController')->except([
            'create', 'edit'
        ]);

        //PELATIHAN
        Route::resource('/pelatihan', 'PelatihanController');

        // PESERTA
        Route::get('/peserta/{pelatihan_id}/create', 'PesertaController@create')->name('peserta.create');
        Route::post('/peserta', 'PesertaController@store')->name('peserta.store');
        Route::get('/peserta/{pelatihan_id}/{peserta_id}/edit', 'PesertaController@edit')->name('peserta.edit');
        Route::patch('/peserta/{pelatihan_id}/{peserta_id}/update', 'PesertaController@update')->name('peserta.update');
        Route::delete('/peserta/{peserta_id}/destroy', 'PesertaController@destroy')->name('peserta.destroy');
        Route::get('/peserta/{peserta_id}/createQrcode', 'PesertaController@createQrcode')->name('peserta.createQrcode');
        Route::get('/peserta/{pelatihan_id}/{peserta_id}/pemantauan', 'PesertaController@pemantauan')->name('peserta.pemantauan');
        Route::patch('/peserta/pemantauan/{peserta_id}', 'PesertaController@store_pemantauan')->name('peserta.store_pemantauan');

        //ROUTE USER
        // Route::get('/user', 'UserController@index')->name('user.index');
        Route::get('/user/form', 'UserController@form')->name('user.form');
        Route::patch('/user/{user_id}/profile_picture_update', 'UserController@profile_picture_update')->name('user.profile_picture_update');
        Route::patch('/user/{user_id}/password_update', 'UserController@password_update')->name('user.password_update');
    });

    Route::group(['middleware' => ['ifNotAdministrator']], function() {
        Route::get('/home', 'DashboardController@index')->name('home.index');
        //SKPD ADMIN
        Route::get('home/skpd', 'DashboardController@skpd')->name('home.skpd');
        Route::get('home/skpd/{provinsi_id}/list_skpd', 'DashboardController@list_skpd')->name('home.list_skpd');
        
        
        //KINERJA ANGGARAN ADMIN
        Route::get('home/kinerja_anggaran', 'DashboardController@kinerja_anggaran')->name('home.kinerja_anggaran');
        Route::get('home/kinerja_anggaran/{provinsi_id}/list_kinerja_anggaran', 'DashboardController@list_kinerja_anggaran')->name('home.list_kinerja_anggaran');
        
        //PELATIHAN ADMIN
        Route::get('home/pelatihan', 'DashboardController@pelatihan')->name('home.pelatihan');
        Route::get('home/pelatihan/{provinsi_id}/list_user', 'DashboardController@list_user')->name('home.list_user');
        Route::get('home/pelatihan/{user_id}/list_pelatihan', 'DashboardController@list_pelatihan')->name('home.list_pelatihan');
        
        //PENDAMPING ADMIN
        Route::get('home/pendamping', 'DashboardController@pendamping')->name('home.pendamping');
        Route::get('home/pendamping/{provinsi_id}/list_user', 'DashboardController@list_user_pendamping')->name('home.list_user_pendamping');
        Route::get('home/pendamping/{user_id}/list_pendamping', 'DashboardController@list_pendamping')->name('home.list_pendamping');
    });
});
