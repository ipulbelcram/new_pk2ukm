<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('/total_peserta_provinsi', 'API\HomeApi@total_peserta_provinsi');
Route::get('/total_peserta_jenis_pelatihan', 'API\HomeApi@total_peserta_jenis_pelatihan');
Route::get('/total_peserta_jenis_kelamin', 'API\HomeApi@total_peserta_jenis_kelamin');
Route::get('/total_peserta_agama', 'API\HomeApi@total_peserta_agama');
Route::get('/total_peserta_provinsi', 'API\HomeApi@total_peserta_provinsi');
Route::get('/total_peserta_pendidikan', 'API\HomeApi@total_peserta_pendidikan');
Route::get('/total_peserta_status_peserta', 'API\HomeApi@total_peserta_status_peserta');
Route::get('/bidang_usaha_umkm', 'API\HomeApi@bidang_usaha_umkm');
Route::POST('image_upload', 'API\HomeApi@image_upload');