jQuery(document).ready(function($) {
  var user_level = $("#user_level").val();

  if (user_level == 10) {
    $("#anggaranTahap1").remove();
    $("#anggaranTahap2").remove();
    $("#pesertaTahap1").remove();
    $("#pesertaTahap2").remove();
    $("#pendampingTahap1").remove();
  }

  $("#inputGroupFile01").change(function(e) {
    var fileName = e.target.files[0].name;
    $("#fileName01").html(fileName);
  });

  $("#inputGroupFile02").change(function(e) {
    var fileName = e.target.files[0].name;
    $("#fileName02").html(fileName);
  });
});
