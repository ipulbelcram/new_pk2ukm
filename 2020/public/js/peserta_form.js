jQuery(document).ready(function($) {
    var status_usaha = $( "#status_usaha" ).val();
    
    if(status_usaha == 25) {
        $("#form-koperasi").show();
        $("#form-umkm").hide();
    } else if(status_usaha == 26) {
        $("#form-koperasi").hide();
        $("#form-umkm").show();
    } else {
        $("#form-koperasi").hide();
        $("#form-umkm").hide();    
    }
    
    $( "#status_usaha" ).on('change', function() {
        if($(this).val() == 25) {
            $("#form-koperasi").show();
            $("#form-umkm").hide();
        } else if($(this).val() == 26) {
            $("#form-koperasi").hide();
            $("#form-umkm").show();
        } else {
            $("#form-koperasi").hide();
            $("#form-umkm").hide();
        }
    })


    // Select option bidang usaha
    var jenis_usaha_id = $("#jenis_usaha").val();
    if(jenis_usaha_id != '') {
    	$.ajax({
    		url:"http://pk2ukm.kemenkopukm.go.id/2020/api/bidang_usaha_umkm",
    		type:"GET",
    		dataType:"JSON",
    		data: {
    			jenis_usaha_id: jenis_usaha_id,
    		}, 
    		success: function(result) {
    			var data = result.data;
    			$.each(data, function(key, value) {
    				$("#bidang_usaha").append("<option value="+ value.id +">"+value.bidang_usaha_umkm+"</option>")
    			})
    			var bidang_usaha_id = $("#bidang_usaha_value").val();
    			$("#bidang_usaha option[value="+bidang_usaha_id+"]").attr('selected',true);
    		}
    	})
    } else {
    	$("#bidang_usaha").attr("disabled", true);
    }
    $("#jenis_usaha option:first").attr('disabled',true);
    $("#bidang_usaha option:first").attr('disabled',true);

    $("#jenis_usaha").on('change', function() {
    	var jenis_usaha_id = $(this).val();
    	$("#bidang_usaha").html('');
    	$.ajax({
    		url:"http://pk2ukm.kemenkopukm.go.id/2020/api/bidang_usaha_umkm",
    		type:"GET",
    		dataType:"JSON",
    		data: {
    			jenis_usaha_id: jenis_usaha_id,
    		}, 
    		success: function(result) {
    			var data = result.data;
    			$.each(data, function(key, value) {
    				$("#bidang_usaha").append("<option value="+ value.id +">"+value.bidang_usaha_umkm+"</option>")
    			})
    			$("#bidang_usaha").attr("disabled", false);
    		}
    	})
    })
});