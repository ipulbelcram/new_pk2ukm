<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use App\Peserta;

class PesertaExport implements FromView, ShouldAutoSize
{
    public function __construct(string $peserta_id)
    {
        $this->peserta_id = $peserta_id;
    }
    
    public function view(): View
    {
        return view('export.PesertaDetail', [
            'data' => Peserta::findOrFail($this->peserta_id),
        ]);
    }
}
