<?php

namespace App\Exports;

use App\Skpd;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;


class SkpdExport implements FromView, ShouldAutoSize, WithEvents
{
    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:H1'; // All headers
                $cellRange2 = 'A2:H2'; // All headers
                $cellRange3 = 'A3:H4'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange2)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange3)->getFont()->setSize(14);
            },
        ];
    }

    public function view(): View
    {
        return view('export.SkpdLengkap', [
            'skpd_data' => Skpd::all(),
        ]);
    }
}
