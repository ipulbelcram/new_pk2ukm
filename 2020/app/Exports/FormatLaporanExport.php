<?php

namespace App\Exports;

use App\Peserta;
use App\Pelatihan;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class FormatLaporanExport implements FromView, ShouldAutoSize, WithEvents
{
    public function __construct(string $pelatihan_id)
    {
        $this->pelatihan_id = $pelatihan_id;
    }
       
    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:N1'; // All headers
                $cellRange2 = 'A2:N2'; // All headers
                $cellRange3 = 'A3:N4'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange2)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange3)->getFont()->setSize(14);
            },
        ];
    }

    public function view(): View
    {
        return view('export.FormatLaporan', [
            'data' => Peserta::where('pelatihan_id', $this->pelatihan_id)->get(),
            'pelatihan' => Pelatihan::findOrFail($this->pelatihan_id),
            'jumlah_data' => Peserta::where('pelatihan_id', $this->pelatihan_id)->count()
        ]);
    }
}
