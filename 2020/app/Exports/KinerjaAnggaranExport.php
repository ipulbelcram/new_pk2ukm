<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class KinerjaAnggaranExport implements FromView, ShouldAutoSize
{
    
    // public function __construct(string $provinsi_id)
    // {
    //     $this->provinsi_id = $provinsi_id;
    // }
    
    public function view(): View
    {
        return view('export.KinerjaAnggaran', [
            'data' => DB::table('vw_user_kinerja_anggaran')->where('user_level_id', '!=', 10)->orderBy('order_prov', 'ASC')->orderBy('order_kab', 'ASC')->get()
        ]);
    }
}
