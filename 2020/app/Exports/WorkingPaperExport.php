<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use App\Pelatihan;

class WorkingPaperExport implements FromView, ShouldAutoSize
{
    public function __construct(string $user_id = null)
    {
        $this->user_id = $user_id;
    }
    
    public function view(): View
    {
        if(!empty($this->user_id)) {
            return view('export.WorkingPaper', [
                'data' => DB::table('vw_peserta_pelatihan')->where('user_id', $this->user_id)->get()
            ]);
        } else {
            return view('export.WorkingPaper', [
                'data' => DB::table('vw_peserta_pelatihan')->get()
            ]);
        }
    }
}
