<?php

namespace App\Exports;

// use Maatwebsite\Excel\Concerns\FromCollection;
use App\Pendamping;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class PendampingExport implements FromView, ShouldAutoSize, WithEvents
{
    public function __construct(string $user_id = NULL)
    {
        $this->user_id = $user_id;
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:K1'; // All headers
                $cellRange2 = 'A2:K2'; // All headers
                $cellRange3 = 'A3:K4'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange2)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange3)->getFont()->setSize(14);
            },
        ];
    }

    public function view(): View
    {
        if(!empty($this->user_id)) {
            $pendamping_data = Pendamping::where('user_id', $this->user_id)->get();
        } else {
            $pendamping_data = Pendamping::all();
        }
        return view('export.PendampingLengkap', [
            'pendamping_data' => $pendamping_data,
        ]);
    }
}
