<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KinerjaAnggaran extends Model
{
    protected $table = 'kinerja_anggaran';
    protected $fillable = [
        'user_id', 
        'pagu_anggaran', 
        'realisasi_anggaran1', 
        'upload_realisasi_anggaran1',
        'realisasi_anggaran2', 
        'upload_realisasi_anggaran2',
        'target_peserta', 
        'realisasi_peserta1', 
        'realisasi_peserta2', 
        'target_pendamping', 
        'realisasi_pendamping',
    ];
}
