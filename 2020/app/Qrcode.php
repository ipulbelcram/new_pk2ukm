<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Qrcode extends Model
{
    protected $table = 'qrcode';
    protected $fillable = [
        'peserta_id', 'user_id', 'tanggal', 'urut', 
    ];

    public $timestamps = false;

    public function peserta()
    {
        return $this->belongsTo('App\Peserta', 'peserta_id');
    }
}
