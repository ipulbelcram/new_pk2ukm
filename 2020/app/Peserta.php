<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Peserta extends Model
{
    protected $table = 'peserta';
    protected $fillable = [
        'pelatihan_id', 'nama', 'no_ktp', 'status', 'jenis_kelamin', 'tempat_lahir', 'tanggal_lahir', 'agama', 'pendidikan', 'alamat_rumah', 'kab_kota_id', 'no_hp', 'email', 'foto_profile', 'status_peserta', 'status_usaha', 'nama_koperasi', 'alamat_koperasi', 'jenis_koperasi', 'bidang_usaha_koperasi', 'no_induk_koperasi', 'jumlah_tenaga_kerja_koperasi', 'omset_koperasi', 'nama_usaha', 'alamat_usaha', 'jenis_usaha_id', 'bidang_usaha_umkm_id', 'bidang_usaha', 'lama_usaha', 'jumlah_tenaga_kerja_umkm', 'omset_usaha',
    ];

    protected $casts = [
        'tanggal_lahir' => 'datetime:d-F-Y',
        'created_at' => 'datetime:d-F-Y',
    ];

    public function agamaData()
    {
        return $this->belongsTo('App\Params', 'agama');
    }

    public function pendidikanData()
    {
        return $this->belongsTo('App\Params', 'pendidikan');
    }

    public function satusPesertaData()
    {
        return $this->belongsTo('App\Params', 'status_peserta');
    }

    public function statusUsahaData()
    {
        return $this->belongsTo('App\Params', 'status_usaha');
    }

    public function jenisKoperasiData()
    {
        return $this->belongsTo('App\Params', 'jenis_koperasi');
    }

    public function bidangUsahaKoperasiData()
    {
        return $this->belongsTo('App\Params', 'bidang_usaha_koperasi');
    }

    public function bidangUsahaData()
    {
        return $this->belongsTo('App\Params', 'bidang_usaha');
    }

    public function kab_kota()
    {
        return $this->belongsTo('App\KabKota', 'kab_kota_id');
    }

    public function qrcode()
    {
        return $this->hasOne('App\Qrcode', 'peserta_id');
    }

    public function pertanyaanPeserta()
    {
        return $this->hasOne('App\PertanyaanPeserta', 'peserta_id');
    }

    public function pelatihan()
    {
        return $this->belongsTo('App\Pelatihan', 'pelatihan_id');
    }

    public function jenisUsahaData()
    {
        return $this->belongsTo('App\Params', 'jenis_usaha_id');
    }

    public function bidangUsahaUmkm()
    {
        return $this->belongsTo('App\Params', 'bidang_usaha_umkm_id');
    }


}
