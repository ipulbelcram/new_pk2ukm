<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TotalPesertaJenisKelaminResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'jenis_kelamin' => $this->jenis_kelamin,
            'total_peserta' => $this->total_peserta,
        ];
    }
}
