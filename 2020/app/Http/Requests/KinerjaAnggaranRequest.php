<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class KinerjaAnggaranRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'pagu_anggaran' => 'required|string',
            'realisasi_anggaran1' => 'nullable|string',
            'upload_realisasi_anggaran1' => 'nullable|mimes:pdf|max:3000',
            'realisasi_anggaran2' => 'nullable|string',
            'upload_realisasi_anggaran2' => 'nullable|mimes:pdf|max:3000',
            'target_peserta' => 'required|string',
            'realisasi_peserta1' => 'nullable|string',
            'realisasi_peserta2' => 'nullable|string',
            'target_pendamping' => 'required|string', 
            'realisasi_pendamping' => 'nullable|string',
        ];
    }
}
