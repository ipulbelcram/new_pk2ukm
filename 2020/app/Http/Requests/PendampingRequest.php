<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PendampingRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        if($this->method() == 'PATCH') {
            $foto_profile = 'sometimes|image|mimes:jpeg,png,jpg,gif,svg';
        } else {
            $foto_profile = 'required|image|mimes:jpeg,png,jpg,gif,svg';
        }

        return [
            'nama_lengkap' => 'required|string',
            'no_ktp' => 'required|numeric|digits:16',
            'status' => 'required|in:Lajang,Menikah',
            'jenis_kelamin' => 'required|in:Laki-laki,Perempuan',
            'tempat_lahir' => 'required|string',
            'tanggal_lahir' => 'required|date',
            'agama' => 'required|exists:params,id',
            'pendidikan' => 'required|exists:params,id',
            'alamat_rumah' => 'required|string',
            'kab_kota_id' => 'required|exists:kab_kota,id',
            'no_hp' => 'required|numeric|digits_between:8,20',
            'email' => 'required|email',
            'foto_profile' => $foto_profile,
        ];
    }
}
