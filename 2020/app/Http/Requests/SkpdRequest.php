<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SkpdRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nama_kepala_dinas' => 'required|string',
            'nip_kepala_dinas' => 'required|numeric|digits:18',
            'no_hp_kep_dinas' => 'required|numeric|digits_between:8,20',
            'penanggung_jawab' => 'required|string',
            'no_hp_peng_jawab' => 'required|numeric|digits_between:8,20',
        ];
    }
}
