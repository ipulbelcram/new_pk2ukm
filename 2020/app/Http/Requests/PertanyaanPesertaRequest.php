<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PertanyaanPesertaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'pelatihan_id' => 'required|numeric',
            'peserta_id' => 'required|numeric',
            'p1' => 'required|string',
            'p2' => 'required|string',
            'p3' => 'required|string',
            'p4' => 'required|string',
            'p5' => 'required|string',
            'p6' => 'required|string',
            'p7' => 'required|string',
            'p8' => 'required|string',
        ];
    }
}
