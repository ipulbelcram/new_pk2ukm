<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PesertaRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        if($this->method() == 'PATCH') {
            $foto_profile = 'sometimes|image|mimes:jpeg,png,jpg,gif,svg';
        } else {
            $foto_profile = 'required|image|mimes:jpeg,png,jpg,gif,svg';
        }

        if($this->status_usaha == 25) {
            //KOPERASI REQUIRED
            $bidang_usaha_koperasi = 'required|exists:params,id'; // SEKTOR USAHA KOPERASI
            $nama_koperasi = 'required|string';
            $alamat_koperasi = 'required|string';
            $jenis_koperasi = 'required|exists:params,id';
            $no_induk_koperasi = 'required|in:true,false';
            $jumlah_tenaga_kerja_koperasi = 'required|numeric';
            $omset_koperasi = 'required|string';

            //USAHA NULLABLE
            $bidang_usaha = 'nullable|exists:params,id'; //SEKTOR USAHA UMKM
            $nama_usaha = 'nullable|string';
            $alamat_usaha = 'nullable|string';
            $jenis_usaha_id = 'nullable|exists:params,id';
            $bidang_usaha_umkm_id = 'nullable|exists:params,id';
            $lama_usaha = 'nullable|string';
            $jumlah_tenaga_kerja_umkm = 'nullable|numeric';
            $omset_usaha = 'nullable|string';
        } else if($this->status_usaha == 26) {
            //KOPERASI NULLABLE
            $bidang_usaha_koperasi = 'nullable|exists:params,id'; //SEKTOR USAHA KOPERASI
            $nama_koperasi = 'nullable|string';
            $alamat_koperasi = 'nullable|string';
            $jenis_koperasi = 'nullable|exists:params,id';
            $no_induk_koperasi = 'nullable|in:true,false';
            $jumlah_tenaga_kerja_koperasi = 'nullable|numeric';
            $omset_koperasi = 'nullable|string';

            //USAHA (UMKM) REQUIRED
            $bidang_usaha = 'required|exists:params,id'; //SEKTOR USAHA UMKM
            $nama_usaha = 'required|string';
            $alamat_usaha = 'required|string';
            $jenis_usaha_id = 'required|exists:params,id';
            $bidang_usaha_umkm_id = 'required|exists:params,id';
            $lama_usaha = 'required|string';
            $jumlah_tenaga_kerja_umkm = 'required|numeric';
            $omset_usaha = 'required|string';
        } else {
            //KOPERASI NULLABLE
            $bidang_usaha_koperasi = 'nullable|exists:params,id'; //SEKTOR USAHA KOPERASI
            $nama_koperasi = 'nullable|string';
            $alamat_koperasi = 'nullable|string';
            $jenis_koperasi = 'nullable|exists:params,id';
            $no_induk_koperasi = 'nullable|in:true,false';
            $jumlah_tenaga_kerja_koperasi = 'nullable|numeric';
            $omset_koperasi = 'nullable|string';

            //USAHA NULLABLE
            $bidang_usaha = 'nullable|exists:params,id'; //SEKTOR USAHA UMKM
            $nama_usaha = 'nullable|string';
            $alamat_usaha = 'nullable|string';
            $jenis_usaha_id = 'nullable|exists:params,id';
            $bidang_usaha_umkm_id = 'nullable|exists:params,id';
            $lama_usaha = 'nullable|string';
            $jumlah_tenaga_kerja_umkm = 'nullable|numeric';
            $omset_usaha = 'nullable|string';
        }
        return [
            'nama' => 'required|string',
            'no_ktp' => 'required|numeric|digits:16',
            'status' => 'required|in:Lajang,Menikah',
            'jenis_kelamin' => 'required|in:Laki-laki,Perempuan',
            'tempat_lahir' => 'required|string',
            'tanggal_lahir' => 'required|date',
            'agama' => 'required|exists:params,id',
            'pendidikan' => 'required|exists:params,id',
            'alamat_rumah' => 'required|string',
            'kab_kota_id' => 'required|exists:kab_kota,id',
            'no_hp' => 'required|numeric|digits_between:8,20',
            'email' => 'nullable|email',
            'foto_profile' => $foto_profile,
            'status_peserta' => 'required|exists:params,id',
            'status_usaha' => 'required|exists:params,id',

            //KOPERASI
            'bidang_usaha_koperasi' => $bidang_usaha_koperasi,
            'nama_koperasi' => $nama_koperasi,
            'alamat_koperasi' => $alamat_koperasi,
            'jenis_koperasi' => $jenis_koperasi,
            'no_induk_koperasi' => $no_induk_koperasi,
            'jumlah_tenaga_kerja_koperasi' => $jumlah_tenaga_kerja_koperasi,
            'omset_koperasi' => $omset_koperasi,

            //USAHA (UMKM)
            'bidang_usaha' => $bidang_usaha,
            'nama_usaha' => $nama_usaha,
            'alamat_usaha' => $alamat_usaha,
            'jenis_usaha_id' => $jenis_usaha_id,
            'bidang_usaha_umkm_id' => $bidang_usaha_umkm_id,
            'lama_usaha' => $lama_usaha,
            'jumlah_tenaga_kerja_umkm' => $jumlah_tenaga_kerja_umkm,
            'omset_usaha' => $omset_usaha,
        ];
    }
}
