<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PelatihanRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'jenis_pelatihan_id' => 'required|exists:params,id',
            'judul_pelatihan' => 'required|string',
            'tanggal_mulai' => 'required|date',
            'tanggal_selesai' => 'required|date',
            'tempat' => 'required|string',
            'kab_kota_id' => 'required|exists:kab_kota,id'
        ];
    }
}
