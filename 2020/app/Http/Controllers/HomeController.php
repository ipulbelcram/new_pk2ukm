<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Response;
use App\User;


class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = User::findOrFail(Auth::user()->id);   
        return view('home', compact('user'));
    }

    public function bantuan()
    {
        // return response()->file('assets/file/modul_pk2ukm.pdf');
        return view('bantuan');
    }
}
