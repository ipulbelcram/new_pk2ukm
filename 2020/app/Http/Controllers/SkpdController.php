<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\SkpdExport;
use App\Http\Requests\SkpdRequest;
use App\Skpd;
use Session;

class SkpdController extends Controller
{
    public function index()
    {
        $skpd= DB::table('vw_userskpd')->where('user_id', Auth::user()->id)->first();
        return view('skpd.index', compact('skpd'));
    }
 
    public function create()
    {
        return view('skpd.create');
    }

    public function store(SkpdRequest $request)
    {
        $input = $request->all();
        $input['user_id'] = Auth::user()->id;
        Skpd::create($input);
        Session::flash('flash_message', 'Data Berhasil di Tambah');
        return redirect('skpd');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $skpd = Skpd::findOrFail($id);
        return view('skpd.edit', compact('skpd'));
    }

    public function update(SkpdRequest $request, $id)
    {
        $input = $request->all();
        $skpd = Skpd::findOrFail($id);
        $skpd->update($input);

        Session::flash('flash_message', 'Data Berhasil di Simpan');
        return redirect('skpd');
    }

    public function destroy($id)
    {
        //
    }

    public function skpd_export()
    {
        $skpd_data = Skpd::all();
        return view('export/SkpdLengkap', compact('skpd_data'));
        // return Excel::download(new SkpdExport, 'SKPD Lengkap.xlsx');
    }
}
