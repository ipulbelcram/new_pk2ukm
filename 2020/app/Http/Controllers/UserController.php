<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;
use Session;
use Storage;

class UserController extends Controller
{
    public function index()
    {
        $user = User::findOrFail(Auth::user()->id);
        return view('user.index', compact('user'));
    }

    public function form()
    {
        $user = User::findOrFail(Auth::user()->id);
        return view('user/profile', compact('user'));
    }

    public function profile_picture_update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'profile_picture' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ])->validate();

        $user = User::findOrFail($id);
        if(!empty($request->image_name)) {
            $profile_picture = $request->image_name;
            Storage::move('foto_coba/'.$profile_picture, 'images/profile/'.$profile_picture);
            $input['profile_picture'] = $profile_picture;
        }

        if($request->hasfile('profile_picture')) {
            //HAPUS FOTO LAMA
            $this->deleteImage($user);
        }
        // Storage::disk('local_public')->deleteDirectory('foto_coba');
        $user->update($input);
        Session::flash('flash_message', 'Foto Profile Berhasil di Simpan !!');
        return redirect()->route('dashboard');
    }

    public function password_update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'required|string|min:8|confirmed',
        ])->validate();

        $user = User::findOrFail($id);
        if(!empty($request->password)) {
            $input['password'] = Hash::make($request->password);
        }
        $user->update($input);
        Session::flash('flash_message', 'Pasword Berhasil di Simpan !!');
        return redirect()->route('dashboard');
    }

    public function deleteImage($user) 
    {
        $exist = Storage::disk('local_public')->exists('images/profile/'.$user->profile_picture);
        if(isset($user->profile_picture)) {
            Storage::disk('local_public')->delete('images/profile/'.$user->profile_picture);
        }
    }
}
