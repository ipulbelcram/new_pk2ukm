<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Exports\FormatLaporanExport;
use App\Exports\PesertaExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Pelatihan;
use App\Http\Requests\PesertaRequest;
use App\Http\Requests\PertanyaanPesertaRequest;
use App\Peserta;
use App\Params;
use App\KabKota;
use App\Qrcode;
use App\PertanyaanPeserta;
use Session;
use Storage;
use PDF;

class PesertaController extends Controller
{

    public function index($pelatihan_id)
    {
        $pelatihan = Pelatihan::findOrFail($pelatihan_id);
        $peserta_data = Peserta::where('pelatihan_id', $pelatihan_id)->paginate(15);
        return view('peserta.index', compact('pelatihan', 'peserta_data'));
    }

    public function create($pelatihan_id)
    {
        $agama = Params::where([['category_params', 'agama'], ['active', 'true']])->pluck('params', 'id');
        $pendidikan = Params::where('category_params', 'pendidikan')->pluck('params', 'id');
        $status_peserta = Params::where('category_params', 'status_peserta')->pluck('params', 'id');
        $status_usaha = Params::where('category_params', 'status_usaha')->pluck('params', 'id');
        $jenis_koperasi = Params::where('category_params', 'jenis_koperasi')->pluck('params', 'id');
        $bidang_usaha_koperasi = Params::where([['category_params', 'bidang_usaha_koperasi'], ['active', 'true']])->pluck('params', 'id');
        $bidang_usaha = Params::where([['category_params', 'bidang_usaha'], ['active', 'true']])->pluck('params', 'id');
        $kab_kota = KabKota::where('provinsi_id', Auth::user()->provinsi_id)->pluck('kab_kota', 'id');
        $jenis_usaha = Params::where([['category_params', 'jenis_umkm'], ['active', 'true']])->pluck('params', 'id');
        return view('peserta.create', compact('pelatihan_id', 'agama', 'pendidikan', 'kab_kota', 'status_peserta', 'status_usaha', 'jenis_koperasi', 'bidang_usaha_koperasi', 'bidang_usaha', 'jenis_usaha'));
    }

    public function store(PesertaRequest $request)
    {
        $jumlah_peserta = Peserta::where([['pelatihan_id', $request->pelatihan_id], ['no_ktp', $request->no_ktp]])->count();
        if($jumlah_peserta >= 1) {
            Session::flash('message_form', 'Nomor KTP Sudah Ada');
            return redirect()->back()->withInput();;
        } else {
            $input = $request->all();
            $input = $this->validasi_status_usaha($request);
            // if(!empty($request->omset_koperasi)){
            //     $input['omset_koperasi'] = $this->number($request->omset_koperasi);
            // }
            // if(!empty($request->omset_usaha)) {
            //     $input['omset_usaha'] = $this->number($request->omset_usaha);
            // }
    
            if(!empty($request->image_name)) {
                $foto_name = $request->image_name;
                Storage::move('foto_coba/'.$foto_name, 'images/peserta_profile/'.$foto_name);
                $input['foto_profile'] = $foto_name;
            }

            // Storage::disk('local_public')->deleteDirectory('foto_coba');
            $peserta = Peserta::create($input);

            $year = date("Y");
            $id_user = Auth::user()->id;
            $urut = Qrcode::where([['tanggal', $year], ['user_id', $id_user]])->select('urut')->max('urut');
            if(empty($urut)) {
                $urut_value = 1;
            } else {
                $urut_value = $urut+1;
            }

            $code_peserta = new Qrcode;
            $code_peserta->user_id = $id_user;
            $code_peserta->tanggal = $year;
            $code_peserta->urut = $urut_value;

            $peserta->qrcode()->save($code_peserta);
            Session::flash('flash_message', 'Data Berhasil di Tambahkan');
            return redirect()->route('peserta.index', [$request->pelatihan_id]);
        }
    }

    public function show($id)
    {
        $peserta = Peserta::findOrFail($id);
        return view('peserta.show', compact('peserta'));
    }

    public function edit($pelatihan_id, $id)
    {
        $peserta = Peserta::findOrFail($id);
        
        $agama = Params::where([['category_params', 'agama'], ['active', 'true']])->pluck('params', 'id');
        $pendidikan = Params::where('category_params', 'pendidikan')->pluck('params', 'id');
        $status_peserta = Params::where('category_params', 'status_peserta')->pluck('params', 'id');
        $status_usaha = Params::where('category_params', 'status_usaha')->pluck('params', 'id');
        $jenis_koperasi = Params::where('category_params', 'jenis_koperasi')->pluck('params', 'id');
        $bidang_usaha_koperasi = Params::where('category_params', 'bidang_usaha_koperasi')->pluck('params', 'id');
        $bidang_usaha = Params::where('category_params', 'bidang_usaha')->pluck('params', 'id');
        $kab_kota = KabKota::where('provinsi_id', Auth::user()->provinsi_id)->pluck('kab_kota', 'id');
        $jenis_usaha = Params::where([['category_params', 'jenis_umkm'], ['active', 'true']])->pluck('params', 'id');
        return view('peserta.edit', compact('peserta','pelatihan_id', 'agama', 'pendidikan', 'kab_kota', 'status_peserta', 'status_usaha', 'jenis_koperasi', 'bidang_usaha_koperasi', 'bidang_usaha', 'jenis_usaha'));
    }

    public function update(PesertaRequest $request, $pelatihan_id, $id)
    {
        $jumlah_peserta = Peserta::where([['pelatihan_id', $request->pelatihan_id], ['no_ktp', $request->no_ktp], ['id', '!=', $id]])->count();
        if($jumlah_peserta >= 1) {
            Session::flash('message_form', 'Nomor KTP Sudah Ada');
            return redirect()->back()->withInput();;
        } else {
            $peserta = Peserta::findOrFail($id);
            $input = $request->all();
    
            $input = $this->validasi_status_usaha($request);

            if(!empty($request->image_name)) {
                $foto_name = $request->image_name;
                Storage::move('foto_coba/'.$foto_name, 'images/peserta_profile/'.$foto_name);
                $input['foto_profile'] = $foto_name;
            }
    
            if($request->hasfile('foto_profile')) {
                //HAPUS FOTO LAMA
                $this->deleteImage($peserta);
            }
            // Storage::disk('local_public')->deleteDirectory('foto_coba');
            // if(!empty($request->omset_koperasi)){
            //     $input['omset_koperasi'] = $this->number($request->omset_koperasi);
            // }
            // if(!empty($request->omset_usaha)) {
            //     $input['omset_usaha'] = $this->number($request->omset_usaha);
            // }
            $peserta->update($input);
    
            Session::flash('flash_message', 'Data Berhasil di Simpan');
            return redirect()->route('peserta.index', [$request->pelatihan_id]);
        }
    }

    public function destroy(Request $request,$id)
    {
        $peserta = Peserta::findOrFail($request->id);
        $peserta->delete();
        $this->deleteImage($peserta);
        Session::flash('flash_message', 'Data Berhasil di Hapus');
        return back();
    }

    private function number($number) 
    {
        $string_number = preg_replace('/[^0-9 &%\']/', '', $number);
        $input_number = trim($string_number);
        return $input_number;
    }

    public function createQrcode($peserta_id)
    {
        $tanggal = date('Ymd');
        $urut = Qrcode::where('tanggal', $tanggal)->select('urut')->max('urut');
        if(empty($urut)) {
            $urut_value = 1;
        } else {
            $urut_value = $urut+1;
        }
        $input['user_id'] = $peserta_id;
        $input['tanggal'] = $tanggal;
        $input['urut'] = $urut_value;

        Qrcode::create($input);
        return back();
    }

    public function lihat_peserta($code)
    {
        $peserta = DB::table('vw_peserta_qrcode')->where('code', $code)->first();
        return view('peserta.lihat_peserta', compact('peserta'));
    }

    public function deleteImage($peserta) 
    {
        $exist = Storage::disk('local_public')->exists('images/peserta_profile/'.$peserta->foto_profile);
        if(isset($peserta->foto_profile)) {
            Storage::disk('local_public')->delete('images/peserta_profile/'.$peserta->foto_profile);
        }
    }

    public function format_laporan($pelatihan_id) 
    {
        $data = Peserta::where('pelatihan_id', $pelatihan_id)->get();
        $pelatihan = Pelatihan::findOrFail($pelatihan_id);
        $jumlah_data = Peserta::where('pelatihan_id', $pelatihan_id)->count();
        return view('export.FormatLaporan', compact('data','pelatihan','jumlah_data'));
        // return Excel::download(new FormatLaporanExport($pelatihan_id), 'Peserta Lengkap.xlsx');
    }

    public function peserta_export($peserta_id)
    {
        $data = Peserta::findOrFail($peserta_id);
        // return view('peserta.pesertaPdf', compact('data'));
        $pdf = PDF::loadView('peserta.pesertaPdf', compact('data'))->setPaper('a4');
        return $pdf->download($data->no_ktp.'_'.$data->nama.'.pdf');
        // return Excel::download(new PesertaExport($peserta_id), 'Peserta Detail.xlsx');
    }

    public function pemantauan($pelatihan_id, $peserta_id)
    {
        $peserta = Peserta::findOrFail($peserta_id);
        return view('peserta.pemantauan', compact('pelatihan_id', 'peserta'));
    }

    public function store_pemantauan(PertanyaanPesertaRequest $request, $peserta_id)
    {
        $jumlah = PertanyaanPeserta::where('peserta_id', $peserta_id)->count();
        $input = $request->all();
        if($jumlah >= 1) {
            $pertanyaan_peserta = PertanyaanPeserta::where('peserta_id', $peserta_id)->first();
            $pertanyaan_peserta->update($input);
        } else {
            PertanyaanPeserta::create($input);
        }
        Session::flash('flash_message', 'Data Berhasil di Simpan');
        return redirect()->route('peserta.index', [$request->pelatihan_id]);
    }

    private function validasi_status_usaha($request)
    {
        $input = $request->all();
        if($request->status_usaha == 25) {
            //KOPERASI REQUIRED
            $input['bidang_usaha_koperasi'] = $request->bidang_usaha_koperasi; // SEKTOR USAHA KOPERASI
            $input['nama_koperasi'] = $request->nama_koperasi;
            $input['alamat_koperasi'] = $request->alamat_koperasi;
            $input['jenis_koperasi'] = $request->jenis_koperasi;
            $input['no_induk_koperasi'] = $request->no_induk_koperasi;
            $input['jumlah_tenaga_kerja_koperasi'] = $request->jumlah_tenaga_kerja_koperasi;
            $input['omset_koperasi'] = $this->number($request->omset_koperasi);

            //USAHA NULLABLE
            $input['bidang_usaha'] = NULL; //SEKTOR USAHA UMKM
            $input['nama_usaha'] = NULL;
            $input['alamat_usaha'] = NULL;
            $input['jenis_usaha_id'] = NULL;
            $input['bidang_usaha_umkm_id'] = NULL;
            $input['lama_usaha'] = NULL;
            $input['jumlah_tenaga_kerja_umkm'] = NULL;
            $input['omset_usaha'] = NULL;
        } else if($request->status_usaha == 26) {
            //KOPERASI NULLABLE
            $input['bidang_usaha_koperasi'] = NULL; // SEKTOR USAHA KOPERASI
            $input['nama_koperasi'] = NULL;
            $input['alamat_koperasi'] = NULL;
            $input['jenis_koperasi'] = NULL;
            $input['no_induk_koperasi'] = NULL;
            $input['jumlah_tenaga_kerja_koperasi'] = NULL;
            $input['omset_koperasi'] = NULL;

            //USAHA REQUIRED
            $input['bidang_usaha'] = $request->bidang_usaha; //SEKTOR USAHA UMKM
            $input['nama_usaha'] = $request->nama_usaha;
            $input['alamat_usaha'] = $request->alamat_usaha;
            $input['jenis_usaha_id'] = $request->jenis_usaha_id;
            $input['bidang_usaha_umkm_id'] = $request->bidang_usaha_umkm_id;
            $input['lama_usaha'] = $request->lama_usaha;
            $input['jumlah_tenaga_kerja_umkm'] = $request->jumlah_tenaga_kerja_umkm;
            $input['omset_usaha'] = $this->number($request->omset_usaha);

        } else {
            //KOPERASI NULLABLE
            $input['bidang_usaha_koperasi'] = NULL; // SEKTOR USAHA KOPERASI
            $input['nama_koperasi'] = NULL;
            $input['alamat_koperasi'] = NULL;
            $input['jenis_koperasi'] = NULL;
            $input['no_induk_koperasi'] = NULL;
            $input['jumlah_tenaga_kerja_koperasi'] = NULL;
            $input['omset_koperasi'] = NULL;

            //USAHA NULLABLE
            $input['bidang_usaha'] = NULL; //SEKTOR USAHA UMKM
            $input['nama_usaha'] = NULL;
            $input['alamat_usaha'] = NULL;
            $input['jenis_usaha_id'] = NULL;
            $input['bidang_usaha_umkm_id'] = NULL;
            $input['lama_usaha'] = NULL;
            $input['jumlah_tenaga_kerja_umkm'] = NULL;
            $input['omset_usaha'] = NULL;
        }
        return $input;
    }
}

