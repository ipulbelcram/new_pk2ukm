<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Requests\PendampingRequest;
use App\Exports\PendampingExport;
use App\Pendamping;
use App\Params;
use App\KabKota;
use Session;
use Storage;

class PendampingController extends Controller
{
    public function __construct()
    {
        $this->middleware('ifNotDinas', [
            'except' => ['show', 'pendamping_export']
        ]);
    }
    
    public function index()
    {
        $pendamping_data = Pendamping::where('user_id', Auth::user()->id)->paginate(15);
        return view('pendamping.index', compact('pendamping_data'));
    }

    
    public function create()
    {
        $agama = Params::where([['category_params', 'agama'], ['active', 'true']])->pluck('params', 'id');
        $pendidikan = Params::where('category_params', 'pendidikan')->pluck('params', 'id');
        $kab_kota = KabKota::where('provinsi_id', Auth::user()->provinsi_id)->pluck('kab_kota', 'id');
        return view('pendamping.create', compact('agama', 'pendidikan', 'kab_kota'));
    }

    
    public function store(PendampingRequest $request)
    {
        $input = $request->all();
        $input['user_id'] = Auth::user()->id;
        if(!empty($request->image_name)) {
            $foto_name = $request->image_name;
            Storage::move('foto_coba/'.$foto_name, 'images/pendamping_profile/'.$foto_name);
            $input['foto_profile'] = $foto_name;
        }
        // Storage::disk('local_public')->deleteDirectory('foto_coba');

        Pendamping::create($input);
        Session::flash('flash_message', 'Data Berhasil di Tambah');
        return redirect('pendamping');
    }

    public function show($id)
    {
        $pendamping = Pendamping::findOrFail($id);
        return view('pendamping.show', compact('pendamping'));
    }

    public function edit($id)
    {
        $pendamping = Pendamping::findOrFail($id);
        $agama = Params::where([['category_params', 'agama'], ['active', 'true']])->pluck('params', 'id');
        $pendidikan = Params::where('category_params', 'pendidikan')->pluck('params', 'id');
        $kab_kota = KabKota::where('provinsi_id', Auth::user()->provinsi_id)->pluck('kab_kota', 'id');
        return view('pendamping.edit', compact('pendamping', 'agama', 'pendidikan', 'kab_kota'));
    }

    public function update(PendampingRequest $request, $id)
    {
        $pendamping = Pendamping::findOrFail($id);
        $input = $request->all();

        if(!empty($request->image_name)) {
            $foto_name = $request->image_name;
            Storage::move('foto_coba/'.$foto_name, 'images/pendamping_profile/'.$foto_name);
            $input['foto_profile'] = $foto_name;
        }

        if($request->hasfile('foto_profile')) {
            //HAPUS FOTO LAMA
            $this->deleteImage($pendamping);
        }
        // Storage::disk('local_public')->deleteDirectory('foto_coba');

        $pendamping->update($input);
        Session::flash('flash_message', 'Data Berhasil di Simapan');
        return redirect('pendamping');
    }

    public function destroy(Request $request, $id)
    {
        $pendamping = Pendamping::findOrFail($request->id);
        $pendamping->delete();
        $this->deleteImage($pendamping);
        Session::flash('flash_message', 'Data Berhasil di Hapus');
        return back();
    }

    public function deleteImage($peserta) 
    {
        $exist = Storage::disk('local_public')->exists('images/peserta_profile/'.$peserta->foto_profile);
        if(isset($peserta->foto_profile)) {
            Storage::disk('local_public')->delete('images/peserta_profile/'.$peserta->foto_profile);
        }
    }

    public function pendamping_export($user_id = NULL)
    {
        if(!empty($user_id)) {
            $pendamping_data = Pendamping::where('user_id', $user_id)->get();
            return view('export.PendampingLengkap', compact('pendamping_data'));
            // return Excel::download(new PendampingExport($user_id), 'Pendamping Lengkap.xlsx');
        } else {
            $pendamping_data = $pendamping_data = Pendamping::all();
            return view('export.PendampingLengkap', compact('pendamping_data'));
            // return Excel::download(new PendampingExport, 'Pendamping Lengkap.xlsx');
        }
    }
}
