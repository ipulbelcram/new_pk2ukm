<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Exports\KinerjaAnggaranExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Str;
use App\Http\Requests\KinerjaAnggaranRequest;
use App\KinerjaAnggaran;
use Session;
use Storage;

class KinerjaAnggaranController extends Controller
{
    
    public function index()
    {
        $kinerjaAnggaran = DB::table('vw_user_kinerja_anggaran')->where('user_id', Auth::user()->id)->first();
        return view('kinerja_anggaran.index', compact('kinerjaAnggaran'));
    }

    
    public function create($provinsi_id = null, $user_id = null)
    {
        return view('kinerja_anggaran.create', compact('user_id', 'provinsi_id'));
    }

    public function store(KinerjaAnggaranRequest $request)
    {
        if(Auth::user()->user_level_id == 10) {
            $input['user_id'] = $request->user_id;
            $input['pagu_anggaran'] = $this->number($request->pagu_anggaran);
            $input['target_peserta'] = $this->number($request->target_peserta);
            $input['target_pendamping'] = $this->number($request->target_pendamping);
            KinerjaAnggaran::create($input);
            Session::flash('flash_message', 'Data Berhasil di Tambah');
            return redirect()->route('home.list_kinerja_anggaran', [$request->provinsi_id]);
        } else {
            $input = $request->all();
            $input['user_id'] = Auth::user()->id;
            $input['pagu_anggaran'] = $this->number($request->pagu_anggaran);
            $input['realisasi_anggaran1'] = $this->number($request->realisasi_anggaran1);
            $input['realisasi_anggaran2'] = $this->number($request->realisasi_anggaran2);
            $input['target_peserta'] = $this->number($request->target_peserta);
            $input['realisasi_peserta1'] = $this->number($request->realisasi_peserta1);
            $input['realisasi_peserta2'] = $this->number($request->realisasi_peserta2);
            $input['target_pendamping'] = $this->number($request->target_pendamping);
            $input['realisasi_pendamping'] = $this->number($request->realisasi_pendamping);
            if($request->hasfile('upload_realisasi_anggaran1')) {
                $input['upload_realisasi_anggaran1'] = $this->uploadFile($request, 'upload_realisasi_anggaran1');
            }

            if($request->hasfile('upload_realisasi_anggaran2')) {
                $input['upload_realisasi_anggaran2'] = $this->uploadFile($request, 'upload_realisasi_anggaran2');
            }
            KinerjaAnggaran::create($input);
            Session::flash('flash_message', 'Data Berhasil di Tambah');
            return redirect('kinerja_anggaran');
        }

    }

    public function show($id)
    {
        //
    }

    public function edit($id, $provinsi_id = null)
    {
        $kinerjaAnggaran = KinerjaAnggaran::findOrFail($id);
        return  view('kinerja_anggaran.edit', compact('provinsi_id','kinerjaAnggaran'));
    }

    public function update(KinerjaAnggaranRequest $request, $id)
    {
        $kinerjaAnggaran = KinerjaAnggaran::findOrFail($id);


        $realisasi_anggaran1 = $this->number($request->realisasi_anggaran1);
        $realisasi_anggaran2 = $this->number($request->realisasi_anggaran2);

        if($realisasi_anggaran1 > $kinerjaAnggaran->pagu_anggaran) {
            session::flash('error_realisasi_anggaran1', 'Realisasi anggaran tahap 1 tidak boleh melebihi pagu anggaran');
            return back();
        }

        if($realisasi_anggaran2 > $kinerjaAnggaran->pagu_anggaran) {
            session::flash('error_realisasi_anggaran2', 'Realisasi anggaran tahap 2 tidak boleh melebihi pagu anggaran');
            return back();
        }

        if(Auth::user()->user_level_id == 10) {
            $input['pagu_anggaran'] = $this->number($request->pagu_anggaran);
            $input['target_peserta'] = $this->number($request->target_peserta);
            $input['target_pendamping'] = $this->number($request->target_pendamping);
            $kinerjaAnggaran->update($input);
            Session::flash('flash_message', 'Data Berhasil di Simpan');
            return redirect()->route('home.list_kinerja_anggaran', [$request->provinsi_id]);
        } else {
            $input = $request->all();
            $input['pagu_anggaran'] = $this->number($request->pagu_anggaran);
            $input['realisasi_anggaran1'] = $this->number($request->realisasi_anggaran1);
            $input['realisasi_anggaran2'] = $this->number($request->realisasi_anggaran2);
            $input['target_peserta'] = $this->number($request->target_peserta);
            $input['realisasi_peserta1'] = $this->number($request->realisasi_peserta1);
            $input['realisasi_peserta2'] = $this->number($request->realisasi_peserta2);
            $input['target_pendamping'] = $this->number($request->target_pendamping);
            $input['realisasi_pendamping'] = $this->number($request->realisasi_pendamping);

            if($request->hasfile('upload_realisasi_anggaran1')) {
                $input['upload_realisasi_anggaran1'] = $this->uploadFile($request, 'upload_realisasi_anggaran1');
                $this->deleteFile($kinerjaAnggaran->upload_realisasi_anggaran1);
            }

            if($request->hasfile('upload_realisasi_anggaran2')) {
                $input['upload_realisasi_anggaran2'] = $this->uploadFile($request, 'upload_realisasi_anggaran2');
                $this->deleteFile($kinerjaAnggaran->upload_realisasi_anggaran2);
            }
    
            $kinerjaAnggaran->update($input);
            Session::flash('flash_message', 'Data Berhasil di Simpan');
            return redirect('kinerja_anggaran');
        }
    }

    public function destroy($id)
    {
        //
    }

    private function number($number) 
    {
        $string_number = preg_replace('/[^0-9 &%\']/', '', $number);
        $input_number = trim($string_number);
        return $input_number;
    }

    public function export_excel() 
    {
        // $data = DB::table('vw_user_kinerja_anggaran')->where('user_level_id', '!=', 10)->orderBy('order_prov', 'ASC')->orderBy('order_kab', 'ASC')->get();
        // return view('export/KinerjaAnggaran', compact('data'));
        return Excel::download(new KinerjaAnggaranExport, 'Kinerja Anggaran.xlsx');
    }

    public function uploadFile(KinerjaAnggaranRequest $request, $fileName) {
        $file = $request->file($fileName);
        $ext = $file->getClientOriginalExtension();

        if($request->file($fileName)->isValid()) {
            $file_name = Str::random(17).".$ext";
            $upload_path = 'public/images/kinerja_anggaran_file';
            $request->file($fileName)->move($upload_path, $file_name);
            return $file_name;
        }
        return false;
    }

    public function deleteFile($fileName) 
    {
        $exist = Storage::disk('local_public')->exists('images/kinerja_anggaran_file/'.$fileName);
        if(isset($fileName)) {
            Storage::disk('local_public')->delete('images/kinerja_anggaran_file/'.$fileName);
        }
    }

    public function download_file($kinerja_anggaran_id, $tahap)
    {
        $kinerja_anggaran = DB::table('vw_user_kinerja_anggaran')->where('kinerja_anggaran_id', $kinerja_anggaran_id)->first();

        if($tahap == 1) {
            return Storage::download("images/kinerja_anggaran_file/".$kinerja_anggaran->upload_realisasi_anggaran1, 'Realisasi Anggaran Tahap 1 '.$kinerja_anggaran->name.'.pdf');
        } else {
            return Storage::download("images/kinerja_anggaran_file/".$kinerja_anggaran->upload_realisasi_anggaran2, 'Realisasi Anggaran Tahap 2 '.$kinerja_anggaran->name.'.pdf');
        }
    }
}
