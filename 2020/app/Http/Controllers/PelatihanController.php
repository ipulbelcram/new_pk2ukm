<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\PelatihanRequest;
use App\Pelatihan;
use App\Params;
use App\Provinsi;
use App\KabKota;
use Session;

class PelatihanController extends Controller
{
    
    public function index()
    {
        // $pelatihan_data = Pelatihan::where('user_id', Auth::user()->id)->orderBy('tanggal_mulai', 'DESC')->paginate(12);
        $pelatihan_data = DB::table('vw_total_peserta_pelatihan')->where('user_id', Auth::user()->id)->orderBy('tanggal_mulai', 'DESC')->paginate(12);
        return view('pelatihan.index', compact('pelatihan_data'));
    }

    public function create()
    {
        $kab_kota = KabKota::where('provinsi_id', Auth::user()->provinsi_id)->pluck('kab_kota', 'id');
        $jenis_pelatihan = Params::where('category_params', 'jenis_pelatihan')->pluck('params','id');
        return view('pelatihan.create', compact('jenis_pelatihan', 'provinsi', 'kab_kota'));
    }

    public function store(PelatihanRequest $request)
    {
        $input = $request->all();
        $input['provinsi_id'] = Auth::user()->provinsi_id;
        $input['user_id'] = Auth::user()->id;
        Pelatihan::create($input);
        Session::flash('flash_message', 'Data Berhasil');
        return redirect('pelatihan');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $jenis_pelatihan = Params::where('category_params', 'jenis_pelatihan')->pluck('params', 'id');
        $kab_kota = KabKota::where('provinsi_id', Auth::user()->provinsi_id)->pluck('kab_kota', 'id');
        $pelatihan = Pelatihan::findOrFail($id);
        return view('pelatihan.edit', compact('pelatihan', 'jenis_pelatihan', 'kab_kota'));
    }

    public function update(PelatihanRequest $request, $id)
    {
        $pelatihan = Pelatihan::findOrFail($id);
        $input = $request->all();
        $pelatihan->update($input);
        Session::flash('flash_message', 'Data Berhasil di Simpan');
        return redirect('pelatihan');
    }

    public function destroy(Request $request, $id)
    {   
        $pelatihan = Pelatihan::findOrFail($request->id);
        $pelatihan->delete();
        Session::flash('flash_message','Data Berhasil di Hapus');
        return back();
    }
}
