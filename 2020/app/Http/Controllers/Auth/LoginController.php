<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    // protected $redirectTo = '/dashboard';
    public function authenticated()
    {
        $user_level_id = auth()->user()->user_level_id;
        if($user_level_id == 10) {
            return redirect('/home');
        } else {
            return redirect('/dashboard');
        }
    }

    public function username()
    {
        return 'username';
    }

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
