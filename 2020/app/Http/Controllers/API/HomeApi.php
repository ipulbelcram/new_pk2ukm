<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\TotalPesertaProvinsiResource;
use App\Http\Resources\TotalPesertaJenisPelatihanResource;
use App\Http\Resources\TotalPesertaJenisKelaminResource;
use App\Http\Resources\TotalPesertaAgamaResource;
use App\Http\Resources\TotalPesertaPendidikanResource;
use App\Http\Resources\TotalPesertaStatusPesertaResource;
use App\Http\Resources\BidangUsahaUmkmResource;
use Illuminate\Http\Request;
use App\Params;
use Storage;


class HomeApi extends Controller
{
    public function total_peserta_provinsi()
    {
        $total_peserta_provinsi = DB::table('vw_total_peserta_provinsi')->get();
        return TotalPesertaProvinsiResource::collection($total_peserta_provinsi);
    }

    public function total_peserta_jenis_pelatihan()
    {
        $total_peserta_jensi_pelatihan = DB::table('vw_total_peserta_jenis_pelatihan')->get();
        return TotalPesertaJenisPelatihanResource::collection($total_peserta_jensi_pelatihan);
    }

    public function total_peserta_jenis_kelamin()
    {
        $total_peserta_jensi_kelamin = DB::table('vw_total_peserta_jenis_kelamin')->get();
        return TotalPesertaJenisKelaminResource::collection($total_peserta_jensi_kelamin);
    }

    public function total_peserta_agama()
    {
        $total_peserta_agama = DB::table('vw_total_peserta_agama')->get();
        return TotalPesertaAgamaResource::collection($total_peserta_agama);
    }

    public function total_peserta_pendidikan()
    {
        $total_peserta_pendidikan = DB::table('vw_total_peserta_pendidikan')->get();
        return TotalPesertaPendidikanResource::collection($total_peserta_pendidikan);
    }

    public function total_peserta_status_peserta()
    {
        $total_peserta_status_peserta = DB::table('vw_total_peserta_status_peserta')->get();
        return TotalPesertaStatusPesertaResource::collection($total_peserta_status_peserta);
    }

    public function image_upload(Request $request) 
    {
        $data = $request->foto_profile;
        list($type, $data) = explode(';', $data);
        list(, $data)      = explode(',', $data);
        $data = base64_decode($data);
        $image_name = date('YmdHis').".png";
        Storage::makeDirectory('foto_coba');
        $path = public_path() . "/foto_coba/" . $image_name;
        file_put_contents($path, $data);
        return response()->json(['image_name' => $image_name], 200);
    }

    public function bidang_usaha_umkm(Request $request)
    {
        if(!empty($request->jenis_usaha_id)) {
            $bidang_usaha_umkm = Params::where([['pid', $request->jenis_usaha_id], ['active', 'true']])->orderBy('order', 'ASC')->get();
        } else {
            $bidang_usaha_umkm = Params::where([['category_params', 'bidang_usaha_umkm'], ['active', 'true']])->orderBy('order', 'ASC')->get();
        }
        return BidangUsahaUmkmResource::collection($bidang_usaha_umkm);
    }

}
