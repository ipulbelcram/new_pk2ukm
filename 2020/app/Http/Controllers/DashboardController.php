<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Exports\WorkingPaperExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Provinsi;
use App\Pelatihan;
use App\Pendamping;
use App\Peserta;

class DashboardController extends Controller
{
    public function index()
    {   
        $total_peserta = Peserta::count();
        $total_pendamping = Pendamping::count();
        $realisasi_kinerja_anggaran = DB::table('vw_realisasi_kinerja_anggaran')->get();
        $vw_realisasi_perprovinsi = DB::table('vw_realisasi_perprovinsi')->get();
        $total_realisasi_perprovinsi = DB::table('vw_realisasi_perprovinsi')->select(DB::raw('SUM(jumlah_penerima_dak) as jumlah_penerima_dak'), DB::raw('SUM(pagu_anggaran) as pagu_anggaran'), DB::raw('SUM(realisasi_anggaran) as realisasi_anggaran'), DB::raw('SUM(realisasi_peserta) as realisasi_peserta'), DB::raw('SUM(target_peserta) as target_peserta'), DB::raw('SUM(target_pendamping) as target_pendamping'), DB::raw('SUM(realisasi_pendamping) as realisasi_pendamping'))->get();
        return view('admin.index', compact('realisasi_kinerja_anggaran', 'total_peserta', 'total_pendamping', 'vw_realisasi_perprovinsi', 'total_realisasi_perprovinsi'));
    }
    public function skpd()
    {
        $provinsi_user_data = DB::table('vw_total_user_provinsi')->get();
        return view('admin.skpd', compact('provinsi_user_data'));
    }

    public function kinerja_anggaran()
    {
        $provinsi_user_data = DB::table('vw_total_user_provinsi')->get();
        return view('admin.kinerja_anggaran', compact('provinsi_user_data'));
    }

    public function list_kinerja_anggaran($provinsi_id)
    {
        $kinerja_anggaran_data = DB::table('vw_user_kinerja_anggaran')->where('provinsi_id', $provinsi_id)->orderBy('order_prov', 'ASC')->orderBy('order_kab', 'ASC')->paginate(15);
        return view('admin.list_kinerja_anggaran', compact('kinerja_anggaran_data'));
    }

    public function list_skpd($provinsi_id)
    {
        $skpd_data = DB::table('vw_userskpd')->where('provinsi_id', $provinsi_id)->orderBy('order_prov', 'ASC')->orderBy('order_kab', 'ASC')->paginate(14);
        return view('admin.list_skpd', compact('skpd_data'));
    }

    public function pelatihan()
    {
        $provinsi_pelatihan_data = DB::table('vw_total_pelatihan_provinsi')->get();
        return view('admin.pelatihan', compact('provinsi_pelatihan_data'));
    }

    public function list_user($provinsi_id)
    {
        $user_data = DB::table('vw_total_pelatihan_user')->where('provinsi_id', $provinsi_id)->orderBy('order_prov', 'ASC')->orderBy('order_kab', 'ASC')->paginate(15);
        return view('admin.list_user', compact('user_data'));
    }

    public function list_pelatihan($user_id)
    {
        // $pelatihan_data = Pelatihan::where('user_id', $user_id)->paginate(15);
        $pelatihan_data = DB::table('vw_total_peserta_pelatihan')->where('user_id', $user_id)->orderBy('id','desc')->paginate(15);
        return view('admin.list_pelatihan', compact('pelatihan_data', 'user_id'));
    }

    public function pendamping()
    {
        $provinsi_pendamping_data = DB::table('vw_total_pendamping_provinsi')->get();
        return view('admin.pendamping', compact('provinsi_pendamping_data'));
    }

    public function list_user_pendamping($provinsi_id)
    {
        $user_data = DB::table('vw_total_pendamping_user')->where('provinsi_id', $provinsi_id)->orderBy('order_prov', 'ASC')->orderBy('order_kab', 'ASC')->paginate(15);
        return view('admin.list_user_pendamping', compact('user_data'));
    }

    public function list_pendamping($user_id)
    {
        $pendamping_data = Pendamping::where('user_id', $user_id)->paginate(15);
        return view('pendamping.index', compact('pendamping_data'));
    }

    public function working_paper($user_id=null)
    {
        if(!empty($user_id)) {
            $data = DB::table('vw_peserta_pelatihan')->where('user_id', $user_id)->get();
            return view('export.WorkingPaper', compact('data'));
            // return Excel::download(new WorkingPaperExport($user_id), 'Kegiatan Lengkap.xlsx');
        } else {
            $data = DB::table('vw_peserta_pelatihan')->get();
            return view('export.WorkingPaper', compact('data'));
            // return Excel::download(new WorkingPaperExport, 'Kegiatan Lengkap.xlsx');
        }
    }
}
