<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pemantauan extends Model
{
    protected $table = 'pemantauan';
    protected $fillable = [
        'user_id', 'p1', 'p2', 'p3', 'p4', 'p5', 'p6', 'p7', 'p8',
    ];

    public $timestamps = false;
}
