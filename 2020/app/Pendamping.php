<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pendamping extends Model
{
    protected $table = 'pendamping';
    protected $fillable = [
        'user_id', 
        'nama_lengkap', 
        'no_ktp', 
        'status', 
        'jenis_kelamin', 
        'tempat_lahir', 
        'tanggal_lahir', 
        'agama', 
        'pendidikan', 
        'alamat_rumah', 
        'kab_kota_id', 
        'no_hp', 
        'email', 
        'foto_profile'
    ];

    protected $casts = [
        'tanggal_lahir' => 'datetime:d-m-Y',
        'created_at' => 'datetime:d-F-Y',
    ];

    public function agamaData()
    {
        return $this->belongsTo('App\Params', 'agama');
    }

    public function pendidikanData()
    {
        return $this->belongsTo('App\Params', 'pendidikan');
    }

    public function kab_kota()
    {
        return $this->belongsTo('App\KabKota', 'kab_kota_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
