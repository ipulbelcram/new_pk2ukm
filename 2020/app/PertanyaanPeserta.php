<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PertanyaanPeserta extends Model
{
    protected $table = "pertanyaan_peserta";
    protected $fillable = [
        'peserta_id', 'p1', 'p2', 'p3', 'p4', 'p5', 'p6', 'p7', 'p8',
    ];

    public $timestamps = false;

    public function peserta()
    {
        return $this->belongsTo('App\Peserta', 'peserta_id');
    }
}
