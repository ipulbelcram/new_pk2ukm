<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Request;

class AppServiceProvider extends ServiceProvider
{
    
    public function register()
    {
        //
    }

    public function boot()
    {
        setlocale(LC_TIME, 'id_ID.utf8');

        $halaman = "Kosong";

        if(Request::segment(1) == "dashboard") {
            $halaman = "dashboard";
        }

        if(Request::segment(1) == "user") {
            $halaman = "user";
        }

        if(Request::segment(1) == "skpd") {
            $halaman = "skpd";
        }

        if(Request::segment(1) == "kinerja_anggaran") {
            $halaman = "kinerja_anggaran";
        }

        if(Request::segment(1) == "pelatihan") {
            $halaman = "pelatihan";
        }

        if(Request::segment(1) == "peserta") {
            $halaman = "peserta";
        }

        if(Request::segment(1) == "peserta" && Request::segment(4) == "edit") {
            $halaman = "peserta_edit";
        }

        if(Request::segment(1) == "peserta" && Request::segment(4) == "pemantauan") {
            $halaman = "peserta_pemantauan";
        }

        if(Request::segment(1) == "pendamping") {
            $halaman = "pendamping";
        }

        if(Request::segment(1) == "bantuan") {
            $halaman = "bantuan";
        }

        //UNTUK ADMIN
        if(Request::segment(2) == "skpd") {
            $halaman = "skpd";
        }

        if(Request::segment(2) == "kinerja_anggaran") {
            $halaman = "kinerja_anggaran";
        }

        if(Request::segment(2) == "pelatihan") {
            $halaman = "pelatihan";
        }

        if(Request::segment(2) == "peserta") {
            $halaman = "peserta";
        }

        if(Request::segment(2) == "pendamping") {
            $halaman = "pendamping";
        }        

        view()->share('halaman', $halaman);

        Schema::defaultStringLength(191);
    }
}
