<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pelatihan extends Model
{
    protected $table = 'pelatihan';
    protected $fillable = [
        'user_id', 'jenis_pelatihan_id', 'judul_pelatihan', 'tanggal_mulai', 'tanggal_selesai', 'tempat', 'provinsi_id', 'kab_kota_id',
    ];

    protected $casts = [
        'tanggal_mulai' => 'datetime:d-F-Y',
        'tanggal_selesai' => 'datetime:d-F-Y',
    ];

    public function provinsi()
    {
        return $this->belongsTo('App\Provinsi', 'provinsi_id');
    }

    public function kab_kota()
    {
        return $this->belongsTo('App\KabKota', 'kab_kota_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function jenisPelatihanData()
    {
        return $this->belongsTo('App\Params', 'jenis_pelatihan_id');
    }

    public function peserta()
    {
        return $this->hasMany('App\Peserta', 'pelatihan_id');
    }
}
