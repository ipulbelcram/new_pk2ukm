<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'username', 'name', 'provinsi_id', 'kab_kota_id', 'profile_picture', 'password', 'order_prov', 'order_kab',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];
    
    // protected $casts = [
    //     'email_verified_at' => 'datetime',
    // ];

    public function provinsi()
    {
        return $this->belongsTo('App\Provinsi', 'provinsi_id');
    }

    public function skpd()
    {
        return $this->hasOne('App\Skpd', 'user_id');
    }
}
