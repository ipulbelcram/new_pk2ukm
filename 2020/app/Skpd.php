<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Skpd extends Model
{
    protected $table = 'skpd';
    protected $fillable = [
        'user_id', 'nama_kepala_dinas', 'nip_kepala_dinas', 'no_hp_kep_dinas', 'penanggung_jawab', 'no_hp_peng_jawab',
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
