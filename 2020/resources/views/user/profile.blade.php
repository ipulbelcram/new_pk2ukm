@extends('templateDashboard')
@section('breadcrumbs')
    <div class="breadcrumbs">
        <div class="breadcrumbs-inner">
            <div class="row m-0">
                <div class="col-sm-4">
                    <div class="page-header float-left">
                        <div class="page-title">
                            <h1>Dashboard</h1>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="page-header float-right">
                        <div class="page-title">
                            <ol class="breadcrumb text-right">
                                <li><a href="#">Dashboard</a></li>
                                <li><a href="#">Profile</a></li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endSection
@section('content')
<div class="card">
    <div class="card-body">
        <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-item nav-link active pl-3 pr-3" id="nav-foto-profile-tab" data-toggle="tab" href="#nav-foto-profile" role="tab" aria-controls="nav-foto-profile" aria-selected="true">Foto Profile</a>
                <a class="nav-item nav-link pl-3 pr-3" id="nav-password-tab" data-toggle="tab" href="#nav-password" role="tab" aria-controls="nav-password" aria-selected="false">Password</a>
            </div>
        </nav>
        <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active" id="nav-foto-profile" role="tabpanel" aria-labelledby="nav-foto-profile-tab">
                {!! Form::model($user, ['method' => 'patch', 'files' => true, 'route' => ['user.profile_picture_update', $user->id]] ) !!}
                <div class="col-md-3">
                    <div id="image_name"></div>
                    @if(!empty($user->profile_picture))
                    <div id="uploaded_image"></div>
                        <img src="{{ asset('images/profile/'.$user->profile_picture) }}" id="photo" class="img-thumbnail mb-3 mt-3"/>
                    @else 
                    <div id="uploaded_image"></div>
                        <img src="{{ asset('images/profile/Male.jpg') }}" id="photo" class="img-thumbnail mb-3 mt-3"/>
                    @endif
                        <div class="text-danger">
                            @if($errors->has('profile_picture'))
                                {{ $errors->first('profile_picture') }}
                            @endif
                        </div>
                    <label class="btn btn-primary btn-block">
                        Browse&hellip; {{ Form::file('profile_picture', ['id' => 'upload_image', 'accept' => 'image/*', 'style' =>'display: none']) }}
                    </label>
                    <div>
                        <button class="btn btn-success btn-block">Simpan</button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <div class="tab-pane fade" id="nav-password" role="tabpanel" aria-labelledby="nav-password-tab"> 
                {!! Form::model($user, ['method' => 'patch', 'route' => ['user.password_update', $user->id]] ) !!}
                <div class="mt-3 mb-3">
                    <div class="form-group">
                        <label class="font-weight-bold">Pasword Baru</label>
                        {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Pasword Baru']) !!}
                        <div class="text-danger">
                            @if($errors->has('password'))
                                {{ $errors->first('password') }}
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="font-weight-bold">Ulangi Password</label>
                        {!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Ulangi Password']) !!}
                        <div class="text-danger">
                            @if($errors->has('password_confirmation'))
                                {{ $errors->first('password_confirmation') }}
                            @endif
                        </div>
                    </div>
                    <button class="btn btn-success">Simpan</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
    @include('partial.modalUpload')
@endSection