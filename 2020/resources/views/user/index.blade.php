@extends('templateDashboard')
@section('breadcrumbs')
    <div class="breadcrumbs">
        <div class="breadcrumbs-inner">
            <div class="row m-0">
                <div class="col-sm-4">
                    <div class="page-header float-left">
                        <div class="page-title">
                            <h1>Dashboard</h1>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="page-header float-right">
                        <div class="page-title">
                            <ol class="breadcrumb text-right">
                                <li><a href="#">Dashboard</a></li>
                                <li class="active">Profile</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endSection
@section('content')
    @include('partial/flash_message')
    <div class="card">
        <div class="card-body">
            <div class="text-center">
               <img src="{{ asset(!empty($user->profile_picture) ? 'images/profile/'.$user->profile_picture : 'images/profile/Male.jpg') }}" class="rounded" alt="...">
               <div class="mb-3 mt-3">
                    <h3 class="font-weight-bold">{{ Auth::user()->name }}</h3>
               </div>
               <div>
                    <a href="{{ route('user.form') }}"><button class="btn btn-outline-success">Ubah Profile</button></a>
               </div>
            </div>
        </div>
    </div>
@endSection