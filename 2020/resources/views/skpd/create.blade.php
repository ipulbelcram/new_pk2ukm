@extends('templateDashboard')
@section('breadcrumbs')
    <div class="breadcrumbs">
        <div class="breadcrumbs-inner">
            <div class="row m-0">
                <div class="col-sm-4">
                    <div class="page-header float-left">
                        <div class="page-title">
                            <h1>Data SKPD</h1>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="page-header float-right">
                        <div class="page-title">
                            <ol class="breadcrumb text-right">
                                <li><a href="/dashboard">Dashboard</a></li>
                                <li><a href="/skpd">Data SKPD</a></li>
                                <li class="active">Tambah SKPD</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endSection
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong class="card-title">Form Data SKPD</strong>
                </div>
                <div class="card-body">
                    {!! Form::open(['files' => true, 'route' => 'skpd.store'] ) !!}
                        @include('skpd.form', ['btn_submit' => 'Tambah'])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endSection