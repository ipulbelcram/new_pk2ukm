@extends('templateDashboard')
@section('breadcrumbs')
    <div class="breadcrumbs">
        <div class="breadcrumbs-inner">
            <div class="row m-0">
                <div class="col-sm-4">
                    <div class="page-header float-left">
                        <div class="page-title">
                            <h1>Data SKPD</h1>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="page-header float-right">
                        <div class="page-title">
                            <ol class="breadcrumb text-right">
                                <li><a href="/dashboard">Dashboard</a></li>
                                <li class="active">Data SKPD</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endSection
@section('content')
    <div class="row">
        <div class="col-lg-12">
            @include('partial.flash_message')
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <strong class="card-title">Data SKPD</strong> 
                    @if(empty($skpd->skpd_id))
                        <a href="{{ route('skpd.create') }}"><button class=" btn btn-outline-success btn-sm float-right">Tambah SKPD</button></a>
                    @else
                        <a href="{{ route('skpd.edit', [$skpd->skpd_id]) }}"><button class=" btn btn-outline-success btn-sm float-right">Edit SKPD</button></a>
                    @endif
                </div>
                <div class="card-body">
                    <table>
                        <tr>
                            <td>Provinsi</td>
                            <td class="pr-3 pl-3">:</td>
                            <td>{{ $skpd->provinsi }}</td>
                        </tr>

                        <tr>
                            <td>Kab/ Kota</td>
                            <td class="pr-3 pl-3">:</td>
                            <td>{{ $skpd->kab_kota }}</td>
                        </tr>

                        <tr>
                            <td>Nama Kepala Dinas</td>
                            <td class="pr-3 pl-3">:</td>
                            <td>{{ $skpd->nama_kepala_dinas }}</td>
                        </tr>

                        <tr>
                            <td>NIP Kepala Dinas</td>
                            <td class="pr-3 pl-3">:</td>
                            <td>{{ $skpd->nip_kepala_dinas }}</td>
                        </tr>

                        <tr>
                            <td>No HP(WA) Kepala Dinas</td>
                            <td class="pr-3 pl-3">:</td>
                            <td>{{ $skpd->no_hp_kep_dinas }}</td>
                        </tr>

                        <tr>
                            <td>Nama Penanggung Jawab Operasional Harian DAK Non Fisik PK2UKM</td>
                            <td class="pr-3 pl-3">:</td>
                            <td>{{ $skpd->penanggung_jawab }}</td>
                        </tr>

                        <tr>
                            <td>No HP(WA) Penanggung Jawab Operasional Harian DAK Non Fisik PK2UKM</td>
                            <td class="pr-3 pl-3">:</td>
                            <td>{{ $skpd->no_hp_peng_jawab }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endSection