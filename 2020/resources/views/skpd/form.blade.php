<div class="form-group">
    <label class="font-weight-bold">Nama Kepala Dinas</label>
    {!! Form::text('nama_kepala_dinas', null, ['class' => 'form-control', 'placeholder' => 'Nama Kepala Dinas']) !!}
    <div class="text-danger">
        @if($errors->has('nama_kepala_dinas')) 
            {{ $errors->first('nama_kepala_dinas') }}
        @endif
    </div>
</div>

<div class="form-group">
    <label class="font-weight-bold">NIP Kepala Dinas</label>
    {!! Form::number('nip_kepala_dinas', null, ['class' => 'form-control', 'placeholder' => 'NIP Kepala Dinas']) !!}
    <div class="text-danger">
        @if($errors->has('nip_kepala_dinas')) 
            {{ $errors->first('nip_kepala_dinas') }}
        @endif
    </div>
</div>

<div class="form-group">
    <label class="font-weight-bold">No HP(WA) Kepala Dinas</label>
    {!! Form::number('no_hp_kep_dinas', null, ['class' => 'form-control', 'placeholder' => 'No HP(WA) Kepala Dinas']) !!}
    <div class="text-danger">
        @if($errors->has('no_hp_kep_dinas')) 
            {{ $errors->first('no_hp_kep_dinas') }}
        @endif
    </div>
</div>

<div class="form-group">
    <label class="font-weight-bold">Nama Penanggung Jawab Operasional Harian DAK Non Fisik PK2UKM</label>
    {!! Form::text('penanggung_jawab', null, ['class' => 'form-control', 'placeholder' => 'Nama Penanggung Jawab Operasional Harian DAK Non Fisik PK2UKM']) !!}
    <div class="text-danger">
        @if($errors->has('penanggung_jawab')) 
            {{ $errors->first('penanggung_jawab') }}
        @endif
    </div>
</div>

<div class="form-group">
    <label class="font-weight-bold">No HP(WA) Penanggung Jawab Operasional Harian DAK Non Fisik PK2UKM</label>
    {!! Form::number('no_hp_peng_jawab', null, ['class' => 'form-control', 'placeholder' => 'No HP(WA) Penanggung Jawab Operasional Harian DAK Non Fisik PK2UKM']) !!}
    <div class="text-danger">
        @if($errors->has('no_hp_peng_jawab')) 
            {{ $errors->first('no_hp_peng_jawab') }}
        @endif
    </div>
</div>
<button class="btn btn-success">{{ $btn_submit }}</button>