@extends('templateDashboard')
@section('breadcrumbs')
    <div class="breadcrumbs">
        <div class="breadcrumbs-inner">
            <div class="row m-0">
                <div class="col-sm-4">
                    <div class="page-header float-left">
                        <div class="page-title">
                            <h1>Data Pendamping</h1>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="page-header float-right">
                        <div class="page-title">
                            <ol class="breadcrumb text-right">
                                <li><a href="/dashboard">Dashboard</a></li>
                                <li><a href="/pendamping">Data Pendamping</a></li>
                                <li class="active">User</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endSection
@section('content')
    <div class="row">
        <div class="col-md-4">
            <aside class="profile-nav alt">
                <section class="card">
                    <div class="card-header user-header alt bg-dark">
                        <div class="media">
                            <a href="#">
                                <img class="align-self-center rounded-circle mr-3" style="width:85px; height:85px;" alt="" src="{{ asset('images/pendamping_profile/'.$pendamping->foto_profile) }}">
                            </a>
                            <div class="media-body">
                                <h3 class="text-light display-6">{{ $pendamping->nama_lengkap }}</h3>
                                <p>{{ $pendamping->no_ktp }}</p>
                            </div>
                        </div>
                    </div>

                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            
                        </li>
                    </ul>

                </section>
            </aside>
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                </div>
                <div class="card-body">
                    <table class="table table-borderless">
                        <tbody>
                            <tr>
                                <th>Nama Lengkap</th>
                                <th>:</th>
                                <td>{{ $pendamping->nama_lengkap }}</td>
                            </tr>
                            <tr>
                                <th>Nomor KTP</th>
                                <th>:</th>
                                <td>{{ $pendamping->no_ktp }}</td>
                            </tr>
                            <tr>
                                <th>Status</th>
                                <th>:</th>
                                <td>{{ $pendamping->status }}</td>
                            </tr>
                            <tr>
                                <th>Jenis Kelamin</th>
                                <th>:</th>
                                <td>{{ $pendamping->jenis_kelamin }}</td>
                            </tr>
                            <tr>
                                <th>Tempat, Tanggal Lahir</th>
                                <th>:</th>
                                <td>{{ $pendamping->tempat_lahir }}, {{ empty($pendamping->tanggal_lahir) ? '' : strftime('%d %B %Y', strtotime($pendamping->tanggal_lahir))}}</td>
                            </tr>
                            <tr>
                                <th>Agama</th>
                                <th>:</th>
                                <td>{{ $pendamping->agamaData->params }}</td>
                            </tr>
                            <tr>
                                <th>Pendidikan Terakhir</th>
                                <th>:</th>
                                <td>{{ $pendamping->pendidikanData->params }}</td>
                            </tr>
                            <tr>
                                <th>Alamat Rumah</th>
                                <th>:</th>
                                <td>{{ $pendamping->alamat_rumah }}</td>
                            </tr>
                            <tr>
                                <th>Kab/Kota</th>
                                <th>:</th>
                                <td>{{ $pendamping->kab_kota->kab_kota }}</td>
                            </tr>
                            <tr>
                                <th>Nomor HP (WA)</th>
                                <th>:</th>
                                <td>{{ $pendamping->no_hp }}</td>
                            </tr>
                            <tr>
                                <th>E-Mail</th>
                                <th>:</th>
                                <td>{{ $pendamping->email }}</td>
                            </tr>
                        </tbody>
                    </table>        
                </div>
            </div>
        </div>
    </div>
@endSection