@extends('templateDashboard')
@section('breadcrumbs')
    <div class="breadcrumbs">
        <div class="breadcrumbs-inner">
            <div class="row m-0">
                <div class="col-sm-4">
                    <div class="page-header float-left">
                        <div class="page-title">
                            <h1>Data Pendamping</h1>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="page-header float-right">
                        <div class="page-title">
                            <ol class="breadcrumb text-right">
                                <li><a href="/dashboard">Dashboard</a></li>
                                <li class="active">Data Pendamping</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endSection
@section('content')
    <div class="row">
        <div class="col-lg-12">
            @include('partial.flash_message')
            <a href="{{ route('pendamping_export', ['user_id' => Auth::user()->id]) }}" target="_blank"><button class="btn btn-outline-success mb-3">Unduh Pendamping Lengkap</button></a>
            <div class="card">
                <div class="card-header">
                    <strong class="card-title">Tabel Data Pendamping</strong>
                    @if(Auth::user()->user_level_id != 10)
                    <div class="float-right">
                        <a href="{{ route('pendamping.create') }}"><button class="btn btn-outline-success btn-sm">Tambah Pendamping</button></a>
                    </div>
                    @endif
                </div>
                <div class="table-stats order-table">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="serial">#</th>
                                <th>Nama Pendamping</th>
                                <th>Jenis Kelamin</th>
                                <th>Usia</th>
                                <th>No HP (WA)</th>
                                @if(Auth::user()->user_level_id != 10)
                                <th>Action</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $no=1;
                            ?>
                            @foreach($pendamping_data as $pendamping)
                            
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td><a href="{{ route('pendamping.show', $pendamping->id) }}">{{ $pendamping->nama_lengkap }}</a></td>
                                <td>{{ $pendamping->jenis_kelamin}}</td>
                                <td>{{ Carbon\Carbon::parse($pendamping->tanggal_lahir)->age }} Th</td>
                                <td>{{ $pendamping->no_hp }}</td>
                                @if(Auth::user()->user_level_id != 10)
                                <td>
                                    <a href="{{ route('pendamping.edit', [$pendamping->id]) }}"><button class="btn btn-primary btn-sm">Edit</button></a>
                                    <button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#ModalDelete" data-id="{{ $pendamping->id }}" id="hapusData">Delete</button>
                                </td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div> <!-- /.table-stats -->
            </div>
            <div class="float-right">
                {{$pendamping_data->links()}}
            </div>
        </div>
    </div>
    @include('partial.modalDelete', ['route' => 'pendamping.destroy'])
@endSection