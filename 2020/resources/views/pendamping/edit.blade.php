@extends('templateDashboard')
@section('breadcrumbs')
    <div class="breadcrumbs">
        <div class="breadcrumbs-inner">
            <div class="row m-0">
                <div class="col-sm-4">
                    <div class="page-header float-left">
                        <div class="page-title">
                            <h1>Data Pendamping</h1>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="page-header float-right">
                        <div class="page-title">
                            <ol class="breadcrumb text-right">
                                <li><a href="/dashboard">Dashboard</a></li>
                                <li><a href="/pendamping">Data Pendamping</a></li>
                                <li class="active">Edit Pendamping</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endSection
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong class="card-title">Form Edit Pendamping</strong>
                </div>
                <div class="card-body">
                    {!! Form::model($pendamping, ['method' => 'patch', 'files' => true, 'route' => ['pendamping.update', $pendamping->id]] ) !!}
                        @include('pendamping.form', ['btn_submit' => 'Simpan'])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- MODAL UPLOAD IMAGE -->
    @include('partial.modalUpload')
@endSection