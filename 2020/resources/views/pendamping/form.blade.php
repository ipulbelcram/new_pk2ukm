<div class="row">
    <div id="image_name"></div>
    <div class="col-md-3">
        @if(isset($pendamping->foto_profile))
        <div id="uploaded_image"></div>
            <img src="{{ asset('images/pendamping_profile/'.$pendamping->foto_profile) }}" id="photo" class="img-thumbnail"/>
        @else 
        <div id="uploaded_image"></div>
            <img src="{{ asset('images/profile/Male.jpg') }}" id="photo" class="img-thumbnail"/>
        @endif
        <h6>Upload a different photo...</h6>
            <div class="text-danger">
                @if($errors->has('foto_profile'))
                    {{ $errors->first('foto_profile') }}
                @endif
            </div>
        <label class="btn btn-primary">
            Browse&hellip; {{ Form::file('foto_profile', ['id' => 'upload_image', 'accept' => 'image/*', 'style' =>'display: none']) }}
        </label>
    </div>
    <div class="col-lg-9">
        <div class="form-group">
            <label class="font-weight-bold">Nama Lengkap</label>
            {!! Form::text('nama_lengkap', null, ['class' => 'form-control', 'placeholder' => 'Nama Lengkap']) !!}
            <div class="text-danger">
                @if($errors->has('nama_lengkap')) 
                    {{ $errors->first('nama_lengkap') }}
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4">
                <div class="form-group">
                    <label class="font-weight-bold">Nomor KTP</label>
                    {!! Form::number('no_ktp', null, ['class' => 'form-control', 'placeholder' => 'Nomor KTP']) !!}
                    <div class="text-danger">
                        @if($errors->has('no_ktp')) 
                            {{ $errors->first('no_ktp') }}
                        @endif
                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="form-group">
                    <label class="font-weight-bold">Status</label>
                    {!! Form::select('status', ['Lajang' => 'Lajang', 'Menikah' => 'Menikah'], null, ['class' => 'form-control', 'placeholder' => 'Status']) !!}
                    <div class="text-danger">
                        @if($errors->has('status')) 
                            {{ $errors->first('status') }}
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label class="font-weight-bold">Jenis Kelamin</label>
                    {!! Form::select('jenis_kelamin', ['Laki-laki' => 'Laki-laki', 'Perempuan' => 'Perempuan'], null, ['class' => 'form-control', 'placeholder' => 'Jenis Kelamin']) !!}
                    <div class="text-danger">
                        @if($errors->has('jenis_kelamin')) 
                            {{ $errors->first('jenis_kelamin') }}
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="font-weight-bold">Tempat Lahir</label>
                    {!! Form::text('tempat_lahir', null, ['class' => 'form-control', 'placeholder' => 'Tempat Lahir']) !!}
                    <div class="text-danger">
                        @if($errors->has('tempat_lahir')) 
                            {{ $errors->first('tempat_lahir') }}
                        @endif
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="form-group">
                    <label class="font-weight-bold">Tanggal Lahir</label>
                    {!! Form::date('tanggal_lahir', !empty($pendamping->tanggal_lahir) ? $pendamping->tanggal_lahir : null, ['class' => 'form-control', 'placeholder' => 'Tanggal Lahir']) !!}
                    <div class="text-danger">
                        @if($errors->has('tanggal_lahir')) 
                            {{ $errors->first('tanggal_lahir') }}
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="font-weight-bold">Agama</label>
                    {!! Form::select('agama', $agama, null, ['class' => 'form-control', 'placeholder' => 'Agama']) !!}
                    <div class="text-danger">
                        @if($errors->has('agama')) 
                            {{ $errors->first('agama') }}
                        @endif
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="form-group">
                    <label class="font-weight-bold">Pendidikan</label>
                    {!! Form::select('pendidikan', $pendidikan, null, ['class' => 'form-control', 'placeholder' => 'Pendidikan']) !!}
                    <div class="text-danger">
                        @if($errors->has('pendidikan')) 
                            {{ $errors->first('pendidikan') }}
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="font-weight-bold">Alamat Rumah</label>
            {!! Form::textarea('alamat_rumah', null, ['class' => 'form-control', 'placeholder' => 'Alamat Rumah']) !!}
            <div class="text-danger">
                @if($errors->has('alamat_rumah')) 
                    {{ $errors->first('alamat_rumah') }}
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4">
                <div class="form-group">
                    <label class="font-weight-bold">Kab/ Kota Pendamping</label>
                    {!! Form::select('kab_kota_id', $kab_kota, null, ['class' => 'form-control', 'placeholder' => 'Kab/ Kota Pendamping']) !!}
                    <div class="text-danger">
                        @if($errors->has('kab_kota_id')) 
                            {{ $errors->first('kab_kota_id') }}
                        @endif
                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="form-group">
                    <label class="font-weight-bold">No HP (WA)</label>
                    {!! Form::number('no_hp', null, ['class' => 'form-control', 'placeholder' => 'No HP (WA)']) !!}
                    <div class="text-danger">
                        @if($errors->has('no_hp')) 
                            {{ $errors->first('no_hp') }}
                        @endif
                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="form-group">
                    <label class="font-weight-bold">E-Mail</label>
                    {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'E-Mail']) !!}
                    <div class="text-danger">
                        @if($errors->has('email')) 
                            {{ $errors->first('email') }}
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<button class="btn btn-success">{{ $btn_submit }}</button>