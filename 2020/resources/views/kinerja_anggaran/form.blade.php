<?php
    if(Auth::user()->user_level_id == 10) {
        $readonly ='';
    } else {
        $readonly = "readonly";
    }
?>

<div class="card">
    <div class="card-header">
        <strong class="card-title">ANGGARAN DAK NON FISIK PK2UKM</strong>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="form-group col-lg-4">
                <label>Pagu Anggaran</label>
                @if(Auth::user()->user_level_id == 10)
                <input type="hidden" name="user_id" value="{{ !empty($user_id) ? $user_id : '' }}">
                <input type="hidden" name="provinsi_id" value="{{ $provinsi_id }}">
                @endif
                <input type="hidden" id="user_level" value="{{ Auth::user()->user_level_id }}">
                {!! Form::text('pagu_anggaran', null, ['class' => 'form-control price', 'placeholder' => 'Pagu Anggaran', $readonly]) !!}
                <div class="text-danger">
                    @if($errors->has('pagu_anggaran')) 
                        {{ $errors->first('pagu_anggaran') }}
                    @endif
                </div>
            </div>
            <div class="form-group col-lg-4" id="anggaranTahap1">
                <label>Realisasi Anggaran Tahap 1</label>
                {!! Form::text('realisasi_anggaran1', null, ['class' => 'form-control price', 'placeholder' => 'Realisasi Anggaran Tahap 1']) !!}
                <div class="text-danger">
                    @if(Session::has('error_realisasi_anggaran1'))
                        {{ Session::get('error_realisasi_anggaran1') }}
                    @endif
                    @if($errors->has('realisasi_anggaran1')) 
                        {{ $errors->first('realisasi_anggaran1') }}
                    @endif
                </div>
            </div>
            <div class="form-group col-lg-4" id="anggaranTahap2">
                <label>Realisasi Anggaran Sampai Dengan Tahap 2</label>
                {!! Form::text('realisasi_anggaran2', null, ['class' => 'form-control price', 'placeholder' => 'Realisasi Anggaran Sampai Dengan Tahap 2']) !!}
                <div class="text-danger">
                    @if(Session::has('error_realisasi_anggaran2'))
                        {{ Session::get('error_realisasi_anggaran2') }}
                    @endif
                    @if($errors->has('realisasi_anggaran2')) 
                        {{ $errors->first('realisasi_anggaran2') }}
                    @endif
                </div>
            </div>
            <div class="form-group col-lg-4"><label>File Kinerja Anggaran : </label></div>
            <div class="col-lg-4 mb-3">
                <div class="custom-file">
                    <input type="file" name="upload_realisasi_anggaran1" class="custom-file-input" id="inputGroupFile01" accept="application/pdf">
                    <label class="custom-file-label" for="inputGroupFile01" id="fileName01">{{ !empty($kinerjaAnggaran->upload_realisasi_anggaran1) ? 'Realisasi Anggaran Tahap 1' : 'Pilih File' }}</label>
                </div>
                <div style="font-size: 13px; color:red;">*format pdf</div>
                <div class="text-danger">
                    @if($errors->has('upload_realisasi_anggaran1')) 
                        {{ $errors->first('upload_realisasi_anggaran1') }}
                    @endif
                </div>
            </div>

            <div class="col-lg-4 mb-3">
                <div class="custom-file">
                    <input type="file" name="upload_realisasi_anggaran2" class="custom-file-input" id="inputGroupFile02" accept="application/pdf">
                    <label class="custom-file-label" for="inputGroupFile02" id="fileName02">{{ !empty($kinerjaAnggaran->upload_realisasi_anggaran2) ? 'Realisasi Anggaran Tahap 2' : 'Pilih File' }}</label>
                </div>
                <div style="font-size: 13px; color:red;">*format pdf</div>
                <div class="text-danger">
                    @if($errors->has('upload_realisasi_anggaran2')) 
                        {{ $errors->first('upload_realisasi_anggaran2') }}
                    @endif
                </div>
            </div>

        </div>  
    </div>
</div>

<div class="card">
    <div class="card-header">
        <strong class="card-title">PELATIHAN</strong>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="form-group col-lg-4">
                <label>Target Peserta</label>
                {!! Form::text('target_peserta', null, ['class' => 'form-control number', 'placeholder' => 'Target Peserta', $readonly]) !!}
                <div class="text-danger">
                    @if($errors->has('terget_peserta')) 
                        {{ $errors->first('target_peserta') }}
                    @endif
                </div>
            </div>

            <div class="form-group col-lg-4" id="pesertaTahap1">
                <label>Realisasi Peserta Tahap 1</label>
                {!! Form::text('realisasi_peserta1', null, ['class' => 'form-control number', 'placeholder' => 'Realisasi Peserta Tahap 1']) !!}
                <div class="text-danger">
                    @if($errors->has('realisasi_peserta1')) 
                        {{ $errors->first('realisasi_peserta1') }}
                    @endif
                </div>
            </div>

            <div class="form-group col-lg-4" id="pesertaTahap2">
                <label>Realisasi Peserta Sampai Dengan Tahap 2</label>
                {!! Form::text('realisasi_peserta2', null, ['class' => 'form-control number', 'placeholder' => 'Realisasi Peserta Sampai Dengan Tahap 2']) !!}
                <div class="text-danger">
                    @if($errors->has('realisasi_peserta2')) 
                        {{ $errors->first('realisasi_peserta2') }}
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>


<div class="card">
    <div class="card-header">
        <strong class="card-title">PENDAMPING</strong>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="form-group col-lg-6">
                <label>Target Pendamping</label>
                {!! Form::text('target_pendamping', null, ['class' => 'form-control number', 'placeholder' => 'Target Pendamping', $readonly]) !!}
                <div class="text-danger">
                    @if($errors->has('target_pendamping')) 
                        {{ $errors->first('target_pendamping') }}
                    @endif
                </div>
            </div>

            <div class="form-group col-lg-6" id="pendampingTahap1">
                <label>Realisasi Pendamping</label>
                {!! Form::text('realisasi_pendamping', null, ['class' => 'form-control number', 'maxlength' => '2', 'placeholder' => 'Realisasi pendamping']) !!}
                <div class="text-danger">
                    @if($errors->has('realisasi_pendamping')) 
                        {{ $errors->first('realisasi_pendamping') }}
                    @endif
                </div>
            </div>
        </div>  
    </div>
</div>
<button class="btn btn-success">{{ $btn_submit }}</button>