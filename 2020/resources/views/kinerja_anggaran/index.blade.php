@extends('templateDashboard')
@section('breadcrumbs')
    <div class="breadcrumbs">
        <div class="breadcrumbs-inner">
            <div class="row m-0">
                <div class="col-sm-4">
                    <div class="page-header float-left">
                        <div class="page-title">
                            <h1>Kinerja Anggaran</h1>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="page-header float-right">
                        <div class="page-title">
                            <ol class="breadcrumb text-right">
                                <li><a href="/dashboard">Dashboard</a></li>
                                <li class="active">Kinerja Anggaran</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endSection
@section('content')
    <div class="row">
        <div class="col-lg-12">
            @include('partial.flash_message')
        </div>
        <div class="col-lg-12 mb-3">
            @if(empty($kinerjaAnggaran->kinerja_anggaran_id))
                <a href="{{ route('kinerja_anggaran.create') }}"><button class=" btn btn-outline-success btn-sm float-right">Tambah Kinerja Anggaran</button></a>
            @else
                <a href="{{ route('kinerja_anggaran.edit', ['kinerja_anggaran_id' => $kinerjaAnggaran->kinerja_anggaran_id]) }}"><button class=" btn btn-outline-success btn-sm float-right">Edit Kinerja Anggaran</button></a>
            @endif
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <strong class="card-title">ANGGARAN DAK NON FISIK PK2UKM</strong> 
                </div>
                <div class="card-body">
                    <table>
                        <tr>
                            <td>Pagu Anggaran</td>
                            <td class="pr-3 pl-3">:</td>
                            <td>Rp. {{ number_format($kinerjaAnggaran->pagu_anggaran, '0',',','.') }}</td>
                        </tr>

                        <tr>
                            <td>Realisasi Anggaran Tahap 1</td>
                            <td class="pr-3 pl-3">:</td>
                            <td>Rp. {{ number_format($kinerjaAnggaran->realisasi_anggaran1, '0',',','.') }}</td>
                            @if(!empty($kinerjaAnggaran->upload_realisasi_anggaran1)) 
                            <td><a href="{{ route('kinerja_anggaran.download_file', [$kinerjaAnggaran->kinerja_anggaran_id, 1]) }}" class="ml-5"><button class="btn btn-success btn-sm">Download File</button></a></td>
                            @endif
                        </tr>

                        <tr>
                            <td class="font-weight-bold">Realisasi Anggaran Sampai Dengan Tahap 2</td>
                            <td class="pr-3 pl-3">:</td>
                            <td>Rp. {{ number_format($kinerjaAnggaran->realisasi_anggaran2, '0',',','.') }}</td>
                            @if(!empty($kinerjaAnggaran->upload_realisasi_anggaran2)) 
                                <td><a href="{{ route('kinerja_anggaran.download_file', [$kinerjaAnggaran->kinerja_anggaran_id, 2]) }}" class="ml-5"><button class="btn btn-success btn-sm">Download File</button></a></td>
                            @endif
                        </tr>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <strong class="card-title">PELATIHAN</strong> 
                </div>
                <div class="card-body">
                    <table>
                        <tr>
                            <td>Target Peserta</td>
                            <td class="pr-3 pl-3">:</td>
                            <td>{{ number_format($kinerjaAnggaran->target_peserta, '0',',','.') }}</td>
                        </tr>

                        <tr>
                            <td>Realisasi Peserta Tahap 1</td>
                            <td class="pr-3 pl-3">:</td>
                            <td>{{ number_format($kinerjaAnggaran->realisasi_peserta1, '0',',','.') }}</td>
                        </tr>

                        <tr>
                            <td class="font-weight-bold">Realisasi Peserta Sampai Dengan Tahap 2</td>
                            <td class="pr-3 pl-3">:</td>
                            <td>{{ number_format($kinerjaAnggaran->realisasi_peserta2,'0',',','.') }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <strong class="card-title">PENDAMPING</strong> 
                </div>
                <div class="card-body">
                    <table>
                        <tr>
                            <td>Target Pendamping</td>
                            <td class="pr-3 pl-3">:</td>
                            <td>{{ number_format($kinerjaAnggaran->target_pendamping,'0',',','.') }}</td>
                        </tr>

                        <tr>
                            <td>Realisasi Pendamping</td>
                            <td class="pr-3 pl-3">:</td>
                            <td>{{ number_format($kinerjaAnggaran->realisasi_pendamping,'0',',','.') }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endSection