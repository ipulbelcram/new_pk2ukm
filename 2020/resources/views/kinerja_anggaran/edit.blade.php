@extends('templateDashboard')
@section('breadcrumbs')
    <div class="breadcrumbs">
        <div class="breadcrumbs-inner">
            <div class="row m-0">
                <div class="col-sm-4">
                    <div class="page-header float-left">
                        <div class="page-title">
                            <h1>Kinerja Anggaran</h1>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="page-header float-right">
                        <div class="page-title">
                            <ol class="breadcrumb text-right">
                                <li><a href="/dashboard">Dashboard</a></li>
                                <li><a href="/kinerja_anggaran">Kinerja Anggaran</a></li>
                                <li class="active">Edit Kinerja Anggaran</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endSection
@section('content')
    <div class="row">
        <div class="col-lg-12">
            {!! Form::model($kinerjaAnggaran, ['method' => 'patch', 'files' => true, 'route' => ['kinerja_anggaran.update', $kinerjaAnggaran->id]] ) !!}
                @include('kinerja_anggaran.form', ['btn_submit' => 'Simpan'])
            {!! Form::close() !!}
        </div>
    </div>
@endSection
@section('script')
    <script src="{{ asset('js/kinerja_anggaran_form.js') }}"></script>
@endSection