<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>PK2UKM - Sistem Pemantauan DAK Non Fisik</title>
    <meta name="description" content="PK2UKM
    ">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{csrf_token()}}">

    <link rel="shortcut icon" href="http://pk2ukm.kemenkopukm.go.id/2020/public/images/icon.ico">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/normalize.css@8.0.0/normalize.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pixeden-stroke-7-icon@1.2.3/pe-icon-7-stroke/dist/pe-icon-7-stroke.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.2.0/css/flag-icon.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">
    <link rel="stylesheet" href="{{ asset('assets/css/cs-skin-elastic.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('vendors/croppie/croppie.css') }}">
    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->
    <link href="https://cdn.jsdelivr.net/npm/chartist@0.11.0/dist/chartist.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/jqvmap@1.5.1/dist/jqvmap.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/fullcalendar@3.9.0/dist/fullcalendar.min.css" rel="stylesheet" />

    <style>
        #weatherWidget .currentDesc {
            color: #ffffff!important;
        }
        .traffic-chart {
            min-height: 335px;
        }
        #flotPie1  {
            height: 150px;
        }
        #flotPie1 td {
            padding:3px;
        }
        #flotPie1 table {
            top: 20px!important;
            right: -10px!important;
        }
        .chart-container {
            display: table;
            min-width: 270px ;
            text-align: left;
            padding-top: 10px;
            padding-bottom: 10px;
        }
        #flotLine5  {
             height: 105px;
        }

        #flotBarChart {
            height: 150px;
        }
        #cellPaiChart{
            height: 160px;
        }

    </style>
    @yield('style')
</head>

<body>
    <!-- Left Panel -->
    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">
            <div id="main-menu" class="main-menu collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        @if(Auth::user()->user_level_id == 10) 
                        <li class="{{ !empty($halaman && $halaman == 'home') ? 'active' : '' }}">
                            <a href="{{ route('home.index') }}"><i class="menu-icon fa fa-chart-bar mt-2"></i>Dashboard</a>
                        </li>
                        <li class="menu-title">Menu</li><!-- /.menu-title -->
                        <li class="{{ !empty($halaman && $halaman == 'skpd') ? 'active' : '' }}">
                            <a href="{{ route('home.skpd') }}"> <i class="menu-icon fa fa-address-card mt-2"></i>Data SKPD</a>
                        </li>
                        <li class="{{ !empty($halaman && $halaman == 'kinerja_anggaran') ? 'active' : '' }}">
                            <a href="{{ route('home.kinerja_anggaran') }}"> <i class="menu-icon fa fa-chart-line mt-2"></i>Kinerja Anggaran</a>
                        </li>
                        <li class="{{ !empty($halaman && $halaman == 'pelatihan' || $halaman == 'peserta') ? 'active' : '' }}">
                            <a href="{{ route('home.pelatihan') }}"> <i class="menu-icon fa fa-chalkboard-teacher mt-2"></i>Peserta Pelatihan</a>
                        </li>
                        <li class="{{ !empty($halaman && $halaman == 'pendamping') ? 'active' : '' }}">
                            <a href="{{ route('home.pendamping') }}"> <i class="menu-icon fa fa-user-friends mt-2"></i>Pendamping Pelatihan</a>
                        </li>
                        @else
                        <li class="{{ !empty($halaman && $halaman == 'dashboard') ? 'active' : '' }}">
                            <a href="{{ route('dashboard') }}"><i class="menu-icon fa fa-chart-bar mt-2"></i>Dashboard</a>
                        </li>
                        <li class="{{ !empty($halaman && $halaman == 'user') ? 'active' : '' }}">
                            <a href="{{ route('user.form') }}"><i class="menu-icon fa fa-chart-bar mt-2"></i>Profile</a>
                        </li>
                        <li class="menu-title">Menu</li><!-- /.menu-title -->
                        <li class="{{ !empty($halaman && $halaman == 'skpd') ? 'active' : '' }}">
                            <a href="{{ route('skpd.index') }}"> <i class="menu-icon fa fa-address-card mt-2"></i>Data SKPD</a>
                        </li>
                        <li class="{{ !empty($halaman && $halaman == 'kinerja_anggaran') ? 'active' : '' }}">
                            <a href="{{ route('kinerja_anggaran.index') }}"> <i class="menu-icon fa fa-chart-line mt-2"></i>Kinerja Anggaran</a>
                        </li>
                        <li class="{{ !empty($halaman && $halaman == 'pelatihan' || $halaman == 'peserta') ? 'active' : '' }}">
                            <a href="{{ route('pelatihan.index') }}"> <i class="menu-icon fa fa-chalkboard-teacher mt-2"></i>Data Pelatihan</a>
                        </li>
                        <li class="{{ !empty($halaman && $halaman == 'pendamping') ? 'active' : '' }}">
                            <a href="{{ route('pendamping.index') }}"> <i class="menu-icon fa fa-user-friends mt-2"></i>Data Pendamping</a>
                        </li>
                        @endif
                        <li class="{{ !empty($halaman && $halaman == 'bantuan') ? 'active' : '' }}">
                            <a href="{{ route('bantuan') }}"> <i class="menu-icon fa fa-question-circle mt-2"></i>Bantuan</a>
                        </li>
                    </ul>
                    

            </div><!-- /.navbar-collapse -->
        </nav>
    </aside>
    <!-- /#left-panel -->
    <!-- Right Panel -->
    <div id="right-panel" class="right-panel">
        <!-- Header-->
        <header id="header" class="header">
            <div class="top-left">
                <div class="navbar-header">
                    <a class="navbar-brand" href="/dashboard">
                        <img src="{{ asset('images/logo.png') }}" alt="Logo" width="150">
                    </a>
                    <a id="menuToggle" class="menutoggle"><i class="fa fa-bars"></i></a>
                </div>
            </div>
            <div class="top-right">
                <div class="header-menu">
                    <div class="user-area dropdown float-right">
                        <a href="#" class="dropdown-toggle active" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <!-- <span class="mr-3">Ahmad Syaiful Akbar</span> -->
                            <img class="user-avatar rounded-circle border" src="{{ asset(!empty(Auth::user()->profile_picture) ? 'images/profile/'.Auth::user()->profile_picture : 'images/profile/Male.jpg') }}" alt="User Avatar">
                        </a>

                        <div class="user-menu dropdown-menu">
                            <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();  document.getElementById('logout-form').submit();"><i class="fa fa-power -off"></i>{{ __('Logout') }}</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </header>
        <!-- /#header -->

        <!-- Breadcrumbs -->
        @yield('breadcrumbs')
        <!-- /.Breadcrumbs -->

        <!-- Content -->
        <div class="content">
            <!-- Animated -->
            <div class="animated fadeIn">
                @yield('content')
            </div>
            <!-- .animated -->
        </div>
        <!-- /.content -->
        <div class="clearfix"></div>
        <!-- Footer -->
        <footer class="site-footer">
            <div class="footer-inner bg-white">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        Copyright &copy; 2020 <a href="http://pk2ukm.siwira.id" class="text-primary">PK2UKM</a> - Deputi Bidang Pengembangan SDM
                    </div>
                </div>
            </div>
        </footer>
        <!-- /.site-footer -->
    </div>
    <!-- /#right-panel -->
    
    <!-- Scripts -->
    <script src="https://cdn.jsdelivr.net/npm/jquery@2.2.4/dist/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.4/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-match-height@0.7.2/dist/jquery.matchHeight.min.js"></script>
    <script src="{{ asset('assets/js/main.js') }}"></script>
    <script src="{{ asset('js/alert.js') }}"></script>
    

    <!--  Chart js -->
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.7.3/dist/Chart.bundle.min.js"></script>

    <!--Chartist Chart-->
    <script src="https://cdn.jsdelivr.net/npm/chartist@0.11.0/dist/chartist.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartist-plugin-legend@0.6.2/chartist-plugin-legend.min.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/jquery.flot@0.8.3/jquery.flot.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/flot-pie@1.0.0/src/jquery.flot.pie.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/flot-spline@0.0.1/js/jquery.flot.spline.min.js"></script>
    
    <!-- Cropie.js -->
    <script src="{{ asset('vendors/croppie/croppie.js') }}"></script>
    <script>
        //Delete Data
        jQuery(document).ready(function($) {
            $('#ModalDelete').on('show.bs.modal', function(e) {
            var id = $(e.relatedTarget).data('id');
            $(this).find('.modal-body #id').val(id);
            });
        });

    //Format Rupiah
    jQuery(document).ready(function($) {
        // $("#price").on('keyup', function(e) {
        //     $("#price").val(formatRupiah(this.value, 'Rp. '));
        // });

        $(document).on('keyup', '.number', function(e) {
            $(this).val(formatRupiah(this.value))
        })

        $(document).on('keyup', '.price', function(e) {
            $(this).val(formatRupiah(this.value, 'Rp. '))
        })
        /* Fungsi formatRupiah */
        function formatRupiah(angka, prefix){
            var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split   		= number_string.split(','),
            sisa     		= split[0].length % 3,
            rupiah     		= split[0].substr(0, sisa),
            ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if(ribuan){
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }

            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
            return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
        }
    })
    </script>
    @yield('script')
    <script>
    jQuery(document).ready(function($) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $image_crop = $('#image_demo').croppie({
            enableExif: true,
            enableOrientation: true,
            viewport: {
                width:225,
                height:300,
                type:'square' //circle
            },
            boundary:{
                width:300,
                height:375,
            }
        });

        $('.vanilla-rotate').on('click', function(event) {
            $image_crop.croppie('rotate', parseInt($(this).data('deg')));
        });

        $('#upload_image').on('change', function(){
        var reader = new FileReader();
        reader.onload = function (event) {
            $image_crop.croppie('bind', {
            url: event.target.result
            }).then(function(){
                console.log('jQuery bind complete');
            });
        }
        reader.readAsDataURL(this.files[0]);
            $('#uploadimageModal').modal('show');
        });

        $('.crop_image').click(function(event){
            // $.ajax({
            //     url:"http://localhost/dak/public/api/image_upload",
            //     type:"POST",
            //     dataType:"JSON",
                
            //     success:function(result){
            //         console.log(result);
            //     }
            // })
            $image_crop.croppie('result', {
                type: 'canvas',
                size: 'viewport'
                // size: 'original'
            }).then(function(response){
                $.ajax({
                    url:"http://pk2ukm.kemenkopukm.go.id/2020/api/image_upload",
                    type:"POST",
                    data:{"foto_profile": response}, 
                    success:function(data) {
                        $('#uploadimageModal').modal('hide');
                        image_name = '<input name="image_name" type="text" value="' + data.image_name + '" hidden>'
                        $('#image_name').html(image_name);
                        html = '<img src="' + response + '" class="img-thumbnail" />';
                        $('#photo').css('display', 'none');
                        $('#uploaded_image').html(html);
                    }
                });
            })
        });
    });
    </script>
</body>
</html>