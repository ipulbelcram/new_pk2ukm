@extends('templateDashboard')
@section('content')
<style>
    .pdfobject-container { height: 60rem; border: 1rem solid rgba(0,0,0,.1); }
</style>
    <div class="text-center">
        <iframe width="100%" height="480" src="https://www.youtube.com/embed/5BXygUW7tpM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
    <iframe src="https://docs.google.com/viewer?url={{ asset('assets/file/modul_pk2ukm.pdf') }}&embedded=true" frameborder="0" width="100%" height="700px"></iframe>
@endsection
@section('script')
@endsection
