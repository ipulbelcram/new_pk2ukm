@extends('templateDashboard')
@section('content')
    <div class="row">
        <div class="col-lg-12"> 
            @include('partial/flash_message')
            <div class="card">
                <div class="card-body">
                    <div class="">
                        <h1 class="font-weight-bold mb-3">SELAMAT DATANG, {{ Auth::user()->name }}</h1>
                        <img src="{{ asset(!empty($user->profile_picture) ? 'images/profile/'.$user->profile_picture : 'images/profile/Male.jpg') }}" class="rounded" alt="...">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endSection