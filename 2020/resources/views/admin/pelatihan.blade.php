@extends('templateDashboard')
@section('breadcrumbs')
    <div class="breadcrumbs">
        <div class="breadcrumbs-inner">
            <div class="row m-0">
                <div class="col-sm-4">
                    <div class="page-header float-left">
                        <div class="page-title">
                            <h1>Peserta Pelatihan</h1>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="page-header float-right">
                        <div class="page-title">
                            <ol class="breadcrumb text-right">
                                <li><a href="/">Dashboard</a></li>
                                <li class="active">Peserta Pelatihan</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endSection
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <a href="{{ route('working_paper') }}" target="_blank"><button class="btn btn-outline-success mb-3">Unduh Kegiatan Lengkap</button></a>
        </div>
    </div>
    <div class="row">
        @foreach($provinsi_pelatihan_data as $provinsi_pelatihan)
        <div class="col-lg-4 col-md-6">
            <a href="{{ route('home.list_user', $provinsi_pelatihan->provinsi_id) }}">
                <div class="card">
                    <div class="card-body">
                        <div class="stat-widget-five">
                            <div class="stat-icon dib flat-color-1">
                                <!-- <i class="pe-7s-cash"></i> -->
                                <i class="fa fa-chalkboard-teacher"></i>
                            </div>
                            <div class="stat-content">
                                <div class="text-left dib">
                                    <div class="stat-text"><span class="count">{{ $provinsi_pelatihan->total_pelatihan }}</span> Pelatihan</div>
                                    <div class="stat-heading">{{ $provinsi_pelatihan->provinsi }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        @endforeach
    </div>
@endSection