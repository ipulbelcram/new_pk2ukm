@extends('templateDashboard')
@section('breadcrumbs')
    <div class="breadcrumbs">
        <div class="breadcrumbs-inner">
            <div class="row m-0">
                <div class="col-sm-4">
                    <div class="page-header float-left">
                        <div class="page-title">
                            <h1>Kinerja Anggaran</h1>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="page-header float-right">
                        <div class="page-title">
                            <ol class="breadcrumb text-right">
                                <li><a href="/">Dashboard</a></li>
                                <li><a href="/home/kinerja_anggaran">Kinerja Anggaran</a></li>
                                <li class="active">Provinsi</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endSection
@section('content')
<div class="row">
    @foreach($kinerja_anggaran_data as $kinerja_anggaran)
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                <strong class="card-title">{{ $kinerja_anggaran->name }}</strong> 
                @if(empty($kinerja_anggaran->kinerja_anggaran_id))
                    <a href="{{ route('kinerja_anggaran.create', [$kinerja_anggaran->provinsi_id, $kinerja_anggaran->user_id]) }}"><button class=" btn btn-outline-success btn-sm float-right">Tambah Kinerja Anggaran</button></a>
                @else
                    <a href="{{ route('kinerja_anggaran.edit', [$kinerja_anggaran->kinerja_anggaran_id, $kinerja_anggaran->provinsi_id]) }}"><button class=" btn btn-outline-success btn-sm float-right">Edit Kinerja Anggaran</button></a>
                @endif
            </div>
            <div class="card-body">
                <!-- ANGGARAN -->
                <table class="table table-borderless mb-3">
                    <thead class="border-bottom">
                        <th colspan="3" class="pb-2 pt-2">Anggaran DAK Non Fisik PK2UKM</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Pagu Anggaran</td>
                            <td class="pr-3 pl-3">:</td>
                            <td>Rp. {{ number_format($kinerja_anggaran->pagu_anggaran, '0',',','.') }}</td>
                        </tr>

                        <tr>
                            <td>Realisasi Anggaran Tahap 1</td>
                            <td class="pr-3 pl-3">:</td>
                            <td>Rp. {{ number_format($kinerja_anggaran->realisasi_anggaran1, '0',',','.') }}</td>
                        </tr>

                        <tr>
                            <td>Realisasi Anggaran Tahap 2</td>
                            <td class="pr-3 pl-3">:</td>
                            <td>Rp. {{ number_format($kinerja_anggaran->realisasi_anggaran2, '0',',','.') }}</td>
                        </tr>
                    </tbody>
                </table>

                <!-- PELATIHAN -->
                <table class="table table-borderless mb-3">
                    <thead class="border-bottom">
                        <th colspan="3" class="pb-2 pt-2">Pelatihan</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Target Peserta</td>
                            <td class="pr-3 pl-3">:</td>
                            <td>{{ number_format($kinerja_anggaran->target_peserta, '0',',','.') }}</td>
                        </tr>

                        <tr>
                            <td>Realisasi Peserta Tahap 1</td>
                            <td class="pr-3 pl-3">:</td>
                            <td>{{ number_format($kinerja_anggaran->realisasi_peserta1, '0',',','.') }}</td>
                        </tr>

                        <tr>
                            <td>Realisasi Peserta Tahap 2</td>
                            <td class="pr-3 pl-3">:</td>
                            <td>{{ number_format($kinerja_anggaran->realisasi_peserta2,'0',',','.') }}</td>
                        </tr>
                    </tbody>
                </table>

                <!-- PENDAMPING -->
                <table class="table table-borderless mb-3">
                    <thead class="border-bottom">
                        <th colspan="3" class="pb-2 pt-2">Pendamping</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Target Pendamping</td>
                            <td class="pr-3 pl-3">:</td>
                            <td>{{ number_format($kinerja_anggaran->target_pendamping,'0',',','.') }}</td>
                        </tr>

                        <tr>
                            <td>Realisasi Pendamping</td>
                            <td class="pr-3 pl-3">:</td>
                            <td>{{ number_format($kinerja_anggaran->realisasi_pendamping,'0',',','.') }}</td>
                        </tr>
                    </tbody>
                </table>

                <!-- DOWNLOAD FILE REALISASI ANGGARAN -->
                <table class="table table-borderless mb-3">
                    <thead class="border-bottom">
                        <th colspan="3" class="pb-2 pt-2">File Realisasi Anggaran</th>
                    </thead>
                    <tbody>
                        
                        <tr>    
                            <td>Tahap 1</td>
                            <td class="pr-3 pl-3">:</td>
                            @if(!empty($kinerja_anggaran->upload_realisasi_anggaran1)) 
                            <td><a href="{{ route('kinerja_anggaran.download_file', [$kinerja_anggaran->kinerja_anggaran_id, 1]) }}"><button class="btn btn-success btn-sm">Download File</button></a></td>
                            @else
                            <td>Belum Upload File</td>
                            @endif
                        </tr>

                        <tr>
                            <td>Tahap 2</td>
                            <td class="pr-3 pl-3">:</td>
                            @if(!empty($kinerja_anggaran->upload_realisasi_anggaran2)) 
                            <td><a href="{{ route('kinerja_anggaran.download_file', [$kinerja_anggaran->kinerja_anggaran_id, 2]) }}"><button class="btn btn-success btn-sm">Download File</button></a></td>
                            @else
                            <td>Belum Upload File</td>
                            @endif
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @endforeach
    <div class="col-md-12">
        {{ $kinerja_anggaran_data->links() }}
    </div>
</div>
@endSection