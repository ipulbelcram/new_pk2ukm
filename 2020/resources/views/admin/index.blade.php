@extends('templateDashboard')
@section('breadcrumbs')
    <div class="breadcrumbs">
        <div class="breadcrumbs-inner">
            <div class="row m-0">
                <div class="col-sm-8">
                    <div class="page-header float-left">
                        <div class="page-title">
                            <h1 class="font-weight-bold" style="font-size:25px;">Kinerja DAK Nonfisik PK2UKM TA. 2020</h1>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="page-header float-right">
                        <div class="page-title">
                            <ol class="breadcrumb text-right">
                                
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endSection
@section('content')
<?php
    foreach($realisasi_kinerja_anggaran as $kinerja_anggaran) {
        $total_pagu_anggaran = $kinerja_anggaran->total_pagu_anggaran;
        $total_realisasi_anggaran = $kinerja_anggaran->total_realisasi_anggaran;

        $total_target_peserta = $kinerja_anggaran->total_target_peserta;
        $total_realisasi_peserta = $total_peserta;

        $total_target_pendamping = $kinerja_anggaran->total_target_pendamping;
        $total_realisasi_pendamping = $total_pendamping;
    }
    $presentase_anggaran = ($total_realisasi_anggaran/$total_pagu_anggaran)*100;
    $presentase_peserta = ($total_realisasi_peserta/$total_target_peserta)*100;
    $presentase_pendamping = ($total_realisasi_pendamping/$total_target_pendamping)*100;
?>
    <div class="row">
        <div class="col-lg-4 col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="stat-widget-five">
                        <div class="stat-icon dib flat-color-1">
                            <i class="pe-7s-cash"></i>
                        </div>
                        <div class="stat-content">
                            <div class="text-left dib">
                                <div class="stat-text">Rp.{{ number_format($total_realisasi_anggaran,0,'.',',') }} </div>
                                <div class="stat-text">{{ number_format($presentase_anggaran,2) }} %</div>
                                <div class="stat-heading">Realisasi Anggaran</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-4 col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="stat-widget-five">
                        <div class="stat-icon dib flat-color-4">
                            <i class="pe-7s-users"></i>
                        </div>
                        <div class="stat-content">
                            <div class="text-left dib">
                                <div class="stat-text">{{ number_format($total_realisasi_peserta,0,'.',',') }} Peserta</div>
                                <div class="stat-text">{{ number_format($presentase_peserta,2) }} %</div>
                                <div class="stat-heading">Realisasi Peserta</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-4 col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="stat-widget-five">
                        <div class="stat-icon dib flat-color-4">
                            <i class="pe-7s-users"></i>
                        </div>
                        <div class="stat-content">
                            <div class="text-left dib">
                                <div class="stat-text">{{ number_format($total_realisasi_pendamping,0,'.',',') }} Pendamping</div>
                                <div class="stat-text">{{ number_format($presentase_pendamping,2) }} %</div>
                                <div class="stat-heading">Realisasi Pendamping</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <!-- TOTAL PESERTA PROVINSI CHAR -->
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="mb-3">Rekapitulasi Per Provinsi</h4>
                    <!-- <canvas id="provinsi"></canvas> -->
                    <table class="table table table-bordered">
                        <thead class="text-center">
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Nama Provinsi</th>
                                <th scope="col">Jumlah Provinsi/ Kab/ Kota Penerima DAK</th>
                                <th scope="col">Pagu</th>
                                <th scope="col">Realisasi Anggaran</th>
                                <th scope="col">Target Peserta</th>
                                <th scope="col">Realisasi Peserta</th>
                                <th scope="col">Target Pendamping</th>
                                <th scope="col">Realisasi Pendamping</th>
                            </tr>
                        </thead>
                        <tbody id="provinsi-table">
                            <?php $no = 1; ?>
                            @foreach($vw_realisasi_perprovinsi as $realisasi_perprovinsi)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $realisasi_perprovinsi->provinsi }}</td>
                                <td class="text-center">{{ $realisasi_perprovinsi->jumlah_penerima_dak }}</td>
                                <td>Rp.{{ number_format($realisasi_perprovinsi->pagu_anggaran,'0',',','.') }}</td>
                                <td>Rp.{{ number_format($realisasi_perprovinsi->realisasi_anggaran,'0',',','.') }}</td>
                                <td>{{ number_format($realisasi_perprovinsi->target_peserta,0,',','.') }} Peserta</td>
                                <td>{{ number_format($realisasi_perprovinsi->realisasi_peserta,0,',','.') }} Peserta</td>
                                <td>{{ number_format($realisasi_perprovinsi->target_pendamping,0,',','.') }} Pendamping</td>
                                <td>{{ number_format($realisasi_perprovinsi->realisasi_pendamping,0,',','.') }} Pendamping</td>
                            </tr>
                            @endforeach
                            @foreach($total_realisasi_perprovinsi as $t_realisasi_perprovinsi)
                            <tr>
                                <td class="font-weight-bold text-center" colspan = "2">TOTAL</td>
                                <td class="font-weight-bold text-center">{{ $t_realisasi_perprovinsi->jumlah_penerima_dak }}</td>
                                <td class="font-weight-bold">Rp.{{ number_format($t_realisasi_perprovinsi->pagu_anggaran,0,',','.') }}</td>
                                <td class="font-weight-bold">Rp.{{ number_format($t_realisasi_perprovinsi->realisasi_anggaran,'0',',','.') }}</td>
                                <td class="font-weight-bold">{{ number_format($t_realisasi_perprovinsi->target_peserta,0,',','.') }} Peserta</td>
                                <td class="font-weight-bold">{{ number_format($t_realisasi_perprovinsi->realisasi_peserta,0,',','.') }} Peserta</td>
                                <td class="font-weight-bold">{{ number_format($t_realisasi_perprovinsi->target_pendamping,0,',','.') }} Pendamping</td>
                                <td class="font-weight-bold">{{ number_format($t_realisasi_perprovinsi->realisasi_pendamping,0,',','.') }} Pendamping</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div><!-- /# column -->

        <div class="col-lg-6">
            <div class="card">
                <div class="card-body">
                    <h4 class="mb-3">Peserta Berdasarkan Jenis Kelamin </h4>
                    <canvas id="jenisKelaminChart"></canvas>
                    <table>
                        <tbody id="jenisKelaminTable"></tbody>
                    </table>
                </div>
            </div>
        </div><!-- /# column -->

        <div class="col-lg-6">
            <div class="card">
                <div class="card-body">
                    <h4 class="mb-3">Peserta Berdasarkan Agama </h4>
                    <canvas id="AgamaChart"></canvas>
                </div>
            </div>
        </div><!-- /# column -->

        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="mb-3">Peserta Berdasarkan Jenis Pelatihan </h4>
                    <canvas id="jenisPelatihanChart"></canvas>
                </div>
            </div>
        </div><!-- /# column -->

        <div class="col-lg-6">
            <div class="card">
                <div class="card-body">
                    <h4 class="mb-3">Peserta Berdasarkan Pendidikan Terakhir </h4>
                    <canvas id="PendidikanChart"></canvas>
                </div>
            </div>
        </div><!-- /# column -->

        <div class="col-lg-6">
            <div class="card">
                <div class="card-body">
                    <h4 class="mb-3">Peserta Berdasarkan Status Peserta </h4>
                    <canvas id="StatusPesertaChart"></canvas>
                </div>
            </div>
        </div><!-- /# column -->
    </div>
@endSection

@section('script')
<script>
    jQuery(document).ready(function($) {
        var server = "http://pk2ukm.kemenkopukm.go.id/2020/";
        // TOTAL PESERTA PROVINSI CHART
        // $.ajax({
        //     url : server+"api/total_peserta_provinsi",
        //     type : "GET",
        //     dataType : "JSON",
        //     success : function(result) {
        //         var data = result.data
        //         // var ctx = document.getElementById( "provinsi" );
        //         // ctx.height = 250;
        //         // var provinsi_label = []
        //         // var provinsi_data = []
        //         var no = 1;
        //         $.each(data, function(key, value) {
        //             // provinsi_label.push(value.provinsi);
        //             // provinsi_data.push(value.total_peserta);
        //             $("#provinsi-table").append(`
        //                 <tr>
        //                     <td>`+ no++ +`</td>
        //                     <td>`+ value.provinsi +`</td>
        //                     <td>`+ value.total_peserta +` Peserta</td>
        //                 </tr>
        //             `)
        //         });
        //         // var myChart = new Chart( ctx, {
        //         //     type: 'horizontalBar',
        //         //     data: {
        //         //         labels: provinsi_label,
        //         //         datasets: [
        //         //             {
        //         //                 label: "Total Peserta",
        //         //                 data: provinsi_data,
        //         //                 borderColor: "rgba(0, 194, 146, 0.9)",
        //         //                 borderWidth: "0",
        //         //                 backgroundColor: "rgba(0, 194, 146, 0.5)"
        //         //                         }
        //         //                     ]
        //         //     },
        //         //     options: {
        //         //         scales: {
        //         //             yAxes: [ {
        //         //                 ticks: {
        //         //                     beginAtZero: true
        //         //                 }
        //         //                             } ]
        //         //         }
        //         //     }
        //         // } );
        //     }
        // })

        //TOTAL PESERTA JENIS PELTIHAN
        $.ajax({
            url : server+"api/total_peserta_jenis_pelatihan",
            type : "GET",
            dataType : "JSON",
            success: function(result){
                var data = result.data
                var jenisPelatihanLabel = []
                var jenisPelatihanData = []
                $.each(data, function(key, value){
                    jenisPelatihanLabel.push(value.jenis_pelatihan);
                    jenisPelatihanData.push(value.total_peserta);
                })
                var ctx = document.getElementById( "jenisPelatihanChart" );
                ctx.height = 80;
                var myChart = new Chart( ctx, {
                    type: 'horizontalBar',
                    data: {
                        labels: jenisPelatihanLabel,
                        datasets: [
                            {
                                label: "Total Peserta",
                                data: jenisPelatihanData,
                                borderColor: "rgba(0, 194, 146, 0.9)",
                                borderWidth: "0",
                                backgroundColor: "rgba(0, 194, 146, 0.5)"
                                        }
                                    ]
                    },
                    options: {
                        scales: {
                            yAxes: [ {
                                ticks: {
                                    beginAtZero: true
                                }
                                            } ]
                        }
                    }
                } );
            }
        })

        //TOTAL PESERTA JENIS KELAMIN
        $.ajax({
            url : server+"api/total_peserta_jenis_kelamin",
            type : "GET",
            dataType : "JSON",
            success: function(result){
                var data = result.data
                var jenisKelaminLabel = []
                var jenisKelaminData = []
                $.each(data, function(key, value){
                    jenisKelaminLabel.push(value.jenis_kelamin);
                    jenisKelaminData.push(value.total_peserta);
                    // $("#jenisKelaminTable").append("<tr><td>"+value.jenis_kelamin+"</td><td class='px-4'>:</td><td>"+value.total_peserta+"</td></tr>")
                }) 
                var ctx = document.getElementById( "jenisKelaminChart" );
                ctx.height = 150;
                var myChart = new Chart( ctx, {
                    type: 'pie',
                    data: {
                        datasets: [ {
                            data: jenisKelaminData,
                            backgroundColor: [
                                "rgba(116, 185, 255,1.0)",
                                "rgba(253, 121, 168,1.0)"
                            ],
                            hoverBackgroundColor: [
                                "rgba(116, 185, 255,0.7)",
                                "rgba(253, 121, 168,0.7)"
                            ]
                        } ],
                        labels: jenisKelaminLabel,
                    },
                    options: {
                        responsive: true
                    }
                } );
            }
        })
        

        //TOTAL PESERTA AGAMA
        $.ajax({
            url : server+"api/total_peserta_agama",
            type : "GET",
            dataType : "JSON",
            success: function(result){
                var data = result.data
                var agamaLabel = []
                var agamaData = []

                $.each(data, function(key, value) {
                    agamaLabel.push(value.agama)
                    agamaData.push(value.total_peserta)
                })

                var ctx = document.getElementById( "AgamaChart" );
                ctx.height = 150;
                var myChart = new Chart( ctx, {
                    type: 'doughnut',
                    data: {
                        datasets: [ {
                            data: agamaData,
                            backgroundColor: [
                                "rgba(231, 76, 60,1.0)",
                                "rgba(241, 196, 15,1.0)",
                                "rgba(46, 204, 113,1.0)", 
                                "rgba(52, 152, 219,1.0)",
                                "rgba(155, 89, 182,1.0)",
                                "rgba(52, 73, 94,1.0)",
                                "rgba(149, 165, 166,1.0)"
                            ],
                            hoverBackgroundColor: [
                                "rgba(231, 76, 60,0.7)",
                                "rgba(241, 196, 15,0.7)",
                                "rgba(46, 204, 113,0.7)", 
                                "rgba(52, 152, 219,0.7)",
                                "rgba(155, 89, 182,0.7)",
                                "rgba(52, 73, 94,0.7)",
                                "rgba(149, 165, 166,0.7)"
                            ]
                        } ],
                        labels: agamaLabel
                    },
                    options: {
                        responsive: true
                    }
                } );
            }
        })

        //TOTAL PESERTA PENDIDKAN
        $.ajax({
            url : server+"api/total_peserta_pendidikan",
            type : "GET",
            dataType : "JSON",
            success: function(result){
                var data = result.data
                var pendidikanlabel = []
                var pendidikanData = []

                $.each(data, function(key, value) {
                    pendidikanlabel.push(value.pendidikan)
                    pendidikanData.push(value.total_peserta)
                })

                var ctx = document.getElementById( "PendidikanChart" );
                ctx.height = 150;
                var myChart = new Chart( ctx, {
                    type: 'bar',
                    data: {
                        labels: pendidikanlabel,
                        datasets: [
                            {
                                label: "Total Peserta",
                                data: pendidikanData,
                                borderColor: "rgba(0, 194, 146, 0.9)",
                                borderWidth: "0",
                                backgroundColor: "rgba(0, 194, 146, 0.5)"
                                        }
                                    ]
                    },
                    options: {
                        scales: {
                            yAxes: [ {
                                ticks: {
                                    beginAtZero: true
                                }
                                            } ]
                        }
                    }
                });
            }
        })

        //TOTAL PESERTA STATUS PESERTA
        $.ajax({
            url : server+"api/total_peserta_status_peserta",
            type : "GET",
            dataType : "JSON",
            success: function(result){
                var data = result.data
                var statusPesertaLabel = []
                var statusPesertaData = []
                $.each(data, function(key, value){
                    statusPesertaLabel.push(value.status_peserta)
                    statusPesertaData.push(value.total_peserta)
                })

                var ctx = document.getElementById( "StatusPesertaChart" );
                ctx.height = 150;
                var myChart = new Chart( ctx, {
                    type: 'doughnut',
                    data: {
                        datasets: [{
                            data: statusPesertaData,
                            backgroundColor: [
                                "rgba(231, 76, 60,1.0)",
                                "rgba(241, 196, 15,1.0)",
                                "rgba(46, 204, 113,1.0)", 
                                "rgba(52, 152, 219,1.0)"
                            ],
                            hoverBackgroundColor: [
                                "rgba(231, 76, 60,0.7)",
                                "rgba(241, 196, 15,0.7)",
                                "rgba(46, 204, 113,0.7)", 
                                "rgba(52, 152, 219,0.7)",
                            ]
                        }],
                        labels: statusPesertaLabel
                    },
                    options: {
                        responsive: true
                    }
                });
            }
        })
    });
</script>
@endSection