@extends('templateDashboard')
@section('breadcrumbs')
    <div class="breadcrumbs">
        <div class="breadcrumbs-inner">
            <div class="row m-0">
                <div class="col-sm-4">
                    <div class="page-header float-left">
                        <div class="page-title">
                            <h1>Kinerja Anggaran</h1>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="page-header float-right">
                        <div class="page-title">
                            <ol class="breadcrumb text-right">
                                <li><a href="/">Dashboard</a></li>
                                <li class="active">Kinerja Anggaran</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endSection
@section('content')
    <a href="{{ route('kinerja_anggaran.export_excel') }}" target="_blank"><button class="btn btn-outline-success mb-3">Unduh Kinerja Anggaran</button></a>
    <div class="row">
        @foreach($provinsi_user_data as $provinsi_user)
        <div class="col-lg-4 col-md-6">
            <a href="{{ route('home.list_kinerja_anggaran', $provinsi_user->provinsi_id) }}">
                <div class="card">
                    <div class="card-body">
                        <div class="stat-widget-five">
                            <div class="stat-icon dib flat-color-1">
                                <i class="far fa-address-card"></i>
                            </div>
                            <div class="stat-content">
                                <div class="text-left dib">
                                    <div class="stat-text"><span class="count">{{ $provinsi_user->total_users }}</span> Pengguna</div>
                                    <div class="stat-heading">{{ $provinsi_user->provinsi }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        @endforeach
    </div>
@endSection