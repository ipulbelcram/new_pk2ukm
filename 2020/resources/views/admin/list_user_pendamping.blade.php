@extends('templateDashboard')
@section('breadcrumbs')
    <div class="breadcrumbs">
        <div class="breadcrumbs-inner">
            <div class="row m-0">
                <div class="col-sm-4">
                    <div class="page-header float-left">
                        <div class="page-title">
                            <h1>Pendamping Pelatihan</h1>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="page-header float-right">
                        <div class="page-title">
                            <ol class="breadcrumb text-right">
                                <li><a href="/">Dashboard</a></li>
                                <li><a href="/home/pendamping">Pendamping Pelatihan</a></li>
                                <li class="active">Provinsi</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endSection
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong class="card-title">user Table</strong>
                </div>
                <div class="table-stats order-table">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="serial">#</th>
                                <th>Nama User</th>
                                <th>Total Pendamping</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $no=1;
                            ?>
                            @foreach($user_data as $user)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td><a href="{{ route('home.list_pendamping', $user->user_id) }}">{{ $user->name }}</a></td>
                                <td>{{ $user->total_pendamping}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div> <!-- /.table-stats -->
            </div>
            <div class="float-right">
                {{$user_data->links()}}
            </div>
        </div>
    </div>
@endSection