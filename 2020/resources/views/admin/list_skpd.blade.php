@extends('templateDashboard')
@section('breadcrumbs')
    <div class="breadcrumbs">
        <div class="breadcrumbs-inner">
            <div class="row m-0">
                <div class="col-sm-4">
                    <div class="page-header float-left">
                        <div class="page-title">
                            <h1>Data SKPD</h1>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="page-header float-right">
                        <div class="page-title">
                            <ol class="breadcrumb text-right">
                                <li><a href="/">Dashboard</a></li>
                                <li><a href="/home/skpd">Data SKPD</a></li>
                                <li class="active">Provinsi</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endSection
@section('content')
<div class="row">
    @foreach($skpd_data as $skpd)
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                <strong class="card-title">{{ $skpd->name }}</strong> 
            </div>
            <div class="card-body">
                <table>
                    <thead>
                        <th style="width:50%"></th>
                        <th></th>
                        <th></th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Provinsi</td>
                            <td class="pr-3 pl-3">:</td>
                            <td>{{ $skpd->provinsi }}</td>
                        </tr>

                        <tr>
                            <td>Kab/ Kota</td>
                            <td class="pr-3 pl-3">:</td>
                            <td>{{ $skpd->kab_kota }}</td>
                        </tr>

                        <tr>
                            <td>Nama kepala Dinas</td>
                            <td class="pr-3 pl-3">:</td>
                            <td>{{ $skpd->nama_kepala_dinas }}</td>
                        </tr>

                        <tr>
                            <td>NIP Kepala Dinas</td>
                            <td class="pr-3 pl-3">:</td>
                            <td>{{ $skpd->nip_kepala_dinas }}</td>
                        </tr>

                        <tr>
                            <td>No HP(WA) Kepala Dinas</td>
                            <td class="pr-3 pl-3">:</td>
                            <td>{{ $skpd->no_hp_kep_dinas }}</td>
                        </tr>

                        <tr>
                            <td>Nama Penanggung Jawab Operasional Harian DAK dan Non Fisik PK2UMKM</td>
                            <td class="pr-3 pl-3">:</td>
                            <td>{{ $skpd->penanggung_jawab }}</td>
                        </tr>

                        <tr>
                            <td>No HP(WA) Penanggung Jawab Operasional Harian DAK dan Non Fisik PK2UMKM</td>
                            <td class="pr-3 pl-3">:</td>
                            <td>{{ $skpd->no_hp_peng_jawab }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @endforeach
    <div class="col-md-12">
        {{ $skpd_data->links() }}
    </div>
</div>
@endSection