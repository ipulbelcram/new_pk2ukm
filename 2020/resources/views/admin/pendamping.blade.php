@extends('templateDashboard')
@section('breadcrumbs')
    <div class="breadcrumbs">
        <div class="breadcrumbs-inner">
            <div class="row m-0">
                <div class="col-sm-4">
                    <div class="page-header float-left">
                        <div class="page-title">
                            <h1>Pendamping Pelatihan</h1>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="page-header float-right">
                        <div class="page-title">
                            <ol class="breadcrumb text-right">
                                <li><a href="/">Dashboard</a></li>
                                <li class="active">Pendamping Pelatihan</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endSection
@section('content')
    <a href="{{ route('pendamping_export') }}" target="_blank"><button class="btn btn-outline-success mb-3">Unduh Pendamping Lengkap</button></a>
    <div class="row">
        @foreach($provinsi_pendamping_data as $provinsi_pendamping)
        <div class="col-lg-4 col-md-6">
            <a href="{{ route('home.list_user_pendamping', $provinsi_pendamping->provinsi_id) }}">
                <div class="card">
                    <div class="card-body">
                        <div class="stat-widget-five">
                            <div class="stat-icon dib flat-color-1">
                                <i class="fa fa-user-friends"></i>
                            </div>
                            <div class="stat-content">
                                <div class="text-left dib">
                                    <div class="stat-text"><span class="count">{{ $provinsi_pendamping->total_pendamping }}</span> Pendamping</div>
                                    <div class="stat-heading">{{ $provinsi_pendamping->provinsi }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        @endforeach
    </div>
@endSection