@extends('templateDashboard')
@section('breadcrumbs')
    <div class="breadcrumbs">
        <div class="breadcrumbs-inner">
            <div class="row m-0">
                <div class="col-sm-4">
                    <div class="page-header float-left">
                        <div class="page-title">
                            <h1>Data Pelatihan</h1>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="page-header float-right">
                        <div class="page-title">
                            <ol class="breadcrumb text-right">
                                <li><a href="/dashboard">Dashboard</a></li>
                                <li class="active">Data Pelatihan</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endSection
@section('content')
    <div class="row">
        <div class="col-lg-12">
            @include('partial.flash_message')
            <a href="{{ route('working_paper', Auth::user()->id) }}" target="_blank"><button class="btn btn-outline-success mb-3">Unduh Kegiatan Lengkap</button></a>
            <div class="card">
                <div class="card-header">
                    <strong class="card-title">Tabel Data Kegiatan</strong>
                    <div class="float-right">
                        <a href="{{ route('pelatihan.create') }}"><button class="btn btn-outline-success btn-sm">Tambah Kegiatan</button></a>
                    </div>
                </div>
                <div class="table-stats order-table">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="serial">#</th>
                                <th>Nama Kegiatan</th>
                                <th>Kab/ Kota Kegiatan</th>
                                <th style="width:18%">Tanggal Pelaksanaan</th>
                                <th>User</th>
                                <th>Action</th>
                                <th>Total Peserta</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $no=1;
                            ?>
                            @foreach($pelatihan_data as $pelatihan)
                            
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td><a href="{{ route('peserta.index', $pelatihan->id) }}">{{ $pelatihan->judul_pelatihan }}</a></td>
                                <td>{{ $pelatihan->kab_kota}}</td>
                                <td>{{ strftime('%d %B %Y', strtotime($pelatihan->tanggal_mulai)) }} <span class="text-lowercase">s.d</span> {{ strftime('%d %B %Y', strtotime($pelatihan->tanggal_selesai)) }}</td>
                                <td>{{ $pelatihan->name_user }}</td>
                                <td>
                                    <a href="{{ route('pelatihan.edit', [$pelatihan->id]) }}"><button class="btn btn-primary btn-sm">Edit</button></a>
                                    <button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#ModalDelete" data-id="{{ $pelatihan->id }}" id="hapusData">Delete</button>
                                </td>
                                <td>{{ $pelatihan->total_peserta }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div> <!-- /.table-stats -->
            </div>
            <div class="float-right">
                {{$pelatihan_data->links()}}
            </div>
        </div>
    </div>
    @include('partial.modalDelete', ['route' => 'pelatihan.destroy'])
@endSection