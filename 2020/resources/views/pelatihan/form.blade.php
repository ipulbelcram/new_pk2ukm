<div class="row md-3">
    <div class="col-md-6">
        <div class="form-group">
            <label class="font-weight-bold">Jenis Pelatihan</label>
            {!! Form::select('jenis_pelatihan_id', $jenis_pelatihan, null, ['class' =>'form-control', 'placeholder' => 'Jenis Pelatihan']) !!}
            <div class="text-danger">
                @if($errors->has('jenis_pelatihan_id')) 
                    {{ $errors->first('jenis_pelatihan_id') }}
                @endif
            </div>
        </div>
    </div>

    <div class="col-lg-6">
        <div class="form-group">
            <label class="font-weight-bold">Judul Pelatihan</label>
            {!! Form::text('judul_pelatihan', null, ['class' => 'form-control', 'placeholder' => 'Judul Pelatihan']) !!}
            <div class="text-danger">
                @if($errors->has('judul_pelatihan')) 
                    {{ $errors->first('judul_pelatihan') }}
                @endif
            </div>
        </div>
    </div>
</div>

<div class="row mb-3">
    <div class="col-lg-12">
        <label class="font-weight-bold">Tanggal Pelaksanaan</label>
    </div>
    <div class="form-grup col-lg-5">
        {!! Form::date('tanggal_mulai', !empty($pelatihan->tanggal_mulai) ? $pelatihan->tanggal_mulai : null, ['class' => 'form-control']) !!}
        <div class="text-danger">
            @if($errors->has('tanggal_mulai')) 
                {{ $errors->first('tanggal_mulai') }}
            @endif
        </div>
    </div>
    <div class="text-center col-lg-2">
        <span class="font-weight-bold">sampai dengan</span>
    </div>
    <div class="form-grup col-lg-5">
        {!! Form::date('tanggal_selesai', !empty($pelatihan->tanggal_selesai) ? $pelatihan->tanggal_mulai : null, ['class' => 'form-control']) !!}
        <div class="text-danger">
            @if($errors->has('tanggal_selesai')) 
                {{ $errors->first('tanggal_selesai') }}
            @endif
        </div>
    </div>
</div>
<div class="form-group">
    <label class="font-weight-bold">Tempat Pelaksanaan</label>
    {!! Form::text('tempat', null, ['class' => 'form-control', 'placeholder' => 'Tempat Pelaksanaan']) !!}
    <div class="text-danger">
        @if($errors->has('tempat')) 
            {{ $errors->first('tempat') }}
        @endif
    </div>
</div>


<div class="form-group">
    <label class="font-weight-bold">Kab/ Kota Kegiatan</label>
    {!! Form::select('kab_kota_id', $kab_kota, null, ['class' =>'form-control', 'placeholder' => 'Kab/ Kota Kegiatan']) !!}
    <div class="text-danger">
        @if($errors->has('kab_kota_id')) 
            {{ $errors->first('kab_kota_id') }}
        @endif
    </div>
</div>
    

<button class="btn btn-success">{{ $btn_submit }}</button>