@extends('templateDashboard')
@section('breadcrumbs')
    <div class="breadcrumbs">
        <div class="breadcrumbs-inner">
            <div class="row m-0">
                <div class="col-sm-4">
                    <div class="page-header float-left">
                        <div class="page-title">
                            <h1>Dashboard</h1>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="page-header float-right">
                        <div class="page-title">
                            <ol class="breadcrumb text-right">
                                <li><a href="#">Dashboard</a></li>
                                <li><a href="#">Peserta</a></li>
                                <li class="active">Index</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endSection
@section('content')
    <div class="row">
        <div class="col-md-4">
            <aside class="profile-nav alt">
                <section class="card">
                    <div class="card-header user-header alt bg-dark">
                        <div class="media">
                            <a href="#">
                                <img class="align-self-center rounded-circle mr-3" style="width:85px; height:85px;" alt="" src="images/admin.jpg">
                            </a>
                            <div class="media-body">
                                <h2 class="text-light display-6">{{ $peserta->nama }}</h2>
                                <p>{{ $peserta->no_ktp }}</p>
                            </div>
                        </div>
                    </div>

                    <?php
                        if(!empty($peserta->code)) {
                            $host = request()->getSchemeAndHttpHost();
                            $url = $host."/peserta/".$peserta->code."/lihat_peserta";
                            $qrcode = QrCode::size(250)->generate($url);
                        } else {
                            $qrcode = "Belum Membuat QrCode";
                        }
                    ?>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <div class="col-md-12">
                                {!! $qrcode !!}
                            </div>
                        </li>
                    </ul>

                </section>
            </aside>
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <strong>Profile Peserta</strong>
                </div>
                <div class="card-body">
                    <table class="table table-borderless">
                        <tbody>
                            <tr>
                                <th>Nama Lengkap</th>
                                <th>:</th>
                                <td>{{ $peserta->nama }}</td>
                            </tr>
                            <tr>
                                <th>Nomor KTP</th>
                                <th>:</th>
                                <td>{{ $peserta->no_ktp }}</td>
                            </tr>
                            <tr>
                                <th>Tempat, Tanggal lahir</th>
                                <th>:</th>
                                <td>{{ $peserta->tempat_lahir }}, {{ empty($peserta->tanggal_lahir) ? '' : date_format($peserta->tanggal_lahir, 'd F Y') }}</td>
                            </tr>
                            <tr>
                                <th>Telp/HP</th>
                                <th>:</th>
                                <td>{{ $peserta->no_hp }}</td>
                            </tr>
                            <tr>
                                <th>Status Peserta</th>
                                <th>:</th>
                                <td>{{ empty($peserta->statusPesertaData) ? '' : $peserta->satusPesertaData->params }}</td>
                            </tr>
                            <tr>
                                <th>Nama Koperasi</th>
                                <th>:</th>
                                <td>{{ $peserta->nama_koperasi }}</td>
                            </tr>
                            <tr>
                                <th>Nama Usaha(UMKM)</th>
                                <th>:</th>
                                <td>{{ $peserta->nama_usaha }}</td>
                            </tr>
                            <tr>
                                <th colspan="3" class="border-bottom border-top">Data Pelatihan</th>
                            </tr>
                            <tr>
                                <th>Nama Pelatihan</th>
                                <th>:</th>
                                <td>{{ $peserta->judul_pelatihan }}</td>
                            </tr>
                            <tr>
                                <th>Tanggal Pelatihan</th>
                                <th>:</th>
                                <td>{{ date('d F Y', strtotime($peserta->tanggal_mulai)) }} s/d {{ date('d F Y', strtotime($peserta->tanggal_selesai)) }}</td>
                            </tr>
                            <tr>
                                <th>Kab/Kota Pelatihan</th>
                                <th>:</th>
                                <td>{{ $peserta->kab_kota_pelatihan }}</td>
                            </tr>
                        </tbody>
                    </table>        
                </div>
            </div>
        </div>
    </div>
@endSection