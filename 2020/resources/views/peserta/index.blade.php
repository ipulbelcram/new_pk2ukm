@extends('templateDashboard')
@section('breadcrumbs')
    <div class="breadcrumbs">
        <div class="breadcrumbs-inner">
            <div class="row m-0">
                <div class="col-sm-4">
                    <div class="page-header float-left">
                        <div class="page-title">
                            <h1>List Peserta</h1>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="page-header float-right">
                        <div class="page-title">
                            <ol class="breadcrumb text-right">
                                <li><a href="#">Dashboard</a></li>
                                <li><a href="#">Peserta</a></li>
                                <li class="active">Index</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endSection
@section('content')
    <div class="row">
        <div class="col-lg-12">
            @include('partial.flash_message')
            <div class="mb-3">
                <table>
                    <tr>
                        <th>Jenis Pelatihan</th>
                        <td class="pl-3 pr-3">:</td>
                        <td>{{ $pelatihan->jenisPelatihanData->params }}</td>
                    </tr>
                    <tr>
                        <th>Judul Pelatihan</th>
                        <td class="pl-3 pr-3">:</td>
                        <td>{{ $pelatihan->judul_pelatihan }}</td>
                    </tr>
                    <tr>
                        <th>Tempat Pelaksanaan</th>
                        <td class="pl-3 pr-3">:</td>
                        <td>{{ $pelatihan->tempat }}</td>
                    </tr>
                    <tr>
                        <th>Tanggal Pelaksanaan</th>
                        <td class="pl-3 pr-3">:</td>
                        <td>{{ strftime("%d %B %Y", strtotime($pelatihan->tanggal_mulai)) }} s.d {{ strftime('%d %B %Y', strtotime($pelatihan->tanggal_selesai)) }}</td>
                    </tr>
                    <tr>
                        <th>Kab/ Kota Kegiatan</th>
                        <td class="pl-3 pr-3">:</td>
                        <td>{{ $pelatihan->kab_kota->kab_kota }}</td>
                    </tr>
                    <tr>
                        <th>Provinsi</th>
                        <td class="pl-3 pr-3">:</td>
                        <td>{{ $pelatihan->provinsi->provinsi }}</td>
                    </tr>
                </table>
            </div>
            <a href="{{ route('peserta.format_laporan', $pelatihan->id) }}" target="_blank"><button class="btn btn-outline-success mb-3">Unduh Peserta Lengkap</button></a>
            <div class="card">
                <div class="card-header">
                    <strong class="card-title">Peserta Table</strong>
                    @if(Auth::user()->user_level_id != 10)
                    <div class="float-right">
                        <a href="{{ route('peserta.create', $pelatihan->id) }}"><button class="btn btn-outline-success btn-sm">Tambah Peserta</button></a>
                    </div>
                    @endif
                </div>
                <div class="table-stats order-table">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="serial">#</th>
                                <th>Nama Lengkap</th>
                                <th>Jenis Kelamin</th>
                                <th>Usia</th>
                                <th>Telp/ HP</th>
                                <!-- <th>Code</th> -->
                                @if(Auth::user()->user_level_id != 10)
                                <th>Action</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no=1; ?>
                            @foreach($peserta_data as $peserta)
                            <?php
                                if(!empty($peserta->qrcode)) {
                                    $urut = sprintf('%04d', $peserta->qrcode->urut);
                                    $qrcode = $peserta->qrcode->tanggal.''.$urut;
                                } else {
                                    $qrcode = "Belum Membuat QrCode";
                                }
                            ?>
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td><a href="{{ route('peserta.show', $peserta->id) }}">{{ $peserta->nama }}</a></td>
                                <td>{{ $peserta->jenis_kelamin}}</td>
                                <td>{{ Carbon\Carbon::parse($peserta->tanggal_lahir)->age }} Th</td>
                                <td>{{ $peserta->no_hp }}</td>
                                <!-- <td>{{ $qrcode }}</td> -->
                                @if(Auth::user()->user_level_id != 10)
                                <td class="text-left">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Action
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a href="{{ route('peserta.edit', [$pelatihan->id, $peserta->id]) }}"><button class="dropdown-item" type="button">Edit</button></a>
                                            <button class="dropdown-item" type="button" data-toggle="modal" data-target="#ModalDelete" data-id="{{ $peserta->id }}" id="hapusData">Delete</button>
                                            <!-- @if(empty($peserta->qrcode))
                                                <a href="{{ route('peserta.createQrcode', $peserta->id) }}"><button class="dropdown-item" type="button">Buat QrCode</button></a>
                                            @endif -->
                                        </div>
                                    </div>
                                    <!-- <div class="btn-group">
                                        <button class="btn btn-info btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Action
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a href="{{ route('peserta.edit', [$pelatihan->id, $peserta->id]) }}" class="dropdown-item">Edit</a>
                                            <a class="dropdown-item" data-toggle="modal" data-target="#ModalDelete" data-id="{{ $peserta->id }}" id="hapusData">Delete</a>
                                        </div>
                                    </div> -->
                                </td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div> <!-- /.table-stats -->
            </div>
            <div class="float-right">
                {{$peserta_data->links()}}
            </div>
        </div>
    </div>
    @include('partial.modalDelete', ['route' => 'peserta.destroy'])
@endSection