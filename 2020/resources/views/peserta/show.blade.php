@extends('templateDashboard')
@section('breadcrumbs')
    <div class="breadcrumbs">
        <div class="breadcrumbs-inner">
            <div class="row m-0">
                <div class="col-sm-4">
                    <div class="page-header float-left">
                        <div class="page-title">
                            <h1>Dashboard</h1>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="page-header float-right">
                        <div class="page-title">
                            <ol class="breadcrumb text-right">
                                <li><a href="#">Dashboard</a></li>
                                <li><a href="#">Peserta</a></li>
                                <li class="active">Index</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endSection
@section('content')
    <div class="row">
        <div class="col-lg-12 mb-3">
            <a href="{{ route('peserta.peserta_export', $peserta->id) }}"><button class="btn btn-outline-success">Unduh Profile</button></a>
        </div>
        <div class="col-md-4">
            <aside class="profile-nav alt">
                <section class="card">
                    <div class="card-header user-header alt bg-dark">
                        <div class="media">
                            <a href="#">
                                <img class="align-self-center rounded-circle mr-3" style="width:85px; height:85px;" alt="" src="{{ asset('images/peserta_profile/'.$peserta->foto_profile) }}">
                            </a>
                            <div class="media-body">
                                <h3 class="text-light display-6">{{ $peserta->nama }}</h3>
                                <p>{{ $peserta->no_ktp }}</p>
                            </div>
                        </div>
                    </div>

                    <?php
                        if(!empty($peserta->qrcode)) {
                            $urut = sprintf('%04d', $peserta->qrcode->urut);
                            $qrcode = $peserta->qrcode->tanggal.''.$urut;
                            $host = request()->getSchemeAndHttpHost();
                            $url = $host."/peserta/".$qrcode."/lihat_peserta";
                            $qrcodeJadi = QrCode::size(250)->generate($url);
                        } else {
                            $qrcodeJadi = "Belum Membuat QrCode";
                        }
                    ?>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <div class="col-md-12">
                                <!-- {!! $qrcodeJadi !!} -->
                            </div>
                        </li>
                    </ul>

                </section>
            </aside>
        </div>

        <!-- DATA PESERTA -->
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <strong class="text-uppercase">Data Peserta</strong>
                </div>
                <div class="card-body">
                    <table class="table table-borderless">
                        <tbody>
                            <tr>
                                <th>Nama Lengkap</th>
                                <th>:</th>
                                <td>{{ $peserta->nama }}</td>
                            </tr>
                            <tr>
                                <th>Nomor KTP</th>
                                <th>:</th>
                                <td>{{ $peserta->no_ktp }}</td>
                            </tr>
                            <tr>
                                <th>Status</th>
                                <th>:</th>
                                <td>{{ $peserta->status }}</td>
                            </tr>
                            <tr>
                                <th>Jenis Kelamin</th>
                                <th>:</th>
                                <td>{{ $peserta->jenis_kelamin }}</td>
                            </tr>
                            <tr>
                                <th>Tempat, Tanggal lahir</th>
                                <th>:</th>
                                <td>{{ $peserta->tempat_lahir }}, {{ empty($peserta->tanggal_lahir) ? '' : strftime('%d %B %Y', strtotime($peserta->tanggal_lahir)) }}</td>
                            </tr>
                            <tr>
                                <th>Agama</th>
                                <th>:</th>
                                <td>{{ empty($peserta->agamaData) ? '' : $peserta->agamaData->params }}</td>
                            </tr>
                            <tr>
                                <th>Pendidikan Terakhir</th>
                                <th>:</th>
                                <td>{{ empty($peserta->pendidikanData) ? '' : $peserta->pendidikanData->params }}</td>
                            </tr>
                            <tr>
                                <th>Alamat Rumah</th>
                                <th>:</th>
                                <td>{{ $peserta->alamat_rumah }}</td>
                            </tr>
                            <tr>
                                <th>Kab/Kota</th>
                                <th>:</th>
                                <td>{{ empty($peserta->kab_kota) ? '' : $peserta->kab_kota->kab_kota }}</td>
                            </tr>
                            <tr>
                                <th>Telp/HP</th>
                                <th>:</th>
                                <td>{{ $peserta->no_hp }}</td>
                            </tr>
                            <tr>
                                <th>E-Mail</th>
                                <th>:</th>
                                <td>{{ $peserta->email }}</td>
                            </tr>
                            <tr>
                                <th>Status Peserta</th>
                                <th>:</th>
                                <td>{{ empty($peserta->status_peserta) ? '' : $peserta->satusPesertaData->params }}</td>
                            </tr>
                        </tbody>
                    </table>        
                </div>
            </div>
        </div>

        <!-- DATA KOPERASI / UMKM PESERTA -->
        <div class="col-md-4"></div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <strong class="text-uppercase">Data Koperasi/Umkm Peserta</strong>
                </div>
                <div class="card-body">
                    <table class="table table-borderless">
                        <tbody>
                            <tr>
                                <th>Status Usaha</th>
                                <th>:</th>
                                <td>{{ empty($peserta->statusUsahaData) ? '' : $peserta->statusUsahaData->params }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <!-- DATA KOPERASI -->
        <div class="col-md-4"></div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <strong>DATA KOPERASI</strong>
                </div>
                <div class="card-body">
                    <table class="table table-borderless">
                        <tbody>
                            <tr>
                                <th>Sektor Usaha Koperasi</th>
                                <th>:</th>
                                <td>{{ !empty($peserta->bidangUsahaKoperasiData->params) ? $peserta->bidangUsahaKoperasiData->params : '' }}</td>
                            </tr>
                            <tr>
                                <th>Nama Koperasi</th>
                                <th>:</th>
                                <td>{{ $peserta->nama_koperasi }}</td>
                            </tr>
                            <tr>
                                <th>Alamat Koperasi</th>
                                <th>:</th>
                                <td>{{ $peserta->alamat_koperasi }}</td>
                            </tr>
                            <tr>
                                <th>Jenis Koperasi</th>
                                <th>:</th>
                                <td>{{ empty($peserta->jenisKoperasiData) ? '' : $peserta->jenisKoperasiData->params }}</td>
                            </tr>
                            <tr>
                                <th>Nomor Induk Koperasi</th>
                                <th>:</th>
                                <td>
                                    @if($peserta->no_induk_koperasi == 'true')
                                        Ada
                                    @elseif($peserta->no_induk_koperasi == 'false')
                                        Tidak Ada
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>Jumlah Tenaga Kerja Koperasi</th>
                                <th>:</th>
                                <td>{{ $peserta->jumlah_tenaga_kerja_koperasi }}</td>
                            </tr>
                            <tr>
                                <th>Omset Koperasi per Tahun</th>
                                <th>:</th>
                                <td>{{ !empty($peserta->omset_koperasi) ? 'Rp. ' : '' }} {{ number_format($peserta->omset_koperasi,0,',','.') }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <!-- DATA UMKM -->
        <div class="col-md-4"></div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <strong>DATA UMKM</strong>
                </div>
                <div class="card-body">
                    <table class="table table-borderless">
                        <tbody>
                            <tr>
                                <th>Sektor Usaha UMKM</th>
                                <th>:</th>
                                <td>{{ !empty($peserta->bidangUsahaData->params) ? $peserta->bidangUsahaData->params : '' }}</td>
                            </tr>
                            <tr>
                                <th>Nama Usaha (UMKM)</th>
                                <th>:</th>
                                <td>{{ $peserta->nama_usaha }}</td>
                            </tr>
                            <tr>
                                <th>Alamat Usaha (UMKM)</th>
                                <th>:</th>
                                <td>{{ $peserta->alamat_usaha }}</td>
                            </tr>

                            <tr>
                                <th>Jenis Usaha (UMKM)</th>
                                <th>:</th>
                                <td>{{ !empty($peserta->jenisUsahaData->params) ? $peserta->jenisUsahaData->params : '' }}</td>
                            </tr>

                            <tr>
                                <th>Bidang Usaha (UMKM)</th>
                                <th>:</th>
                                <td>{{ !empty($peserta->bidangUsahaUmkm->params) ? substr($peserta->bidangUsahaUmkm->params,5) : '' }}</td>
                            </tr>

                            <tr>
                                <th>Lama Usaha (UMKM)</th>
                                <th>:</th>
                                <td>{{ $peserta->lama_usaha }}</td>
                            </tr>
                            <tr>
                                <th>Jumlah Tenaga Kerja UMKM</th>
                                <th>:</th>
                                <td>{{ $peserta->jumlah_tenaga_kerja_umkm }}</td>
                            </tr>
                            <tr>
                                <th>Omset Usaha per Bulan</th>
                                <th>:</th>
                                <td>{{ !empty($peserta->omset_usaha) ? 'Rp. ' : '' }}{{ number_format($peserta->omset_usaha,0,',','.') }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <!-- PEMANTAUAN -->
        <div class="col-md-4"></div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <strong class="text-uppercase">PEMANTAUAN PELAKSANAAN PELATIHAN</strong>
                </div>
                <div class="card-body">
                    <div>
                        <strong>1. Apakah pelatihan yang anda ikuti sesuatu hal yang baru ? </strong>
                        <p>Jawaban : {{ !empty($peserta->pertanyaanPeserta->p1) ? $peserta->pertanyaanPeserta->p1 : '-' }}</p>
                    </div>
                    <div class="mt-3">
                        <strong>2. Apakah pelatihan yang anda ikuti sesuai dengan kebutuhan anda ? </strong>
                        <p>Jawaban : {{ !empty($peserta->pertanyaanPeserta->p2) ? $peserta->pertanyaanPeserta->p2 : '-' }}</p>
                    </div>
                    <div class="mt-3">
                        <strong>3. Apakah pelatihan yang anda ikuti bermanfaat dalam meningkatkan pengetahuan/ kemampuan anda ?</strong>
                        <p>Jawaban : {{ !empty($peserta->pertanyaanPeserta->p3) ? $peserta->pertanyaanPeserta->p3 : '-' }}</p>
                    </div>
                    <div class="mt-3">
                        <strong>4. Apakah pelatihan yang anda ikuti bermanfaat dalam pekerjaan atau usaha anda ?</strong>
                        <p>Jawaban : {{ !empty($peserta->pertanyaanPeserta->p4) ? $peserta->pertanyaanPeserta->p4 : '-' }}</p>
                    </div>
                    <div class="mt-3">
                        <strong>5. Menurut Anda, hal utama yang perlu ditingkatkan dalam penyelenggaraan pelatihan selanjutnya ?</strong>
                        <p>Jawaban : {{ !empty($peserta->pertanyaanPeserta->p5) ? $peserta->pertanyaanPeserta->p5 : '-' }}</p>
                    </div>
                    <div class="mt-3">
                        <strong>6. Apakah masalah utama yang anda hadapi dalam mengelola koperasi/usaha (umkm) ?</strong>
                        <p>Jawaban : {{ !empty($peserta->pertanyaanPeserta->p6) ? $peserta->pertanyaanPeserta->p6 : '-' }}</p>
                    </div>
                    <div class="mt-3">
                        <strong>7. Pelatihan apa yang anda butuhkan selanjutnya ? </strong>
                        <p>Jawaban : {{ !empty($peserta->pertanyaanPeserta->p7) ? $peserta->pertanyaanPeserta->p7 : '-' }}</p>
                    </div>
                    <div class="mt-3">
                        <strong>8. Materi pelatihan terkait Koperasi/UMKM/Kewirausahaan yang anda butuhkan ?</strong>
                        <p>Jawaban : {{ !empty($peserta->pertanyaanPeserta->p8) ? $peserta->pertanyaanPeserta->p8 : '-' }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endSection