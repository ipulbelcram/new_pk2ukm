@extends('templateDashboard')
@section('breadcrumbs')
    <div class="breadcrumbs">
        <div class="breadcrumbs-inner">
            <div class="row m-0">
                <div class="col-sm-4">
                    <div class="page-header float-left">
                        <div class="page-title">
                            <h1>Dashboard</h1>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="page-header float-right">
                        <div class="page-title">
                            <ol class="breadcrumb text-right">
                                <li><a href="#">Dashboard</a></li>
                                <li><a href="#">Data Peserta</a></li>
                                <li class="active">Form Peserta</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endSection
@section('content')
    @include('partial/navPeserta')
    <div class="row">
        <div class="col-md-3">
            <img src="{{ asset('images/peserta_profile/'.$peserta->foto_profile) }}" id="photo" class="img-thumbnail"/>
        </div>
        <div class="col-md-9">
            {!! Form::model($peserta, ['method' => 'PATCH', 'route' => ['peserta.store_pemantauan', $peserta->id]]) !!}
            <div class="card mb-4">
                <div class="card-header">
                    <strong class="card-title text-uppercase">Pemantauan Pelaksanaan Pelatihan</strong>
                </div>
                <div class="card-body">
                    <input type="hidden" name="pelatihan_id" value="{{ $pelatihan_id }}">
                    <input type="hidden" name="peserta_id" value="{{ $peserta->id }}">
                    <div class="form-group">
                        <label class="font-weight-bold">1. Apakah pelatihan yang anda ikuti sesuatu hal yang baru ?</label>
                        <div class="custom-control custom-radio">
                            {!! Form::radio('p1', 'Ya', !empty($peserta->pertanyaanPeserta->p1) && $peserta->pertanyaanPeserta->p1 == 'Ya' ? true : false, ['class' => 'custom-control-input', 'id' => 'p11']) !!} <label class="custom-control-label" for="p11">Ya</label>
                        </div>
                        <div class="custom-control custom-radio">
                            {!! Form::radio('p1', 'Tidak', !empty($peserta->pertanyaanPeserta->p1) && $peserta->pertanyaanPeserta->p1 == 'Tidak' ? true : false, ['class' => 'custom-control-input', 'id' => 'p12']) !!} <label class="custom-control-label" for="p12">Tidak</label>
                        </div>
                        <div class="text-danger">
                            @if($errors->has('p1')) 
                                {{ $errors->first('p1') }}
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="font-weight-bold">2. Apakah pelatihan yang anda ikuti sesuai dengan kebutuhan anda? </label>
                        <div class="custom-control custom-radio">
                            {!! Form::radio('p2', 'Ya', !empty($peserta->pertanyaanPeserta->p2) && $peserta->pertanyaanPeserta->p2 == 'Ya' ? true : false, ['class' => 'custom-control-input', 'id' => 'p21']) !!} <label class="custom-control-label" for="p21">Ya</label>
                        </div>
                        <div class="custom-control custom-radio">
                            {!! Form::radio('p2', 'Tidak', !empty($peserta->pertanyaanPeserta->p2) && $peserta->pertanyaanPeserta->p2 == 'Tidak' ? true : false, ['class' => 'custom-control-input', 'id' => 'p22']) !!} <label class="custom-control-label" for="p22">Tidak</label>
                        </div>
                        <div class="text-danger">
                            @if($errors->has('p2')) 
                                {{ $errors->first('p2') }}
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="font-weight-bold">3. Apakah pelatihan yang anda ikuti bermanfaat dalam meningkatkan pengetahuan/ kemampuan anda? </label>
                        <div class="custom-control custom-radio">
                            {!! Form::radio('p3', 'Tidak Bermanfaat', !empty($peserta->pertanyaanPeserta->p3) && $peserta->pertanyaanPeserta->p3 == 'Tidak Bermanfaat' ? true : false, ['class' => 'custom-control-input', 'id' => 'p31']) !!} <label class="custom-control-label" for="p31">Tidak Bermanfaat</label>
                        </div>
                        <div class="custom-control custom-radio">
                            {!! Form::radio('p3', 'Cukup Bermanfaat', !empty($peserta->pertanyaanPeserta->p3) && $peserta->pertanyaanPeserta->p3 == 'Cukup Bermanfaat' ? true : false, ['class' => 'custom-control-input', 'id' => 'p32']) !!} <label class="custom-control-label" for="p32">Cukup Bermanfaat</label>
                        </div>
                        <div class="custom-control custom-radio">
                            {!! Form::radio('p3', 'Bermanfaat', !empty($peserta->pertanyaanPeserta->p3) && $peserta->pertanyaanPeserta->p3 == 'Bermanfaat' ? true : false, ['class' => 'custom-control-input', 'id' => 'p33']) !!} <label class="custom-control-label" for="p33">Bermanfaat</label>
                        </div>
                        <div class="custom-control custom-radio">
                            {!! Form::radio('p3', 'Sangat Bermanfaat', !empty($peserta->pertanyaanPeserta->p3) && $peserta->pertanyaanPeserta->p3 == 'Sangat Bermanfaat' ? true : false, ['class' => 'custom-control-input', 'id' => 'p34']) !!} <label class="custom-control-label" for="p34">Sangat Bermanfaat</label>
                        </div>
                        <div class="text-danger">
                            @if($errors->has('p3')) 
                                {{ $errors->first('p3') }}
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="font-weight-bold">4. Apakah pelatihan yang anda ikuti bermanfaat dalam pekerjaan atau usaha anda? </label>
                        <div class="custom-control custom-radio">
                            {!! Form::radio('p4', 'Tidak Bermanfaat', !empty($peserta->pertanyaanPeserta->p4) && $peserta->pertanyaanPeserta->p4 == 'Tidak Bermanfaat' ? true : false, ['class' => 'custom-control-input', 'id' => 'p41']) !!} <label class="custom-control-label" for="p41">Tidak Bermanfaat</label>
                        </div>
                        <div class="custom-control custom-radio">
                            {!! Form::radio('p4', 'Cukup Bermanfaat', !empty($peserta->pertanyaanPeserta->p4) && $peserta->pertanyaanPeserta->p4 == 'Cukup Bermanfaat' ? true : false, ['class' => 'custom-control-input', 'id' => 'p42']) !!} <label class="custom-control-label" for="p42">Cukup Bermanfaat</label>
                        </div>
                        <div class="custom-control custom-radio">
                            {!! Form::radio('p4', 'Bermanfaat', !empty($peserta->pertanyaanPeserta->p4) && $peserta->pertanyaanPeserta->p4 == 'Bermanfaat' ? true : false, ['class' => 'custom-control-input', 'id' => 'p43']) !!} <label class="custom-control-label" for="p43">Bermanfaat</label>
                        </div>
                        <div class="custom-control custom-radio">
                            {!! Form::radio('p4', 'Sangat Bermanfaat', !empty($peserta->pertanyaanPeserta->p4) && $peserta->pertanyaanPeserta->p4 == 'Sangat Bermanfaat' ? true : false, ['class' => 'custom-control-input', 'id' => 'p44']) !!} <label class="custom-control-label" for="p44">Sangat Bermanfaat</label>
                        </div>
                        <div class="text-danger">
                            @if($errors->has('p4')) 
                                {{ $errors->first('p4') }}
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="font-weight-bold">5. Menurut Anda, hal utama yang perlu ditingkatkan dalam penyelenggaraan pelatihan selanjutnya ? </label>
                        <div class="custom-control custom-radio">
                            {!! Form::radio('p5', 'Kapasitas dan Kualitas Narasumber atau Fasilitator', !empty($peserta->pertanyaanPeserta->p5) && $peserta->pertanyaanPeserta->p5 == 'Kapasitas dan Kualitas Narasumber atau Fasilitator' ? true : false, ['class' => 'custom-control-input', 'id' => 'p51']) !!} <label class="custom-control-label" for="p51">Kapasitas dan Kualitas Narasumber atau Fasilitator</label>
                        </div>
                        <div class="custom-control custom-radio">
                            {!! Form::radio('p5', 'Materi Pembelajaran/ Pelatihan', !empty($peserta->pertanyaanPeserta->p5) && $peserta->pertanyaanPeserta->p5 == 'Materi Pembelajaran/ Pelatihan' ? true : false, ['class' => 'custom-control-input', 'id' => 'p52']) !!} <label class="custom-control-label" for="p52">Materi Pembelajaran/ Pelatihan</label>
                        </div>
                        <div class="custom-control custom-radio">
                            {!! Form::radio('p5', 'Metode Pembelajaran/ Pelatihan', !empty($peserta->pertanyaanPeserta->p5) && $peserta->pertanyaanPeserta->p5 == 'Metode Pembelajaran/ Pelatihan' ? true : false, ['class' => 'custom-control-input', 'id' => 'p53']) !!} <label class="custom-control-label" for="p53">Metode Pembelajaran/ Pelatihan</label>
                        </div>
                        <div class="custom-control custom-radio">
                            {!! Form::radio('p5', 'Waktu Penyelenggaraan Pembelajaran/ Pelatihan', !empty($peserta->pertanyaanPeserta->p5) && $peserta->pertanyaanPeserta->p5 == 'Waktu Penyelenggaraan Pembelajaran/ Pelatihan' ? true : false, ['class' => 'custom-control-input', 'id' => 'p54']) !!} <label class="custom-control-label" for="p54">Waktu Penyelenggaraan Pembelajaran/ Pelatihan</label>
                        </div>
                        <div class="custom-control custom-radio">
                            {!! Form::radio('p5', 'Sarana dan Prasarana Pembelajaran/ Pelatihan', !empty($peserta->pertanyaanPeserta->p5) && $peserta->pertanyaanPeserta->p5 == 'Sarana dan Prasarana Pembelajaran/ Pelatihan' ? true : false, ['class' => 'custom-control-input', 'id' => 'p55']) !!} <label class="custom-control-label" for="p55">Sarana dan Prasarana Pembelajaran/ Pelatihan</label>
                        </div>
                        <div class="custom-control custom-radio">
                            {!! Form::radio('p5', 'Pelayanan Panitia Penyelenggara Pelatihan', !empty($peserta->pertanyaanPeserta->p5) && $peserta->pertanyaanPeserta->p5 == 'Pelayanan Panitia Penyelenggara Pelatihan' ? true : false, ['class' => 'custom-control-input', 'id' => 'p56']) !!} <label class="custom-control-label" for="p56">Pelayanan Panitia Penyelenggara Pelatihan</label>
                        </div>
                        <div class="text-danger">
                            @if($errors->has('p5')) 
                                {{ $errors->first('p5') }}
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="font-weight-bold">6. Apakah masalah utama yang anda hadapi dalam mengelola koperasi/usaha (umkm) ? </label>
                        <div class="custom-control custom-radio">
                            {!! Form::radio('p6', 'Pemasaran', !empty($peserta->pertanyaanPeserta->p6) && $peserta->pertanyaanPeserta->p6 == 'Pemasaran' ? true : false, ['class' => 'custom-control-input', 'id' => 'p61']) !!} <label class="custom-control-label" for="p61">Pemasaran</label>
                        </div>
                        <div class="custom-control custom-radio">
                            {!! Form::radio('p6', 'Keuangan', !empty($peserta->pertanyaanPeserta->p6) && $peserta->pertanyaanPeserta->p6 == 'Keuangan' ? true : false, ['class' => 'custom-control-input', 'id' => 'p62']) !!} <label class="custom-control-label" for="p62">Keuangan</label>
                        </div>
                        <div class="custom-control custom-radio">
                            {!! Form::radio('p6', 'Operasional', !empty($peserta->pertanyaanPeserta->p6) && $peserta->pertanyaanPeserta->p6 == 'Operasional' ? true : false, ['class' => 'custom-control-input', 'id' => 'p63']) !!} <label class="custom-control-label" for="p63">Operasional</label>
                        </div>
                        <div class="custom-control custom-radio">
                            {!! Form::radio('p6', 'SDM', !empty($peserta->pertanyaanPeserta->p6) && $peserta->pertanyaanPeserta->p6 == 'SDM' ? true : false, ['class' => 'custom-control-input', 'id' => 'p64']) !!} <label class="custom-control-label" for="p64">SDM</label>
                        </div>
                        <div class="custom-control custom-radio">
                            {!! Form::radio('p6', 'Budaya dan Sistem Kerja', !empty($peserta->pertanyaanPeserta->p6) && $peserta->pertanyaanPeserta->p6 == 'Budaya dan Sistem Kerja' ? true : false, ['class' => 'custom-control-input', 'id' => 'p65']) !!} <label class="custom-control-label" for="p65">Budaya dan Sistem Kerja</label>
                        </div>
                        <div class="custom-control custom-radio">
                            {!! Form::radio('p6', 'Teknologi', !empty($peserta->pertanyaanPeserta->p6) && $peserta->pertanyaanPeserta->p6 == 'Teknologi' ? true : false, ['class' => 'custom-control-input', 'id' => 'p66']) !!} <label class="custom-control-label" for="p66">Teknologi</label>
                        </div>
                        <div class="custom-control custom-radio">
                            {!! Form::radio('p6', 'Produk', !empty($peserta->pertanyaanPeserta->p6) && $peserta->pertanyaanPeserta->p6 == 'Produk' ? true : false, ['class' => 'custom-control-input', 'id' => 'p67']) !!} <label class="custom-control-label" for="p67">Produk</label>
                        </div>
                        <div class="custom-control custom-radio">
                            {!! Form::radio('p6', 'Lainnya', !empty($peserta->pertanyaanPeserta->p6) && $peserta->pertanyaanPeserta->p6 == 'Lainnya' ? true : false, ['class' => 'custom-control-input', 'id' => 'p68']) !!} <label class="custom-control-label" for="p68">Lainnya</label>
                        </div>
                        <div class="text-danger">
                            @if($errors->has('p6')) 
                                {{ $errors->first('p6') }}
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="font-weight-bold">7. Pelatihan apa yang anda butuhkan selanjutnya ? </label>
                        <div class="custom-control custom-radio">
                            {!! Form::radio('p7', 'Pelatihan Kewirausahaan', !empty($peserta->pertanyaanPeserta->p7) && $peserta->pertanyaanPeserta->p7 == 'Pelatihan Kewirausahaan' ? true : false, ['class' => 'custom-control-input', 'id' => 'p71']) !!} <label class="custom-control-label" for="p71">Pelatihan Kewirausahaan</label>
                        </div>
                        <div class="custom-control custom-radio">
                            {!! Form::radio('p7', 'Pelatihan Perkoperasian', !empty($peserta->pertanyaanPeserta->p7) && $peserta->pertanyaanPeserta->p7 == 'Pelatihan Perkoperasian' ? true : false, ['class' => 'custom-control-input', 'id' => 'p72']) !!} <label class="custom-control-label" for="p72">Pelatihan Perkoperasian</label>
                        </div>
                        <div class="custom-control custom-radio">
                            {!! Form::radio('p7', 'Pelatihan Keterampilan Teknis atau Vocational', !empty($peserta->pertanyaanPeserta->p7) && $peserta->pertanyaanPeserta->p7 == 'Pelatihan Keterampilan Teknis atau Vocational' ? true : false, ['class' => 'custom-control-input', 'id' => 'p73']) !!} <label class="custom-control-label" for="p73">Pelatihan Keterampilan Teknis atau Vocational</label>
                        </div>
                        <div class="custom-control custom-radio">
                            {!! Form::radio('p7', 'Pelatihan Berbasis Kompetensi', !empty($peserta->pertanyaanPeserta->p7) && $peserta->pertanyaanPeserta->p7 == 'Pelatihan Berbasis Kompetensi' ? true : false, ['class' => 'custom-control-input', 'id' => 'p74']) !!} <label class="custom-control-label" for="p74">Pelatihan Berbasis Kompetensi</label>
                        </div>
                        <div class="custom-control custom-radio">
                            {!! Form::radio('p7', 'Inkubasi Usaha', !empty($peserta->pertanyaanPeserta->p7) && $peserta->pertanyaanPeserta->p7 == 'Inkubasi Usaha' ? true : false, ['class' => 'custom-control-input', 'id' => 'p75']) !!} <label class="custom-control-label" for="p75">Inkubasi Usaha</label>
                        </div>
                        <div class="custom-control custom-radio">
                            {!! Form::radio('p7', 'Pelatihan Pemanfaatan Teknologi Bagi Koperasi', !empty($peserta->pertanyaanPeserta->p7) && $peserta->pertanyaanPeserta->p7 == 'Pelatihan Pemanfaatan Teknologi Bagi Koperasi' ? true : false, ['class' => 'custom-control-input', 'id' => 'p76']) !!} <label class="custom-control-label" for="p76">Pelatihan Pemanfaatan Teknologi Bagi Koperasi</label>
                        </div>
                        <div class="custom-control custom-radio">
                            {!! Form::radio('p7', 'Pelatihan Pemanfaatan Teknologi Bagi UMKM', !empty($peserta->pertanyaanPeserta->p7) && $peserta->pertanyaanPeserta->p7 == 'Pelatihan Pemanfaatan Teknologi Bagi UMKM' ? true : false, ['class' => 'custom-control-input', 'id' => 'p77']) !!} <label class="custom-control-label" for="p77">Pelatihan Pemanfaatan Teknologi Bagi UMKM</label>
                        </div>
                        <div class="custom-control custom-radio">
                            {!! Form::radio('p7', 'Lainnya', !empty($peserta->pertanyaanPeserta->p7) && $peserta->pertanyaanPeserta->p7 == 'Lainnya' ? true : false, ['class' => 'custom-control-input', 'id' => 'p78']) !!} <label class="custom-control-label" for="p78">Lainnya</label>
                        </div>
                        <div class="text-danger">
                            @if($errors->has('p7')) 
                                {{ $errors->first('p7') }}
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="font-weight-bold">8. Materi pelatihan terkait Koperasi/UMKM/Kewirausahaan yang anda butuhkan ? </label>
                        {!! Form::textarea('p8', !empty($peserta->pertanyaanPeserta->p8) ? $peserta->pertanyaanPeserta->p8 : null, ['class' => 'form-control', 'placeholder' => 'Jawab']) !!}
                        <div class="text-danger">
                            @if($errors->has('p8')) 
                                {{ $errors->first('p8') }}
                            @endif
                        </div>
                    </div>
                
                </div>
            </div>
            <button class="btn btn-success btn-block">Simpan</button>
            {!! Form::close() !!}
        </div>
    </div>
@endSection