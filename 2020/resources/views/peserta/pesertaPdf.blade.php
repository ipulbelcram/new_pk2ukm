<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="{{ asset('vendors/bootstrap-4.4.1-dist/css/bootstrap.min.css') }}">
    <style>
        .page-break{
            page-break-after : always;
        }
    </style>
</head>
<body>
    <div class="row">
        <div class="col-12">
            <div class="border-bottom">
                <h3 class="pb-2">BIODATA PESERTA</h3>
            </div>
        </div>
        <!-- DATA PESERTA -->
        <div class="col-3 mt-4">
            <img src="{{ asset('images/peserta_profile/'.$data->foto_profile) }}" class="img-fluid rounded">
        </div>
        <div class="col-12 mt-4">
            <div class="card">
                <div class="card-header">
                    <strong class="text-uppercase">Data Peserta</strong>
                </div>
                <div class="card-body">
                    <table class="table table-borderless">
                        <tbody>
                            <tr>
                                <th>Nama Lengkap</th>
                                <th>:</th>
                                <td>{{ $data->nama }}</td>
                            </tr>
                            <tr>
                                <th>Nomor KTP</th>
                                <th>:</th>
                                <td>{{ $data->no_ktp }}</td>
                            </tr>
                            <tr>
                                <th>Status</th>
                                <th>:</th>
                                <td>{{ $data->status }}</td>
                            </tr>
                            <tr>
                                <th>Jenis Kelamin</th>
                                <th>:</th>
                                <td>{{ $data->jenis_kelamin }}</td>
                            </tr>
                            <tr>
                                <th>Tempat, Tanggal lahir</th>
                                <th>:</th>
                                <td>{{ $data->tempat_lahir }}, {{ empty($data->tanggal_lahir) ? '' : strftime('%d %B %Y', strtotime($data->tanggal_lahir)) }}</td>
                            </tr>
                            <tr>
                                <th>Agama</th>
                                <th>:</th>
                                <td>{{ empty($data->agamaData) ? '' : $data->agamaData->params }}</td>
                            </tr>
                            <tr>
                                <th>Pendidikan Terakhir</th>
                                <th>:</th>
                                <td>{{ empty($data->pendidikanData) ? '' : $data->pendidikanData->params }}</td>
                            </tr>
                            <tr>
                                <th>Alamat Rumah</th>
                                <th>:</th>
                                <td>{{ $data->alamat_rumah }}</td>
                            </tr>
                            <tr>
                                <th>Kab/Kota</th>
                                <th>:</th>
                                <td>{{ empty($data->kab_kota) ? '' : $data->kab_kota->kab_kota }}</td>
                            </tr>
                            <tr>
                                <th>Telp/HP</th>
                                <th>:</th>
                                <td>{{ $data->no_hp }}</td>
                            </tr>
                            <tr>
                                <th>E-Mail</th>
                                <th>:</th>
                                <td>{{ $data->email }}</td>
                            </tr>
                            <tr>
                                <th>Status Peserta</th>
                                <th>:</th>
                                <td>{{ empty($data->status_peserta) ? '' : $data->satusPesertaData->params }}</td>
                            </tr>
                        </tbody>
                    </table>        
                </div>
            </div>
        </div>

        <!-- PAGE BREAK -->
        <div class="page-break"></div>

        <div class="col-12">
            <div class="border-bottom">
                <h3 class="pb-2">DATA KOPERASI/ UMKM PESERTA</h3>
            </div>
        </div>

        <!-- DATA KOPERASI / UMKM PESERTA -->
        <div class="col-12 mt-4">
            <div class="card">
                <div class="card-header">
                    <strong class="text-uppercase">Data Koperasi/Umkm Peserta</strong>
                </div>
                <div class="card-body">
                    <table class="table table-borderless">
                        <tbody>
                            <tr>
                                <th>Status Usaha</th>
                                <th>:</th>
                                <td>{{ empty($data->statusUsahaData) ? '' : $data->statusUsahaData->params }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    
        <!-- DATA KOPERASI -->
        <div class="col-12 mt-4">
            <div class="card">
                <div class="card-header">
                    <strong>DATA KOPERASI</strong>
                </div>
                <div class="card-body">
                    <table class="table table-borderless">
                        <tbody>
                            <tr>
                                <th>Sektor Usaha Koperasi</th>
                                <th>:</th>
                                <td>{{ !empty($data->bidangUsahaKoperasiData->params) ? $data->bidangUsahaKoperasiData->params : '' }}</td>
                            </tr>
                            <tr>
                                <th>Nama Koperasi</th>
                                <th>:</th>
                                <td>{{ $data->nama_koperasi }}</td>
                            </tr>
                            <tr>
                                <th>Alamat Koperasi</th>
                                <th>:</th>
                                <td>{{ $data->alamat_koperasi }}</td>
                            </tr>
                            <tr>
                                <th>Jenis Koperasi</th>
                                <th>:</th>
                                <td>{{ empty($data->jenisKoperasiData) ? '' : $data->jenisKoperasiData->params }}</td>
                            </tr>
                            <tr>
                                <th>Nomor Induk Koperasi</th>
                                <th>:</th>
                                <td>
                                    @if($data->no_induk_koperasi == 'true')
                                        Ada
                                    @elseif($data->no_induk_koperasi == 'false')
                                        Tidak Ada
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>Jumlah Tenaga Kerja Koperasi</th>
                                <th>:</th>
                                <td>{{ $data->jumlah_tenaga_kerja_koperasi }}</td>
                            </tr>
                            <tr>
                                <th>Omset Koperasi per Tahun</th>
                                <th>:</th>
                                <td>{{ !empty($data->omset_koperasi) ? 'Rp. ' : '' }} {{ number_format($data->omset_koperasi,0,',','.') }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <!-- DATA UMKM -->
        <div class="col-12 mt-4">
            <div class="card">
                <div class="card-header">
                    <strong>DATA UMKM</strong>
                </div>
                <div class="card-body">
                    <table class="table table-borderless">
                        <tbody>
                            <tr>
                                <th>Sektor Usaha UMKM</th>
                                <th>:</th>
                                <td>{{ !empty($data->bidangUsahaData->params) ? $data->bidangUsahaData->params : '' }}</td>
                            </tr>
                            <tr>
                                <th>Nama Usaha (UMKM)</th>
                                <th>:</th>
                                <td>{{ $data->nama_usaha }}</td>
                            </tr>
                            <tr>
                                <th>Alamat Usaha (UMKM)</th>
                                <th>:</th>
                                <td>{{ $data->alamat_usaha }}</td>
                            </tr>

                            <tr>
                                <th>Jenis Usaha (UMKM)</th>
                                <th>:</th>
                                <td>{{ !empty($data->jenisUsahaData->params) ? $data->jenisUsahaData->params : '' }}</td>
                            </tr>

                            <tr>
                                <th>Bidang Usaha (UMKM)</th>
                                <th>:</th>
                                <td>{{ !empty($data->bidangUsahaUmkm->params) ? substr($data->bidangUsahaUmkm->params,5) : '' }}</td>
                            </tr>

                            <tr>
                                <th>Lama Usaha (UMKM)</th>
                                <th>:</th>
                                <td>{{ $data->lama_usaha }}</td>
                            </tr>
                            <tr>
                                <th>Jumlah Tenaga Kerja UMKM</th>
                                <th>:</th>
                                <td>{{ $data->jumlah_tenaga_kerja_umkm }}</td>
                            </tr>
                            <tr>
                                <th>Omset Usaha per Bulan</th>
                                <th>:</th>
                                <td>{{ !empty($data->omset_usaha) ? 'Rp. ' : '' }}{{ number_format($data->omset_usaha,0,',','.') }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <!-- PAGE BREAK -->
        <div class="page-break"></div>

        <!-- PEMANTAUAN -->
        <div class="col-12">
            <div class="border-bottom">
                <h3 class="pb-2">PEMANTAUAN PELAKSANAAN PELATIHAN</h3>
            </div>
        </div>
        <div class="col-12 mt-3">
            <table class="table table-borderless">
                <tr>
                    <th>Nomor KTP</th>
                    <td>{{ $data->no_ktp }}</td>
                </tr>
                <tr>
                    <th>Nama Lengkap</th>
                    <td>{{ $data->nama }}</td>
                </tr>
                <tr>
                    <th>Tempat, Tanggal Lahir</th>
                    <td>{{ $data->tempat_lahir }}, {{ empty($data->tanggal_lahir) ? '' : strftime('%d %B %Y', strtotime($data->tanggal_lahir)) }}</td>
                </tr>

                <tr>
                    <th>Nama Kegiatan</th>
                    <td>{{ $data->pelatihan->judul_pelatihan }}</td>
                </tr>

                <tr>
                    <th>Kabupaten/ Kota Kegiatan</th>
                    <td>{{ $data->pelatihan->kab_kota->kab_kota }}</td>
                </tr>
            </table>
        </div>
        <div class="col-12 mt-3">
            <div class="card">
                <div class="card-body">
                    <div>
                        <strong>1. Apakah pelatihan yang anda ikuti sesuatu hal yang baru ? </strong>
                        <p>Jawaban : {{ !empty($data->pertanyaanPeserta->p1) ? $data->pertanyaanPeserta->p1 : '-' }}</p>
                    </div>
                    <div class="mt-3">
                        <strong>2. Apakah pelatihan yang anda ikuti sesuai dengan kebutuhan anda ? </strong>
                        <p>Jawaban : {{ !empty($data->pertanyaanPeserta->p2) ? $data->pertanyaanPeserta->p2 : '-' }}</p>
                    </div>
                    <div class="mt-3">
                        <strong>3. Apakah pelatihan yang anda ikuti bermanfaat dalam meningkatkan pengetahuan/ kemampuan anda ?</strong>
                        <p>Jawaban : {{ !empty($data->pertanyaanPeserta->p3) ? $data->pertanyaanPeserta->p3 : '-' }}</p>
                    </div>
                    <div class="mt-3">
                        <strong>4. Apakah pelatihan yang anda ikuti bermanfaat dalam pekerjaan atau usaha anda ?</strong>
                        <p>Jawaban : {{ !empty($data->pertanyaanPeserta->p4) ? $data->pertanyaanPeserta->p4 : '-' }}</p>
                    </div>
                    <div class="mt-3">
                        <strong>5. Menurut Anda, hal utama yang perlu ditingkatkan dalam penyelenggaraan pelatihan selanjutnya ?</strong>
                        <p>Jawaban : {{ !empty($data->pertanyaanPeserta->p5) ? $data->pertanyaanPeserta->p5 : '-' }}</p>
                    </div>
                    <div class="mt-3">
                        <strong>6. Apakah masalah utama yang anda hadapi dalam mengelola koperasi/usaha (umkm) ?</strong>
                        <p>Jawaban : {{ !empty($data->pertanyaanPeserta->p6) ? $data->pertanyaanPeserta->p6 : '-' }}</p>
                    </div>
                    <div class="mt-3">
                        <strong>7. Pelatihan apa yang anda butuhkan selanjutnya ? </strong>
                        <p>Jawaban : {{ !empty($data->pertanyaanPeserta->p7) ? $data->pertanyaanPeserta->p7 : '-' }}</p>
                    </div>
                    <div class="mt-3">
                        <strong>8. Materi pelatihan terkait Koperasi/UMKM/Kewirausahaan yang anda butuhkan ?</strong>
                        <p>Jawaban : {{ !empty($data->pertanyaanPeserta->p8) ? $data->pertanyaanPeserta->p8 : '-' }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    

    
</body>
</html>