@extends('templateDashboard')
@section('breadcrumbs')
    <div class="breadcrumbs">
        <div class="breadcrumbs-inner">
            <div class="row m-0">
                <div class="col-sm-4">
                    <div class="page-header float-left">
                        <div class="page-title">
                            <h1>Dashboard</h1>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="page-header float-right">
                        <div class="page-title">
                            <ol class="breadcrumb text-right">
                                <li><a href="#">Dashboard</a></li>
                                <li><a href="#">Data Peserta</a></li>
                                <li class="active">Form Peserta</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endSection
@section('content')
    @include('partial.navPeserta')
    {!! Form::model($peserta, ['method' => 'patch', 'files' => true, 'route' => ['peserta.update', $pelatihan_id, $peserta->id]] ) !!}
        @include('peserta.form', ['btn_submit' => 'Simpan'])
    {!! Form::close() !!}
    <!-- Modal -->
    @include('partial.modalUpload')
@endSection
@section('script')
    <script src="{{ asset('js/peserta_form.js') }}"></script>
@endSection