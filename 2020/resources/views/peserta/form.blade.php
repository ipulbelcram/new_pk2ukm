<div class="row">
    <div class="col-lg-3">
        <div id="image_name"></div>
        @if(isset($peserta->foto_profile))
        <div id="uploaded_image"></div>
            <img src="{{ asset('images/peserta_profile/'.$peserta->foto_profile) }}" id="photo" class="img-thumbnail"/>
        @else 
        <div id="uploaded_image"></div>
            <img src="{{ asset('images/profile/Male.jpg') }}" id="photo" class="img-thumbnail"/>
        @endif
        <h6>Upload a different photo...</h6>
            <div class="text-danger">
                @if($errors->has('foto_profile'))
                    {{ $errors->first('foto_profile') }}
                @endif
            </div>
        <label class="btn btn-primary">
            Browse&hellip; {{ Form::file('foto_profile', ['id' => 'upload_image', 'accept' => 'image/*', 'style' =>'display: none']) }}
        </label>
    </div>
    <div class="col-lg-9">
        <div class="card mb-4">
            <div class="card-header">
                <strong class="card-title text-uppercase">Data Peserta</strong>
            </div>
            <div class="card-body">
                <input type="hidden" name="pelatihan_id" value="{{ $pelatihan_id }}">
                <div class="form-group pb-2">
                    <label class="font-weight-bold">Nama Lengkap</label>
                    {!! Form::text('nama', null, ['class' => 'form-control', 'placeholder' => 'Nama Lengkap']) !!}
                    <div class="text-danger">
                        @if($errors->has('nama')) 
                            {{ $errors->first('nama') }}
                        @endif
                    </div>
                </div>

                <div class="form-group pb-2">
                    <label class="font-weight-bold">Nomor KTP</label>
                    {!! Form::number('no_ktp', null, ['class' => 'form-control', 'placeholder' => 'Nomor KTP']) !!}
                    <div class="text-danger">
                        @if(Session::has('message_form'))
                            {{ Session::get('message_form') }}
                        @endif
                        @if($errors->has('no_ktp')) 
                            {{ $errors->first('no_ktp') }}
                        @endif
                    </div>
                </div>

                <div class="form-row pb-2">
                    <div class="form-group col">
                        <label class="font-weight-bold">Status</label>
                        {!! Form::select('status', ['Lajang' => 'Lajang', 'Menikah' => 'Menikah'], null, ['class' => 'form-control', 'placeholder' => 'Pilih']) !!}
                        <div class="text-danger">
                            @if($errors->has('status')) 
                                {{ $errors->first('status') }}
                            @endif
                        </div>
                    </div>

                    <div class="form-group col">
                        <label class="font-weight-bold">Jenis Kelamin</label>
                        {!! Form::select('jenis_kelamin', ['Laki-laki' => 'Laki-laki', 'Perempuan' => 'Perempuan'], null, ['class' => 'form-control', 'placeholder' => 'Pilih']) !!}
                        <div class="text-danger">
                            @if($errors->has('jenis_kelamin')) 
                                {{ $errors->first('jenis_kelamin') }}
                            @endif
                        </div>
                    </div>
                </div>

                <div class="form-row pb-2">
                    <div class="form-group col">
                        <label class="font-weight-bold">Tempat Lahir</label>
                        {!! Form::text('tempat_lahir', null, ['class' => 'form-control', 'placeholder' => 'Tempat Lahir']) !!}
                        <div class="text-danger">
                            @if($errors->has('tempat_lahir')) 
                                {{ $errors->first('tempat_lahir') }}
                            @endif
                        </div>
                    </div>

                    <div class="form-group col">
                        <label class="font-weight-bold">Tanggal Lahir</label>
                        {!! Form::date('tanggal_lahir', !empty($peserta->tanggal_lahir) ? $peserta->tanggal_lahir : null, ['class' => 'form-control', 'placeholder' => 'Tanggal Lahir']) !!}
                        <div class="text-danger">
                            @if($errors->has('tanggal_lahir')) 
                                {{ $errors->first('tanggal_lahir') }}
                            @endif
                        </div>
                    </div>
                </div>

                <div class="form-row pb-2">
                    <div class="form-group col">
                        <label class="font-weight-bold">Agama</label>
                        {!! Form::select('agama', $agama, null, ['class' => 'form-control', 'placeholder' => 'Pilih']) !!}
                        <div class="text-danger">
                            @if($errors->has('agama')) 
                                {{ $errors->first('agama') }}
                            @endif
                        </div>
                    </div>

                    <div class="form-group col">
                        <label class="font-weight-bold">Pendidikan</label>
                        {!! Form::select('pendidikan', $pendidikan, null, ['class' => 'form-control', 'placeholder' => 'Pilih']) !!}
                        <div class="text-danger">
                            @if($errors->has('pendidikan')) 
                                {{ $errors->first('pendidikan') }}
                            @endif
                        </div>
                    </div>
                </div>

                <div class="form-group pb-2">
                    <label class="font-weight-bold">Alamat Rumah</label>
                    {!! Form::textarea('alamat_rumah', null, ['class' => 'form-control', 'placeholder' => 'Alamat Rumah']) !!}
                    <div class="text-danger">
                        @if($errors->has('alamat_rumah')) 
                            {{ $errors->first('alamat_rumah') }}
                        @endif
                    </div>
                </div>
                
                <div class="form-row pb-2">
                    <div class="form-group col">
                        <label class="font-weight-bold">Kabupaten/ Kota Peserta</label>
                        {!! Form::select('kab_kota_id', $kab_kota, null, ['class' => 'form-control', 'placeholder' => 'Pilih']) !!}
                        <div class="text-danger">
                            @if($errors->has('kab_kota_id')) 
                                {{ $errors->first('kab_kota_id') }}
                            @endif
                        </div>
                    </div>
                    <div class="form-group col">
                        <label class="font-weight-bold">Telp/ HP</label>
                        <div class="form-group">
                            {!! Form::number('no_hp', null, ['class' => 'form-control', 'placeholder' => 'Telp/ HP']) !!}
                            <div class="text-danger">
                                @if($errors->has('no_hp')) 
                                    {{ $errors->first('no_hp') }}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-row pb-2">
                    <div class="form-group col">
                        <label class="font-weight-bold">E-Mail</label>
                        <div class="form-group">
                            {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'E-Mail']) !!}
                            <div class="text-danger">
                                @if($errors->has('email')) 
                                    {{ $errors->first('email') }}
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group col">
                        <label class="font-weight-bold">Status Peserta</label>
                        {!! Form::select('status_peserta', $status_peserta, null, ['class' => 'form-control', 'placeholder' => 'Status Peserta']) !!}
                        <div class="text-danger">
                            @if($errors->has('status_peserta')) 
                                {{ $errors->first('status_peserta') }}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card mb-4">
            <div class="card-header">
                <strong class="card-title text-uppercase">Data Koperasi atau UMKM Peserta</strong>
            </div>
            <div class="card-body">
                <div class="form-group pb-2">
                    <label class="font-weight-bold">Status Usaha</label>
                    {!! Form::select('status_usaha', $status_usaha, null, ['class' => 'form-control', 'id' => 'status_usaha','placeholder' => 'Pilih']) !!}
                    <div class="text-danger">
                        @if($errors->has('status_usaha')) 
                            {{ $errors->first('status_usaha') }}
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="card mb-4" id="form-koperasi">
            <div class="card-header">
                <strong class="card-title text-uppercase">Data Koperasi</strong>
            </div>
            <div class="card-body">
                <div class="form-group pb-2">
                    <label class="font-weight-bold">Sektor Usaha Koperasi</label>
                    {!! Form::select('bidang_usaha_koperasi', $bidang_usaha_koperasi, null, ['class' => 'form-control', 'placeholder' => 'Pilih']) !!}
                    <div class="text-danger">
                        @if($errors->has('bidang_usaha_koperasi')) 
                            {{ $errors->first('bidang_usaha_koperasi') }}
                        @endif
                    </div>
                </div>
                <div class="form-group pb-2">
                    <label class="font-weight-bold">Nama Koperasi</label>
                    {!! Form::text('nama_koperasi', null, ['class' => 'form-control', 'placeholder' => 'Nama Koperasi']) !!}
                    <div class="text-danger">
                        @if($errors->has('nama_koperasi')) 
                            {{ $errors->first('nama_koperasi') }}
                        @endif
                    </div>
                </div>

                <div class="form-group pb-2">
                    <label class="font-weight-bold">Alamat Koperasi</label>
                    {!! Form::textarea('alamat_koperasi', null, ['class' => 'form-control', 'placeholder' => 'Alamat Koperasi']) !!}
                    <div class="text-danger">
                        @if($errors->has('alamat_koperasi')) 
                            {{ $errors->first('alamat_koperasi') }}
                        @endif
                    </div>
                </div>

                <div class="form-row pb-2">
                    <div class="form-group col">
                        <label class="font-weight-bold">Jenis Koperasi</label>
                        {!! Form::select('jenis_koperasi', $jenis_koperasi, null, ['class' => 'form-control', 'placeholder' => 'Pilih']) !!}
                        <div class="text-danger">
                            @if($errors->has('jenis_koperasi')) 
                                {{ $errors->first('jenis_koperasi') }}
                            @endif
                        </div>
                    </div>
                </div>

                <div class="form-group pb-2">
                    <label class="font-weight-bold">Nomor Induk Koperasi</label>
                    {!! Form::select('no_induk_koperasi', ['true' => 'Ada', 'false' => 'Tidak Ada'], null, ['class' => 'form-control', 'placeholder' => 'Nomor Induk Koperasi']) !!}
                    <div class="text-danger">
                        @if($errors->has('no_induk_koperasi')) 
                            {{ $errors->first('no_induk_koperasi') }}
                        @endif
                    </div>
                </div>

                <div class="form-row pb-2">
                    <div class="form-group col">
                        <label class="font-weight-bold">Jumlah Tenaga Kerja Koperasi</label>
                        {!! Form::number('jumlah_tenaga_kerja_koperasi', null, ['class' => 'form-control', 'placeholder' => 'Jumlah Tenaga Kerja Koperasi']) !!}
                        <div class="text-danger">
                            @if($errors->has('jumlah_tenaga_kerja_koperasi')) 
                                {{ $errors->first('jumlah_tenaga_kerja_koperasi') }}
                            @endif
                        </div>
                    </div>

                    <div class="form-group col">
                        <label class="font-weight-bold">Omset Koperasi per Tahun</label>
                        {!! Form::text('omset_koperasi', null, ['class' => 'form-control price', 'placeholder' => 'Omset Koperasi per Tahun']) !!}
                        <div class="text-danger">
                            @if($errors->has('omset_koperasi')) 
                                {{ $errors->first('omset_koperasi') }}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card mb-4" id="form-umkm">
            <div class="card-header">
                <strong class="card-title text-uppercase">Data UMKM</strong>
            </div>
            <div class="card-body">
                <div class="form-group pb-2">
                    <label class="font-weight-bold">Sektor Usaha UMKM</label>
                    {!! Form::select('bidang_usaha', $bidang_usaha, null, ['class' => 'form-control', 'placeholder' => 'Pilih']) !!}
                    <div class="text-danger">
                        @if($errors->has('bidang_usaha')) 
                            {{ $errors->first('bidang_usaha') }}
                        @endif
                    </div>
                </div>
                <div class="form-group pb-2">
                    <label class="font-weight-bold">Nama Usaha (UMKM)</label>
                    {!! Form::text('nama_usaha', null, ['class' => 'form-control', 'placeholder' => 'Nama Usaha (UMKM)']) !!}
                    <div class="text-danger">
                        @if($errors->has('nama_usaha')) 
                            {{ $errors->first('nama_usaha') }}
                        @endif
                    </div>
                </div>

                <div class="form-group pb-2">
                    <label class="font-weight-bold">Alamat Usaha (UMKM)</label>
                    {!! Form::textarea('alamat_usaha', null, ['class' => 'form-control', 'placeholder' => 'Alamat Usaha (UMKM)']) !!}
                    <div class="text-danger">
                        @if($errors->has('alamat_usaha')) 
                            {{ $errors->first('alamat_usaha') }}
                        @endif
                    </div>
                </div>

                <div class="form-row pb-2">
                    <div class="form-group col">
                        <label class="font-weight-bold">Jenis Usaha (UMKM)</label>
                        {!! Form::select('jenis_usaha_id', $jenis_usaha, null, ['class' => 'form-control', 'id' => 'jenis_usaha', 'placeholder' => 'Jenis Usaha']) !!}
                        <div class="text-danger">
                            @if($errors->has('jenis_usaha_id')) 
                                {{ $errors->first('jenis_usaha_id') }}
                            @endif
                        </div>
                    </div>
                    <div class="form-group col">
                        <label class="font-weight-bold">Bidang Usaha (UMKM)</label>
                        <input type="hidden" value="{{ !empty($peserta->bidang_usaha_umkm_id) ? $peserta->bidang_usaha_umkm_id : '' }}" id="bidang_usaha_value">
                        {!! Form::select('bidang_usaha_umkm_id', [], null, ['class' => 'form-control', 'id' => 'bidang_usaha', 'placeholder' => 'Bidang Usaha (UMKM)']) !!}
                        <div class="text-danger">
                            @if($errors->has('bidang_usaha_umkm_id')) 
                                {{ $errors->first('bidang_usaha_umkm_id') }}
                            @endif
                        </div>
                    </div>
                </div>
                <div class="form-row pb-2">
                    <div class="form-group col">
                        <label class="font-weight-bold">Lama Usaha (UMKM)</label>
                        {!! Form::select('lama_usaha', ['< 12 Bulan' => '< 12 Bulan', '< 24 Bulan' => '< 24 Bulan', '< 36 Bulan' => '< 36 Bulan', '< 48 Bulan' => '< 48 Bulan', 'Lebih dari 48 Bulan (4 Tahun)' => 'Lebih dari 48 Bulan (4 Tahun)'], null, ['class' => 'form-control', 'placeholder' => 'Pilih']) !!}
                        <div class="text-danger">
                            @if($errors->has('lama_usaha')) 
                                {{ $errors->first('lama_usaha') }}
                            @endif
                        </div>
                    </div>
                </div>

                <div class="form-row pb-2">
                    <div class="form-group col">
                        <label class="font-weight-bold">Jumlah Tenaga Kerja UMKM</label>
                        {!! Form::number('jumlah_tenaga_kerja_umkm', null, ['class' => 'form-control', 'placeholder' => 'Jumlah Tenaga Kerja UMKM']) !!}
                        <div class="text-danger">
                            @if($errors->has('jumlah_tenaga_kerja_umkm')) 
                                {{ $errors->first('jumlah_tenaga_kerja_umkm') }}
                            @endif
                        </div>
                    </div>

                    <div class="form-group col">
                        <label class="font-weight-bold">Omset Usaha per Bulan</label>
                        {!! Form::text('omset_usaha', null, ['class' => 'form-control price', 'placeholder' => 'Omset Usaha per Bulan']) !!}
                        <div class="text-danger">
                            @if($errors->has('omset_usaha')) 
                                {{ $errors->first('omset_usaha') }}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <button class="btn btn-success btn-block">{{ $btn_submit }}</button>
    </div>
</div>