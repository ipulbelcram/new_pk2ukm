<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Unduh Pendamping Lengkap</title>
    <link rel="stylesheet" href="{{ asset('vendors/bootstrap-4.4.1-dist/css/bootstrap.min.css') }}">
    <style>
        .preloader {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background-color: #fff;
        }

        .preloader .loading {
            position: absolute;
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%);
            font: 14px arial;
        }
    </style>
</head>
<body>
    <div class="preloader">
        <div class="loading">
            <img src="{{ asset('images/spiner.gif') }}" width="220">
            <p>Harap Tunggu File Sedang di Proses</p>
        </div>
    </div>
    <div class="d-flex justify-content-center" style="margin-top: 200px">
        <div id="download-success" class="d-none col-lg-7 text-center">
            <div class="alert alert-success py-5" role="alert">
                File anda berhasil di download
            </div>
        </div>
    </div>    
    <div class="d-none">
        <table id="data">
            <tr><th>DATA PENDAMPING PK2UKM</th></tr>
            <tr><th>SELURUH INDONESIA</th></tr>
            <tr><th>TAHUN 2020</th></tr>
        
            <tr></tr>
        
            <tr>
                <th>PROV</th>
                <th>NO</th>
                <th>Dinas Prov/Kab/Kota</th>
                <th>Nama Lengkap</th>
                <th>Nomor KTP</th>
                <th>Jenis Kelamin</th>
                <th>Agama</th>
                <th>Pendidikan</th>
                <th>Alamat Rumah</th>
                <th>No.Hp (WA)</th>
                <th>E-Mail</th>
            </tr>
        
            @foreach($pendamping_data as $pendamping)
            <tr>
                <td>{{ $pendamping->user->provinsi->provinsi }}</td>
                <td>{{ $pendamping->user->order_prov }}</td>
                <td>{{ $pendamping->user->name }}</td>
                <td>{{ $pendamping->nama_lengkap }}</td>
                <td>'{{ $pendamping->no_ktp }}</td>
                <td>{{ $pendamping->jenis_kelamin }}</td>
                <td>{{ $pendamping->agamaData->params }}</td>
                <td>{{ $pendamping->pendidikanData->params }}</td>
                <td>{{ $pendamping->alamat_rumah }}</td>
                <td>'{{ $pendamping->no_hp }}</td>
                <td>{{ $pendamping->email }}</td>
            </tr>
            @endforeach
        
        </table>
    </div>

    <script type="text/javascript" src="{{ asset('vendors/jquery.min.js') }}"></script>
    <script src="{{ asset('vendors/bootstrap-4.4.1-dist/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/exportExcel.js') }}"></script>
    <script>
        $(document).ready(function() {
            exportTableToExcel('data', 'Pendamping Lengkap')
            $(".preloader").fadeOut();
            $("#download-success").removeClass('d-none');
        })
    </script>
</body>
</html>