<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Unduh Kegiatan Lengkap</title>
    <link rel="stylesheet" href="{{ asset('vendors/bootstrap-4.4.1-dist/css/bootstrap.min.css') }}">
    <style>
        .preloader {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background-color: #fff;
        }

        .preloader .loading {
            position: absolute;
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%);
            font: 14px arial;
        }
    </style>
</head>
<body>
    <div class="preloader">
        <div class="loading">
            <img src="{{ asset('images/spiner.gif') }}" width="220">
            <p>Harap Tunggu File Sedang di Proses</p>
        </div>
    </div>
    <div class="d-flex justify-content-center" style="margin-top: 200px">
        <div id="download-success" class="d-none col-lg-7 text-center">
            <div class="alert alert-success py-5" role="alert">
                File anda berhasil di download
            </div>
        </div>
    </div>
    <div class="d-none">
        <table id="data">
            <tr>
                <td>Jenis Pelatihan</td>
                <td>Judul Pelatihan</td>
                <td>Kab/Kota Pelatihan</td>
                <td>Tempat Pelaksanaan</td>
                <td>Tanggal Mulai</td>
                <td>Tanggal Selesai</td>
                <td>No</td>
                <td>Nama Lengkap</td>
                <td>Nomor KTP</td>
                <td>Status</td>
                <td>Jenis Kelamin</td>
                <td>Tempat Lahir</td>
                <td>Tanggal Lahir</td>
                <td>Agama</td>
                <td>Pendidikan Terakhir</td>
                <td>Alamat Rumah</td>
                <td>Email</td>
                <td>Telp/Hp</td>
                <td>Kab/Kota Peserta</td>
                <td>Status Peserta</td>
                <td>Status Usaha</td>
                <td>Sektor Usaha Koperasi/ UMKM</td>
                <td>Nama Koperasi/ UMKM</td>
                <td>Alamat Koperasi/ UMKM</td>
                <td>No Induk Koperasi (NIK)</td>
                <td>Jenis Usaha Koperasi/ UMKM</td>
                <td>Bidang Usaha UMKM</td>
                <td>Lama Usaha UMKM</td>
                <td>Jumlah Tenaga Kerja Koperasi/ UMKM</td>
                <td>Omset Koperasi per Tahun/ Usaha Per Bulan</td>
                <td>Code Peserta</td>
                <td>Entry Date</td>
                <td>Nama User</td>
                <td>Apakah pelatihan yang anda ikuti sesuatu hal yang baru ?</td>
                <td>Apakah pelatihan yang anda ikuti sesuai dengan kebutuhan anda ?</td>
                <td>Apakah pelatihan yang anda ikuti bermanfaat dalam meningkatkan pengetahuan/ kemampuan anda ?</td>
                <td>Apakah pelatihan yang anda ikuti bermanfaat dalam pekerjaan atau usaha anda?</td>
                <td>Menurut Anda, hal utama yang perlu ditingkatkan dalam penyelenggaraan pelatihan selanjutnya ?</td>
                <td>Apakah masalah utama yang anda hadapi dalam mengelola koperasi/usaha (umkm) ?</td>
                <td>Pelatihan apa yang anda butuhkan selanjutnya ?</td>
                <td>Materi pelatihan terkait Koperasi/UMKM/Kewirausahaan yang anda butuhkan ?</td>
            </tr>
    
            <?php $no=1; ?>
            @foreach($data as $data)
            <tr>
                <td>{{ $data->jenis_pelatihan }}</td>
                <td>{{ $data->judul_pelatihan }}</td>
                <td>{{ $data->kab_kota_pelatihan }}</td>
                <td>{{ $data->tempat }}</td>
                <td>{{ $data->tanggal_mulai }}</td>
                <td>{{ $data->tanggal_selesai }}</td>
                <td>{{ $no++ }}</td>
                <td>{{ $data->nama }}</td>
                <td>'{{ $data->no_ktp }}</td>
                <td>{{ $data->status }}</td>
                <td>{{ $data->jenis_kelamin }}</td>
                <td>{{ $data->tempat_lahir }}</td>
                <td>{{ $data->tanggal_lahir }}</td>
                <td>{{ $data->agama }}</td>
                <td>{{ $data->pendidikan }}</td>
                <td>{{ $data->alamat_rumah }}</td>
                <td>{{ $data->email }}</td>
                <td>'{{ $data->no_hp }}</td>
                <td>{{ $data->kab_kota_peserta }}</td>
                <td>{{ $data->status_peserta }}</td>
                <td>{{ $data->status_usaha }}</td>
                <td>{{ $data->bidang_usaha_koperasi }}  {{ $data->bidang_usaha }}</td>
                <td>{{ $data->nama_koperasi }} {{ $data->nama_usaha }}</td>
                <td>{{ $data->alamat_koperasi }} {{ $data->alamat_usaha }}</td>
                <td>
                    @if($data->no_induk_koperasi == 'true')
                        Ada
                    @elseif($data->no_induk_koperasi == 'false')
                        Tidak Ada
                    @else
                        -
                    @endif
                </td>
                <td>{{ $data->jenis_koperasi }} {{ $data->jenis_usaha }}</td>
                <td>{{ substr($data->bidang_usaha_umkm,5) }}</td>
                <td>{{ $data->lama_usaha }}</td>
                <td>'{{ $data->jumlah_tenaga_kerja_koperasi }} {{ $data->jumlah_tenaga_kerja_umkm }}</td>
                <td>'{{ $data->omset_koperasi }} {{ $data->omset_usaha }}</td>
                <td>'{{ $data->code }}</td>
                <td>{{ $data->created_at }}</td>
                <td>{{ $data->nama_user }}</td>
                <td>{{ $data->p1 }}</td>
                <td>{{ $data->p2 }}</td>
                <td>{{ $data->p3 }}</td>
                <td>{{ $data->p4 }}</td>
                <td>{{ $data->p5 }}</td>
                <td>{{ $data->p6 }}</td>
                <td>{{ $data->p7 }}</td>
                <td>{{ $data->p8 }}</td>
            </tr>
            @endforeach
            
        </table>
    </div>

    <script type="text/javascript" src="{{ asset('vendors/jquery.min.js') }}"></script>
    <script src="{{ asset('vendors/bootstrap-4.4.1-dist/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/exportExcel.js') }}"></script>
    <script>
        $(document).ready(function() {
            exportTableToExcel('data', 'Kegiatan Lengkap')
            $(".preloader").fadeOut();
            $("#download-success").removeClass('d-none');
        })
    </script>
</body>
</html>
