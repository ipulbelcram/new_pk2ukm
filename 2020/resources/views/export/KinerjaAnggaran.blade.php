<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Unduh Kinerja Anggaran</title>
    <link rel="stylesheet" href="{{ asset('vendors/bootstrap-4.4.1-dist/css/bootstrap.min.css') }}">
    <style>
        .preloader {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background-color: #fff;
        }

        .preloader .loading {
            position: absolute;
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%);
            font: 14px arial;
        }
    </style>
</head>
<body>
    {{-- <div class="preloader">
        <div class="loading">
            <img src="{{ asset('images/spiner.gif') }}" width="220">
            <p>Harap Tunggu File Sedang di Proses</p>
        </div>
    </div> --}}
    {{-- <div class="d-flex justify-content-center" style="margin-top: 200px">
        <div id="download-success" class="d-none col-lg-7 text-center">
            <div class="alert alert-success py-5" role="alert">
                File anda berhasil di download
            </div>
        </div>
    </div> --}}
    
    <table id="data">
        <tr>
            <td rowspan="4">PROV</td>
            <td rowspan="4">NO</td>
            <td rowspan="4">Dinas Prov/Kab/Kota</td>
            <td colspan="4">PAGU</td>
            <td colspan="6">REALISASI TAHAP 1</td>
            <td colspan="6">REALISASI TAHAP 2</td>
            <td colspan="5">SISA REALISASI</td>
        </tr>
        <tr>
            
            <td colspan="4">VOLUME</td>
    
            <td colspan="5">VOLUME</td>
            <td rowspan="3">PERSENT(%)</td>
    
            <td colspan="5">VOLUME</td>
            <td rowspan="3">PERSENT(%)</td>
    
            <td colspan="4">VOLUME</td>
            <td rowspan="3">PERSENT(%)</td>
        </tr>
        <tr>
            <td>DIKLAT</td>
            <td>PENDAMPING</td>
            <td>TOTAL</td>
            <td>ANGGARAN</td>
    
            <td>DIKLAT</td>
            <td>PENDAMPING</td>
            <td>TOTAL</td>
            <td rowspan="2">PERSENT (%)</td>
            <td>ANGGARAN</td>
    
            <td>DIKLAT</td>
            <td>PENDAMPING</td>
            <td>TOTAL</td>
            <td rowspan="2">PERSENT (%)</td>
            <td>ANGGARAN</td>
    
            <td>DIKLAT</td>
            <td>PENDAMPING</td>
            <td>TOTAL</td>
            <td>ANGGARAN</td>
        </tr>
        <tr>
            <td>(ORANG)</td>
            <td>(ORANG)</td>
            <td>(ORANG)</td>
            <td>(RP)</td>
    
            <td>(ORANG)</td>
            <td>(ORANG)</td>
            <td>(ORANG)</td>
            <td>(RP)</td>
    
            <td>(ORANG)</td>
            <td>(ORANG)</td>
            <td>(ORANG)</td>
            <td>(RP)</td>
    
            <td>(ORANG)</td>
            <td>(ORANG)</td>
            <td>(ORANG)</td>
            <td>(RP)</td>
        </tr>
    
        <!-- ISI DATA -->
        @foreach($data as $data)
            <?php
                $total_pagu = $data->target_peserta + $data->target_pendamping;
                $total_realisasi1 = $data->realisasi_peserta1 + $data->realisasi_pendamping;
                $total_realisasi2 = $data->realisasi_peserta2 + $data->realisasi_pendamping;
    
                $sisa_pelatihan = $data->target_peserta - $data->realisasi_peserta2;
                $sisa_pendamping = $data->target_pendamping - $data->realisasi_pendamping;
                $total_sisa = $sisa_pelatihan+$sisa_pendamping;
                $sisa_anggaran = $data->pagu_anggaran - $data->realisasi_anggaran2;
    
                if($sisa_anggaran != 0 ) {
                    $presetase_sisa = ($sisa_anggaran/$data->pagu_anggaran)*100;
                } else {
                    $presetase_sisa = 0;
                }
    
                if($total_realisasi1 != 0) {
                    $presetase_orangR1 = ($total_realisasi1/$total_pagu)*100;
                } else {
                    $presetase_orangR1 = 0;
                }
    
                if($data->realisasi_anggaran1 != 0) {
                    $presetase_anggaranR1 = ($data->realisasi_anggaran1/$data->pagu_anggaran)*100;
                } else {
                    $presetase_anggaranR1 = 0;
                }
    
                if($total_realisasi2 != 0) {
                    $presetase_orangR2 = ($total_realisasi2/$total_pagu)*100;
                } else {
                    $presetase_orangR2 = 0;
                }
    
                if($data->realisasi_anggaran2 != 0) {
                    $presetase_anggaranR2 = ($data->realisasi_anggaran2/$data->pagu_anggaran)*100;
                } else {
                    $presetase_anggaranR2 = 0;
                }
            ?>
            <tr>
                <td>{{ $data->provinsi }}</td>
                <td>{{ $data->order_prov }}</td>
                <td>{{ $data->name }}</td>
    
                <!-- PAGU -->
                <td>{{ $data->target_peserta }}</td>
                <td>{{ $data->target_pendamping }}</td>
                <td>{{ $total_pagu }}</td>
                <td>{{ $data->pagu_anggaran }}</td>
    
                <!-- REALISASI 1 -->
                <td>{{ $data->realisasi_peserta1 }}</td>
                <td>{{ $data->realisasi_pendamping }}</td>
                <td>{{ $total_realisasi1 }}</td>
                <td>{{ number_format($presetase_orangR1,2,',','.') }}</td>
                <td>{{ $data->realisasi_anggaran1 }}</td>
                <td>{{ number_format($presetase_anggaranR1,2,',','.') }}</td>
    
                <!-- REALISASI 2 -->
                <td>{{ $data->realisasi_peserta2 }}</td>
                <td>{{ $data->realisasi_pendamping }}</td>
                <td>{{ $total_realisasi2 }}</td>
                <td>{{ number_format($presetase_orangR2,2,',','.') }}</td>
                <td>{{ $data->realisasi_anggaran2 }}</td>
                <td>{{ number_format($presetase_anggaranR2,2,',','.') }}</td>
    
                <!-- SISA REALISASI -->
                <td>{{ $sisa_pelatihan }}</td>
                <td>{{ $sisa_pendamping }}</td>
                <td>{{ $total_sisa }}</td>
                <td>{{ $sisa_anggaran }}</td>
                <td>{{ number_format($presetase_sisa,2,',','.') }}</td>
            </tr>
        @endforeach
    </table>
    {{-- <div class="d-none">
    </div> --}}

    <script type="text/javascript" src="{{ asset('vendors/jquery.min.js') }}"></script>
    <script src="{{ asset('vendors/bootstrap-4.4.1-dist/js/bootstrap.min.js') }}"></script>
    {{-- <script type="text/javascript" src="{{ asset('js/exportExcel.js') }}"></script> --}}
    {{-- <script>
        $(document).ready(function() {
            exportTableToExcel('data', 'Kinerja Anggaran')
            $(".preloader").fadeOut();
            $("#download-success").removeClass('d-none');
        })
    </script> --}}
</body>
</html>