<table>
    <tr>
        <th>Nama Lengkap</th>
        <td>{{ $data->nama }}</td>
    </tr>
    <tr>
        <th>Nomor KTP</th>
        <td>'{{ $data->no_ktp }}</td>
    </tr>
    <tr>
        <th>Status</th>
        <td>{{ $data->status }}</td>
    </tr>
    <tr>
        <th>Jenis Kelamin</th>
        <td>{{ $data->jenis_kelamin }}</td>
    </tr>
    <tr>
        <th>Tempat, Tanggal lahir</th>
        <td>{{ $data->tempat_lahir }}, {{ empty($data->tanggal_lahir) ? '' : date_format($data->tanggal_lahir, 'd F Y') }}</td>
    </tr>
    <tr>
        <th>Agama</th>
        <td>{{ empty($data->agamaData) ? '' : $data->agamaData->params }}</td>
    </tr>
    <tr>
        <th>Pendidikan Terakhir</th>
        <td>{{ empty($data->pendidikanData) ? '' : $data->pendidikanData->params }}</td>
    </tr>
    <tr>
        <th>Alamat Rumah</th>
        <td>{{ $data->alamat_rumah }}</td>
    </tr>
    <tr>
        <th>Kab/Kota</th>
        <td>{{ empty($data->kab_kota) ? '' : $data->kab_kota->kab_kota }}</td>
    </tr>
    <tr>
        <th>Telp/HP</th>
        <td>'{{ $data->no_hp }}</td>
    </tr>
    <tr>
        <th>E-Mail</th>
        <td>{{ $data->email }}</td>
    </tr>
    <tr>
        <th>Status Peserta</th>
        <td>{{ empty($data->status_peserta) ? '' : $data->satusPesertaData->params }}</td>
    </tr>
    <tr>
        <td colspan="2">DATA KOPERASI/UMKM PESERTA</td>
    </tr>
    <tr>
        <th>Status Usaha</th>
        <td>{{ empty($data->statusUsahaData) ? '' : $data->statusUsahaData->params }}</td>
    </tr>
    <tr>
        <td colspan="2">DATA KOPERASI</td>
    </tr>
    <tr>
        <th>Nama Koperasi</th>
        <td>{{ $data->nama_koperasi }}</td>
    </tr>
    <tr>
        <th>Alamat Koperasi</th>
        <td>{{ $data->alamat_koperasi }}</td>
    </tr>
    <tr>
        <th>Jenis Koperasi</th>
        <td>{{ empty($data->jenisKoperasiData) ? '' : $data->jenisKoperasiData->params }}</td>
    </tr>
    <tr>
        <th>Bidang Usaha Koperasi</th>
        <td>{{ empty($data->bidangUsahaKoperasiData) ? '' : $data->bidangUsahaKoperasiData->params}}</td>
    </tr>
    <tr>
        <th>Nomor Induk Koperasi</th>
        <td>{{ $data->no_induk_koperasi }}</td>
    </tr>
    <tr>
        <th>Jumlah Tenaga Kerja Koperasi</th>
        <td>{{ $data->jumlah_tenaga_kerja_koperasi }}</td>
    </tr>
    <tr>
        <th>Omset Koperasi Per Tahun</th>
        <td>{{ !empty($data->omset_koperasi) ? 'Rp. ' : '' }} {{ number_format($data->omset_koperasi,0,',','.') }}</td>
    </tr>
    <tr>
        <td colspan="2">DATA UMKM</td>
    </tr>
    <tr>
        <th>Nama Usaha(UMKM)</th>
        <td>{{ $data->nama_usaha }}</td>
    </tr>
    <tr>
        <th>Alamat Usaha(UMKM)</th>
        <td>{{ $data->alamat_usaha }}</td>
    </tr>
    <tr>
        <th>Bidang Usaha(UMKM)</th>
        <td>{{ empty($data->bidangUsahaData) ? '' : $data->bidangUsahaData->params }}</td>
    </tr>
    <tr>
        <th>Lama Usaha(UMKM)</th>
        <td>{{ $data->lama_usaha }}</td>
    </tr>
    <tr>
        <th>Jumlah Tenaga Kerja UMKM</th>
        <td>{{ $data->jumlah_tenaga_kerja_umkm }}</td>
    </tr>
    <tr>
        <th>Omset Usaha Per Bulan</th>
        <td>{{ !empty($data->omset_usaha) ? 'Rp. ' : '' }}{{ number_format($data->omset_usaha,0,',','.') }}</td>
    </tr>
</table>