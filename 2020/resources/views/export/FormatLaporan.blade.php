<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Peserta Lengkap</title>
    <link rel="stylesheet" href="{{ asset('vendors/bootstrap-4.4.1-dist/css/bootstrap.min.css') }}">
    <style>
        .preloader {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background-color: #fff;
        }

        .preloader .loading {
            position: absolute;
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%);
            font: 14px arial;
        }
    </style>
</head>
<body>
    <div class="preloader">
        <div class="loading">
            <img src="{{ asset('images/spiner.gif') }}" width="220">
            <p>Harap Tunggu File Sedang di Proses</p>
        </div>
    </div>
    <div class="d-flex justify-content-center" style="margin-top: 200px">
        <div id="download-success" class="d-none col-lg-7 text-center">
            <div class="alert alert-success py-5" role="alert">
                File anda berhasil di download
            </div>
        </div>
    </div>
    <div class="d-none">
        <table id="data">
            <thead>
                <tr>
                    <th colspan="14">LAPORAN DATA PESERTA KEGIATAN</th>
                </tr>
                <tr>
                    <th colspan="14">DAK NONFISIK PK2UKM</th>
                </tr>
                <tr>
                    <th colspan="14">DEPUTI BIDANG PENGEMBANGAN SDM</th>
                </tr>
                <tr></tr><tr></tr>
                    <tr>
                        <th>Jenis Kegiatan</th>
                        <th>{{ $pelatihan->jenisPelatihanData->params }}</th>
                    </tr>
                    <tr>
                        <th>Judul Kegiatan</th>
                        <th>{{ $pelatihan->judul_pelatihan }}</th>
                    </tr>
                    <tr>
                        <th>Provinsi</th>
                        <th>{{ $pelatihan->provinsi->provinsi }}</th>
                    </tr>
                    <tr>
                        <th>Kab/Kota</th>
                        <th>{{ $pelatihan->kab_kota->kab_kota }}</th>
                    </tr>
                    <tr>
                        <th>Jumlah Peserta</th>
                        <th>{{ $jumlah_data }}</th>
                    </tr>
                    <tr>
                        <th>Tempat Pelaksanaan</th>
                        <th>{{ $pelatihan->tempat }}</th>
                    </tr>
                    <tr>
                        <th>Tanggal Pelaksanaan</th>
                        <th>{{ date("d-m-Y", strtotime($pelatihan->tanggal_mulai)) }} s.d {{ date("d-m-Y", strtotime($pelatihan->tanggal_selesai)) }}</th>
                    </tr>
                    <tr></tr>
                <tr>
                    <th>No</th>
                    <th>Nama Lengkap</th>
                    <th>Nomor KTP</th>
                    <th>Status</th>
                    <th>Jenis Kelamin</th>
                    <th>Tempat Lahir</th>
                    <th>Tanggal Lahir</th>
                    <th>Agama</th>
                    <th>Pendidikan Terakhir</th>
                    <th>Kab/ Kota </th>
                    <th>Alamat Rumah</th>
                    <th>Telp/ HP</th>
                    <th>Status Usaha</th>
                    <th>Sektor Usaha Koperasi/ UMKM</th>
                    <th>Nama Koperasi/UMKM</th>
                    <th>Alamat Koperasi/UMKM</th>
                </tr>
            </thead>
            <tbody>
                @php 
                    $no=1;
                @endphp
                @foreach($data as $item)
                    <tr>
                        <td>{{ $no++ }}</td>
                        <td>{{ $item->nama }}</td>
                        <td>'{{ $item->no_ktp }}</td>
                        <td>{{ $item->status }}</td>
                        <td>{{ $item->jenis_kelamin }}</td>
                        <td>{{ $item->tempat_lahir }}</td>
                        <td>{{ date('d-m-Y', strtotime($item->tanggal_lahir)) }}</td>
                        <td>{{ !empty($item->agamaData->params) ? $item->agamaData->params : '' }}</td>
                        <td>{{ !empty($item->pendidikanData->params) ? $item->pendidikanData->params : '' }}</td>
                        <th>{{ !empty($item->kab_kota->kab_kota) ? $item->kab_kota->kab_kota : '' }}</th>
                        <td>{{ $item->alamat_rumah }}</td>
                        <td>'{{ $item->no_hp }}</td>
                        <td>{{ !empty($item->statusUsahaData->params) ? $item->statusUsahaData->params : '' }}</td>
                        <td>{{ !empty($item->bidangUsahaKoperasiData->params) ? $item->bidangUsahaKoperasiData->params : '' }} {{ !empty($item->bidangUsahaData->params) ? $item->bidangUsahaData->params : '' }}</td>
                        <td>{{ $item->nama_koperasi }} {{ $item->nama_usaha }}</td>
                        <td>{{ $item->alamat_koperasi }} {{ $item->alamat_usaha }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <script type="text/javascript" src="{{ asset('vendors/jquery.min.js') }}"></script>
    <script src="{{ asset('vendors/bootstrap-4.4.1-dist/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/exportExcel.js') }}"></script>
    <script>
        $(document).ready(function() {
            exportTableToExcel('data', 'Peseta Lengkap')
            $(".preloader").fadeOut();
            $("#download-success").removeClass('d-none');
        })
    </script>
</body>
</html>