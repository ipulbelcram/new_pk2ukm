<div class="modal fade" id="uploadimageModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Upload & Crop Image</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div id="image_demo" style="height:55% !important"></div>
          <div class="col text-center">
            <button class="vanilla-rotate btn btn-primary mr-1" data-deg="90">
              <i class="fa fa-undo" aria-hidden="true"></i>
            </button>
            <button class="vanilla-rotate btn btn-primary mr-1" data-deg="-90">
              <i class="fa fa-undo fa-flip-horizontal" aria-hidden="true"></i>
            </button>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary crop_image">Crop & Upload Image</button>
      </div>
    </div>
  </div>
</div>