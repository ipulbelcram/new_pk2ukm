<div class="row mb-2">
    <div class="col-lg-3"></div>
    <ul class="nav">
        <li class="nav-item">
            <a class="nav-link" href="{{ route('peserta.edit', ['pelatihan_id' => $pelatihan_id, 'peserta_id' => $peserta->id]) }}"><button class="btn {{ !empty($halaman) && $halaman == 'peserta_edit' ? 'btn-success' : 'btn-outline-success' }} ">BIODATA PESERTA</button></a>
        </li>
        
        <li class="nav-item">
            <a class="nav-link" href="{{ route('peserta.pemantauan', ['pelatihan_id' => $pelatihan_id, 'peserta_id' => $peserta->id]) }}"><button class="btn {{ !empty($halaman) && $halaman == 'peserta_pemantauan' ? 'btn-success' : 'btn-outline-success' }}">PEMANTAUAN PELAKSANAAN PELATIHAN</button></a>
        </li>
    </ul>
</div>