<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKinerjaAnggaranTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kinerja_anggaran', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned()->unique();
            $table->bigInteger('pagu_anggaran')->nullable();
            $table->bigInteger('realisasi_anggaran1')->nullable();
            $table->bigInteger('realisasi_anggaran2')->nullable();
            $table->bigInteger('target_peserta')->nullable();
            $table->bigInteger('realisasi_peserta1')->nullable();
            $table->bigInteger('realisasi_peserta2')->nullable();
            $table->bigInteger('target_pendamping')->nullable();
            $table->bigInteger('realisasi_pendamping')->nullable();
            $table->timestamps();
        });

        Schema::table('kinerja_anggaran', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kinerja_anggaran');
    }
}
