<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePesertaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peserta', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('pelatihan_id')->unsigned();
            $table->string('nama');
            $table->bigInteger('no_ktp');
            $table->enum('status', ['Lajang', 'Menikah']);
            $table->enum('jenis_kelamin', ['Laki-laki', 'Perempuan']);
            $table->string('tempat_lahir');
            $table->date('tanggal_lahir');
            $table->bigInteger('agama')->unsigned();
            $table->bigInteger('pendidikan')->unsigned();
            $table->text('alamat_rumah');
            $table->bigInteger('kab_kota_id')->unsigned();
            $table->bigInteger('no_hp');
            $table->string('email');
            $table->bigInteger('status_peserta')->unsigned();
            $table->bigInteger('status_usaha')->unsigned();
            $table->string('nama_koperasi');
            $table->text('alamat_koperasi');
            $table->bigInteger('jenis_koperasi')->unsigned();
            $table->bigInteger('bidang_usaha_koperasi')->unsigned();
            $table->string('no_induk_koperasi');
            $table->bigInteger('jumlah_tenaga_kerja_koperasi');
            $table->bigInteger('omset_koperasi');
            $table->string('nama_usaha');
            $table->text('alamat_usaha');
            $table->bigInteger('bidang_usaha')->unsigned();
            $table->bigInteger('lama_usaha');
            $table->bigInteger('jumlah_tenaga_kerja_umkm');
            $table->bigInteger('omset_usaha');
            $table->timestamps();
        });

        Schema::table('peserta', function (Blueprint $table) {
            $table->foreign('pelatihan_id')->references('id')->on('pelatihan')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('agama')->references('id')->on('params')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('pendidikan')->references('id')->on('params')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('kab_kota_id')->references('id')->on('kab_kota')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('status_peserta')->references('id')->on('params')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('status_usaha')->references('id')->on('params')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('jenis_koperasi')->references('id')->on('params')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('bidang_usaha_koperasi')->references('id')->on('params')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('bidang_usaha')->references('id')->on('params')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('peserta');
    }
}
