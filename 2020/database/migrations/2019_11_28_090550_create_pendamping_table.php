<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePendampingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pendamping', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_lengkap');
            $table->string('no_ktp');
            $table->enum('status', ['lajang','menikah']);
            $table->enum('jenis_kelamin', ['Laki-laki','Perempuan']);
            $table->string('tempat_lahir');
            $table->date('tanggal_lahir');
            $table->bigInteger('agama')->unsigned();
            $table->bigInteger('pendidikan')->unsigned();
            $table->text('alamat_rumah');
            $table->bigInteger('kab_kota_id')->unsigned();
            $table->bigInteger('no_hp');
            $table->string('email');
            $table->timestamps();
        });
        Schema::table('pendamping', function (Blueprint $table) {
            $table->foreign('agama')->references('id')->on('params')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('pendidikan')->references('id')->on('params')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('kab_kota_id')->references('id')->on('kab_kota')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pendamping');
    }
}
