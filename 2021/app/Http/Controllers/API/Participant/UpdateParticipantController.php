<?php

namespace App\Http\Controllers\API\Participant;

use App\Helpers\FileHelpers;
use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;
use App\Http\Resources\Participant\ParticipantResource;
use App\Models\Param;
use App\Models\Participant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;

class UpdateParticipantController extends Controller
{
    public function __invoke(Request $request, Participant $participant)
    {
        if($request->business_status_id == 14) {
            $umkm = 'required';
            $koperasi = 'nullable';
            $entrepreneur = 'nullable';
            $umkm_koperasi = 'required';
        } else if($request->business_status_id == 15) {
            $umkm = 'nullable';
            $koperasi = 'required';
            $entrepreneur = 'nullable';
            $umkm_koperasi = 'required';
        } else {
            $umkm = 'nullable';
            $koperasi = 'nullable';
            $entrepreneur = 'required';
            $umkm_koperasi = 'nullable';
        }

        $this->validate($request, [
            // profile
            'name' => ['required', 'string'],
            'gender' => ['required', 'in:laki-laki,perempuan'],
            'religion_id' => [
                'required',
                Rule::exists('params', 'id')->where(function ($query) {
                    return $query->where('category_param', 'religion');
                })
            ],
            'id_number' => ['required', 'numeric'],
            'place_birth' => ['required', 'string'],
            'date_birth' => ['required', 'date'],
            'education_id' => [
                'required',
                Rule::exists('params', 'id')->where(function ($query) {
                    return $query->where('category_param', 'education');
                })
            ],
            'phone_number' => ['required', 'numeric'],
            'email' => ['required', 'email'],
            'address_participant' => ['required', 'string'],
            'province_id' => ['required', 'exists:provinces,id'],
            'districts_city_id' => ['required', 'exists:districts_cities,id'],
            'profile_photo' => ['nullable', 'mimes:png,jpg,jpge', 'max:2048'],

            // busines status
            'business_status_id' => [
                'required',
                Rule::exists('params', 'id')->where(function ($query) {
                    return $query->where('category_param', 'business_status');
                })
            ],

            // umkm
            'name_umkm' => [$umkm, 'string'],
            'address_umkm' => [$umkm, 'string'],
            'date_establishment_umkm' => [$umkm, 'date'],
            'nib_umkm' => ['nullable', 'string'],
            'number_employees' => [$umkm, 'numeric'],
            'asset_umkm' => [$umkm, 'numeric'],
            'omset_umkm' => [$umkm, 'numeric'],
            'npwp_umkm' => [$umkm, 'string'],
            'business_field_umkm_id' => [
                $umkm, 
                Rule::exists('params', 'id')->where(function ($query) {
                    return $query->where('category_param', 'business_field');
                })
            ],
            'production_capacity' => [$umkm, 'string'],
            'member_koperasi' => [$umkm, 'boolean'],
            'banking_credit' => [$umkm, 'boolean'],
            'savings' => [$umkm, 'boolean'],
            'export' => ['nullable', 'in:pengiriman forwarder,pengiriman langsung'],
            'export_destination' => [
                Rule::requiredIf(!empty($request->export)),
                'string'
            ],
            'export_value' => [
                Rule::requiredIf(!empty($request->export)),
                'string'
            ],
            'export_volume' => [
                Rule::requiredIf(!empty($request->export)),
                'string'
            ],
            'product_supply' => ['nullable', 'boolean'],

            // koperasi
            'name_koperasi' => [$koperasi, 'string'],
            'address_koperasi' => [$koperasi, 'string'],
            'type_koperasi_id' => [
                $koperasi, 
                Rule::exists('params', 'id')->where(function ($query) {
                    return $query->where('category_param', 'type_koperasi');
                })
            ],
            'date_establishment_koperasi' => [$koperasi, 'date'],
            'registrasion_number_koperasi' => [$koperasi, 'string'],
            'position_koperasi_id' => [
                $koperasi,
                Rule::exists('params', 'id')->where(function ($query) {
                    return $query->where('category_param', 'position_koperasi');
                })
            ],
            'npwp_koperasi' => [$koperasi, 'string'],
            'legal_entity_number' => [$koperasi, 'string'],
            'last_rat' => [$koperasi, 'numeric'],
            'nik_status' => [$koperasi, 'in:sudah bersertifikat,belum bersertifikat'],
            'form_koperasi_id' => [
                $koperasi, 
                Rule::exists('params', 'id')->where(function ($query) {
                    return $query->where('category_param', 'form_koperasi');
                }),
            ],
            'asset_koperasi' => [$koperasi, 'numeric'],
            'omset_koperasi' => [$koperasi, 'numeric'],
            'shu_koperasi' => [$koperasi, 'numeric'],
            'total_member_koperasi_men' => [$koperasi, 'numeric'],
            'total_member_koperasi_women' => [$koperasi, 'numeric'],
            'total_employee_koperasi_men' => [$koperasi, 'numeric'],
            'total_employee_koperasi_women' => [$koperasi, 'numeric'],
            'group_koperasi_id' => [
                $koperasi, 
                Rule::exists('params', 'id')->where(function ($query) {
                    return $query->where('category_param', 'group_koperasi');
                }),
            ],
            'fostered_koperasi_id' => [
                $koperasi, 
                Rule::exists('params', 'id')->where(function ($query) {
                    return $query->where('category_param', 'fostered_koperasi');
                }),
            ],

            // umkm dan koperasi
            'business_email_validation' => [$umkm_koperasi, 'in:yes,no'],
            'business_email' => [
                Rule::requiredIf($request->business_email_validation == 'yes'),
                'email'
            ],
            'business_website_validation' => [$umkm_koperasi, 'in:yes,no'],
            'business_website' => [
                Rule::requiredIf($request->business_website_validation == 'yes'),
                'string'
            ],
            'partnership' => [$umkm_koperasi, 'boolean'],
            'social_media_id' => [
                $umkm_koperasi, 
                Rule::exists('params', 'id')->where(function ($query) {
                    return $query->where('category_param', 'social_media');
                }),
                'array'
            ],
            'marketplace_id' => [
                $umkm_koperasi, 
                Rule::exists('params', 'id')->where(function ($query) {
                    return $query->where('category_param', 'marketplace');
                }),
                'array'
            ],
            'business_licensing_id' => [
                $umkm_koperasi, 
                Rule::exists('params', 'id')->where(function ($query) {
                    return $query->where('category_param', 'business_licensing');
                }),
                'array'
            ],
            'business_certificate_id' => [
                $umkm_koperasi, 
                Rule::exists('params', 'id')->where(function ($query) {
                    return $query->where('category_param', 'business_certificate');
                }),
                'array'
            ],
            'marketing_reach_id' => [
                $umkm_koperasi,
                Rule::exists('params', 'id')->where(function ($query) {
                    return $query->where('category_param', 'marketing_reach');
                })
            ],
            'marketing_reach_optional' => [
                Rule::requiredIf(function () use ($request) {
                    $marketing_reach_id = Param::find($request->marketing_reach_id);
                    if($marketing_reach_id) {
                        if($request->business_status_id == 14) {
                            return $marketing_reach_id->optional == 1 && $marketing_reach_id->param != 'Ekspor';
                        } else {
                            return $marketing_reach_id->optional == 1;
                        }
                    }
                }),
                'string'
            ],
        
            // umkm, koperasi, dan entrepreneur
            'business_sector_id' => [
                'required',
                Rule::exists('params', 'id')->where(function ($query) {
                    return $query->where('category_param', 'business_sector');
                })
            ],

            // entrepreneur
            'business_idea' => [$entrepreneur, 'string'],
            'entrepreneur_address' => [$entrepreneur, 'string'],
            'business_field_entrepreneur_id' => [
                $entrepreneur, 
                Rule::exists('params', 'id')->where(function ($query) {
                    return $query->where('category_param', 'business_field');
                })
            ],
            'accompaniment_entrepreneur_id' => [
                $entrepreneur, 
                Rule::exists('params', 'id')->where(function ($query) {
                    return $query->where('category_param', 'accompaniment');
                })
            ],

            // Monitoring
            'm1' => [
                'required', 
                Rule::exists('params', 'id')->where(function ($query) {
                    return $query->where('category_param', 'participant_problem');
                }) 
            ],
            'm2_id' => [
                'required',
                Rule::exists('params', 'id')->where(function ($query) {
                    return $query->where('category_param', 'training_needs');
                }) 
            ],
            'm3' => ['required', 'string'],
            'other_m1' => [
                Rule::requiredIf(function () use ($request) {
                    $m1 = Param::find($request->m1);
                    if($m1) {
                        return $m1->optional == 1;
                    }
                }), 
                'string'
            ],
        ]);

        $cek_id_number = Participant::where([['id', '!=', $participant->id], ['training_id', $participant->training_id], ['id_number', $request->id_number]])->count();
        if($cek_id_number > 0) {
            return ResponseFormatter::error([
                "id number is already exists in this training"
            ], 'create participant failed', 422);
        }

        $input = $request->all();
        if($request->business_status_id == 14) {
            $input['name_koperasi'] = null;
            $input['address_koperasi'] = null;
            $input['type_koperasi_id'] = null;
            $input['date_establishment_koperasi'] = null;
            $input['registrasion_number_koperasi'] = null;
            $input['position_koperasi_id'] = null;
            $input['npwp_koperasi'] = null;
            $input['legal_entity_number'] = null;
            $input['last_rat'] = null;
            $input['nik_status'] = null;
            $input['form_kope rasi_id'] = null;
            $input['asset_koperasi'] = null;
            $input['omset_koperasi'] = null;
            $input['shu_koperasi'] = null;
            $input['total_member_koperasi_men'] = null;
            $input['total_member_koperasi_women'] = null;
            $input['total_employee_koperasi_men'] = null;
            $input['total_employee_koperasi_women'] = null;
            $input['group_koperasi_id'] = null;
            $input['fostered_koperasi_id'] = null;

            $input['business_idea'] = null;
            $input['entrepreneur_address'] = null;
            $input['business_field_entrepreneur_id'] = null;
            $input['accompaniment_entrepreneur_id'] = null;
            
        } else if($request->business_status_id == 15) {
            $input['name_umkm'] = null;
            $input['address_umkm'] = null;
            $input['date_establishment_umkm'] = null;
            $input['nib_umkm'] = null;
            $input['number_employees'] = null;
            $input['asset_umkm'] = null;
            $input['omset_umkm'] = null;
            $input['npwp_umkm'] = null;
            $input['business_field_umkm_id'] = null;
            $input['production_capacity'] = null;
            $input['member_koperasi'] = null;
            $input['banking_credit'] = null;
            $input['savings'] = null;
            $input['export'] = null;
            $input['export_destination'] = null;
            $input['export_value'] = null;
            $input['export_volume'] = null;
            $input['product_supply'] = null;

            $input['business_idea'] = null;
            $input['entrepreneur_address'] = null;
            $input['business_field_entrepreneur_id'] = null;
            $input['accompaniment_entrepreneur_id'] = null;
        } else {
            $input['name_koperasi'] = null;
            $input['address_koperasi'] = null;
            $input['type_koperasi_id'] = null;
            $input['date_establishment_koperasi'] = null;
            $input['registrasion_number_koperasi'] = null;
            $input['position_koperasi_id'] = null;
            $input['npwp_koperasi'] = null;
            $input['legal_entity_number'] = null;
            $input['last_rat'] = null;
            $input['nik_status'] = null;
            $input['form_koperasi_id'] = null;
            $input['asset_koperasi'] = null;
            $input['omset_koperasi'] = null;
            $input['shu_koperasi'] = null;
            $input['total_member_koperasi_men'] = null;
            $input['total_member_koperasi_women'] = null;
            $input['total_employee_koperasi_men'] = null;
            $input['total_employee_koperasi_women'] = null;
            $input['group_koperasi_id'] = null;
            $input['fostered_koperasi_id'] = null;
            
            $input['name_umkm'] = null;
            $input['address_umkm'] = null;
            $input['date_establishment_umkm'] = null;
            $input['nib_umkm'] = null;
            $input['number_employees'] = null;
            $input['asset_umkm'] = null;
            $input['omset_umkm'] = null;
            $input['npwp_umkm'] = null;
            $input['business_field_umkm_id'] = null;
            $input['production_capacity'] = null;
            $input['member_koperasi'] = null;
            $input['banking_credit'] = null;
            $input['savings'] = null;
            $input['export'] = null;
            $input['export_destination'] = null;
            $input['export_value'] = null;
            $input['export_volume'] = null;
            $input['product_supply'] = null;

            $input['business_email'] = null;
            $input['business_website'] = null;
            $input['partnership'] = null;
            $input['social_media_id'] = null;
            $input['marketplace_id'] = null;
            $input['business_licensing_id'] = null;
            $input['business_certificate_id'] = null;
            $input['marketing_reach_id'] = null;
        }
        $marketing_reach_id = Param::find($request->marketing_reach_id);
        if($marketing_reach_id) {
            ($marketing_reach_id->optional == 1 ?: $input['marketing_reach_optional'] = null);
        }

        if($request->file('profile_photo')) {
            $path = FileHelpers::upload_file('participant_photo', $request->file('profile_photo'));
            $input['profile_photo'] = $path;
            if($participant->profile_photo) {
                Storage::disk('public')->delete($participant->profile_photo);
            }
        }

        $participant->update($input);
        if($request->business_status_id == 14 || $request->business_status_id == 15) {
            $participant->participant_choices()->delete();
            $choice_data = [];
            foreach($request->social_media_id as $social_media) {
                $social_media_data = [
                    'type' => 'social_media',
                    'choice_id' => $social_media,
                ];
                $choice_data[] = $social_media_data;
            }
            foreach($request->marketplace_id as $marketplace) {
                $marketplace_data = [
                    'type' => 'marketplace',
                    'choice_id' => $marketplace,
                ];
                $choice_data[] = $marketplace_data;
            }
            foreach($request->business_licensing_id as $business_licensing) {
                $business_licensing_data = [
                    'type' => 'business_licensing',
                    'choice_id' => $business_licensing,
                ];
                $choice_data[] = $business_licensing_data;
            }
            foreach($request->business_certificate_id as $business_certificate) {
                $business_certificate_data = [
                    'type' => 'business_certificate',
                    'choice_id' => $business_certificate,
                ];
                $choice_data[] = $business_certificate_data;
            }
            $participant->participant_choices()->createMany($choice_data);
        }

        $participant->monitoring()->update([
            'm1' => $request->m1,
            'm2_id' => $request->m2_id,
            'm3' => $request->m3,
            'other_m1' => (!empty($request->other_m1)) ? $request->other_m1 : null,
        ]);

        return ResponseFormatter::success(
            new ParticipantResource($participant),
            'success update participant'
        );
    }
}
