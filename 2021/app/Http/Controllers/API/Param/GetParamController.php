<?php

namespace App\Http\Controllers\API\Param;

use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;
use App\Http\Resources\DistrictsCity\DistrictsCityResource;
use App\Http\Resources\Param\ParamResource;
use App\Models\DistrictsCity;
use App\Models\Param;
use App\Models\Province;

class GetParamController extends Controller
{
    public function province() 
    {
        $province = Province::all();
        return ResponseFormatter::success(
            $province,
            'success get data province'
        );
    }
    
    public function districts_city($province_id)
    {
        $districts_city = DistrictsCity::where('province_id', $province_id)->get();
        return ResponseFormatter::success(
            DistrictsCityResource::collection($districts_city),
            'success get data districts city'
        );

    }
    public function education()
    {
        $education = Param::where([['category_param', 'education'], ['active', 1]])->orderBy('order', 'asc')->get();
        return ResponseFormatter::success(
            ParamResource::collection($education),
            'success get education data'
        );
    }

    public function religion()
    {
        $religion = Param::where([['category_param', 'religion'], ['active', 1]])->orderBy('order', 'asc')->get();
        return ResponseFormatter::success(
            ParamResource::collection($religion),
            'success get religion data'
        );
    }

    public function business_status()
    {
        $business_status = Param::where([['category_param', 'business_status'], ['active', 1]])->orderBy('order', 'asc')->get();
        return ResponseFormatter::success(
            ParamResource::collection($business_status),
            'success get business status data'
        );
    }

    public function business_sector()
    {
        $business_sector = Param::where([['category_param', 'business_sector'], ['active', 1]])->orderBy('order', 'asc')->get();
        return ResponseFormatter::success(
            ParamResource::collection($business_sector),
            'success get business sector data'
        );
    }

    public function type_koperasi()
    {
        $type_koperasi = Param::where([['category_param', 'type_koperasi'], ['active', 1]])->orderBy('order', 'asc')->get();
        return ResponseFormatter::success(
            ParamResource::collection($type_koperasi),
            'success get type koperasi data'
        );
    }

    public function position_koperasi()
    {
        $position_koperasi = Param::where([['category_param', 'position_koperasi'], ['active', 1]])->orderBy('order', 'asc')->get();
        return ResponseFormatter::success(
            ParamResource::collection($position_koperasi),
            'success get position koperasi data'
        );
    }

    public function training_type()
    {
        $training_type = Param::where([['category_param', 'training_type'], ['active', 1]])->orderBy('order', 'asc')->get();
        return ResponseFormatter::success(
            ParamResource::collection($training_type),
            'success get training type data'
        );
    }

    public function expertise_companion()
    {
        $expertise_companion = Param::where([['category_param', 'expertise_companion'], ['active', 1]])->orderBy('order', 'asc')->get();
        return ResponseFormatter::success(
            ParamResource::collection($expertise_companion),
            'success get expertise companion type data'
        );
    }

    public function business_field()
    {
        $business_field = Param::where([['category_param', 'business_field'], ['active', 1]])->orderBy('order', 'asc')->get();
        return ResponseFormatter::success(
            ParamResource::collection($business_field),
            'success get bussines field data'
        );
    }

    public function social_media()
    {
        $social_media = Param::where([['category_param', 'social_media'], ['active', 1]])->orderBy('order', 'asc')->get();
        return ResponseFormatter::success(
            ParamResource::collection($social_media),
            'success get social media data'
        );
    }

    public function marketplace()
    {
        $marketplace = Param::where([['category_param', 'marketplace'], ['active', 1]])->orderBy('order', 'asc')->get();
        return ResponseFormatter::success(
            ParamResource::collection($marketplace),
            'success get marketplace data'
        );
    }

    public function business_licensing()
    {
        $business_licensing = Param::where([['category_param', 'business_licensing'], ['active', 1]])->orderBy('order', 'asc')->get();
        return ResponseFormatter::success(
            ParamResource::collection($business_licensing),
            'success get business licensing data'
        );
    }

    public function business_certificate()
    {
        $business_certificate = Param::where([['category_param', 'business_certificate'], ['active', 1]])->orderBy('order', 'asc')->get();
        return ResponseFormatter::success(
            ParamResource::collection($business_certificate),
            'success get business certificate data'
        );
    }

    public function marketing_reach()
    {
        $marketing_reach = Param::where([['category_param', 'marketing_reach'], ['active', 1]])->orderBy('order', 'asc')->get();
        return ResponseFormatter::success(
            ParamResource::collection($marketing_reach),
            'success get marketing reach data'
        );
    }
    
    public function accompaniment()
    {
        $accompaniment = Param::where([['category_param', 'accompaniment'], ['active', 1]])->orderBy('order', 'asc')->get();
        return ResponseFormatter::success(
            ParamResource::collection($accompaniment),
            'success get accompaniment data'
        );
    }

    public function participant_problem()
    {
        $participant_problem = Param::where([['category_param', 'participant_problem'], ['active', 1]])->orderBy('order', 'asc')->get();
        return ResponseFormatter::success(
            ParamResource::collection($participant_problem),
            'success get training type data'
        );
    }

    public function training_needs()
    {
        $training_needs = Param::where([['category_param', 'training_needs'], ['active', 1]])->orderBy('order', 'asc')->get();
        return ResponseFormatter::success(
            ParamResource::collection($training_needs),
            'success get training needs data'
        );
    }

    public function form_koperasi()
    {
        $form_koperasi = Param::where([['category_param', 'form_koperasi'], ['active', 1]])->orderBy('order', 'asc')->get();
        return ResponseFormatter::success(
            ParamResource::collection($form_koperasi),
            'success get form koperasi data'
        );
    }

    public function group_koperasi()
    {
        $group_koperasi = Param::where([['category_param', 'group_koperasi'], ['active', 1]])->orderBy('order', 'asc')->get();
        return ResponseFormatter::success(
            ParamResource::collection($group_koperasi),
            'success get group koperasi data'
        );
    }

    public function fostered_koperasi()
    {
        $fostered_koperasi = Param::where([['category_param', 'fostered_koperasi'], ['active', 1]])->orderBy('order', 'asc')->get();
        return ResponseFormatter::success(
            ParamResource::collection($fostered_koperasi),
            'success get fostered koperasi data'
        );
    }
    
}
