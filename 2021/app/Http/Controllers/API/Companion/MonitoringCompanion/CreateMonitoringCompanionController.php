<?php

namespace App\Http\Controllers\API\Companion\MonitoringCompanion;

use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;
use App\Http\Resources\Companion\MonitoringCompanionResource;
use App\Models\MonitoringCompanion;
use Illuminate\Http\Request;

class CreateMonitoringCompanionController extends Controller
{
    public function __invoke(Request $request)
    {
        $this->validate($request, [
            'companion_id' => ['required', 'exists:companions,id'],
            'month' => ['required', 'numeric', 'between:1,12'],
            'date' => ['required', 'date'],
            'activity_details' => ['required', 'string'],
            'output_koperasi' => ['required', 'numeric'],
            'output_umkm' => ['required', 'numeric'],
        ]);


        $input = $request->all();
        $monitoring_companion_data = MonitoringCompanion::where([['companion_id', $request->companion_id], ['month', $request->month]]);
        if($monitoring_companion_data->count() > 0) {
            $monitoring_companion = $monitoring_companion_data->first();
            $monitoring_companion->update($input);
            return ResponseFormatter::success(
                new MonitoringCompanionResource($monitoring_companion),
                'success update monitoring companion data'
            );
        } else {
            $monitoring_companion = MonitoringCompanion::create($input);
            return ResponseFormatter::success(
                new MonitoringCompanionResource($monitoring_companion),
                'success create monitoring companion data'
            );
        }
    }
}
