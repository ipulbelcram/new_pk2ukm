<?php

namespace App\Http\Controllers\API\Companion\MonitoringCompanion;

use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;
use App\Models\MonitoringCompanion;
use Illuminate\Http\Request;

class DeleteMonitoringCompanionController extends Controller
{
    public function __invoke(MonitoringCompanion $monitoring_companion)
    {
        $result = $monitoring_companion->delete();
        return ResponseFormatter::success(
            $result,
            'success delete monitoring companion data'
        );
    }
}
