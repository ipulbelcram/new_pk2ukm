<?php

namespace App\Http\Controllers\API\Companion\MonitoringCompanion;

use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;
use App\Http\Resources\Companion\MonitoringCompanionResource;
use App\Models\Companion;
use App\Models\MonitoringCompanion;
use Exception;
use Illuminate\Http\Request;

class GetMonitoringCompanionController extends Controller
{
    public function fetch(Companion $companion, $month = null)
    {
        if($month) {
            $monitoring_companion = $companion->monitoring_companion()->where('month', $month)->first();
            if($monitoring_companion) {
                return ResponseFormatter::success(
                    new MonitoringCompanionResource($monitoring_companion),
                    'success get monitoring companion data'
                );
            }
        } else {
            $monitoring_companion = MonitoringCompanion::where('companion_id', $companion->id)->get();
            return ResponseFormatter::success(
                MonitoringCompanionResource::collection($monitoring_companion),
                'success get monitoring companion data'
            );
        }
    }

    public function validation_by_month(Companion $companion)
    {
        try {
            for($i = 1; $i <= 12; $i++) {
                $cek = $companion->monitoring_companion()->where('month', $i)->count();
                $validation = ($cek > 0) ? true : false;
                $results[] =  [
                    'month' => $i,
                    'validation' => $validation
                ];
            }
    
            return ResponseFormatter::success(
                $results,
                'success get validation monitoring companion by month'
            );
        } catch (Exception $e) {
            return ResponseFormatter::error([
                'message' => $e->getMessage(),
            ], 'error get validation monitoring companion by mont', 500);
        }
    }
}
