<?php

namespace App\Http\Resources\Companion;

use Illuminate\Http\Resources\Json\JsonResource;

class MonitoringCompanionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'companion_id' => $this->companion_id,
            'month' => $this->month,
            'date' => $this->date,
            'activity_details' => $this->activity_details,
            'output_koperasi' => $this->output_koperasi,
            'output_umkm' => $this->output_umkm,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
