<?php

namespace App\Http\Resources\Participant;

use App\Http\Resources\Monitoring\MonitoringResource;
use App\Http\Resources\Param\ParamResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ParticipantResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'training_id' => $this->training_id,
            'name' => $this->name,
            'gender' => $this->gender,
            'religion_id' => new ParamResource($this->religion),
            'id_number' => $this->id_number,
            'place_birth' => $this->place_birth,
            'date_birth' => $this->date_birth,
            'education' => new ParamResource($this->education),
            'phone_number' => $this->phone_number,
            'email' => $this->email,
            'address_participant' => $this->address_participant,
            'province' => $this->province,
            'districts_city' => $this->districts_city,
            'profile_photo' => $this->profile_photo_url,
            'unique_code' => $this->unique_code,

            // Business Status
            'business_status' => new ParamResource($this->business_status),

            // UMKM
            'name_umkm' => $this->name_umkm,
            'address_umkm' => $this->address_umkm,
            'date_establishment_umkm' => $this->date_establishment_umkm,
            'nib_umkm' => $this->nib_umkm,
            'number_employees' => $this->number_employees,
            'asset_umkm' => $this->asset_umkm,
            'omset_umkm' => $this->omset_umkm,
            'npwp_umkm' => $this->npwp_umkm,
            'business_field_umkm' => $this->business_field_umkm,
            'production_capacity' => $this->production_capacity,
            'member_koperasi' => ($this->member_koperasi == "1" ? 'yes' : ($this->member_koperasi == "0" ? 'no' : null)),
            'banking_credit' => ($this->banking_credit == "1" ? 'yes' : ($this->banking_credit == "0" ? 'no' : null)),
            'savings' => ($this->savings == "1" ? 'yes' : ($this->savings == "0" ? 'no' : null)),
            'export' => (empty($this->export) ? 'no' : $this->export),
            'export_destination' => $this->export_destination,
            'export_value' => $this->export_value,
            'export_volume' => $this->export_volume,
            'product_supply' => ($this->product_supply == "1" ? 'yes' : ($this->product_supply == "0" ? 'no' : null)),

            // Koperasi
            'name_koperasi' => $this->name_koperasi,
            'address_koperasi' => $this->address_koperasi,
            'type_koperasi' =>  new ParamResource($this->type_koperasi),
            'date_establishment_koperasi' => $this->date_establishment_koperasi,
            'registrasion_number_koperasi' => $this->registrasion_number_koperasi,
            'position_koperasi' => new ParamResource($this->position_koperasi),
            'npwp_koperasi' => $this->npwp_koperasi,
            'legal_entity_number' => $this->legal_entity_number,
            'last_rat' => $this->last_rat,
            'nik_status' => $this->nik_status,
            'form_koperasi' => $this->form_koperasi,
            'asset_koperasi' => $this->asset_koperasi,
            'omset_koperasi' => $this->omset_koperasi,
            'shu_koperasi' => $this->shu_koperasi,
            'total_member_koperasi_men' => $this->total_member_koperasi_men,
            'total_member_koperasi_women' => $this->total_member_koperasi_women,
            'total_employee_koperasi_men' => $this->total_employee_koperasi_men,
            'total_employee_koperasi_women' => $this->total_employee_koperasi_women,
            'group_koperasi' => new ParamResource($this->group_koperasi),
            'fostered_koperasi' => new ParamResource($this->fostered_koperasi),

            // UMKM dan Koperasi
            'business_email' => $this->business_email,
            'business_website' => $this->business_website,
            'partnership' => ($this->partnership == "1" ? 'yes' : ($this->partnership == "0" ? 'no' : null)),
            'social_media' => ParticipantChoiceResource::collection($this->participant_choices()->where('type', 'social_media')->get()),
            'marketplace' => ParticipantChoiceResource::collection($this->participant_choices()->where('type', 'marketplace')->get()),
            'business_licensing' => ParticipantChoiceResource::collection($this->participant_choices()->where('type', 'business_licensing')->get()),
            'business_certificate' => ParticipantChoiceResource::collection($this->participant_choices()->where('type', 'business_certificate')->get()),
            'marketing_reach' => $this->marketing_reach,
            'marketing_reach_optional' => $this->marketing_reach_optional,

            // UMKM dan Koperasi Entrepreneur
            'business_sector' => new ParamResource($this->business_sector),
            
            // Entrepreneur
            'business_idea' => $this->business_idea,
            'entrepreneur_address' => $this->entrepreneur_address,
            'business_field_entrepreneur' => new ParamResource($this->business_field_entrepreneur),
            'accompaniment_entrepreneur' => new ParamResource($this->accompaniment_entrepreneur),

            // Monitoring
            'monitoring' => new MonitoringResource($this->monitoring),
            
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
