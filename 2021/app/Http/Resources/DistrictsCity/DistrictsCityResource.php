<?php

namespace App\Http\Resources\DistrictsCity;

use Illuminate\Http\Resources\Json\JsonResource;

class DistrictsCityResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'province_id' => $this->province_id,
            'districts_city' => $this->districts_city,
        ];
    }
}
