<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Storage;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Str;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'username',
        'email',
        'province_id',
        'districts_city_id',
        'order_province',
        'order_city',
        'role_id',
        'profile_photo_path',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends = [
        'profile_photo_url',
    ];

    public function getProfilePhotoUrlAttribute()
    {
        if($this->attributes['profile_photo_path']) {
            $url = url('') . Storage::url($this->attributes['profile_photo_path']);
        } else {
            $name = Str::slug($this->attributes['name'], '+');
            $url = "https://ui-avatars.com/api/?name=" . $name . "&color=ffffff&background=random";
        }
        return $url;
    }

    public function  training()
    {
        return $this->hasMany(Training::class, 'user_id');
    }

    public function skpd()
    {
        return $this->hasOne(Skpd::class, 'user_id', 'id');
    }

    public function budged_performance()
    {
        return $this->hasOne(BudgedPerformance::class, 'user_id');
    }

    public function  companion()
    {
        return $this->hasOne(Companion::class, 'user_id');
    }

    public function companion_parent()
    {
        return $this->hasMany(Companion::class, 'parent_user_id');
    }

    public function province()
    {
        return $this->belongsTo(Province::class, 'province_id');
    }

    public function districts_city()
    {
        return $this->belongsTo(DistrictsCity::class, 'districts_city_id');
    }
}
