<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Participant extends Model
{
    protected $table = 'participants';
    protected $fillable = [
        'training_id',

        // general data participant
        'name',
        'gender',
        'religion_id',
        'id_number',
        'place_birth',
        'date_birth',
        'education_id',
        'phone_number', 
        'email',
        'address_participant',
        'province_id',
        'districts_city_id',
        'profile_photo',
        'unique_code',

        // participant status
        'business_status_id',

        // Data UMKMM
        'name_umkm', 
        'address_umkm',
        'date_establishment_umkm',
        'nib_umkm',
        'number_employees',
        'asset_umkm',
        'omset_umkm',
        'npwp_umkm',
        'business_field_umkm_id',
        'production_capacity',
        'member_koperasi',
        'banking_credit',
        'savings',
        'export',
        'export_destination',
        'export_value',
        'export_volume',
        'product_supply',

        // data koperasi
        'name_koperasi',
        'address_koperasi',
        'type_koperasi_id',
        'date_establishment_koperasi',
        'registrasion_number_koperasi',
        'position_koperasi_id',
        'npwp_koperasi',
        'legal_entity_number',
        'last_rat',
        'nik_status',
        'form_koperasi_id',
        'asset_koperasi',
        'omset_koperasi',
        'shu_koperasi',
        'total_member_koperasi_men',
        'total_member_koperasi_women',
        'total_employee_koperasi_men',
        'total_employee_koperasi_women',
        'group_koperasi_id',
        'fostered_koperasi_id',
        
        // ada di koperasi dan UMKM
        'business_sector_id',
        'business_email',
        'business_website',
        'partnership',
        'marketing_reach_id',
        'marketing_reach_optional',

        // data entrepreneur
        'business_idea',
        'entrepreneur_address',
        'business_field_entrepreneur_id',
        'accompaniment_entrepreneur_id',
    ]; 


    protected $appends = [
        'profile_photo_url'
    ];

    public function getProfilePhotoUrlAttribute()
    {
        return url('') . Storage::url($this->attributes['profile_photo']);
    }

    public function training ()
    {
        return $this->belongsTo(Training::class, 'training_id');
    }

    public function monitoring()
    {
        return $this->hasOne(Monitoring::class, 'participant_id');
    }
    public function religion()
    {
        return $this->belongsTo(Param::class, 'religion_id');
    }

    public function education()
    {
        return $this->belongsTo(Param::class, 'education_id');
    }

    public function province()
    {
        return $this->belongsTo(Province::class, 'province_id');
    }

    public function districts_city()
    {
        return $this->belongsTo(DistrictsCity::class, 'districts_city_id');
    }

    public function business_status()
    {
        return $this->belongsTo(Param::class, 'business_status_id');
    }

    public function type_koperasi()
    {
        return $this->belongsTo(Param::class, 'type_koperasi_id');
    }

    public function position_koperasi()
    {
        return $this->belongsTo(Param::class, 'position_koperasi_id');
    }

    public function business_sector()
    {
        return $this->belongsTo(Param::class, 'business_sector_id');
    }

    public function business_field_umkm()
    {
        return $this->belongsTo(Param::class, 'business_field_umkm_id');
    }

    public function marketing_reach()
    {
        return $this->belongsTo(Param::class, 'marketing_reach_id');
    }

    public function business_field_entrepreneur()
    {
        return $this->belongsTo(Param::class, 'business_field_entrepreneur_id');
    }

    public function accompaniment_entrepreneur()
    {
        return $this->belongsTo(Param::class, 'accompaniment_entrepreneur_id');
    }

    public function form_koperasi()
    {
        return $this->belongsTo(Param::class, 'form_koperasi_id');
    }

    public function participant_choices()
    {
        return $this->hasMany(ParticipantChoice::class, 'participant_id');
    }

    public function group_koperasi()
    {
        return $this->belongsTo(Param::class, 'group_koperasi_id');
    }
    
    public function fostered_koperasi()
    {
        return $this->belongsTo(Param::class, 'fostered_koperasi_id');
    }
}
