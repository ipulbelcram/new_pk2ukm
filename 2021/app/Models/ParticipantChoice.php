<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ParticipantChoice extends Model
{
    protected $table = 'participant_choices';
    protected $fillable = [
        'participant_id',
        'type',
        'choice_id',
    ];

    public $timestamps = false;

    public function choice()
    {
        return $this->belongsTo(Param::class, 'choice_id');
    }
}
