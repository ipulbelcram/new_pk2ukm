<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $table = 'provinces';
    protected $fillable = [
        'id',
        'province'
    ];

    public function  training()
    {
        return $this->hasMany(Training::class, 'province_id');
    }
}
