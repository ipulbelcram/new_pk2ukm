<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MonitoringCompanion extends Model
{
    protected $table = 'monitoring_companions';

    protected $fillable = [
        'companion_id',
        'month',
        'date',
        'activity_details',
        'output_koperasi',
        'output_umkm'
    ];

    public function compion()
    {
        return $this->belongsTo(Companion::class, 'companion_id');
    }
}
