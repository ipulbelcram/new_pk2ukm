<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DistrictsCity extends Model
{
    protected $table = 'districts_cities';
    protected $fillable = [
        'province_id',
        'districts_city',
        'type',
        'state_capital',
    ];

    public function  training()
    {
        return $this->hasMany(Training::class, 'districts_city_id');
    }
}
