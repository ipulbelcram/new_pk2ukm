<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('API')->group(function () {
    Route::namespace('Auth')->group(function () {
        Route::post('login', 'LoginController');
    });

    Route::namespace('Training')->group(function () {
        Route::prefix('training')->group(function () {
            Route::get('{training:id}/by_id', 'GetTrainingController@by_id');
        });
    });

    Route::middleware('auth:sanctum')->group(function () {
        Route::namespace('Auth')->group(function () {
            Route::post('logout', 'LogoutController');
            Route::get('user', 'UserController');
            Route::post('password_reset', 'PasswordResetController');
        });

        Route::namespace('User')->group(function () {
            Route::prefix('user')->group(function () {
                Route::get('/', 'GetUserController@get');
                Route::post('{user:id}/update_photo', 'UserController@update_photo');
            });
        });

        Route::namespace('Training')->group(function () {
            Route::prefix('training')->group(function () {
                Route::get('all/{user_id?}', 'GetTrainingController@all');
                Route::get('with_user/{user_id?}', 'GetTrainingController@with_user');
                Route::post('create', 'CreateTrainingController');
                Route::patch('{training:id}/update', 'UpdateTrainingController@training');
                Route::patch('{training:id}/update_status', 'UpdateTrainingController@status');
                Route::delete('{training:id}/delete', 'DeleteTrainingController');
            });
        });

        Route::namespace('Skpd')->group(function () {
            Route::prefix('skpd')->group(function () {
                Route::get('{user_id?}', 'GetSkpdController@get');
                Route::get('{province_id}/get_by_province', 'GetSkpdController@get_by_province');
                Route::post('create', 'CreateSkpdController');
            });
        });

        Route::namespace('BudgedPerformance')->group(function () {
            Route::prefix('budged_performance')->group(function () {
                Route::get('{user:id}/get', 'GetBudgedPerformanceController@fetch');
                Route::get('{province_id}/by_province', 'GetBudgedPerformanceController@all_data');
                Route::get('all_budged_performance', 'GetBudgedPerformanceController@all_data');
                Route::get('persentase_budged_realization', 'GetBudgedPerformanceController@persentase_budged_realization');
                Route::get('budged_performance_by_province', 'GetBudgedPerformanceController@budged_performance_by_province');
                Route::post('update', 'UpdateBudgedPerformanceController');
            });
        });

        Route::namespace('Participant')->group(function () {
            Route::prefix('participant')->group(function () {
                Route::get('{participant:id}/by_id', 'GetParticipantController@by_id');
                Route::get('{training:id}/by_training', 'GetParticipantController@by_training');
                Route::post('{participant:id}/update', 'UpdateParticipantController');
                Route::delete('{participant:id}/delete', 'DeleteParticipantController');
            });
        });

        Route::namespace('Companion')->group(function () {
            Route::prefix('companion')->group(function () {
                Route::get('/', 'GetCompanionController');
                Route::post('/', 'CreateCompanionController');
                Route::post('{user:id}/update', 'UpdateCompanionController');
                Route::delete('{user_id}/delete', 'DeleteCompanionController');
            });

            Route::namespace('MonitoringCompanion')->group(function () {
                Route::prefix('monitoring_companion')->group(function () {
                    Route::get('/fetch/{companion:id}/{month?}', 'GetMonitoringCompanionController@fetch');
                    Route::get('/validation_by_month/{companion:id}', 'GetMonitoringCompanionController@validation_by_month');
                    Route::post('/create', 'CreateMonitoringCompanionController');
                    Route::delete('/{monitoring_companion:id}/delete', 'DeleteMonitoringCompanionController');
                });
            });
        });

        Route::namespace('Report')->group(function () {
            Route::prefix('report')->group(function () {
                Route::get('total_participant_by_gender', 'GetReportController@total_participant_by_gender');
                Route::get('total_participant_by_religion', 'GetReportController@total_participant_by_religion');
                Route::get('total_participant_by_training_type', 'GetReportController@total_participant_by_training_type');
                Route::get('total_participant_by_education', 'GetReportController@total_participant_by_education');
                Route::get('total_participant_by_business_status', 'GetReportController@total_participant_by_business_status');
                Route::get('total_user_by_province', 'GetReportController@total_user_by_province');
                Route::get('total_training_by_province', 'GetReportController@total_training_by_province');
                Route::get('total_companion_by_province', 'GetReportController@total_companion_by_province');
                Route::get('total_participant_problem', 'GetReportController@total_participant_problem');
                Route::get('klasifikasi_asset_umkm', 'GetReportController@klasifikasi_asset_umkm');
                Route::get('klasifikasi_omset_umkm', 'GetReportController@klasifikasi_omset_umkm');
            });
        });
    });

    Route::namespace('Participant')->group(function () {
        Route::prefix('participant')->group(function () {
            Route::post('{training:id}/create', 'CreateParticipantController');
        });
    });

    Route::namespace('Param')->group(function () {
        Route::prefix('param')->group(function () {
            Route::get('province', 'GetParamController@province');
            Route::get('districts_city/{province_id}', 'GetParamController@districts_city');
            Route::get('education', 'GetParamController@education');
            Route::get('religion', 'GetParamController@religion');
            Route::get('business_status', 'GetParamController@business_status');
            Route::get('business_sector', 'GetParamController@business_sector');
            Route::get('type_koperasi', 'GetParamController@type_koperasi');
            Route::get('position_koperasi', 'GetParamController@position_koperasi');
            Route::get('training_needs', 'GetParamController@training_needs');
            Route::get('training_type', 'GetParamController@training_type');
            Route::get('participant_problem', 'GetParamController@participant_problem');
            Route::get('expertise_companion', 'GetParamController@expertise_companion');
            Route::get('business_field', 'GetParamController@business_field');
            Route::get('social_media', 'GetParamController@social_media');
            Route::get('marketplace', 'GetParamController@marketplace');
            Route::get('business_licensing', 'GetParamController@business_licensing');
            Route::get('business_certificate', 'GetParamController@business_certificate');
            Route::get('marketing_reach', 'GetParamController@marketing_reach');
            Route::get('accompaniment', 'GetParamController@accompaniment');
            Route::get('form_koperasi', 'GetParamController@form_koperasi');
            Route::get('group_koperasi', 'GetParamController@group_koperasi');
            Route::get('fostered_koperasi', 'GetParamController@fostered_koperasi');
        });
    });
});
