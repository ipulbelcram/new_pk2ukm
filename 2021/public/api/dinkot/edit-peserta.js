let stop = false,
    social_media_id = ''
marketplace_id = ''
business_licensing_id = ''
business_certificate_id = ''
marketing_reach_id = ''

$.ajax({
    url: `${api_url}param/religion`,
    type: 'GET',
    beforeSend: function(xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + token)
    },
    success: function(result) {
        // console.log(result)
        $.each(result.data, function(index, value) {
            append = `<option value="${value.id}">${value.param}</option>`
            $('#religion_id').append(append)
        })
    }
})

$.ajax({
    url: `${api_url}param/education`,
    type: 'GET',
    beforeSend: function(xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + token)
    },
    success: function(result) {
        // console.log(result)
        $.each(result.data, function(index, value) {
            append = `<option value="${value.id}">${value.param}</option>`
            $('#education_id').append(append)
        })
    }
})

$.ajax({
    url: `${api_url}param/province`,
    type: 'GET',
    beforeSend: function(xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + token)
    },
    success: function(result) {
        // console.log(result)
        $.each(result.data, function(index, value) {
            append = `<option value="${value.id}">${value.province}</option>`
            $('#province_id').append(append)
        })
    }
})

function get_districs_city(province, districts_city) {
    $.ajax({
        url: `${api_url}param/districts_city/${province}`,
        type: 'GET',
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", "Bearer " + token)
        },
        success: function(result) {
            // console.log(result)
            $('#districts_city_id').empty()
            $('#districts_city_id').attr('disabled', false)
            $('#districts_city_id').html('<option disabled selected>Pilih</option>')
            $.each(result.data, function(index, value) {
                append = `<option value="${value.id}">${value.districts_city}</option>`
                $('#districts_city_id').append(append)
            })
            $('#districts_city_id').val(districts_city)
        }
    })
}

$.ajax({
    url: `${api_url}param/business_status`,
    type: 'GET',
    beforeSend: function(xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + token)
    },
    success: function(result) {
        // console.log(result)
        $.each(result.data, function(index, value) {
            append = `<option value="${value.id}">${value.param}</option>`
            $('#business_status_id').append(append)
        })
    }
})

$.ajax({
    url: `${api_url}param/business_field`,
    type: 'GET',
    beforeSend: function(xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + token)
    },
    success: function(result) {
        // console.log(result)
        $.each(result.data, function(index, value) {
            append = `<option value="${value.id}">${value.param}</option>`
            $('#business_field_umkm_id').append(append)
            $('#business_field_entrepreneur_id').append(append)
        })
    }
})

$.ajax({
    url: `${api_url}param/type_koperasi`,
    type: 'GET',
    beforeSend: function(xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + token)
    },
    success: function(result) {
        // console.log(result)
        $.each(result.data, function(index, value) {
            append = `<option value="${value.id}">${value.param}</option>`
            $('#type_koperasi_id').append(append)
        })
    }
})

$.ajax({
    url: `${api_url}param/group_koperasi`,
    type: 'GET',
    beforeSend: function(xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + token)
    },
    success: function(result) {
        // console.log(result)
        $.each(result.data, function(index, value) {
            append = `<option value="${value.id}">${value.param}</option>`
            $('#group_koperasi_id').append(append)
        })
    }
})

$.ajax({
    url: `${api_url}param/fostered_koperasi`,
    type: 'GET',
    beforeSend: function(xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + token)
    },
    success: function(result) {
        // console.log(result)
        $.each(result.data, function(index, value) {
            append = `<option value="${value.id}">${value.param}</option>`
            $('#fostered_koperasi_id').append(append)
        })
    }
})

$.ajax({
    url: `${api_url}param/position_koperasi`,
    type: 'GET',
    beforeSend: function(xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + token)
    },
    success: function(result) {
        // console.log(result)
        $.each(result.data, function(index, value) {
            append = `<option value="${value.id}">${value.param}</option>`
            $('#position_koperasi_id').append(append)
        })
    }
})

$.ajax({
    url: `${api_url}param/business_sector`,
    type: 'GET',
    beforeSend: function(xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + token)
    },
    success: function(result) {
        // console.log(result)
        $.each(result.data, function(index, value) {
            append = `<option value="${value.id}">${value.param}</option>`
            $('#business_sector_id').append(append)
            $('#koperasi_sector_id').append(append)
            $('#entrepreneur_sector_id').append(append)
        })
    }
})

$.ajax({
    url: `${api_url}param/social_media`,
    type: 'GET',
    beforeSend: function(xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + token)
    },
    success: function(result) {
        // console.log(result)
        $.each(result.data, function(index, value) {
            append = `<div class="form-check">
				<input class="form-check-input" name="social_media" type="checkbox" value="${value.id}" id="social_media${value.id}">
				<label class="form-check-label" for="social_media${value.id}">${value.param}</label>
			</div>`
            $('#social_media_id').append(append)
        })
    }
})

$.ajax({
    url: `${api_url}param/marketplace`,
    type: 'GET',
    beforeSend: function(xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + token)
    },
    success: function(result) {
        // console.log(result)
        $.each(result.data, function(index, value) {
            append = `<div class="form-check">
				<input class="form-check-input" name="marketplace" type="checkbox" value="${value.id}" id="marketplace${value.id}">
				<label class="form-check-label" for="marketplace${value.id}">${value.param}</label>
			</div>`
            $('#marketplace_id').append(append)
        })
    }
})

$.ajax({
    url: `${api_url}param/business_licensing`,
    type: 'GET',
    beforeSend: function(xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + token)
    },
    success: function(result) {
        // console.log(result)
        $.each(result.data, function(index, value) {
            append = `<div class="form-check">
				<input class="form-check-input" name="business_licensing" type="checkbox" value="${value.id}" id="business_licensing${value.id}">
				<label class="form-check-label" for="business_licensing${value.id}">${value.param}</label>
			</div>`
            $('#business_licensing_id').append(append)
        })
    }
})

$.ajax({
    url: `${api_url}param/business_certificate`,
    type: 'GET',
    beforeSend: function(xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + token)
    },
    success: function(result) {
        // console.log(result)
        $.each(result.data, function(index, value) {
            append = `<div class="form-check">
				<input class="form-check-input" name="business_certificate" type="checkbox" value="${value.id}" id="business_certificate${value.id}">
				<label class="form-check-label" for="business_certificate${value.id}">${value.param}</label>
			</div>`
            $('#business_certificate_id').append(append)
        })
    }
})

$.ajax({
    url: `${api_url}param/marketing_reach`,
    type: 'GET',
    beforeSend: function(xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + token)
    },
    success: function(result) {
        // console.log(result)
        $.each(result.data, function(index, value) {
            umkm = `<div class="form-check">
				<input class="form-check-input umkm ${value.optional == 1 ? 'optional' : ''}" name="marketing_reach_umkm" type="radio" value="${value.id}" id="marketing_reach_umkm${value.id}">
				<label class="form-check-label" for="marketing_reach_umkm${value.id}">${value.param}</label>
			</div>`
            koperasi = `<div class="form-check">
				<input class="form-check-input koperasi ${value.optional == 1 ? 'optional' : ''}" name="marketing_reach_koperasi" type="radio" value="${value.id}" id="marketing_reach_koperasi${value.id}">
				<label class="form-check-label" for="marketing_reach_koperasi${value.id}">${value.param}</label>
			</div>`
            $('#marketing_reach_umkm_id').append(umkm)
            $('#marketing_reach_koperasi_id').append(koperasi)
        })
    }
})

$.ajax({
    url: `${api_url}param/form_koperasi`,
    type: 'GET',
    beforeSend: function(xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + token)
    },
    success: function(result) {
        // console.log(result)
        $.each(result.data, function(index, value) {
            append = `<option value="${value.id}">${value.param}</option>`
            $('#form_koperasi_id').append(append)
        })
    }
})

$.ajax({
    url: `${api_url}param/accompaniment`,
    type: 'GET',
    beforeSend: function(xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + token)
    },
    success: function(result) {
        // console.log(result)
        $.each(result.data, function(index, value) {
            append = `<option value="${value.id}">${value.param}</option>`
            $('#accompaniment_entrepreneur_id').append(append)
        })
    }
})

$.ajax({
    url: `${api_url}param/participant_problem`,
    type: 'GET',
    beforeSend: function(xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + token)
    },
    success: function(result) {
        // console.log(result)
        $.each(result.data, function(index, value) {
            append = `<option value="${value.id}" data-optional="${value.optional}">${value.param}</option>`
            $('#m1').append(append)
        })
    }
})

$.ajax({
    url: `${api_url}param/training_needs`,
    type: 'GET',
    beforeSend: function(xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + token)
    },
    success: function(result) {
        // console.log(result)
        $.each(result.data, function(index, value) {
            append = `<option value="${value.id}">${value.param}</option>`
            $('#m2_id').append(append)
        })
    }
})

function get_peserta() {
    $.ajax({
        url: `${api_url}participant/${id}/by_id`,
        type: 'GET',
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", "Bearer " + token)
        },
        success: function(result) {
            let value = result.data
            // console.log(value)
            $('#name').val(value.name)
            $('#id_number').val(value.id_number)
            $('#gender').val(value.gender)
            $('#place_birth').val(value.place_birth)
            $('#date_birth').val(value.date_birth)
            $('#religion_id').val(value.religion_id.id)
            $('#education_id').val(value.education.id)
            $('#address_participant').val(value.address_participant)
            $('#province_id').val(value.province.id)
            get_districs_city(value.province.id, value.districts_city.id)
            $('#phone_number').val(value.phone_number)
            $('#email').val(value.email)
            $('#business_status_id').val(value.business_status.id)

            if (value.business_status.id == 14) {
                $('#umkm').show()
                $('#digitalisasi_usaha').show()
                $('#pembiayaan_usaha').show()
                $('#transformasi_usaha').show()
                $('#rantai_pasok_ekspor').show()
                $('#kemitraan_usaha').show()

                $('#name_umkm').val(value.name_umkm)
                $('#address_umkm').val(value.address_umkm)
                $('#business_field_umkm_id').val(value.business_field_umkm.id)
                $('#business_sector_id').val(value.business_sector.id)
                $('#date_establishment_umkm').val(value.date_establishment_umkm)
                $('#npwp_umkm').val(value.npwp_umkm)
                $('#nib_umkm').val(value.nib_umkm != null ? value.nib_umkm : '')
                $('#asset_umkm').val(convert(value.asset_umkm))
                $('#omset_umkm').val(convert(value.omset_umkm))
                $('#number_employees').val(value.number_employees)
                $('#production_capacity').val(value.production_capacity)
                $('#member_koperasi').val(value.member_koperasi == "yes" ? '1' : '0')

                $('#banking_credit').val(value.banking_credit == "yes" ? '1' : '0')
                $('#savings').val(value.savings == "yes" ? '1' : '0')

                $(`input:radio[name=marketing_reach_umkm][value=${value.marketing_reach.id}]`).attr('checked', true);
                if (value.marketing_reach.optional == 1) {
                    if (value.marketing_reach.id != 95) {
                        $('#marketing_reach_umkm_optional').show()
                        $('#marketing_reach_umkm_optional').val(value.marketing_reach_optional)
                    }
                }
                if (value.export != 'no') {
                    $('#export').val('1')
                    $('#export_yes').show()
                    $('#export_yes').val(value.export)
                    $('#export_form').show()
                    $('#export_destination').val(value.export_destination)
                    $('#export_volume').val(value.export_volume)
                    $('#export_value').val(value.export_value)
                } else {
                    $('#export').val('0')
                }
                $('#product_supply').val(value.product_supply == 'yes' ? '1' : '0')

            } else if (value.business_status.id == 15) {
                $('#koperasi').show()
                $('#digitalisasi_usaha').show()
                $('#transformasi_usaha').show()
                $('#kemitraan_usaha').show()

                $('#registrasion_number_koperasi').val(value.registrasion_number_koperasi)
                $('#name_koperasi').val(value.name_koperasi)
                $('#legal_entity_number').val(value.legal_entity_number)
                $('#date_establishment_koperasi').val(value.date_establishment_koperasi)
                $('#address_koperasi').val(value.address_koperasi)
                $('#type_koperasi_id').val(value.type_koperasi.id)
                $('#group_koperasi_id').val(value.group_koperasi.id)
                $('#koperasi_sector_id').val(value.business_sector.id)
                $('#fostered_koperasi_id').val(value.fostered_koperasi.id)
                $('#position_koperasi_id').val(value.position_koperasi.id)

                $('#npwp_koperasi').val(value.npwp_koperasi)
                $('#last_rat').val(value.last_rat)
                $('#nik_status').val(value.nik_status)
                $('#form_koperasi_id').val(value.form_koperasi.id)
                $('#asset_koperasi').val(convert(value.asset_koperasi))
                $('#omset_koperasi').val(convert(value.omset_koperasi))
                $('#shu_koperasi').val(convert(value.shu_koperasi))
                $('#total_member_koperasi_men').val(value.total_member_koperasi_men)
                $('#total_member_koperasi_women').val(value.total_member_koperasi_women)
                $('#total_employee_koperasi_men').val(value.total_employee_koperasi_men)
                $('#total_employee_koperasi_women').val(value.total_employee_koperasi_women)
                $(`input:radio[name=marketing_reach_koperasi][value=${value.marketing_reach.id}]`).attr('checked', true);
                if (value.marketing_reach.optional == 1) {
                    $('#marketing_reach_koperasi_optional').show()
                    $('#marketing_reach_koperasi_optional').val(value.marketing_reach_optional)
                }
            } else {
                $('#calon').show()
                $('#business_idea').val(value.business_idea)
                $('#entrepreneur_address').val(value.entrepreneur_address)
                $('#entrepreneur_sector_id').val(value.business_sector.id)
                $('#business_field_entrepreneur_id').val(value.business_field_entrepreneur.id)
                $('#accompaniment_entrepreneur_id').val(value.accompaniment_entrepreneur.id)
            }
            if (value.business_status.id == 14 || value.business_status.id == 15) {
                $('#business_email').val(value.business_email != null ? 'yes' : 'no')
                if (value.business_email != null) {
                    $('#business_email_yes').show()
                    $('#business_email_yes').val(value.business_email)
                }
                $('#business_website').val(value.business_website != null ? 'yes' : 'no')
                if (value.business_website != null) {
                    $('#business_website_yes').show()
                    $('#business_website_yes').val(value.business_website)
                }
                $.each(value.social_media, function(index, value) {
                    $(`#social_media${value.choice.id}`).prop('checked', true)
                })
                $.each(value.marketplace, function(index, value) {
                    $(`#marketplace${value.choice.id}`).prop('checked', true)
                })
                $.each(value.business_licensing, function(index, value) {
                    $(`#business_licensing${value.choice.id}`).prop('checked', true)
                })
                $.each(value.business_certificate, function(index, value) {
                    $(`#business_certificate${value.choice.id}`).prop('checked', true)
                })
                $('#partnership').val(value.partnership == 'yes' ? '1' : '0')
            }
            $('#m1').val(value.monitoring.m1.id)
            if (value.monitoring.other_m1 != null) {
                $('#other_m1').show()
                $('#other_m1').val(value.monitoring.other_m1)
            }
            $('#m2_id').val(value.monitoring.m2.id)
            $('#m3').val(value.monitoring.m3)
            $('.profile_photo').attr('src', value.profile_photo)
            stop = true
        }
    })
}

$(document).ajaxStop(function() {
    $('#card').show()
    $('#loading').hide()
    if (stop == false) get_peserta()
})

$('#province_id').change(function() {
    get_districs_city($(this).val())
})

$('#business_status_id').change(function() {
    $('#umkm').hide()
    $('#koperasi').hide()
    $('#digitalisasi_usaha').hide()
    $('#pembiayaan_usaha').hide()
    $('#transformasi_usaha').hide()
    $('#rantai_pasok_ekspor').hide()
    $('#kemitraan_usaha').hide()
    $('#calon').hide()
    if ($(this).val() == 14) {
        $('#umkm').show()
        $('#digitalisasi_usaha').show()
        $('#pembiayaan_usaha').show()
        $('#transformasi_usaha').show()
        $('#rantai_pasok_ekspor').show()
        $('#kemitraan_usaha').show()
    } else if ($(this).val() == 15) {
        $('#koperasi').show()
        $('#digitalisasi_usaha').show()
		$('#transformasi_usaha').show()
        $('#kemitraan_usaha').show()
    } else {
        $('#calon').show()
    }
})

$('#export').change(function() {
    if ($(this).val() == 1) {
        $('#export_yes').show()
        $('#export_form').show()
        $('#export_yes option').eq(0).prop('selected', true);
    } else {
        $('#export_yes').hide()
        $('#export_form').hide()
    }
})

$('#business_email').change(function() {
    if ($(this).val() == 'yes') {
        $('#business_email_yes').show()
    } else {
        $('#business_email_yes').hide()
    }
})

$('#business_website').change(function() {
    if ($(this).val() == 'yes') {
        $('#business_website_yes').show()
    } else {
        $('#business_website_yes').hide()
    }
})

$(document).on('click', 'input[type=radio]', function() {
	if ($(this).hasClass('optional')) {
		if ($(this).hasClass('umkm')) {
			if ($(this).val() != 95) {
				$(this).parents('div').siblings('input').show()
			} else {
				$(this).parents('div').siblings('input').hide()
			}
		} else {
			$(this).parents('div').siblings('input').show()
		}
		$(this).parents('div').siblings('input').focus()
	} else {
		$(this).parents('div').siblings('input').hide()
	}
})

$('#m1').change(function() {
    if ($(this).find(':selected').data('optional') == 1) {
        $('#other_m1').show()
        $('#other_m1').focus()
    } else {
        $('#other_m1').hide()
    }
})

$('form').submit(function(e) {
    $('#profile_photo-feedback').addClass('hide')
    $('.is-invalid').removeClass('is-invalid')
    $('.invalid-feedback').removeClass('d-block')
    e.preventDefault()
    addLoading()
    console.clear()

    let formData = new FormData()
    formData.append('name', $('#name').val())
    formData.append('id_number', $('#id_number').val())
    formData.append('gender', $('#gender').val())
    formData.append('place_birth', $('#place_birth').val())
    formData.append('date_birth', $('#date_birth').val())
    formData.append('religion_id', $('#religion_id').val())
    formData.append('education_id', $('#education_id').val())
    formData.append('address_participant', $('#address_participant').val())
    formData.append('province_id', $('#province_id').val())
    formData.append('districts_city_id', $('#districts_city_id').val())
    formData.append('phone_number', $('#phone_number').val())
    formData.append('email', $('#email').val())
    formData.append('business_status_id', $('#business_status_id').val())
    if ($('#business_status_id').val() == 14) {
        formData.append('name_umkm', $('#name_umkm').val())
        formData.append('address_umkm', $('#address_umkm').val())
        formData.append('business_sector_id', $('#business_sector_id').val())
        formData.append('business_field_umkm_id', $('#business_field_umkm_id').val())
        formData.append('date_establishment_umkm', $('#date_establishment_umkm').val())
        formData.append('npwp_umkm', $('#npwp_umkm').val())
        formData.append('nib_umkm', $('#nib_umkm').val())
        formData.append('asset_umkm', number($('#asset_umkm').val()))
        formData.append('omset_umkm', number($('#omset_umkm').val()))
        formData.append('number_employees', $('#number_employees').val())
        formData.append('production_capacity', $('#production_capacity').val())
        formData.append('member_koperasi', $('#member_koperasi').val())
        formData.append('banking_credit', $('#banking_credit').val())
        formData.append('savings', $('#savings').val())
        if ($('#export').val() != null) {
            if ($('#export').val() == 1) {
                formData.append('export', $('#export_yes').val())
                formData.append('export_destination', $('#export_destination').val())
                formData.append('export_volume', $('#export_volume').val())
                formData.append('export_value', $('#export_value').val()) // type data string, cant format to number
            }
        }
        formData.append('product_supply', $('#product_supply').val())
        formData.append('marketing_reach_id', $('input:radio[name=marketing_reach_umkm]:checked').val())
        if ($('input:radio[name=marketing_reach_umkm]:checked').hasClass('optional')) {
            if ($('input:radio[name=marketing_reach_umkm]:checked').val() != 95) { // jika jangkauan usaha optional & tidak termasuk expor
                formData.append('marketing_reach_optional', $('#marketing_reach_umkm_optional').val())
            }
        }
    } else if ($('#business_status_id').val() == 15) {
        formData.append('registrasion_number_koperasi', $('#registrasion_number_koperasi').val())
        formData.append('name_koperasi', $('#name_koperasi').val())
        formData.append('legal_entity_number', $('#legal_entity_number').val())
        formData.append('date_establishment_koperasi', $('#date_establishment_koperasi').val())
        formData.append('address_koperasi', $('#address_koperasi').val())
        formData.append('type_koperasi_id', $('#type_koperasi_id').val())
        formData.append('group_koperasi_id', $('#group_koperasi_id').val())
        formData.append('business_sector_id', $('#koperasi_sector_id').val())
        formData.append('fostered_koperasi_id', $('#fostered_koperasi_id').val())
        formData.append('position_koperasi_id', $('#position_koperasi_id').val())
        formData.append('npwp_koperasi', $('#npwp_koperasi').val())
        formData.append('last_rat', $('#last_rat').val())
        formData.append('nik_status', $('#nik_status').val())
        formData.append('form_koperasi_id', $('#form_koperasi_id').val())
        formData.append('asset_koperasi', number($('#asset_koperasi').val()))
        formData.append('omset_koperasi', number($('#omset_koperasi').val()))
        formData.append('shu_koperasi', number($('#shu_koperasi').val()))
        formData.append('total_member_koperasi_men', $('#total_member_koperasi_men').val())
        formData.append('total_member_koperasi_women', $('#total_member_koperasi_women').val())
        formData.append('total_employee_koperasi_men', $('#total_employee_koperasi_men').val())
        formData.append('total_employee_koperasi_women', $('#total_employee_koperasi_women').val())
        formData.append('marketing_reach_id', $('input:radio[name=marketing_reach_koperasi]:checked').val())
        if ($('input:radio[name=marketing_reach_koperasi]:checked').hasClass('optional')) {
            formData.append('marketing_reach_optional', $('#marketing_reach_koperasi_optional').val())
        }
    } else {
        formData.append('business_idea', $('#business_idea').val())
        formData.append('entrepreneur_address', $('#entrepreneur_address').val())
        formData.append('business_sector_id', $('#entrepreneur_sector_id').val())
        formData.append('business_field_entrepreneur_id', $('#business_field_entrepreneur_id').val())
        formData.append('accompaniment_entrepreneur_id', $('#accompaniment_entrepreneur_id').val())
    }
    if ($('#business_status_id').val() == 14 || $('#business_status_id').val() == 15) {
        formData.append('partnership', $('#partnership').val())
        formData.append('business_email_validation', $('#business_email').val())
        if ($('#business_email').val() != null) {
            if ($('#business_email').val() == 'yes') {
                formData.append('business_email', $('#business_email_yes').val())
            }
        }
        formData.append('business_website_validation', $('#business_website').val())
        if ($('#business_website').val() != null) {
            if ($('#business_website').val() == 'yes') {
                formData.append('business_website', $('#business_website_yes').val())
            }
        }
        $('input:checkbox[name=social_media]:checked').each(function(index) {
            formData.append(`social_media_id[${index}]`, $(this).val())
        })
        $('input:checkbox[name=marketplace]:checked').each(function(index) {
            formData.append(`marketplace_id[${index}]`, $(this).val())
        })
        $('input:checkbox[name=business_licensing]:checked').each(function(index) {
            formData.append(`business_licensing_id[${index}]`, $(this).val())
        })
        $('input:checkbox[name=business_certificate]:checked').each(function(index) {
            formData.append(`business_certificate_id[${index}]`, $(this).val())
        })
    }
    formData.append('m1', $('#m1').val())
    if ($('#m1').find(':selected').data('optional') == 1) formData.append('other_m1', $('#other_m1').val())
    formData.append('m2_id', $('#m2_id').val())
    formData.append('m3', $('#m3').val())
    profile_photo != null ? formData.append('profile_photo', profile_photo) : ''

    $.ajax({
        url: `${api_url}participant/${id}/update`,
        type: 'POST',
        processData: false,
        contentType: false,
        data: formData,
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", "Bearer " + token)
        },
        success: function(result) {
            location.href = `${root}kegiatan/${result.data.training_id}`
        },
        error: function(xhr) {
            removeLoading('Simpan')
            let err = xhr.responseJSON.errors
            let erd = xhr.responseJSON.data
            let msg = xhr.responseJSON.message
            // console.log(err)
            // console.log(erd)
            // console.log(msg)
            if (err) {
                if (err.profile_photo) {
                    $('#profile_photo-feedback').removeClass('hide')
                    $('#profile_photo-feedback').html('Masukkan Foto')
                    $(window).scrollTop(0)
                }
                if (err.name) {
                    $('#name').addClass('is-invalid')
                    $('#name-feedback').html('Masukkan nama lengkap')
                }
                if (err.id_number) {
                    $('#id_number').addClass('is-invalid')
                    $('#id_number-feedback').html('Masukkan nomor induk kependudukan (nik)')
                }
                if (err.gender) {
                    $('#gender').addClass('is-invalid')
                    $('#gender-feedback').html('Pilih jenis kelamin')
                }
                if (err.place_birth) {
                    $('#place_birth').addClass('is-invalid')
                    $('#place_birth-feedback').html('Masukkan tempat lahir')
                }
                if (err.date_birth) {
                    $('#date_birth').addClass('is-invalid')
                    $('#date_birth-feedback').html('Masukkan tanggal lahir')
                }
                if (err.religion_id) {
                    $('#religion_id').addClass('is-invalid')
                    $('#religion_id-feedback').html('Pilih agama')
                }
                if (err.education_id) {
                    $('#education_id').addClass('is-invalid')
                    $('#education_id-feedback').html('Pilih pendidikan terakhir')
                }
                if (err.address_participant) {
                    $('#address_participant').addClass('is-invalid')
                    $('#address_participant-feedback').html('Masukkan alamat rumah')
                }
                if (err.province_id) {
                    $('#province_id').addClass('is-invalid')
                    $('#province_id-feedback').html('Pilih provinsi')
                }
                if (err.districts_city_id) {
                    $('#districts_city_id').addClass('is-invalid')
                    $('#districts_city_id-feedback').html('Pilih kab/kota')
                }
                if (err.phone_number) {
                    $('#phone_number').addClass('is-invalid')
                    $('#phone_number-feedback').html('Masukkan telp/hp')
                }
                if (err.email) {
                    $('#email').addClass('is-invalid')
                    $('#email-feedback').html('Masukkan email')
                }
                if (err.business_status_id) {
                    $('#business_status_id').addClass('is-invalid')
                    $('#business_status_id-feedback').html('Pilih status usaha')
                }

                // UMKM
                if (err.name_umkm) {
                    $('#name_umkm').addClass('is-invalid')
                    $('#name_umkm-feedback').html('Masukkan nama usaha')
                }
                if (err.address_umkm) {
                    $('#address_umkm').addClass('is-invalid')
                    $('#address_umkm-feedback').html('Masukkan alamat usaha')
                }
                if (err.business_field_umkm_id) {
                    $('#business_field_umkm_id').addClass('is-invalid')
                    $('#business_field_umkm_id-feedback').html('Pilih bidang usaha')
                }
                if (err.date_establishment_umkm) {
                    $('#date_establishment_umkm').addClass('is-invalid')
                    $('#date_establishment_umkm-feedback').html('Masukkan tanggal pendirian usaha')
                }
                if (err.npwp_umkm) {
                    $('#npwp_umkm').addClass('is-invalid')
                    $('#npwp_umkm-feedback').html('Masukkan npwp usaha')
                }
                if (err.nib_umkm) {
                    $('#nib_umkm').addClass('is-invalid')
                    $('#nib_umkm-feedback').html('Masukkan nomor induk berusaha (NIB)')
                }
                if (err.asset_umkm) {
                    $('#asset_umkm').addClass('is-invalid')
                    $('#asset_umkm-feedback').html('Masukkan kekayaan usaha (asset) per tahun')
                }
                if (err.omset_umkm) {
                    $('#omset_umkm').addClass('is-invalid')
                    $('#omset_umkm-feedback').html('Masukkan volume usaha (omset) per tahun')
                }
                if (err.number_employees) {
                    $('#number_employees').addClass('is-invalid')
                    $('#number_employees-feedback').html('Masukkan jumlah karyawan')
                }
                if (err.production_capacity) {
                    $('#production_capacity').addClass('is-invalid')
                    $('#production_capacity-feedback').html('Masukkan kapasitas produksi per tahun')
                }
                if (err.member_koperasi) {
                    $('#member_koperasi').addClass('is-invalid')
                    $('#member_koperasi-feedback').html('Pilih anggota koperasi')
                }
                if (err.banking_credit) {
                    $('#banking_credit').addClass('is-invalid')
                    $('#banking_credit-feedback').html('Pilih apakah pernah mengakses kredit perbankan/non-perbankan')
                }
                if (err.savings) {
                    $('#savings').addClass('is-invalid')
                    $('#savings-feedback').html('Pilih apakah memiliki tabungan')
                }
                if (err.export) {
                    $('#export_yes').addClass('is-invalid')
                    $('#export-feedback').html('Pilih pengiriman')
                }
                if (err.export_destination) {
                    $('#export_destination').addClass('is-invalid')
                    $('#export_destination-feedback').html('Masukkan negara tujuan')
                }
                if (err.export_volume) {
                    $('#export_volume').addClass('is-invalid')
                    $('#export_volume-feedback').html('Masukkan volume ekspor per tahun')
                }
                if (err.export_value) {
                    $('#export_value').addClass('is-invalid')
                    $('#export_value-feedback').html('Masukkan nilai ekspor')
                }
                if (err.product_supply) {
                    $('#product_supply').addClass('is-invalid')
                    $('#product_supply-feedback').html('Masukkan apakah bapak/ibu mensuplai produk sebagai bahan baku pada usaha lain')
                }

                // Koperasi
                if (err.registrasion_number_koperasi) {
                    $('#registrasion_number_koperasi').addClass('is-invalid')
                    $('#registrasion_number_koperasi-feedback').html('Masukkan nomor induk koperasi')
                }
                if (err.name_koperasi) {
                    $('#name_koperasi').addClass('is-invalid')
                    $('#name_koperasi-feedback').html('Masukkan nama koperasi')
                }
                if (err.legal_entity_number) {
                    $('#legal_entity_number').addClass('is-invalid')
                    $('#legal_entity_number-feedback').html('Masukkan nomor badan hukum')
                }
                if (err.date_establishment_koperasi) {
                    $('#date_establishment_koperasi').addClass('is-invalid')
                    $('#date_establishment_koperasi-feedback').html('Masukkan tanggal pendirian koperasi')
                }
                if (err.address_koperasi) {
                    $('#address_koperasi').addClass('is-invalid')
                    $('#address_koperasi-feedback').html('Masukkan alamat koperasi')
                }
                if (err.type_koperasi_id) {
                    $('#type_koperasi_id').addClass('is-invalid')
                    $('#type_koperasi_id-feedback').html('Pilih jenis koperasi')
                }
                if (err.group_koperasi_id) {
                    $('#group_koperasi_id').addClass('is-invalid')
                    $('#group_koperasi_id-feedback').html('Pilih kelompok koperasi')
                }
                if (err.fostered_koperasi_id) {
                    $('#fostered_koperasi_id').addClass('is-invalid')
                    $('#fostered_koperasi_id-feedback').html('Pilih koperasi binaan')
                }
                if (err.position_koperasi_id) {
                    $('#position_koperasi_id').addClass('is-invalid')
                    $('#position_koperasi_id-feedback').html('Pilih jabatan di koperasi')
                }
                if (err.npwp_koperasi) {
                    $('#npwp_koperasi').addClass('is-invalid')
                    $('#npwp_koperasi-feedback').html('Masukkan npwp koperasi')
                }
                if (err.last_rat) {
                    $('#last_rat').addClass('is-invalid')
                    $('#last_rat-feedback').html('Masukkan RAT terakhir')
                }
                if (err.nik_status) {
                    $('#nik_status').addClass('is-invalid')
                    $('#nik_status-feedback').html('Masukkan NIK status')
                }
                if (err.form_koperasi_id) {
                    $('#form_koperasi_id').addClass('is-invalid')
                    $('#form_koperasi_id-feedback').html('Masukkan bentuk koperasi')
                }
                if (err.asset_koperasi) {
                    $('#asset_koperasi').addClass('is-invalid')
                    $('#asset_koperasi-feedback').html('Masukkan asset koperasi')
                }
                if (err.omset_koperasi) {
                    $('#omset_koperasi').addClass('is-invalid')
                    $('#omset_koperasi-feedback').html('Masukkan omzet koperasi')
                }
                if (err.shu_koperasi) {
                    $('#shu_koperasi').addClass('is-invalid')
                    $('#shu_koperasi-feedback').html('Masukkan SHU koperasi')
                }
                if (err.total_member_koperasi_men) {
                    $('#total_member_koperasi_men').addClass('is-invalid')
                    $('#total_member_koperasi_men-feedback').html('Masukkan jumlah anggota laki-laki')
                }
                if (err.total_member_koperasi_women) {
                    $('#total_member_koperasi_women').addClass('is-invalid')
                    $('#total_member_koperasi_women-feedback').html('Masukkan jumlah anggota perempuan')
                }
                if (err.total_employee_koperasi_men) {
                    $('#total_employee_koperasi_men').addClass('is-invalid')
                    $('#total_employee_koperasi_men-feedback').html('Masukkan jumlah karyawan laki-laki')
                }
                if (err.total_employee_koperasi_women) {
                    $('#total_employee_koperasi_women').addClass('is-invalid')
                    $('#total_employee_koperasi_women-feedback').html('Masukkan jumlah karyawan perempuan')
                }

                // Jangkauan Pemasaran Produk
                if (err.marketing_reach_id) {
	                if ($('#business_status_id').val() == 14) {
	                    $('#marketing_reach_umkm_id').siblings('.invalid-feedback').addClass('d-block')
	                    $('#marketing_reach_umkm_id-feedback').html('Pilih jangkauan pemasaran produk/layanan usaha')
	                }
	                else if ($('#business_status_id').val() == 15) {
	                    $('#marketing_reach_koperasi_id').siblings('.invalid-feedback').addClass('d-block')
	                    $('#marketing_reach_koperasi_id-feedback').html('Pilih jangkauan pemasaran produk/layanan koperasi')
	                }
	            }
                if (err.marketing_reach_optional) {
	                if ($('#business_status_id').val() == 14) {
	                    $('#marketing_reach_umkm_id').siblings('input').addClass('is-invalid')
	                    $('#marketing_reach_umkm_id').siblings('.invalid-feedback').addClass('d-block')
	                    $('#marketing_reach_umkm_id-feedback').html('Masukkan lokasi jangkauan pemasaran produk/layanan usaha')
	                }
	                else if ($('#business_status_id').val() == 15) {
	                    $('#marketing_reach_koperasi_id').siblings('input').addClass('is-invalid')
	                    $('#marketing_reach_koperasi_id').siblings('.invalid-feedback').addClass('d-block')
	                    $('#marketing_reach_koperasi_id-feedback').html('Masukkan lokasi jangkauan pemasaran produk/layanan koperasi')
	                }
	            }

                // UMKM & Koperasi
                if (err.partnership) {
                    $('#partnership').addClass('is-invalid')
                    $('#partnership-feedback').html('Pilih apakah melakukan kemitraan dengan lembaga lain')
                }
                if (err.business_email_validation) {
                    $('#business_email').addClass('is-invalid')
                    $('#business_email-feedback').html('Pilih apakah mempunyai email usaha')
                }
                if (err.business_email) {
                    $('#business_email_yes').addClass('is-invalid')
                    $('#business_email-feedback').html('Masukkan email usaha dengan benar')
                }
                if (err.business_website_validation) {
                    $('#business_website').addClass('is-invalid')
                    $('#business_website-feedback').html('Pilih apakah mempunyai website usaha')
                }
                if (err.business_website) {
                    $('#business_website_yes').addClass('is-invalid')
                    $('#business_website-feedback').html('Masukkan website usaha dengan benar')
                }
                if (err.social_media_id) {
                    $('#social_media_id').siblings('.invalid-feedback').addClass('d-block')
                    $('#social_media_id-feedback').html('Pilih media sosial usaha')
                }
                if (err.marketplace_id) {
                    $('#marketplace_id').siblings('.invalid-feedback').addClass('d-block')
                    $('#marketplace_id-feedback').html('Pilih marketplace')
                }
                if (err.business_licensing_id) {
                    $('#business_licensing_id').siblings('.invalid-feedback').addClass('d-block')
                    $('#business_licensing_id-feedback').html('Pilih perizinan usaha')
                }
                if (err.business_certificate_id) {
                    $('#business_certificate_id').siblings('.invalid-feedback').addClass('d-block')
                    $('#business_certificate_id-feedback').html('Pilih sertifikat produk/usaha')
                }

                // Calon Wirausaha
                if (err.business_idea) {
                    $('#business_idea').addClass('is-invalid')
                    $('#business_idea-feedback').html('Masukkan ide bisnis')
                }
                if (err.entrepreneur_address) {
                    $('#entrepreneur_address').addClass('is-invalid')
                    $('#entrepreneur_address-feedback').html('Masukkan alamat calon usaha')
                }
                if (err.business_field_entrepreneur_id) {
                    $('#business_field_entrepreneur_id').addClass('is-invalid')
                    $('#business_field_entrepreneur_id-feedback').html('Pilih bidang calon usaha')
                }
                if (err.accompaniment_entrepreneur_id) {
                    $('#accompaniment_entrepreneur_id').addClass('is-invalid')
                    $('#accompaniment_entrepreneur_id-feedback').html('Pilih pendamping yang didapatkan')
                }

                // Sektor Usaha
                if (err.business_sector_id) {
                    if ($('#business_status_id').val() == 14) {
                        $('#business_sector_id').addClass('is-invalid')
                        $('#business_sector_id-feedback').html('Pilih sektor usaha')
                    } else if ($('#business_status_id').val() == 15) {
                        $('#koperasi_sector_id').addClass('is-invalid')
                        $('#koperasi_sector_id-feedback').html('Pilih sektor usaha')
                    } else {
                        $('#entrepreneur_sector_id').addClass('is-invalid')
                        $('#entrepreneur_sector_id-feedback').html('Pilih sektor calon usaha')
                    }
                }

                // Keterangan
                if (err.m1) {
                    $('#m1').addClass('is-invalid')
                    $('#m1-feedback').html('Masukkan permasalahan yang dihadapi dalam pengembangan usaha')
                }
                if (err.other_m1) {
                    $('#other_m1').addClass('is-invalid')
                    $('#m1-feedback').html('Masukkan permasalahan yang dihadapi dalam pengembangan usaha')
                }
                if (err.m2_id) {
                    $('#m2_id').addClass('is-invalid')
                    $('#m2_id-feedback').html('Pilih kebutuhan diklat')
                }
                if (err.m3) {
                    $('#m3').addClass('is-invalid')
                    $('#m3-feedback').html('Masukkan saran')
                }
            } else if (erd) {
                $('#modal-exist').modal('show')
            }
        }
    })
})
