let companion_id

$.ajax({
    url: `${api_url}companion`,
    type: 'GET',
    data: {
        user_id: user_id
    },
    beforeSend: function(xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + token)
    },
    success: function(result) {
        let value = result.data
        // console.log(value)
        companion_id = value.id
        $('#download').attr('href', `${root}unduh-monitoring-lengkap/${value.user_id}/${value.id}`)
        $('#name').html(value.name)
        $('#no_ktp').html(value.no_ktp)
        get_validation()
    }
})

function get_validation() {
    $.ajax({
        url: `${api_url}monitoring_companion/validation_by_month/${companion_id}`,
        type: 'GET',
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", "Bearer " + token)
        },
        success: function(result) {
            // console.log(result)
            $.each(result.data, function(index, value) {
                value.month == 1 ? active = 'active' : active = ''
                value.validation == true ? check = '<i class="mdi mdi-check-circle"></i>' : check = ''
                append = `<li class="nav-item">
			        <div class="nav-link d-flex align-items-center ${active}" data-id="${value.month}" role="button">
			            <span>${check} ${bulan(value.month)}</span>
			        </div>
			    </li>`
                $('#bulan').append(append)
            })
            get_monitoring(1)
        }
    })
}

function get_monitoring(month) {
    $('#data').hide()
    $('#loading-data').show()
    $.ajax({
        url: `${api_url}monitoring_companion/fetch/${companion_id}/${month}`,
        type: 'GET',
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", "Bearer " + token)
        },
        success: function(result) {
            // console.log(result)
            $('.is-invalid').removeClass('is-invalid')
            $('#month').html(bulan(month))
            if (result != '') {
                let value = result.data
                $('#date').html(tanggal(value.date))
                $('#activity_details').html(value.activity_details)
                $('#output_koperasi').html(value.output_koperasi + ' Koperasi')
                $('#output_umkm').html(value.output_umkm + ' Umkm')
            } else {
                $('#date').html('')
                $('#activity_details').html('')
                $('#output_koperasi').html('')
                $('#output_umkm').html('')
            }
            $('#data').show()
            $('#loading-data').hide()
        }
    })
}

$(document).ajaxStop(function() {
    $('#card').show()
    $('#loading').hide()
    $('#download').removeClass('hide')
})

$(document).on('click', '.nav-link', function() {
    $(this).addClass('active')
    $('.nav-link').removeClass('active')
    get_monitoring($(this).data('id'))
})

if (window.innerWidth < 992) {
    $('#bulan').collapse('hide')
}
$(window).resize(function() {
    if (window.innerWidth < 992) {
        $('#bulan').collapse('hide')
    } else {
        $('#bulan').collapse('show')
    }
})
