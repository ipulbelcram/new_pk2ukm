$.ajax({
    url: `${api_url}participant/${id}/by_id`,
    type: 'GET',
    beforeSend: function(xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + token)
    },
    success: function(result) {
        let value = result.data
        // console.log(value)
        $('.nama').html(value.name)
        $('.id_number').html(value.id_number)
        $('#gender').html(value.gender)
        $('#place_birth').html(value.place_birth)
        $('#date_birth').html(tanggal(value.date_birth))
        $('#religion_id').html(value.religion_id.param)
        $('#education_id').html(value.education.param)
        $('#address_participant').html(value.address_participant)
        $('#province_id').html(value.province.province)
        $('#districts_city_id').html(value.districts_city.districts_city)
        $('#phone_number').html(value.phone_number)
        $('#email').html(value.email)
        $('#business_status_id').html(value.business_status.param)
        if (value.business_status.id == 14) {
            $('#umkm').show()
            $('#digitalisasi_usaha').show()
            $('#pembiayaan_usaha').show()
            $('#transformasi_usaha').show()
            $('#rantai_pasok_ekspor').show()
            $('#kemitraan_usaha').show()
            $('#name_umkm').html(value.name_umkm)
            $('#address_umkm').html(value.address_umkm)
            $('#business_field_umkm_id').html(value.business_field_umkm.param)
            $('#business_sector_id').html(value.business_sector.param)
            $('#date_establishment_umkm').html(tanggal(value.date_establishment_umkm))
            $('#npwp_umkm').html(value.npwp_umkm)
            $('#nib_umkm').html(value.nib_umkm)
            $('#asset_umkm').html(rupiah(value.asset_umkm))
            $('#omset_umkm').html(rupiah(value.omset_umkm))
            $('#number_employees').html(`${value.number_employees} Orang`)
            $('#production_capacity').html(value.production_capacity)
            $('#member_koperasi').html(value.member_koperasi == 'yes' ? 'Ya' : 'Tidak')
            $('#banking_credit').html(value.banking_credit == 'yes' ? 'Ya' : 'Tidak')
            $('#savings').html(value.savings == 'yes' ? 'Ya' : 'Tidak')
            if (value.export != 'no') {
                $('#export').html(`Ya, ${value.export}`)
                $('#export_form').show()
                $('#export_destination').html(value.export_destination)
                $('#export_volume').html(value.export_volume)
                $('#export_value').html(`Rp.${value.export_value}`)
            } else {
                $('#export').html('Tidak')
            }
            $('#product_supply').html(value.product_supply == 'yes' ? 'Ya' : 'Tidak')
            $('#marketing_reach_umkm').html(value.marketing_reach.param)
            if (value.marketing_reach.optional == 1) {
                if (value.marketing_reach.id != 95) {
                    $('#marketing_reach_umkm').append(`: ${value.marketing_reach_optional}`)
                }
            }
        } else if (value.business_status.id == 15) {
            $('#koperasi').show()
            $('#digitalisasi_usaha').show()
            $('#transformasi_usaha').show()
            $('#kemitraan_usaha').show()
            $('#registrasion_number_koperasi').html(value.registrasion_number_koperasi)
            $('#name_koperasi').html(value.name_koperasi)
            $('#legal_entity_number').html(value.legal_entity_number)
            $('#date_establishment_koperasi').html(tanggal(value.date_establishment_koperasi))
            $('#address_koperasi').html(value.address_koperasi)
            $('#type_koperasi_id').html(value.type_koperasi.param)
            $('#group_koperasi_id').html(value.group_koperasi.param)
            $('#koperasi_sector_id').html(value.business_sector.param)
            $('#fostered_koperasi_id').html(value.fostered_koperasi.param)
            $('#position_koperasi_id').html(value.position_koperasi.param)
            $('#npwp_koperasi').html(value.npwp_koperasi)
            $('#last_rat').html(`Tahun ${value.last_rat}`)
            $('#nik_status').html(value.nik_status)
            $('#form_koperasi_id').html(value.form_koperasi.param)
            $('#asset_koperasi').html(rupiah(value.asset_koperasi))
            $('#omset_koperasi').html(rupiah(value.omset_koperasi))
            $('#shu_koperasi').html(rupiah(value.shu_koperasi))
            $('#total_member_koperasi_men').html(`${value.total_member_koperasi_men} Orang`)
            $('#total_member_koperasi_women').html(`${value.total_member_koperasi_women} Orang`)
            $('#total_employee_koperasi_men').html(`${value.total_employee_koperasi_men} Orang`)
            $('#total_employee_koperasi_women').html(`${value.total_employee_koperasi_women} Orang`)
            $('#marketing_reach_koperasi').html(value.marketing_reach.param)
            if (value.marketing_reach.optional == 1) {
                $('#marketing_reach_koperasi').append(`: ${value.marketing_reach_optional}`)
            }
        } else {
            $('#calon').show()
            $('#business_idea').html(value.business_idea)
            $('#entrepreneur_address').html(value.entrepreneur_address)
            $('#entrepreneur_sector_id').html(value.business_sector.param)
            $('#business_field_entrepreneur_id').html(value.business_field_entrepreneur.param)
            $('#accompaniment_entrepreneur_id').html(value.accompaniment_entrepreneur.param)
        }
        if (value.business_status.id == 14 || value.business_status.id == 15) {
            $('#partnership').html(value.partnership == 'yes' ? 'Ya' : 'Tidak')
            $('#business_email').html(value.business_email != null ? value.business_email : 'Tidak Ada')
            $('#business_website').html(value.business_website != null ? value.business_website : 'Tidak Ada')
            let length_social_media = value.social_media.length - 1
            $.each(value.social_media, function(index, value) {
            	if (index < length_social_media) {
	                append = `${value.choice.param}, `
            	} else {
	                append = `${value.choice.param}`
            	}
                $('#social_media_id').append(append)
            })
            let length_marketplace = value.marketplace.length - 1
            $.each(value.marketplace, function(index, value) {
                if (index < length_marketplace) {
	                append = `${value.choice.param}, `
            	} else {
	                append = `${value.choice.param}`
            	}
                $('#marketplace_id').append(append)
            })
            let length_business_licensing = value.business_licensing.length - 1
            $.each(value.business_licensing, function(index, value) {
                if (index < length_business_licensing) {
	                append = `${value.choice.param}, `
            	} else {
	                append = `${value.choice.param}`
            	}
                $('#business_licensing_id').append(append)
            })
            let length_business_certificate = value.business_certificate.length - 1
            $.each(value.business_certificate, function(index, value) {
                if (index < length_business_certificate) {
	                append = `${value.choice.param}, `
            	} else {
	                append = `${value.choice.param}`
            	}
                $('#business_certificate_id').append(append)
            })
        }
        $('#m1').html(value.monitoring.m1.param)
        if (value.monitoring.other_m1 != null) $('#m1').append(`: ${value.monitoring.other_m1}`)
        $('#m2_id').html(value.monitoring.m2.param)
        $('#m3').html(value.monitoring.m3)
        $('.profile_photo').attr('src', value.profile_photo)

        $('#card').show()
        $('#loading').hide()
    }
})
