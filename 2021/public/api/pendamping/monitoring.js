let validation = []
let companion_id, month

$.ajax({
    url: `${api_url}companion`,
    type: 'GET',
    data: {
    	user_id: user
    },
    beforeSend: function(xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + token)
    },
    success: function(result) {
    	let value = result.data
        // console.log(value)
        companion_id = value.id
        month = 1
		$.ajax({
		    url: `${api_url}monitoring_companion/validation_by_month/${companion_id}`,
		    type: 'GET',
		    beforeSend: function(xhr) {
		        xhr.setRequestHeader("Authorization", "Bearer " + token)
		    },
		    success: function(result) {
		    	// console.log(result)
		    	$.each(result.data, function(index, value) {
		    		validation.push({
		    			month: value.month,
		    			validation: value.validation
		    		})
		    		value.month == 1 ? active = 'active' : active = ''
					value.validation == true ? check = '<i class="mdi mdi-check-circle"></i>' : check = ''
					append = `<li class="nav-item">
				        <div class="nav-link d-flex align-items-center ${active}" data-id="${value.month}" role="button">
				            <span id="month${value.month}">${check} ${bulan(value.month)}</span>
				        </div>
				    </li>`
				    $('#bulan').append(append)
		    	})
		    	get_monitoring(1)
		    }
		})
    }
})

function get_monitoring(month) {
	$('form').hide()
	$('#loading').show()
	$.ajax({
	    url: `${api_url}monitoring_companion/fetch/${companion_id}/${month}`,
	    type: 'GET',
	    beforeSend: function(xhr) {
	        xhr.setRequestHeader("Authorization", "Bearer " + token)
	    },
	    success: function(result) {
	    	// console.log(result)
			$('.is-invalid').removeClass('is-invalid')
			$('#month').html(bulan(month))
	    	if (result != '') {
		    	let value = result.data
				$('#date').val(value.date)
				$('#activity_details').val(value.activity_details)
				$('#output_koperasi').val(value.output_koperasi)
				$('#output_umkm').val(value.output_umkm)
			} else {
				$('#date').val('')
				$('#activity_details').val('')
				$('#output_koperasi').val('')
				$('#output_umkm').val('')
			}
	    	$('form').show()
	    	$('#loading').hide()
	    }
	})
}

$(document).ajaxStop(function() {
	$('#card').show()
	$('#card-loading').hide()
})

$(document).on('click', '.nav-link', function() {
	$('.nav-link').removeClass('active')
	$(this).addClass('active')
	get_monitoring($(this).data('id'))
	month = $(this).data('id')
})

$('form').submit(function(e) {
	$('.is-invalid').removeClass('is-invalid')
    e.preventDefault()
    addLoading()

    let formData = new FormData($('form')[0])
    formData.append('companion_id', companion_id)
    formData.append('month', month)
    formData.append('date', $('#date').val())
    formData.append('activity_details', $('#activity_details').val())
    formData.append('output_koperasi', $('#output_koperasi').val())
    formData.append('output_umkm', $('#output_umkm').val())

	$.ajax({
	    url: `${api_url}monitoring_companion/create`,
	    type: 'POST',
        processData: false,
        contentType: false,
        data: formData,
	    beforeSend: function(xhr) {
	        xhr.setRequestHeader("Authorization", "Bearer " + token)
	    },
	    success: function(result) {
	    	let value = result.data
	    	// console.log(value)
	    	$(`#month${value.month}`).html(`<i class="mdi mdi-check-circle"></i> ${bulan(value.month)}`)
	    	customAlert('success', 'Monitoring berhasil disimpan')
            removeLoading('Simpan')
	    },
        error: function(xhr) {
            removeLoading('Simpan')
            let err = xhr.responseJSON.errors
            let msg = xhr.responseJSON.message
            // console.log(err)
            if (err.date) {
                $('#date').addClass('is-invalid')
                $('#date-feedback').html('Masukkan tanggal')
            }
            if (err.activity_details) {
                $('#activity_details').addClass('is-invalid')
                $('#activity_details-feedback').html('Masukkan detail aktivitas')
            }
            if (err.output_koperasi) {
                $('#output_koperasi').addClass('is-invalid')
                $('#output_koperasi-feedback').html('Masukkan koperasi yang didampingi')
            }
            if (err.output_umkm) {
                $('#output_umkm').addClass('is-invalid')
                $('#output_umkm-feedback').html('Masukkan umkm yang didampingi')
            }
        }
	})
})

if (window.innerWidth < 992) {
    $('#bulan').collapse('hide')
}
$(window).resize(function() {
    if (window.innerWidth < 992) {
        $('#bulan').collapse('hide')
    } else {
        $('#bulan').collapse('show')
    }
})
