$.ajax({
    url: `${api_url}budged_performance/persentase_budged_realization`,
    type: 'GET',
    beforeSend: function(xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + token)
    },
    success: function(result) {
    	// console.log(result)
        let value = result.data
        value.total_budged_realization != null ? realisasi_anggaran = value.total_budged_realization : realisasi_anggaran = 0
        value.total_participant_realization != null ? realisasi_peserta = value.total_participant_realization : realisasi_peserta = 0
        value.total_companion_realization != null ? realisasi_pendamping = value.total_companion_realization : realisasi_pendamping = 0
        value.persentase_budged_realization != null ? persentase_anggaran = value.persentase_budged_realization : persentase_anggaran = 0
        value.persentase_participant_realization != null ? persentase_peserta = value.persentase_participant_realization : persentase_peserta = 0
        value.persentase_companion_realization != null ? persentase_pendamping = value.persentase_companion_realization : persentase_pendamping = 0
        $('#realisasi_anggaran').html(rupiah(realisasi_anggaran))
        $('#realisasi_peserta').html(`${convert(realisasi_peserta)} Peserta`)
        $('#realisasi_pendamping').html(`${convert(realisasi_pendamping)} Pendamping`)
        $('#persentase_anggaran').html(`${persentase(persentase_anggaran)}`)
        $('#persentase_peserta').html(`${persentase(persentase_peserta)}`)
        $('#persentase_pendamping').html(`${persentase(persentase_pendamping)}`)
    }
})

$.ajax({
    url: `${api_url}param/province`,
    type: 'GET',
    beforeSend: function(xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + token)
    },
    success: function(result) {
        $.each(result.data, function(index, province) {
            append = `<option value="${province.id}">${province.province}</option>`
	        $("#select-province").append(append)
        })
    }
})

$(".cekRekapitulasiProvinsi").click(function() {
    $("#rekapitulasi-province-table").removeClass('d-none')
    let province_id = $("#select-province option:selected").val()
    let type = $(this).data('type')
    rekapitulasiProvinsi(province_id, type)
});

function rekapitulasiProvinsi(province_id, type) {
    if (type == 'budget') {
        $(".th-budget").show()
        $(".th-participant").hide()
        $(".th-companion").hide()
    } else if (type == 'participant') {
        $(".th-budget").hide()
        $(".th-participant").show()
        $(".th-companion").hide()
    } else if (type == 'companion') {
        $(".th-budget").hide()
        $(".th-participant").hide()
        $(".th-companion").show()
    }
    $.ajax({
        url: `${api_url}budged_performance/${province_id}/by_province`,
        type: 'GET',
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", "Bearer " + token)
        },
        success: function(result) {
            // console.log(result.data)
		    $("#rekapitulasi-provinsi").empty()
            $.each(result.data, function(index, value) {
                budget_realizaition = value.budged_performance.budged_realization1 + value.budged_performance.budged_realization2
                realizaiton_participant = value.budged_performance.realization_participant1 + value.budged_performance.realization_participant2
                realizaiton_companion = value.budged_performance.realization_companion1 + value.budged_performance.realization_companion2
                if (type == 'budget') {
                    append = `<tr>
	                    <td class="text-center">${index + 1}.</td>
	                    <td>${value.province.province}</td>
	                    <td>${value.name}</td>
                        <td>${rupiah(value.budged_performance.budged)}</td>
                        <td>${rupiah(budget_realizaition)}</td>
                        <td>${(budget_realizaition * 100 / value.budged_performance.budged).toFixed(2)}%</td>
                    </tr>`
                } else if (type == 'participant') {
                    append = `<tr>
	                    <td class="text-center">${index + 1}.</td>
	                    <td>${value.province.province}</td>
	                    <td>${value.name}</td>
                        <td>${value.budged_performance.target_participant}</td>
                        <td>${realizaiton_participant}</td>
                        <td>${(realizaiton_participant * 100 / value.budged_performance.target_participant).toFixed(2)}%</td>
                    </tr>`
                } else if (type == 'companion') {
                    append = `<tr>
	                    <td class="text-center">${index + 1}.</td>
	                    <td>${value.province.province}</td>
	                    <td>${value.name}</td>
                        <td>${value.budged_performance.target_companion}</td>
                        <td>${realizaiton_companion}</td>
                        <td>${(realizaiton_companion * 100 / value.budged_performance.target_companion).toFixed(2)}%</td>
                    </tr>`
                }
	            $("#rekapitulasi-provinsi").append(append)
            })
            $("#total-rekapitulasi").html(`Jumlah Provinsi/ Kab/ Kota Penerima DAK: ${result.data.length}`)
        }
    })
}

$.ajax({
    url: `${api_url}budged_performance/budged_performance_by_province`,
    type: 'GET',
    beforeSend: function(xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + token)
    },
    success: function(result) {
        // console.log(result.data)
        let pagu_anggaran = 0, realisasi_anggaran = 0
        let target_peserta = 0, realisasi_peserta = 0
        let target_pendamping = 0, realisasi_pendamping = 0
        $.each(result.data, function(index, value) {
            pagu_anggaran += parseInt(value.total_budged)
            realisasi_anggaran += parseInt(value.total_budged_realization)
            target_peserta += parseInt(value.total_target_participant)
            realisasi_peserta += parseInt(value.total_participant_realization)
            target_pendamping += parseInt(value.total_target_companion)
            realisasi_pendamping += parseInt(value.total_companion_realization)
            
            append = `<tr>
        		<td class="text-center">${index + 1}.</td>
        		<td class="text-truncate">${value.province}</td>
        		<td class="text-truncate">${rupiah(value.total_budged)}</td>
        		<td class="text-truncate">${rupiah(value.total_budged_realization)}</td>
        		<td class="text-truncate">${convert(value.total_target_participant)}</td>
        		<td class="text-truncate">${convert(value.total_participant_realization)}</td>
        		<td class="text-truncate">${convert(value.total_target_companion)}</td>
        		<td class="text-truncate">${convert(value.total_companion_realization)}</td>
        	</tr>`
            $('#peserta-provinsi').append(append)
        })

        let total = `<tr>
    		<td class="text-center" colspan="2"><b>Total</b></td>
    		<td class="text-truncate"><b>${rupiah(pagu_anggaran)}</b></td>
    		<td class="text-truncate"><b>${rupiah(realisasi_anggaran)}</b></td>
    		<td class="text-truncate"><b>${convert(target_peserta)}</b></td>
    		<td class="text-truncate"><b>${convert(realisasi_peserta)}</b></td>
    		<td class="text-truncate"><b>${convert(target_pendamping)}</b></td>
    		<td class="text-truncate"><b>${convert(realisasi_pendamping)}</b></td>
    	</tr>`
        $('#peserta-provinsi').append(total)
    }
})

$.ajax({
    url: `${api_url}report/total_participant_by_gender`,
    type: 'GET',
    beforeSend: function(xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + token)
    },
    success: function(result) {
        let value = result.data
        let dataChart = []
        dataChart.push(value.total_laki_laki)
        dataChart.push(value.total_perempuan)
        new Chart(document.getElementById('chartGender'), {
            type: 'doughnut',
            data: {
                labels: ['Laki-Laki', 'Perempuan'],
                datasets: [{
                    label: 'Jenis Kelamin',
                    data: dataChart,
                    backgroundColor: [
                        'rgb(231, 76, 60)',
                        'rgb(241, 196, 15)'
                    ],
                    hoverOffset: 4
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        })
    }
})

$.ajax({
    url: `${api_url}report/total_participant_by_religion`,
    type: 'GET',
    beforeSend: function(xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + token)
    },
    success: function(result) {
        let labelChart = [],
            dataChart = []
        $.each(result.data, function(index, value) {
            labelChart.push(value.religion)
            dataChart.push(value.total)
        })
        new Chart(document.getElementById('chartReligion'), {
            type: 'doughnut',
            data: {
                labels: labelChart,
                datasets: [{
                    label: 'Agama',
                    data: dataChart,
                    backgroundColor: [
                        'rgb(231, 76, 60)',
                        'rgb(241, 196, 15)',
                        'rgb(46, 204, 113)',
                        'rgb(52, 152, 219)',
                        'rgb(155, 89, 182)',
                        'rgb(52, 73, 94)'
                    ],
                    hoverOffset: 4
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        })
    }
})

$.ajax({
    url: `${api_url}report/total_participant_by_training_type`,
    type: 'GET',
    beforeSend: function(xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + token)
    },
    success: function(result) {
        let value = result.data
        let labelChart = [],
            dataChart = []
        $.each(result.data, function(index, value) {
            labelChart.push(value.training_type)
            dataChart.push(value.total)
        })
        new Chart(document.getElementById('chartTraining'), {
            type: 'bar',
            data: {
                labels: labelChart,
                datasets: [{
                    label: 'Total Peserta',
                    data: dataChart,
                    backgroundColor: [
                        'rgb(231, 76, 60)',
                        'rgb(241, 196, 15)',
                        'rgb(46, 204, 113)',
                        'rgb(52, 152, 219)',
                        'rgb(155, 89, 182)',
                        'rgb(52, 73, 94)'
                    ],
                    hoverOffset: 4
                }]
            },
            options: {
                indexAxis: 'y',
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        })
    }
})

$.ajax({
    url: `${api_url}report/total_participant_by_education`,
    type: 'GET',
    beforeSend: function(xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + token)
    },
    success: function(result) {
    	// console.log(result)
        let value = result.data
        let labelChart = [],
            dataChart = []
        $.each(result.data, function(index, value) {
            labelChart.push(value.education)
            dataChart.push(value.total)
        })
        new Chart(document.getElementById('chartEducation'), {
            type: 'bar',
            data: {
                labels: labelChart,
                datasets: [{
                    label: 'Total Peserta',
                    data: dataChart,
                    backgroundColor: [
                        'rgb(231, 76, 60)',
                        'rgb(241, 196, 15)',
                        'rgb(46, 204, 113)',
                        'rgb(52, 152, 219)',
                        'rgb(155, 89, 182)',
                        'rgb(52, 73, 94)',
                        'rgb(231, 76, 60)'
                    ],
                    hoverOffset: 4
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        })
    }
})

$.ajax({
    url: `${api_url}report/total_participant_by_business_status`,
    type: 'GET',
    beforeSend: function(xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + token)
    },
    success: function(result) {
        let labelChart = [],
            dataChart = []
        $.each(result.data, function(index, value) {
            labelChart.push(value.business_status)
            dataChart.push(value.total)
        })
        new Chart(document.getElementById('chartStatus'), {
            type: 'doughnut',
            data: {
                labels: labelChart,
                datasets: [{
                    label: 'Status Usaha',
                    data: dataChart,
                    backgroundColor: [
                        'rgb(231, 76, 60)',
                        'rgb(241, 196, 15)',
                        'rgb(46, 204, 113)'
                    ],
                    hoverOffset: 4
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        })
    }
})

$.ajax({
    url: `${api_url}report/klasifikasi_asset_umkm`,
    type: 'GET',
    beforeSend: function(xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + token)
    },
    success: function(result) {
    	// console.log(result)
        let value = result.data
        let labelChart = ['< 1 Miliar (Usaha Mikro)', '1 Miliar - 5 Miliar (Usaha Kecil)', '5 Miliar - 10 Miliar (Usaha Menengah)'],
            dataChart = []
        dataChart.push(value.usaha_kecil)
        dataChart.push(value.usaha_menengah)
        dataChart.push(value.usaha_mikro)
        new Chart(document.getElementById('chartAsset'), {
            type: 'bar',
            data: {
                labels: labelChart,
                datasets: [{
                    label: 'Total Peserta',
                    data: dataChart,
                    backgroundColor: [
                        'rgb(231, 76, 60)',
                        'rgb(241, 196, 15)',
                        'rgb(46, 204, 113)',
                    ],
                    hoverOffset: 4
                }]
            },
            options: {
                indexAxis: 'y',
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        })
    }
})

$.ajax({
    url: `${api_url}report/klasifikasi_omset_umkm`,
    type: 'GET',
    beforeSend: function(xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + token)
    },
    success: function(result) {
    	// console.log(result)
        let value = result.data
        let labelChart = ['< 2 Miliar (Usaha Mikro)', '2 Miliar - 15 Miliar (Usaha Kecil)', '15 Miliar - 50 Miliar (Usaha Menengah)'],
            dataChart = []
        dataChart.push(value.usaha_kecil)
        dataChart.push(value.usaha_menengah)
        dataChart.push(value.usaha_mikro)
        new Chart(document.getElementById('chartOmset'), {
            type: 'bar',
            data: {
                labels: labelChart,
                datasets: [{
                    label: 'Total Peserta',
                    data: dataChart,
                    backgroundColor: [
                        'rgb(231, 76, 60)',
                        'rgb(241, 196, 15)',
                        'rgb(46, 204, 113)',
                    ],
                    hoverOffset: 4
                }]
            },
            options: {
                indexAxis: 'y',
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        })
    }
})

$.ajax({
    url: `${api_url}report/total_participant_problem`,
    type: 'GET',
    beforeSend: function(xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + token)
    },
    success: function(result) {
        let value = result.data
        let labelChart = [],
            dataChart = []
        $.each(result.data, function(index, value) {
            labelChart.push(value.participant_problem)
            dataChart.push(value.total)
        })
        new Chart(document.getElementById('chartProblem'), {
            type: 'bar',
            data: {
                labels: labelChart,
                datasets: [{
                    label: 'Total Peserta',
                    data: dataChart,
                    backgroundColor: [
                        'rgb(231, 76, 60)',
                        'rgb(241, 196, 15)',
                        'rgb(46, 204, 113)',
                        'rgb(52, 152, 219)',
                        'rgb(155, 89, 182)',
                        'rgb(52, 73, 94)',
                    ],
                    hoverOffset: 4
                }]
            },
            options: {
                indexAxis: 'y',
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        })
    }
})
