@extends('layouts/app')

@section('title','Edit Peserta')

@section('content')
	<div class="container">
		<div class="d-flex justify-content-between align-items-center mb-2">
			<h4>Edit Peserta</h4>
		</div>
		<div class="none" id="card">
			<div class="row">
				<div class="col-lg-4">
					<div class="card card-custom mb-4">
						<div class="card-header">
							<h6 class="mb-0">Foto Peserta</h6>
						</div>
						<div class="card-body d-flex flex-column">
							<img src="{{asset('assets/images/user.png')}}" class="mx-auto rounded-circle mb-3 profile_photo border" width="150" data-toggle="modal" data-target="#modal-photo" role="button">
							<div class="text-center">
								<label class="btn btn-link btn-block" for="profile_photo">Upload Foto</label>
								<input type="file" class="none" id="profile_photo" accept="image/*">
								<small class="text-danger hide" id="profile_photo-feedback"></small>
							</div>
						</div>
					</div>
				</div>
				<form class="col-lg-8">
					<div class="card card-custom mb-4">
						<div class="card-header">
							<h6 class="mb-0">Profil Peserta</h6>
						</div>
			            <div class="card-body">
			                <div class="form-group row">
			                    <label for="name" class="col-form-label col-md-4">Nama Lengkap</label>
			                    <div class="col-md-8">
			                        <input class="form-control" id="name">
			                        <div class="invalid-feedback" id="name-feedback"></div>
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label for="id_number" class="col-form-label col-md-4">Nomor Induk Kependudukan (NIK)</label>
			                    <div class="col-md-8">
			                        <input type="tel" class="form-control" id="id_number" minlength="16" maxlength="16">
			                        <div class="invalid-feedback" id="id_number-feedback"></div>
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label for="gender" class="col-form-label col-md-4">Jenis Kelamin</label>
			                    <div class="col-md-8">
			                    	<select class="custom-select" id="gender" role="button">
			                    		<option disabled selected>Pilih</option>
			                    		<option value="laki-laki">Laki-Laki</option>
			                    		<option value="perempuan">Perempuan</option>
			                    	</select>
			                        <div class="invalid-feedback" id="gender-feedback"></div>
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label for="place_birth" class="col-form-label col-md-4">Tempat Lahir</label>
			                    <div class="col-md-8">
			                        <input class="form-control" id="place_birth">
			                        <div class="invalid-feedback" id="place_birth-feedback"></div>
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label for="date_birth" class="col-form-label col-md-4">Tanggal Lahir</label>
			                    <div class="col-md-8">
			                        <input type="date" class="form-control" id="date_birth">
			                        <div class="invalid-feedback" id="date_birth-feedback"></div>
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label for="religion_id" class="col-form-label col-md-4">Agama</label>
			                    <div class="col-md-8">
			                    	<select class="custom-select" id="religion_id" role="button">
			                    		<option disabled selected>Pilih</option>
			                    	</select>
			                        <div class="invalid-feedback" id="religion_id-feedback"></div>
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label for="education_id" class="col-form-label col-md-4">Pendidikan Terakhir</label>
			                    <div class="col-md-8">
			                    	<select class="custom-select" id="education_id" role="button">
			                    		<option disabled selected>Pilih</option>
			                    	</select>
			                        <div class="invalid-feedback" id="education_id-feedback"></div>
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label for="address_participant" class="col-form-label col-md-4">Alamat Rumah</label>
			                    <div class="col-md-8">
			                        <textarea class="form-control form-control-sm" id="address_participant" rows="3"></textarea>
			                        <div class="invalid-feedback" id="address_participant-feedback"></div>
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label for="province_id" class="col-form-label col-md-4">Provinsi</label>
			                    <div class="col-md-8">
			                    	<select class="custom-select" id="province_id" role="button">
			                    		<option disabled selected>Pilih</option>
			                    	</select>
			                        <div class="invalid-feedback" id="province_id-feedback"></div>
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label for="districts_city_id" class="col-form-label col-md-4">Kab/Kota</label>
			                    <div class="col-md-8">
			                    	<select class="custom-select" id="districts_city_id" role="button" disabled>
			                    		<option disabled selected>Pilih</option>
			                    	</select>
			                        <div class="invalid-feedback" id="districts_city_id-feedback"></div>
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label for="phone_number" class="col-form-label col-md-4">Telp/HP</label>
			                    <div class="col-md-8">
			                        <input type="tel" class="form-control" id="phone_number" min="10" max="20">
			                        <div class="invalid-feedback" id="phone_number-feedback"></div>
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label for="email" class="col-form-label col-md-4">Email</label>
			                    <div class="col-md-8">
			                        <input type="email" class="form-control" id="email">
			                        <div class="invalid-feedback" id="email-feedback"></div>
			                    </div>
			                </div>
			            </div>
			        </div>
					<div class="card card-custom mb-4">
						<div class="card-header">
							<h6 class="mb-0">Data Koperasi atau UMKM Peserta</h6>
						</div>
			            <div class="card-body">
			                <div class="form-group row">
			                    <label for="business_status_id" class="col-form-label col-md-4">Status Usaha</label>
			                    <div class="col-md-8">
			                    	<select class="custom-select" id="business_status_id" role="button">
			                    		<option disabled selected>Pilih</option>
			                    	</select>
			                        <div class="invalid-feedback" id="business_status_id-feedback"></div>
			                    </div>
			                </div>
			            </div>
			        </div>
					<div class="card card-custom mb-4 none" id="umkm">
						<div class="card-header">
							<h6 class="mb-0">Data UMKM</h6>
						</div>
			            <div class="card-body">
			                <div class="form-group row">
			                    <label for="name_umkm" class="col-form-label col-md-4">Nama Usaha</label>
			                    <div class="col-md-8">
			                        <input class="form-control" id="name_umkm">
			                        <div class="invalid-feedback" id="name_umkm-feedback"></div>
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label for="address_umkm" class="col-form-label col-md-4">Alamat Usaha</label>
			                    <div class="col-md-8">
			                        <textarea class="form-control form-control-sm" id="address_umkm" rows="3"></textarea>
			                        <div class="invalid-feedback" id="address_umkm-feedback"></div>
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label for="business_sector_id" class="col-form-label col-md-4">Sektor Usaha</label>
			                    <div class="col-md-8">
			                    	<select class="custom-select" id="business_sector_id" role="button">
			                    		<option disabled selected>Pilih</option>
			                    	</select>
			                        <div class="invalid-feedback" id="business_sector_id-feedback"></div>
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label for="business_field_umkm_id" class="col-form-label col-md-4">Bidang Usaha</label>
			                    <div class="col-md-8">
			                    	<select class="custom-select" id="business_field_umkm_id" role="button">
			                    		<option disabled selected>Pilih</option>
			                    	</select>
			                        <div class="invalid-feedback" id="business_field_umkm_id-feedback"></div>
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label for="date_establishment_umkm" class="col-form-label col-md-4">Tanggal Pendirian Usaha</label>
			                    <div class="col-md-8">
			                        <input type="date" class="form-control" id="date_establishment_umkm">
			                        <div class="invalid-feedback" id="date_establishment_umkm-feedback"></div>
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label for="npwp_umkm" class="col-form-label col-md-4">NPWP Usaha</label>
			                    <div class="col-md-8">
			                        <input type="tel" class="form-control npwp" id="npwp_umkm" minlength="20" maxlength="20">
			                        <div class="invalid-feedback" id="npwp_umkm-feedback"></div>
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label for="nib_umkm" class="col-form-label col-md-4">Nomor Induk Berusaha (NIB)</label>
			                    <div class="col-md-8">
			                        <input type="tel" class="form-control" id="nib_umkm" maxlength="13">
			                        <div class="invalid-feedback" id="nib_umkm-feedback"></div>
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label for="asset_umkm" class="col-form-label col-md-4">Kekayaan Usaha (Asset) per Tahun</label>
			                    <div class="col-md-8">
									<div class="input-group">
										<div class="input-group-prepend">
											<div class="input-group-text">Rp</div>
										</div>
				                        <input type="tel" class="form-control rupiah" id="asset_umkm">
				                        <div class="invalid-feedback" id="asset_umkm-feedback"></div>
									</div>
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label for="omset_umkm" class="col-form-label col-md-4">Volume Usaha (Omset) per Tahun</label>
			                    <div class="col-md-8">
									<div class="input-group">
										<div class="input-group-prepend">
											<div class="input-group-text">Rp</div>
										</div>
				                        <input type="tel" class="form-control rupiah" id="omset_umkm">
				                        <div class="invalid-feedback" id="omset_umkm-feedback"></div>
									</div>
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label for="number_employees" class="col-form-label col-md-4">Jumlah Karyawan</label>
			                    <div class="col-md-8">
									<div class="input-group">
				                        <input type="number" class="form-control" id="number_employees">
										<div class="input-group-append">
											<div class="input-group-text">Orang</div>
										</div>
				                        <div class="invalid-feedback" id="number_employees-feedback"></div>
									</div>
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label for="production_capacity" class="col-form-label col-md-4">Kapasitas Produksi per Tahun</label>
			                    <div class="col-md-8">
			                        <input class="form-control" id="production_capacity">
			                        <div class="invalid-feedback" id="production_capacity-feedback"></div>
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label for="member_koperasi" class="col-form-label col-md-4">Anggota Koperasi</label>
			                    <div class="col-md-8">
			                    	<select class="custom-select" id="member_koperasi" role="button">
			                    		<option disabled selected>Pilih</option>
			                    		<option value="1">Ya</option>
			                    		<option value="0">Tidak</option>
			                    	</select>
			                        <div class="invalid-feedback" id="member_koperasi-feedback"></div>
			                    </div>
			                </div>
		                </div>
		            </div>
					<div class="card card-custom none mb-4" id="koperasi">
						<div class="card-header">
							<h6 class="mb-0">Data Koperasi</h6>
						</div>
			            <div class="card-body">
			                <div class="form-group row">
			                    <label for="registrasion_number_koperasi" class="col-form-label col-md-4">Nomor Induk Koperasi</label>
			                    <div class="col-md-8">
			                        <input class="form-control" id="registrasion_number_koperasi">
			                        <div class="invalid-feedback" id="registrasion_number_koperasi-feedback"></div>
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label for="name_koperasi" class="col-form-label col-md-4">Nama Koperasi</label>
			                    <div class="col-md-8">
			                        <input class="form-control" id="name_koperasi">
			                        <div class="invalid-feedback" id="name_koperasi-feedback"></div>
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label for="legal_entity_number" class="col-form-label col-md-4">Nomor Badan Hukum</label>
			                    <div class="col-md-8">
			                        <input class="form-control" id="legal_entity_number">
			                        <div class="invalid-feedback" id="legal_entity_number-feedback"></div>
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label for="date_establishment_koperasi" class="col-form-label col-md-4">Tanggal Pendirian Koperasi</label>
			                    <div class="col-md-8">
			                        <input type="date" class="form-control" id="date_establishment_koperasi">
			                        <div class="invalid-feedback" id="date_establishment_koperasi-feedback"></div>
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label for="address_koperasi" class="col-form-label col-md-4">Alamat Koperasi</label>
			                    <div class="col-md-8">
			                        <textarea class="form-control form-control-sm" id="address_koperasi" rows="3"></textarea>
			                        <div class="invalid-feedback" id="address_koperasi-feedback"></div>
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label for="type_koperasi_id" class="col-form-label col-md-4">Jenis Koperasi</label>
			                    <div class="col-md-8">
			                    	<select class="custom-select" id="type_koperasi_id" role="button">
			                    		<option disabled selected>Pilih</option>
			                    	</select>
			                        <div class="invalid-feedback" id="type_koperasi_id-feedback"></div>
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label for="group_koperasi_id" class="col-form-label col-md-4">Kelompok Koperasi</label>
			                    <div class="col-md-8">
			                    	<select class="custom-select" id="group_koperasi_id" role="button">
			                    		<option disabled selected>Pilih</option>
			                    	</select>
			                        <div class="invalid-feedback" id="group_koperasi_id-feedback"></div>
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label for="koperasi_sector_id" class="col-form-label col-md-4">Sektor Usaha</label>
			                    <div class="col-md-8">
			                    	<select class="custom-select" id="koperasi_sector_id" role="button">
			                    		<option disabled selected>Pilih</option>
			                    	</select>
			                        <div class="invalid-feedback" id="koperasi_sector_id-feedback"></div>
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label for="fostered_koperasi_id" class="col-form-label col-md-4">Koperasi Binaan</label>
			                    <div class="col-md-8">
			                    	<select class="custom-select" id="fostered_koperasi_id" role="button">
			                    		<option disabled selected>Pilih</option>
			                    	</select>
			                        <div class="invalid-feedback" id="fostered_koperasi_id-feedback"></div>
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label for="position_koperasi_id" class="col-form-label col-md-4">Jabatan di Koperasi</label>
			                    <div class="col-md-8">
			                    	<select class="custom-select" id="position_koperasi_id" role="button">
			                    		<option disabled selected>Pilih</option>
			                    	</select>
			                        <div class="invalid-feedback" id="position_koperasi_id-feedback"></div>
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label for="npwp_koperasi" class="col-form-label col-md-4">NPWP Koperasi</label>
			                    <div class="col-md-8">
			                        <input class="form-control npwp" id="npwp_koperasi" minlength="20" maxlength="20">
			                        <div class="invalid-feedback" id="npwp_koperasi-feedback"></div>
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label for="last_rat" class="col-form-label col-md-4">RAT Terakhir</label>
			                    <div class="col-md-8">
									<div class="input-group">
										<div class="input-group-prepend">
											<div class="input-group-text">Tahun</div>
										</div>
				                        <input type="tel" class="form-control" id="last_rat">
				                        <div class="invalid-feedback" id="last_rat-feedback"></div>
									</div>
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label for="nik_status" class="col-form-label col-md-4">Status NIK</label>
			                    <div class="col-md-8">
			                    	<select class="custom-select" id="nik_status" role="button">
			                    		<option disabled selected>Pilih</option>
			                    		<option value="sudah bersertifikat">Sudah Bersertifikat</option>
			                    		<option value="belum bersertifikat">Belum Bersertifikat</option>
			                    	</select>
			                        <div class="invalid-feedback" id="nik_status-feedback"></div>
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label for="form_koperasi_id" class="col-form-label col-md-4">Bentuk Koperasi</label>
			                    <div class="col-md-8">
			                    	<select class="custom-select" id="form_koperasi_id" role="button">
			                    		<option disabled selected>Pilih</option>
			                    	</select>
			                        <div class="invalid-feedback" id="form_koperasi_id-feedback"></div>
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label for="asset_koperasi" class="col-form-label col-md-4">Asset Koperasi</label>
			                    <div class="col-md-8">
									<div class="input-group">
										<div class="input-group-prepend">
											<div class="input-group-text">Rp</div>
										</div>
				                        <input type="tel" class="form-control rupiah" id="asset_koperasi">
				                        <div class="invalid-feedback" id="asset_koperasi-feedback"></div>
									</div>
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label for="omset_koperasi" class="col-form-label col-md-4">Omzet Koperasi</label>
			                    <div class="col-md-8">
									<div class="input-group">
										<div class="input-group-prepend">
											<div class="input-group-text">Rp</div>
										</div>
				                        <input type="tel" class="form-control rupiah" id="omset_koperasi">
				                        <div class="invalid-feedback" id="omset_koperasi-feedback"></div>
									</div>
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label for="shu_koperasi" class="col-form-label col-md-4">SHU Koperasi</label>
			                    <div class="col-md-8">
									<div class="input-group">
										<div class="input-group-prepend">
											<div class="input-group-text">Rp</div>
										</div>
				                        <input type="tel" class="form-control rupiah" id="shu_koperasi">
				                        <div class="invalid-feedback" id="shu_koperasi-feedback"></div>
									</div>
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label for="total_member_koperasi_men" class="col-form-label col-md-4">Jumlah Anggota Laki-Laki</label>
			                    <div class="col-md-8">
									<div class="input-group">
				                        <input type="number" class="form-control" id="total_member_koperasi_men">
										<div class="input-group-append">
											<div class="input-group-text">Orang</div>
										</div>
				                        <div class="invalid-feedback" id="total_member_koperasi_men-feedback"></div>
				                    </div>
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label for="total_member_koperasi_women" class="col-form-label col-md-4">Jumlah Anggota Perempuan</label>
			                    <div class="col-md-8">
			                    	<div class="input-group">
				                        <input type="number" class="form-control" id="total_member_koperasi_women">
										<div class="input-group-append">
											<div class="input-group-text">Orang</div>
										</div>
				                        <div class="invalid-feedback" id="total_member_koperasi_women-feedback"></div>
				                    </div>
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label for="total_employee_koperasi_men" class="col-form-label col-md-4">Jumlah Karyawan Laki-Laki</label>
			                    <div class="col-md-8">
			                    	<div class="input-group">
				                        <input type="number" class="form-control" id="total_employee_koperasi_men">
										<div class="input-group-append">
											<div class="input-group-text">Orang</div>
										</div>
				                        <div class="invalid-feedback" id="total_employee_koperasi_men-feedback"></div>
				                    </div>
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label for="total_employee_koperasi_women" class="col-form-label col-md-4">Jumlah Karyawan Perempuan</label>
			                    <div class="col-md-8">
			                    	<div class="input-group">
				                        <input type="number" class="form-control" id="total_employee_koperasi_women">
										<div class="input-group-append">
											<div class="input-group-text">Orang</div>
										</div>
				                        <div class="invalid-feedback" id="total_employee_koperasi_women-feedback"></div>
				                    </div>
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label class="col-form-label col-md-4">Jangkauan Pemasaran Produk/Layanan Koperasi</label>
			                    <div class="col-md-8">
			                    	<div id="marketing_reach_koperasi_id"></div>
			                        <input class="form-control mt-2 none" id="marketing_reach_koperasi_optional" placeholder="Lokasi">
			                        <div class="invalid-feedback" id="marketing_reach_koperasi_id-feedback"></div>
			                    </div>
			                </div>
			            </div>
			        </div>
					<div class="card card-custom none mb-4" id="digitalisasi_usaha">
						<div class="card-header">
							<h6 class="mb-0">Digitalisasi Usaha</h6>
						</div>
			            <div class="card-body">
			            	<div class="form-group row">
					            <label class="col-form-label col-md-4">Email Usaha</label>
					            <div class="col-md-8">
					            	<select class="custom-select" id="business_email" role="button">
					            		<option disabled selected>Pilih</option>
					            		<option value="yes">Ada</option>
					            		<option value="no">Tidak Ada</option>
					            	</select>
			                        <input type="email" class="form-control mt-2 none" id="business_email_yes">
					                <div class="invalid-feedback" id="business_email-feedback"></div>
					            </div>
					        </div>
					        <div class="form-group row">
					            <label class="col-form-label col-md-4">Website Usaha</label>
					            <div class="col-md-8">
					            	<select class="custom-select" id="business_website" role="button">
					            		<option disabled selected>Pilih</option>
					            		<option value="yes">Ada</option>
					            		<option value="no">Tidak Ada</option>
					            	</select>
			                        <input class="form-control mt-2 none" id="business_website_yes">
					                <div class="invalid-feedback" id="business_website-feedback"></div>
					            </div>
					        </div>
					        <div class="form-group row">
					            <label class="col-form-label col-md-4">Media Sosial Usaha</label>
					            <div class="col-md-8">
					            	<div id="social_media_id"></div>
					                <div class="invalid-feedback d-block" id="social_media_id-feedback"></div>
					            </div>
					        </div>
					        <div class="form-group row">
					            <label class="col-form-label col-md-4">Marketplace</label>
					            <div class="col-md-8">
					            	<div id="marketplace_id"></div>
					                <div class="invalid-feedback" id="marketplace_id-feedback"></div>
					            </div>
					        </div>
			            </div>
			        </div>
					<div class="card card-custom none mb-4" id="pembiayaan_usaha">
						<div class="card-header">
							<h6 class="mb-0">Pembiayaan Usaha</h6>
						</div>
			            <div class="card-body">
			            	<div class="form-group row">
			                    <label for="banking_credit" class="col-form-label col-md-4">Apakah pernah mengakses kredit perbankan/non-perbankan?</label>
			                    <div class="col-md-8">
			                    	<select class="custom-select" id="banking_credit" role="button">
			                    		<option disabled selected>Pilih</option>
			                    		<option value="1">Ya</option>
			                    		<option value="0">Tidak</option>
			                    	</select>
			                        <div class="invalid-feedback" id="banking_credit-feedback"></div>
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label for="savings" class="col-form-label col-md-4">Apakah memiliki tabungan?</label>
			                    <div class="col-md-8">
			                    	<select class="custom-select" id="savings" role="button">
			                    		<option disabled selected>Pilih</option>
			                    		<option value="1">Ya</option>
			                    		<option value="0">Tidak</option>
			                    	</select>
			                        <div class="invalid-feedback" id="savings-feedback"></div>
			                    </div>
			                </div>
			            </div>
			        </div>
					<div class="card card-custom mb-4 none" id="transformasi_usaha">
						<div class="card-header">
							<h6 class="mb-0">Transformasi Usaha</h6>
						</div>
			            <div class="card-body">
			            	<div class="form-group row">
					            <label class="col-form-label col-md-4">Perizinan Usaha</label>
					            <div class="col-md-8">
					            	<div id="business_licensing_id"></div>
					                <div class="invalid-feedback" id="business_licensing_id-feedback"></div>
					            </div>
					        </div>
					        <div class="form-group row">
					            <label class="col-form-label col-md-4">Sertifikasi Produk/Usaha</label>
					            <div class="col-md-8">
					            	<div id="business_certificate_id"></div>
					                <div class="invalid-feedback" id="business_certificate_id-feedback"></div>
					            </div>
					        </div>
			            </div>
			        </div>
					<div class="card card-custom mb-4 none" id="rantai_pasok_ekspor">
						<div class="card-header">
							<h6 class="mb-0">Rantai Pasok dan Ekspor</h6>
						</div>
			            <div class="card-body">
			                <div class="form-group row">
			                    <label class="col-form-label col-md-4">Jangkauan Pemasaran Produk/Layanan Usaha</label>
			                    <div class="col-md-8">
			                    	<div id="marketing_reach_umkm_id"></div>
			                        <input class="form-control mt-2 none" id="marketing_reach_umkm_optional" placeholder="Lokasi">
			                        <div class="invalid-feedback" id="marketing_reach_umkm_id-feedback"></div>
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label for="export" class="col-form-label col-md-4">Apakah Pernah Melakukan Ekspor?</label>
			                    <div class="col-md-8">
			                    	<select class="custom-select" id="export" role="button">
			                    		<option disabled selected>Pilih</option>
			                    		<option value="1">Ya</option>
			                    		<option value="0">Tidak</option>
			                    	</select>
			                    	<select class="custom-select mt-2 none" id="export_yes" role="button">
			                    		<option disabled selected>Pilih</option>
			                    		<option value="pengiriman forwarder">Pengiriman Forwarder</option>
			                    		<option value="pengiriman langsung">Pengiriman Langsung</option>
			                    	</select>
			                        <div class="invalid-feedback" id="export-feedback"></div>
			                    </div>
			                </div>
			                <div class="none" id="export_form">
				                <div class="form-group row">
				                    <label for="export_destination" class="col-form-label col-md-4">Negara Tujuan</label>
				                    <div class="col-md-8">
				                        <input class="form-control" id="export_destination">
				                        <div class="invalid-feedback" id="export_destination-feedback"></div>
				                    </div>
				                </div>
				                <div class="form-group row">
				                    <label for="export_volume" class="col-form-label col-md-4">Volume Ekspor per Tahun</label>
				                    <div class="col-md-8">
				                        <input class="form-control" id="export_volume">
				                        <div class="invalid-feedback" id="export_volume-feedback"></div>
				                    </div>
				                </div>
				                <div class="form-group row">
				                    <label for="export_value" class="col-form-label col-md-4">Nilai Ekspor</label>
				                    <div class="col-md-8">
				                    	<div class="input-group">
											<div class="input-group-prepend">
												<div class="input-group-text">Rp</div>
											</div>
					                        <input type="tel" class="form-control rupiah" id="export_value">
					                        <div class="invalid-feedback" id="export_value-feedback"></div>
					                    </div>
				                    </div>
				                </div>
			                </div>
			                <div class="form-group row">
			                    <label for="product_supply" class="col-form-label col-md-4">Apakah bapak/ibu mensuplai produk sebagai bahan baku pada usaha lain?</label>
			                    <div class="col-md-8">
			                    	<select class="custom-select" id="product_supply" role="button">
			                    		<option disabled selected>Pilih</option>
			                    		<option value="1">Ya</option>
			                    		<option value="0">Tidak</option>
			                    	</select>
			                        <div class="invalid-feedback" id="product_supply-feedback"></div>
			                    </div>
			                </div>
			            </div>
			        </div>
					<div class="card card-custom mb-4 none" id="kemitraan_usaha">
						<div class="card-header">
							<h6 class="mb-0">Kemitraan Usaha</h6>
						</div>
			            <div class="card-body">
			                <div class="form-group row">
					            <label class="col-form-label col-md-4">Apakah melakukan kemitraan dengan lembaga lain?</label>
					            <div class="col-md-8">
					            	<select class="custom-select" id="partnership" role="button">
					            		<option disabled selected>Pilih</option>
					            		<option value="1">Ya</option>
					            		<option value="0">Tidak</option>
					            	</select>
					                <div class="invalid-feedback" id="partnership-feedback"></div>
					            </div>
					        </div>
		                </div>
			        </div>
					<div class="card card-custom mb-4 none" id="calon">
						<div class="card-header">
							<h6 class="mb-0">Calon Wirausaha</h6>
						</div>
			            <div class="card-body">
			                <div class="form-group row">
			                    <label for="business_idea" class="col-form-label col-md-4">Ide Bisnis</label>
			                    <div class="col-md-8">
			                        <input class="form-control" id="business_idea">
			                        <div class="invalid-feedback" id="business_idea-feedback"></div>
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label for="entrepreneur_address" class="col-form-label col-md-4">Alamat Calon Usaha</label>
			                    <div class="col-md-8">
			                        <textarea class="form-control form-control-sm" id="entrepreneur_address" rows="3"></textarea>
			                        <div class="invalid-feedback" id="entrepreneur_address-feedback"></div>
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label for="entrepreneur_sector_id" class="col-form-label col-md-4">Sektor Calon Usaha</label>
			                    <div class="col-md-8">
			                    	<select class="custom-select" id="entrepreneur_sector_id" role="button">
			                    		<option disabled selected>Pilih</option>
			                    	</select>
			                        <div class="invalid-feedback" id="entrepreneur_sector_id-feedback"></div>
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label for="business_field_entrepreneur_id" class="col-form-label col-md-4">Bidang Calon Usaha</label>
			                    <div class="col-md-8">
			                    	<select class="custom-select" id="business_field_entrepreneur_id" role="button">
			                    		<option disabled selected>Pilih</option>
			                    	</select>
			                        <div class="invalid-feedback" id="business_field_entrepreneur_id-feedback"></div>
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label for="accompaniment_entrepreneur_id" class="col-form-label col-md-4">Pendamping yang didapatkan</label>
			                    <div class="col-md-8">
			                    	<select class="custom-select" id="accompaniment_entrepreneur_id" role="button">
			                    		<option disabled selected>Pilih</option>
			                    	</select>
			                        <div class="invalid-feedback" id="accompaniment_entrepreneur_id-feedback"></div>
			                    </div>
			                </div>
			            </div>
			        </div>
					<div class="card card-custom">
						<div class="card-header">
							<h6 class="mb-0">Lain-Lainnya</h6>
						</div>
			            <div class="card-body">
			                <div class="form-group row">
			                    <label for="m1" class="col-form-label col-md-4">Permasalahan yang dihadapi dalam pengembangan usaha</label>
			                    <div class="col-md-8">
			                    	<select class="custom-select" id="m1" role="button">
			                    		<option disabled selected>Pilih</option>
			                    	</select>
			                        <input class="form-control none mt-2" id="other_m1">
			                        <div class="invalid-feedback" id="m1-feedback"></div>
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label for="m2_id" class="col-form-label col-md-4">Kebutuhan Diklat</label>
			                    <div class="col-md-8">
			                    	<select class="custom-select" id="m2_id" role="button">
			                    		<option disabled selected>Pilih</option>
			                    	</select>
			                        <div class="invalid-feedback" id="m2_id-feedback"></div>
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label for="m3" class="col-form-label col-md-4">Masukan/Saran</label>
			                    <div class="col-md-8">
			                        <textarea class="form-control form-control-sm" id="m3" rows="3"></textarea>
			                        <div class="invalid-feedback" id="m3-feedback"></div>
			                    </div>
			                </div>
				            <div class="form-group row pt-4">
				                <div class="offset-md-4 col-md-8">
				                    <button class="btn btn-block btn-primary" id="submit">Simpan</button>
				                </div>
				            </div>
			            </div>
			        </div>
				</form>
			</div>
		</div>
		<div id="loading">
			<div class="d-flex flex-column justify-content-center align-items-center state">
				<div class="loader">
					<svg class="circular" viewBox="25 25 50 50">
						<circle class="path-primary" cx="50" cy="50" r="20" fill="none" stroke-width="5" stroke-miterlimit="10"/>
					</svg>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="modal-exist" tabindex="-1" aria-hidden="true">
		<div class="modal-sm modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="d-flex flex-column justify-content-center align-items-center card-body text-center">
					<i class="mdi mdi-close-circle-outline mdi-48px text-danger pr-0"></i>
					<h5>Peserta telah terdaftar di kegiatan!</h5>
				</div>
				<div class="modal-footer border-top-0">
					<button class="btn btn-sm btn-primary btn-block" data-dismiss="modal">Tutup</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal" id="modal-photo" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content bg-transparent border-0 d-block text-center">
				<img src="{{asset('assets/images/user.png')}}" class="profile_photo mb-3" width="400" data-dismiss="modal">
			</div>
		</div>
	</div>
    <!-- <div class="modal fade" id="modal-avatar" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body text-center pb-0">
                    <div class="my-3" id="avatar-form">
                    	<input type="file" class="d-none" id="avatar" accept="image/*">
                    	<label class="avatar-text" for="avatar" style="cursor:pointer;">
                    		<i class="mdi mdi-upload mdi-48px pr-0"></i>
                    		<p class="mb-0">Pilih file</p>
                    	</label>
                    </div>
                    <div id="avatar-preview" class="text-left" style="display:none;">
                    	<i class="mdi mdi-arrow-left mdi-24px text-left" style="cursor:pointer" id="back"></i>
                    </div>
                    <div class="container">
                    	<p class="text-danger none mb-0" id="feedback-file"></p>
                    </div>
                </div>
				<div class="modal-footer border-top-0">
					<div class="btn btn-sm btn-link" data-dismiss="modal">Batal</div>
					<button class="btn btn-sm btn-primary" id="upload" disabled>
	        			<i class="mdi mdi-loading mdi-1x mdi-spin pr-2 none" id="loadingAvatar"></i> Simpan
	        		</button>
				</div>
            </div>
        </div>
    </div> -->
</div>
@endsection

@section('script')
	<script>const id = {{$id}}</script>
	<script src="{{asset('assets/js/photo.js')}}"></script>
	<script src="{{asset('api/dinkot/edit-peserta.js')}}"></script>
@endsection