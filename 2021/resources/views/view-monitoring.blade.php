@extends('layouts/app')

@section('title','Dashboard')

@section('content')
	<div class="container">
		<div class="d-flex justify-content-between align-items-center mb-2">
			<h4 id="title">Monitoring</h4>
			<a class="btn btn-sm btn-outline-primary hide mb-1 ml-3" id="download" target="_blank">Unduh Monitoring Pendamping Lengkap</a>
		</div>
		<div class="none" id="card">
			<div class="row">
				<div class="col-xl-4 col-lg-5">
					<div class="card card-custom mb-3">
						<div class="card-body py-0">
							<h6 class="text-secondary py-3 mb-0" data-toggle="collapse" href="#status" role="button" aria-expanded="false" aria-controls="status">Bulan</h6>
							<div class="collapse show" id="status">
								<ul class="nav nav-pills flex-column text-secondary mb-4" id="bulan"></ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-8 col-lg-7">
					<div class="card card-custom mb-3">
						<div class="card-header">
							<h6 class="mb-0">Data Pendamping</h6>
						</div>
			            <div class="card-body">
			                <div class="row">
			                    <label class="col-form-label col-5">Nama Lengkap</label>
			                    <div class="col-form-label col text-center">:</div>
			                    <div class="col-form-label col-6" id="name"></div>
			                </div>
			                <div class="row">
			                    <label class="col-form-label col-5">Nomor KTP</label>
			                    <div class="col-form-label col text-center">:</div>
			                    <div class="col-form-label col-6" id="no_ktp"></div>
			                </div>
			            </div>
					</div>
					<div class="card card-custom none" id="data">
						<div class="card-header">
							<h6 class="mb-0">Data Monitoring</h6>
						</div>
						<div class="card-body">
			                <div class="row">
			                    <label class="col-form-label col-5">Bulan</label>
			                    <div class="col-form-label col text-center">:</div>
			                    <div class="col-form-label col-6" id="month"></div>
			                </div>
			                <div class="row">
			                    <label class="col-form-label col-5">Tanggal Report</label>
			                    <div class="col-form-label col text-center">:</div>
			                    <div class="col-form-label col-6" id="date"></div>
			                </div>
			                <div class="row">
			                    <label class="col-form-label col-5">Detail Aktivitas</label>
			                    <div class="col-form-label col text-center">:</div>
			                    <pre class="col-form-label col-6" id="activity_details"></pre>
			                </div>
			                <div class="row">
			                    <label class="col-form-label col-5">Output</label>
			                    <div class="col-form-label col text-center">:</div>
			                    <div class="col-form-label col-6" id="output_koperasi"></div>
			                    <div class="col-form-label offset-6 col-6" id="output_umkm"></div>
			                </div>
			            </div>
					</div>
					<div class="none" id="loading-data">
						<div class="d-flex flex-column justify-content-center align-items-center state">
							<div class="loader">
								<svg class="circular" viewBox="25 25 50 50">
									<circle class="path-primary" cx="50" cy="50" r="20" fill="none" stroke-width="5" stroke-miterlimit="10"/>
								</svg>
							</div>
						</div>
					</div>
				</div>
		    </div>
		</div>
		<div id="loading">
			<div class="d-flex flex-column justify-content-center align-items-center state">
				<div class="loader">
					<svg class="circular" viewBox="25 25 50 50">
						<circle class="path-primary" cx="50" cy="50" r="20" fill="none" stroke-width="5" stroke-miterlimit="10"/>
					</svg>
				</div>
			</div>
		</div>
	</div>
	<div class="modal" id="modal-photo" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content bg-transparent border-0 d-block text-center">
				<img src="{{asset('assets/images/user.png')}}" class="profile_photo mb-3" width="400" data-dismiss="modal">
			</div>
		</div>
	</div>
    <div class="modal fade" id="modal-avatar" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body text-center pb-0">
                    <div class="my-3" id="avatar-form">
                    	<input type="file" class="d-none" id="avatar" accept="image/jpg">
                    	<!-- <input type="file" class="d-none" id="avatar" accept="image/jpeg"> -->
                    	<label class="avatar-text" for="avatar" style="cursor:pointer;">
                    		<i class="mdi mdi-upload mdi-48px pr-0"></i>
                    		<p class="mb-0">Pilih file</p>
                    	</label>
                    </div>
                    <div id="avatar-preview" class="text-left" style="display:none;">
                    	<i class="mdi mdi-arrow-left mdi-24px text-left" style="cursor:pointer" id="back"></i>
                    </div>
                    <div class="container">
                    	<p class="text-danger none mb-0" id="feedback-file"></p>
                    </div>
                </div>
				<div class="modal-footer border-top-0">
					<div class="btn btn-sm btn-link" data-dismiss="modal">Batal</div>
					<button class="btn btn-sm btn-primary" id="upload" disabled>
	        			<i class="mdi mdi-loading mdi-1x mdi-spin pr-2 none" id="loadingAvatar"></i> Simpan
	        		</button>
				</div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
	<script>const user_id = {{$id}}</script>
	<script src="{{asset('api/view-monitoring.js')}}"></script>
@endsection