@extends('layouts/app')

@section('title','Dashboard')

@section('content')
	<div class="container">
		<div class="d-flex justify-content-between align-items-center mb-2">
			<h4 id="title">Monitoring</h4>
		</div>
		<div class="none" id="card">
			<div class="row">
				<div class="col-xl-4 col-lg-5">
					<div class="card card-custom mb-3">
						<div class="card-body py-0">
							<h6 class="text-secondary py-3 mb-0" data-toggle="collapse" href="#status" role="button" aria-expanded="false" aria-controls="status">Bulan</h6>
							<div class="collapse show" id="status">
								<ul class="nav nav-pills flex-column text-secondary mb-4" id="bulan">
									
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-8 col-lg-7">
					<form class="card card-custom none" id="card">
						<div class="card-body">
			                <div class="form-group row">
			                    <label for="month" class="col-form-label col-md-4">Bulan</label>
			                    <div class="col-md-8">
			                        <div class="pt-0 pt-sm-2" id="month"></div>
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label for="date" class="col-form-label col-md-4">Tanggal Report</label>
			                    <div class="col-md-8">
			                        <input type="date" class="form-control" id="date">
			                        <div class="invalid-feedback" id="date-feedback"></div>
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label for="activity_details" class="col-form-label col-md-4">Detail Aktivitas</label>
			                    <div class="col-md-8">
			                        <textarea class="form-control form-control-sm" rows="3" id="activity_details"></textarea>
			                        <div class="invalid-feedback" id="activity_details-feedback"></div>
			                    </div>
			                </div>
			                <div class="form-group row">
			                    <label for="output_koperasi" class="col-form-label col-md-4">Output</label>
			                    <div class="col-md-8 mb-3">
									<div class="input-group">
				                        <input type="number" class="form-control" id="output_koperasi">
										<div class="input-group-append">
											<div class="input-group-text">Koperasi</div>
										</div>
				                        <div class="invalid-feedback" id="output_koperasi-feedback"></div>
									</div>
			                    </div>
			                    <div class="offset-md-4 col-md-8">
									<div class="input-group">
				                        <input type="number" class="form-control" id="output_umkm">
										<div class="input-group-append">
											<div class="input-group-text">UMKM</div>
										</div>
				                        <div class="invalid-feedback" id="output_umkm-feedback"></div>
									</div>
			                    </div>
			                </div>
			                <div class="form-group row pt-4">
			                    <div class="offset-md-4 col-md-8">
			                        <button class="btn btn-block btn-primary" id="submit">Simpan</button>
			                    </div>
			                </div>
			            </div>
					</form>
					<div id="loading">
						<div class="d-flex flex-column justify-content-center align-items-center state">
							<div class="loader">
								<svg class="circular" viewBox="25 25 50 50">
									<circle class="path-primary" cx="50" cy="50" r="20" fill="none" stroke-width="5" stroke-miterlimit="10"/>
								</svg>
							</div>
						</div>
					</div>
				</div>
		    </div>
		</div>
		<div id="card-loading">
			<div class="d-flex flex-column justify-content-center align-items-center state">
				<div class="loader">
					<svg class="circular" viewBox="25 25 50 50">
						<circle class="path-primary" cx="50" cy="50" r="20" fill="none" stroke-width="5" stroke-miterlimit="10"/>
					</svg>
				</div>
			</div>
		</div>
	</div>
	<div class="modal" id="modal-photo" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content bg-transparent border-0 d-block text-center">
				<img src="{{asset('assets/images/user.png')}}" class="profile_photo mb-3" width="400" data-dismiss="modal">
			</div>
		</div>
	</div>
    <div class="modal fade" id="modal-avatar" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body text-center pb-0">
                    <div class="my-3" id="avatar-form">
                    	<input type="file" class="d-none" id="avatar" accept="image/jpg">
                    	<!-- <input type="file" class="d-none" id="avatar" accept="image/jpeg"> -->
                    	<label class="avatar-text" for="avatar" style="cursor:pointer;">
                    		<i class="mdi mdi-upload mdi-48px pr-0"></i>
                    		<p class="mb-0">Pilih file</p>
                    	</label>
                    </div>
                    <div id="avatar-preview" class="text-left" style="display:none;">
                    	<i class="mdi mdi-arrow-left mdi-24px text-left" style="cursor:pointer" id="back"></i>
                    </div>
                    <div class="container">
                    	<p class="text-danger none mb-0" id="feedback-file"></p>
                    </div>
                </div>
				<div class="modal-footer border-top-0">
					<div class="btn btn-sm btn-link" data-dismiss="modal">Batal</div>
					<button class="btn btn-sm btn-primary" id="upload" disabled>
	        			<i class="mdi mdi-loading mdi-1x mdi-spin pr-2 none" id="loadingAvatar"></i> Simpan
	        		</button>
				</div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
	<script src="{{asset('api/pendamping/monitoring.js')}}"></script>
@endsection