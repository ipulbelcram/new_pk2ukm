@extends('layouts/app')

@section('title','Profil Peserta')

@section('content')
	<div class="container">
		<div class="d-flex justify-content-between align-items-center mb-2">
			<h4 id="title">Profil Peserta</h4>
			<a href="{{ url('unduh-profile-peserta/'.$id) }}" class="btn btn-sm btn-outline-primary mb-1" target="_blank">Unduh Profil Peserta</a>
		</div>
		<div class="none" id="card">
			<div class="row">
				<div class="col-lg-4">
					<div class="card card-custom mb-4">
						<div class="card-header">
							<h6 class="mb-0">Foto Peserta</h6>
						</div>
						<div class="card-body text-center">
							<img src="{{asset('assets/images/user.png')}}" class="rounded-circle mb-3 profile_photo border" width="150" data-toggle="modal" data-target="#modal-photo" role="button">
							<h5 class="nama"></h5>
							<div class="text-secondary id_number"></div>
						</div>
					</div>
				</div>
				<form class="col-lg-8">
					<div class="card card-custom mb-4">
						<div class="card-header">
							<h6 class="mb-0">Profil Peserta</h6>
						</div>
			            <div class="card-body">
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Nama Lengkap</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7 nama"></div>
			                </div>
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Nomor Induk Kependudukan (NIK)</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7 id_number"></div>
			                </div>
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Jenis Kelamin</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7 text-capitalize" id="gender"></div>
			                </div>
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Tempat Lahir</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="place_birth"></div>
			                </div>
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Tanggal Lahir</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="date_birth"></div>
			                </div>
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Agama</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="religion_id"></div>
			                </div>
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Pendidikan Terakhir</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="education_id"></div>
			                </div>
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Alamat Rumah</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="address_participant"></div>
			                </div>
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Provinsi</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="province_id"></div>
			                </div>
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Kab/Kota</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="districts_city_id"></div>
			                </div>
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Telp/HP</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="phone_number"></div>
			                </div>
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Email</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="email"></div>
			                </div>
			            </div>
			        </div>
					<div class="card card-custom mb-4">
						<div class="card-header">
							<h6 class="mb-0">Data Koperasi atau UMKM Peserta</h6>
						</div>
			            <div class="card-body">
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Status Usaha</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="business_status_id"></div>
			                </div>
			            </div>
			        </div>
					<div class="card card-custom mb-4 none" id="umkm">
						<div class="card-header">
							<h6 class="mb-0">Data UMKM</h6>
						</div>
			            <div class="card-body">
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Nama Usaha</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="name_umkm"></div>
			                </div>
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Alamat Usaha</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="address_umkm"></div>
			                </div>
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Sektor Usaha</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="business_sector_id"></div>
			                </div>
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Bidang Usaha</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="business_field_umkm_id"></div>
			                </div>
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Tanggal Pendirian Usaha</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="date_establishment_umkm"></div>
			                </div>
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">NPWP Usaha</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="npwp_umkm"></div>
			                </div>
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Nomor Induk Berusaha (NIB)</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="nib_umkm"></div>
			                </div>
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Kekayaan Usaha (Asset) per Tahun</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="asset_umkm"></div>
			                </div>
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Volume Usaha (Omset) per Tahun</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="omset_umkm"></div>
			                </div>
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Jumlah Karyawan</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="number_employees"></div>
			                </div>
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Kapasitas Produksi per Tahun</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="production_capacity"></div>
			                </div>
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Anggota Koperasi</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="member_koperasi"></div>
			                </div>
			            </div>
		            </div>
					<div class="card card-custom mb-4 none" id="koperasi">
						<div class="card-header">
							<h6 class="mb-0">Data Koperasi</h6>
						</div>
			            <div class="card-body">
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Nomor Induk Koperasi</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="registrasion_number_koperasi"></div>
			                </div>
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Nama Koperasi</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="name_koperasi"></div>
			                </div>
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Nomor Badan Hukum</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="legal_entity_number"></div>
			                </div>
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Tanggal Pendirian Koperasi</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="date_establishment_koperasi"></div>
			                </div>
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Alamat Koperasi</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="address_koperasi"></div>
			                </div>
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Jenis Koperasi</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="type_koperasi_id"></div>
			                </div>
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Kelompok Koperasi</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="group_koperasi_id"></div>
			                </div>
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Sektor Usaha</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="koperasi_sector_id"></div>
			                </div>
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Kelompok Binaan</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="fostered_koperasi_id"></div>
			                </div>
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Jabatan di Koperasi</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="position_koperasi_id"></div>
			                </div>
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">NPWP Koperasi</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="npwp_koperasi"></div>
			                </div>
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">RAT Terakhir</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="last_rat"></div>
			                </div>
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Status NIK</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7 text-capitalize" id="nik_status"></div>
			                </div>
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Bentuk Koperasi</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7 text-capitalize" id="form_koperasi_id"></div>
			                </div>
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Asset Koperasi</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="asset_koperasi"></div>
			                </div>
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Omzet Koperasi</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="omset_koperasi"></div>
			                </div>
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">SHU Koperasi</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="shu_koperasi"></div>
			                </div>
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Jumlah Anggota Laki-Laki</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="total_member_koperasi_men"></div>
			                </div>
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Jumlah Anggota Perempuan</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="total_member_koperasi_women"></div>
			                </div>
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Jumlah Karyawan Laki-Laki</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="total_employee_koperasi_men"></div>
			                </div>
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Jumlah Karyawan Perempuan</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="total_employee_koperasi_women"></div>
			                </div>
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Jangkauan Pemasaran Produk/Layanan Koperasi</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7 text-capitalize" id="marketing_reach_koperasi"></div>
			                </div>
		                </div>
		            </div>
					<div class="card card-custom mb-4 none" id="calon">
						<div class="card-header">
							<h6 class="mb-0">Calon Wirausaha</h6>
						</div>
			            <div class="card-body">
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Ide Bisnis</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="business_idea"></div>
			                </div>
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Alamat Calon Usaha</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="entrepreneur_address"></div>
			                </div>
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Sektor Calon Usaha</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="entrepreneur_sector_id"></div>
			                </div>
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Bidang Calon Usaha</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="business_field_entrepreneur_id"></div>
			                </div>
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Pendamping yang didapatkan</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="accompaniment_entrepreneur_id"></div>
			                </div>
			            </div>
			        </div>
					<div class="card card-custom none mb-4" id="digitalisasi_usaha">
						<div class="card-header">
							<h6 class="mb-0">Digitalisasi Usaha</h6>
						</div>
			            <div class="card-body">
			            	<div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Email Usaha</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="business_email"></div>
			                </div>
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Website Usaha</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="business_website"></div>
			                </div>
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Media Sosial Usaha</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="social_media_id"></div>
			                </div>
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Marketplace</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="marketplace_id"></div>
			                </div>
			            </div>
			        </div>
					<div class="card card-custom none mb-4" id="pembiayaan_usaha">
						<div class="card-header">
							<h6 class="mb-0">Pembiayaan Usaha</h6>
						</div>
			            <div class="card-body">
			            	<div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Apakah pernah mengakses kredit perbankan/non-perbankan?</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="banking_credit"></div>
			                </div>
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Apakah memiliki tabungan?</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="savings"></div>
			                </div>
			            </div>
			        </div>
					<div class="card card-custom mb-4 none" id="transformasi_usaha">
						<div class="card-header">
							<h6 class="mb-0">Transformasi Usaha</h6>
						</div>
			            <div class="card-body">
			            	<div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Perizinan Usaha</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="business_licensing_id"></div>
			                </div>
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Sertifikasi Produk/Usaha</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="business_certificate_id"></div>
			                </div>
			            </div>
			        </div>
					<div class="card card-custom mb-4 none" id="rantai_pasok_ekspor">
						<div class="card-header">
							<h6 class="mb-0">Rantai Pasok dan Ekspor</h6>
						</div>
			            <div class="card-body">
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Jangkauan Pemasaran Produk/Layanan Usaha</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7 text-capitalize" id="marketing_reach_umkm"></div>
			                </div>
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Apakah pernah melakukan ekspor?</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7 text-capitalize" id="export"></div>
			                </div>
			                <div class="none" id="export_form">
				                <div class="form-group row align-items-center">
				                    <label class="col-form-label col-5 col-md-4">Negara Tujuan</label>
				                    <div class="col">:</div>
				                    <div class="col-6 col-md-7" id="export_destination"></div>
				                </div>
				                <div class="form-group row align-items-center">
				                    <label class="col-form-label col-5 col-md-4">Volume Ekspor per Tahun</label>
				                    <div class="col">:</div>
				                    <div class="col-6 col-md-7" id="export_volume"></div>
				                </div>
				                <div class="form-group row align-items-center">
				                    <label class="col-form-label col-5 col-md-4">Nilai ekspor</label>
				                    <div class="col">:</div>
				                    <div class="col-6 col-md-7" id="export_value"></div>
				                </div>
				            </div>
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Apakah bapak/ibu mensuplai produk sebagai bahan baku pada usaha lain?</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="product_supply"></div>
			                </div>
			            </div>
			        </div>
					<div class="card card-custom mb-4 none" id="kemitraan_usaha">
						<div class="card-header">
							<h6 class="mb-0">Kemitraan Usaha</h6>
						</div>
			            <div class="card-body">
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Apakah melakukan kemitraan dengan lembaga lain?</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="partnership"></div>
			                </div>
			            </div>
			        </div>
					<div class="card card-custom">
						<div class="card-header">
							<h6 class="mb-0">Lain-Lainnya</h6>
						</div>
			            <div class="card-body">
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Permasalahan yang dihadapi dalam pengembangan usaha</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="m1"></div>
			                </div>
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Kebutuhan Diklat</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="m2_id"></div>
			                </div>
			                <div class="form-group row align-items-center">
			                    <label class="col-form-label col-5 col-md-4">Masukkan/Saran</label>
			                    <div class="col">:</div>
			                    <div class="col-6 col-md-7" id="m3"></div>
			                </div>
			            </div>
			        </div>
				</form>
			</div>
		</div>
		<div id="loading">
			<div class="d-flex flex-column justify-content-center align-items-center state">
				<div class="loader">
					<svg class="circular" viewBox="25 25 50 50">
						<circle class="path-primary" cx="50" cy="50" r="20" fill="none" stroke-width="5" stroke-miterlimit="10"/>
					</svg>
				</div>
			</div>
		</div>
	</div>
	<div class="modal" id="modal-photo" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content bg-transparent border-0 d-block text-center">
				<img src="{{asset('assets/images/user.png')}}" class="profile_photo mb-3" width="400" data-dismiss="modal">
			</div>
		</div>
	</div>
</div>
@endsection

@section('script')
	<script>const id = {{$id}}</script>
	<script src="{{asset('api/view-peserta.js')}}"></script>
@endsection