@extends('layouts/app')

@section('title','Manual Book')

@section('content')
	<div class="container">
        <!-- <iframe width="100%" height="557" src="https://www.youtube.com/embed/5BXygUW7tpM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe> -->
        <iframe src="https://docs.google.com/viewer?url={{asset('assets/pdf/Modul Penggunaan PK2UKM 2021.pdf')}}&embedded=true" style="width:100%; height:83vh;" frameborder="0"></iframe>
	</div>
@endsection	