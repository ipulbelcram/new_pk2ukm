<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Unduh Kegiatan Lengkap</title>
    <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap/css/bootstrap.min.css') }}">
    <style>
        .preloader {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background-color: #fff;
        }

        .preloader .loading {
            position: absolute;
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%);
            font: 14px arial;
        }
    </style>
</head>
<body>
    <div class="preloader">
        <div class="loading">
            <img src="{{ asset('assets/images/spiner.gif') }}" width="220">
            <p>Harap Tunggu File Sedang di Proses</p>
        </div>
    </div>
    <div class="d-flex justify-content-center" style="margin-top: 200px">
        <div id="download-success" class="d-none col-lg-7 text-center">
            <div class="alert alert-success py-5" role="alert">
                File anda berhasil di download
            </div>
        </div>
    </div>
    <div class="d-none">
        <table id="data">
            
        </table>
    </div>

    @include('layouts.partials.script')
    <script type="text/javascript" src="{{ asset('assets/js/exportCsv.js') }}"></script>
    <script>
        $(document).ready(function() {
            var getApiUrl = (role == 1) ? `${api_url}training/with_user` : `${api_url}training/with_user/${user}`
            $.ajax({
                url: getApiUrl,
                type: 'GET',
                async: false,
                beforeSend: function(xhr) {
                    xhr.setRequestHeader("Authorization", "Bearer " + token)
                },
                success: function(result) {
                    // Append Header
                    append = `
                        <tr>
                            <td>Jenis Pelatihan</td>
                            <td>Judul Pelatihan</td>
                            <td>Kab/Kota Pelatihan</td>
                            <td>Tempat Pelaksanaan</td>
                            <td>Tanggal Mulai</td>
                            <td>Tanggal Selesai</td>

                            <td>No</td>
                            <td>Nomor Identifikasi</td>
                            <td>Nama Lengkap</td>
                            <td>Nomor KTP</td>
                            <td>Jenis Kelamin</td>
                            <td>Tempat Lahir</td>
                            <td>Tanggal Lahir</td>
                            <td>Agama</td>
                            <td>Pendidikan Terakhir</td>
                            <td>Provinsi</td>
                            <td>Kab/Kota</td>
                            <td>Telp/Hp</td>
                            <td>Email</td>
                            <td>Status Usaha</td>

                            <td>Nama UMKM</td>
                            <td>Alamat UMKM</td>
                            <td>Sektor UMKM</td>
                            <td>Tanggal UMKM Didirikan</td>
                            <td>Nomor NIB</td>
                            <td>Omset Usaha per Bulan</td>
                            <td>Jumlah Tenaga Kerja UMKM</td>

                            <td>Nama Koperasi</td>
                            <td>Alamat Koperasi</td>
                            <td>Jenis Koperasi</td>
                            <td>Tanggal Koperasi didirikan</td>
                            <td>Nomor Induk Koperasi</td>
                            <td>Posisi Koperasi</td>

                            <td>Ide Bisnis</td>
                            <td>Alamat Calon Wirausaha</td>
                            <td>Bidang Bisnis Calon Wirausaha</td>
                            
                            <td>Permasalahan yang dihadapi dalam pengembangan usaha</td>
                            <td>Kebutuhan Diklat</td>
                            <td>Rekomendasi</td>
                        </tr>
                    `
                    var no = 1;
                    $.each(result.data, function(index, value) {
                        // Append Body 
                        append += `
                            <tr>
                                <td>${value.training_type}</td>
                                <td>${value.training_title}</td>
                                <td>${value.districts_city}</td>
                                <td>${value.place}</td>
                                <td>${value.start_date}</td>
                                <td>${value.finish_date}</td>

                                <td>${no++}</td>
                                <td>${value.unique_code}</td>
                                <td>${value.name}</td>
                                <td>'${value.id_number}</td>
                                <td>${value.gender == 'laki-laki' ? 'Laki-Laki' : 'Perempuan'}</td>
                                <td>${value.place_birth}</td>
                                <td>${value.date_birth}</td>
                                <td>${value.religion}</td>
                                <td>${value.education}</td>
                                <td>${value.province_participant}</td>
                                <td>${value.districts_cities_participant}</td>
                                <td>'${value.phone_number}</td>
                                <td>${value.email}</td>
                                <td>${value.business_status}</td>

                                <td>${value.name_umkm}</td>
                                <td>${value.address_umkm}</td>
                                <td>${value.business_sector}</td>
                                <td>${value.date_establishment_umkm}</td>
                                <td>'${value.nib_umkm}</td>
                                <td>${rupiah(value.omset_umkm)}</td>
                                <td>${value.number_employees}</td>
                                
                                <td>${value.name_koperasi}</td>
                                <td>${value.address_koperasi}</td>
                                <td>${value.type_koperasi}</td>
                                <td>${value.date_establishment_koperasi}</td>
                                <td>'${value.registrasion_number_koperasi}</td>
                                <td>${value.position_koperasi}</td>
                                
                                <td>${value.business_idea}</td>
                                <td>${value.entrepreneur_address}</td>
                                <td>${value.business_field_entrepreneur}</td>
                                
                                <td>${value.optional_m1 != 1 ? value.m1 : 'Lainnya: ' + value.other_m1}</td>
                                <td>${value.m2}</td>
                                <td>${value.m3}</td>
                            </tr>
                        `
                    })
                    $("#data").append(append);
                    exportTableToExcel('data', 'Kegiatan Lengkap')
                    $(".preloader").fadeOut();
                    $("#download-success").removeClass('d-none');
                }
            })
        })
    </script>
</body>
</html>
