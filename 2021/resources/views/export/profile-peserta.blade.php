<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Unduh Profile</title>
    <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap/css/bootstrap.min.css') }}">
    <style>
        .preloader {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background-color: #fff;
        }

        .preloader .loading {
            position: absolute;
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%);
            font: 14px arial;
        }
    </style>
</head>
<body>
    <div class="preloader">
        <div class="loading">
            <img src="{{ asset('assets/images/spiner.gif') }}" width="220">
            <p>Harap Tunggu File Sedang di Proses</p>
        </div>
    </div>
    <div class="d-flex justify-content-center" style="margin-top: 200px">
        <div id="download-success" class="d-none col-lg-7 text-center">
            <div class="alert alert-success py-5" role="alert">
                File anda berhasil di download
                <div id="file-name"></div>
            </div>
        </div>
    </div>

    <div class="d-none" id="content-profile-peserta">
        <h3 class="pb-2">BIODATA PESERTA</h3>
        <div class="row">
		    <div class="col-3 mt-4">
		        <img src="#" class="rounded" id="profile_photo" style="width: 150px;height: 150px;">
		    </div>
		    <!-- PROFIL PESERTA -->
            <div class="col-12 mt-4">
                <div class="card">
                    <div class="card-header">
                        <strong>PROFIL PESERTA</strong>
                    </div>
                    <div class="card-body">
                        <table class="table table-borderless">
                            <tbody>
                                <tr>
                                    <th>Nama Lengkap</th>
                                    <th>:</th>
                                    <td class="name"></td>
                                </tr>
                                <tr>
                                    <th>Nomor KTP</th>
                                    <th>:</th>
                                    <td class ="id_number"></td>
                                </tr>
                                <tr>
                                    <th>Jenis Kelamin</th>
                                    <th>:</th>
                                    <td id="gender" class="text-capitalize"></td>
                                </tr>
                                <tr>
                                    <th>Tempat, Tanggal lahir</th>
                                    <th>:</th>
                                    <td class="ttl"></td>
                                </tr>
                                <tr>
                                    <th>Agama</th>
                                    <th>:</th>
                                    <td id="religion"></td>
                                </tr>
                                <tr>
                                    <th>Pendidikan Terakhir</th>
                                    <th>:</th>
                                    <td id="education"></td>
                                </tr>
                                <tr>
                                    <th>Kab/Kota</th>
                                    <th>:</th>
                                    <td id="districts_city"></td>
                                </tr>
                                <tr>
                                    <th>Telp/HP</th>
                                    <th>:</th>
                                    <td id="phone_number"></td>
                                </tr>
                                <tr>
                                    <th>E-Mail</th>
                                    <th>:</th>
                                    <td id="email"></td>
                                </tr>
                            </tbody>
                        </table>        
                    </div>
                </div>
            </div>
		</div>
    	<div class="html2pdf__page-break"></div>
        <!-- <h3 class="pb-2">DATA KOPERASI/ UMKM PESERTA</h3> -->
        <div class="row">
	        <!-- DATA KOPERASI / UMKM PESERTA -->
            <div class="col-12">
                <div class="card mt-4">
                    <div class="card-header">
                        <strong class="text-uppercase">Data Koperasi/Umkm Peserta</strong>
                    </div>
                    <div class="card-body">
                        <table class="table table-borderless">
                            <tbody>
                                <tr>
                                    <th>Status Usaha</th>
                                    <th>:</th>
                                    <td id="business_status_id"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- DATA UMKM -->
            <div class="col-12">
            	<div id="umkm">
	            	<div class="card mt-4">
	                    <div class="card-header">
	                        <strong>DATA UMKM</strong>
	                    </div>
	                    <div class="card-body">
	                        <table class="table table-borderless">
	                            <tbody>
	                                <tr>
	                                    <th>Nama Usaha</th>
	                                    <th>:</th>
	                                    <td id="name_umkm"></td>
	                                </tr>
	                                <tr>
	                                    <th>Alamat Usaha</th>
	                                    <th>:</th>
	                                    <td id="address_umkm"></td>
	                                </tr>
	                                <tr>
	                                    <th>Sektor Usaha</th>
	                                    <th>:</th>
	                                    <td id="business_sector_id"></td>
	                                </tr>
	                                <tr>
	                                    <th>Bidang Usaha</th>
	                                    <th>:</th>
	                                    <td id="business_field_umkm_id"></td>
	                                </tr>
	                                <tr>
	                                    <th>Tanggal Pendirian Usaha</th>
	                                    <th>:</th>
	                                    <td id="date_establishment_umkm"></td>
	                                </tr>
	                                <tr>
	                                    <th>NPWP Usaha</th>
	                                    <th>:</th>
	                                    <td id="npwp_umkm"></td>
	                                </tr>
	                                <tr>
	                                    <th>Nomor Induk Berusaha (NIB)</th>
	                                    <th>:</th>
	                                    <td id="nib_umkm"></td>
	                                </tr>
	                                <tr>
	                                    <th>Kekayaan Usaha (Asset) per Tahun</th>
	                                    <th>:</th>
	                                    <td id="asset_umkm"></td>
	                                </tr>
	                                <tr>
	                                    <th>Volume Usaha (Omset) per Tahun</th>
	                                    <th>:</th>
	                                    <td id="omset_umkm"></td>
	                                </tr>
	                                <tr>
	                                    <th>Jumlah Karyawan</th>
	                                    <th>:</th>
	                                    <td id="number_employees"></td>
	                                </tr>
	                                <tr>
	                                    <th>Kapasitas Produksi per Tahun</th>
	                                    <th>:</th>
	                                    <td id="production_capacity"></td>
	                                </tr>
	                                <tr>
	                                    <th>Anggota Koperasi</th>
	                                    <th>:</th>
	                                    <td id="member_koperasi"></td>
	                                </tr>
	                            </tbody>
	                        </table>
	                    </div>
	                </div>
	                <div class="html2pdf__page-break"></div>
	            </div>
            </div>
            <!-- DATA KOPERASI -->
            <div class="col-12">
            	<div id="koperasi">
	            	<div class="card mt-4">
	                    <div class="card-header">
	                        <strong>DATA KOPERASI</strong>
	                    </div>
	                    <div class="card-body">
	                        <table class="table table-borderless">
	                            <tbody>
	                                <tr>
	                                    <th>Nomor Induk Koperasi</th>
	                                    <th>:</th>
	                                    <td id="registrasion_number_koperasi"></td>
	                                </tr>
	                                <tr>
	                                    <th>Nama Koperasi</th>
	                                    <th>:</th>
	                                    <td id="name_koperasi"></td>
	                                </tr>
	                                <tr>
	                                    <th>Nomor Badan Hukum</th>
	                                    <th>:</th>
	                                    <td id="legal_entity_number"></td>
	                                </tr>
	                                <tr>
	                                    <th>Tanggal Pendirian Koperasi</th>
	                                    <th>:</th>
	                                    <td id="date_establishment_koperasi"></td>
	                                </tr>
	                                <tr>
	                                    <th>Alamat Koperasi</th>
	                                    <th>:</th>
	                                    <td id="address_koperasi"></td>
	                                </tr>
	                                <tr>
	                                    <th>Jenis Koperasi</th>
	                                    <th>:</th>
	                                    <td id="type_koperasi_id"></td>
	                                </tr>
	                                <tr>
	                                    <th>Kelompok Koperasi</th>
	                                    <th>:</th>
	                                    <td id="group_koperasi_id"></td>
	                                </tr>
	                                <tr>
	                                    <th>Sektor Usaha</th>
	                                    <th>:</th>
	                                    <td id="koperasi_sector_id"></td>
	                                </tr>
	                                <tr>
	                                    <th>Kelompok Binaan</th>
	                                    <th>:</th>
	                                    <td id="fostered_koperasi_id"></td>
	                                </tr>
	                                <tr>
	                                    <th>Jabatan di Koperasi</th>
	                                    <th>:</th>
	                                    <td id="position_koperasi_id"></td>
	                                </tr>
	                            </tbody>
	                        </table>
	                    </div>
	                </div>
	                <div class="html2pdf__page-break"></div>
	                <div class="card mt-4">
	                    <div class="card-body">
	                        <table class="table table-borderless">
	                            <tbody>
	                                <tr>
	                                    <th>NPWP Koperasi</th>
	                                    <th>:</th>
	                                    <td id="npwp_koperasi"></td>
	                                </tr>
	                                <tr>
	                                    <th>RAT Terakhir</th>
	                                    <th>:</th>
	                                    <td id="last_rat"></td>
	                                </tr>
	                                <tr>
	                                    <th>Status NIK</th>
	                                    <th>:</th>
	                                    <td id="nik_status"></td>
	                                </tr>
	                                <tr>
	                                    <th>Bentuk Koperasi</th>
	                                    <th>:</th>
	                                    <td id="form_koperasi_id"></td>
	                                </tr>
	                                <tr>
	                                    <th>Asset Koperasi</th>
	                                    <th>:</th>
	                                    <td id="asset_koperasi"></td>
	                                </tr>
	                                <tr>
	                                    <th>Omzet Koperasi</th>
	                                    <th>:</th>
	                                    <td id="omset_koperasi"></td>
	                                </tr>
	                                <tr>
	                                    <th>SHU Koperasi</th>
	                                    <th>:</th>
	                                    <td id="shu_koperasi"></td>
	                                </tr>
	                                <tr>
	                                    <th>Jumlah Anggota Laki-Laki</th>
	                                    <th>:</th>
	                                    <td id="total_member_koperasi_men"></td>
	                                </tr>
	                                <tr>
	                                    <th>Jumlah Anggota Perempuan</th>
	                                    <th>:</th>
	                                    <td id="total_member_koperasi_women"></td>
	                                </tr>
	                                <tr>
	                                    <th>Jumlah Karyawan Laki-Laki</th>
	                                    <th>:</th>
	                                    <td id="total_employee_koperasi_men"></td>
	                                </tr>
	                                <tr>
	                                    <th>Jumlah Karyawan Perempuan</th>
	                                    <th>:</th>
	                                    <td id="total_employee_koperasi_women"></td>
	                                </tr>
	                                <tr>
	                                    <th>Jangkauan Pemasaran Produk/Layanan Koperasi</th>
	                                    <th>:</th>
	                                    <td id="marketing_reach_koperasi"></td>
	                                </tr>
	                            </tbody>
	                        </table>
	                    </div>
	                </div>
	                <div class="html2pdf__page-break"></div>
	            </div>
            </div>
            <!-- CALON WIRAUSAHA -->
            <div class="col-12">
            	<div id="calon">
	            	<div class="card mt-4">
	                    <div class="card-header">
	                        <strong>CALON WIRAUSAHA</strong>
	                    </div>
	                    <div class="card-body">
	                        <table class="table table-borderless">
	                            <tbody>
	                                <tr>
	                                    <th>Ide Bisnis</th>
	                                    <th>:</th>
	                                    <td id="business_idea"></td>
	                                </tr>
	                                <tr>
	                                    <th>Alamat Calon Usaha</th>
	                                    <th>:</th>
	                                    <td id="entrepreneur_address"></td>
	                                </tr>
	                                <tr>
	                                    <th>Sektor Calon Usaha</th>
	                                    <th>:</th>
	                                    <td id="entrepreneur_sector_id"></td>
	                                </tr>
	                                <tr>
	                                    <th>Bidang Calon Usaha</th>
	                                    <th>:</th>
	                                    <td id="business_field_entrepreneur_id"></td>
	                                </tr>
	                                <tr>
	                                    <th>Pendamping yang didapatkan</th>
	                                    <th>:</th>
	                                    <td id="accompaniment_entrepreneur_id"></td>
	                                </tr>
	                            </tbody>
	                        </table>
	                    </div>
	                </div>
	                <div class="html2pdf__page-break"></div>
	            </div>
            </div>
            <!-- DIGITALISASI USAHA -->
            <div class="col-12">
            	<div id="digitalisasi_usaha">
	            	<div class="card mt-4">
	                    <div class="card-header">
	                        <strong>DIGITALISASI USAHA</strong>
	                    </div>
	                    <div class="card-body">
	                        <table class="table table-borderless">
	                            <tbody>
	                                <tr>
	                                    <th>Email Usaha</th>
	                                    <th>:</th>
	                                    <td id="business_email"></td>
	                                </tr>
	                                <tr>
	                                    <th>Website Usaha</th>
	                                    <th>:</th>
	                                    <td id="business_website"></td>
	                                </tr>
	                                <tr>
	                                    <th>Media Sosial Usaha</th>
	                                    <th>:</th>
	                                    <td id="social_media_id"></td>
	                                </tr>
	                                <tr>
	                                    <th>Marketplace</th>
	                                    <th>:</th>
	                                    <td id="marketplace_id"></td>
	                                </tr>
	                            </tbody>
	                        </table>
	                    </div>
	                </div>
	            </div>
            </div>
            <!-- PEMBIAYAAN USAHA -->
            <div class="col-12">
            	<div id="pembiayaan_usaha">
	            	<div class="card mt-4">
	                    <div class="card-header">
	                        <strong>PEMBIAYAAN USAHA</strong>
	                    </div>
	                    <div class="card-body">
	                        <table class="table table-borderless">
	                            <tbody>
	                                <tr>
	                                    <th>Apakah pernah mengakses kredit perbankan/non-perbankan?</th>
	                                    <th>:</th>
	                                    <td id="banking_credit"></td>
	                                </tr>
	                                <tr>
	                                    <th>Apakah memiliki tabungan?</th>
	                                    <th>:</th>
	                                    <td id="savings"></td>
	                                </tr>
	                            </tbody>
	                        </table>
	                    </div>
	                </div>
	            </div>
            </div>
            <!-- TRANSFORMASI USAHA -->
            <div class="col-12">
            	<div id="transformasi_usaha">
	            	<div class="card mt-4">
	                    <div class="card-header">
	                        <strong>TRANSFORMASI USAHA</strong>
	                    </div>
	                    <div class="card-body">
	                        <table class="table table-borderless">
	                            <tbody>
	                                <tr>
	                                    <th>Perizinan Usaha</th>
	                                    <th>:</th>
	                                    <td id="business_licensing_id"></td>
	                                </tr>
	                                <tr>
	                                    <th>Sertifikasi Produk/Usaha</th>
	                                    <th>:</th>
	                                    <td id="business_certificate_id"></td>
	                                </tr>
	                            </tbody>
	                        </table>
	                    </div>
	                </div>
		            <div class="html2pdf__page-break"></div>
	            </div>
            </div>
            <!-- RANTAI PASOK DAN EKSPOR -->
            <div class="col-12">
            	<div id="rantai_pasok_ekspor">
	            	<div class="card mt-4">
	                    <div class="card-header">
	                        <strong>RANTAI PASOK DAN EKSPOR</strong>
	                    </div>
	                    <div class="card-body">
	                        <table class="table table-borderless">
	                            <tbody>
	                                <tr>
	                                    <th>Jangkauan Pemasaran Produk/Layanan Usaha</th>
	                                    <th>:</th>
	                                    <td id="marketing_reach_umkm"></td>
	                                </tr>
	                                <tr>
	                                    <th>Apakah pernah melakukan ekspor?</th>
	                                    <th>:</th>
	                                    <td id="export"></td>
	                                </tr>
	                                <tr>
	                                    <th>Negara Tujuan</th>
	                                    <th>:</th>
	                                    <td id="export_destination"></td>
	                                </tr>
	                                <tr>
	                                    <th>Volume Ekspor per Tahun</th>
	                                    <th>:</th>
	                                    <td id="export_volume"></td>
	                                </tr>
	                                <tr>
	                                    <th>Nilai ekspor</th>
	                                    <th>:</th>
	                                    <td id="export_value"></td>
	                                </tr>
	                                <tr>
	                                    <th>Apakah bapak/ibu mensuplai produk sebagai bahan baku pada usaha lain?</th>
	                                    <th>:</th>
	                                    <td id="product_supply"></td>
	                                </tr>
	                            </tbody>
	                        </table>
	                    </div>
	                </div>
	            </div>
            </div>
            <!-- KEMITRAAN USAHA -->
            <div class="col-12">
            	<div id="kemitraan_usaha">
	            	<div class="card mt-4">
	                    <div class="card-header">
	                        <strong>KEMITRAAN USAHA</strong>
	                    </div>
	                    <div class="card-body">
	                        <table class="table table-borderless">
	                            <tbody>
	                                <tr>
	                                    <th>Apakah melakukan kemitraan dengan lembaga lain?</th>
	                                    <th>:</th>
	                                    <td id="partnership"></td>
	                                </tr>
	                            </tbody>
	                        </table>
	                    </div>
	                </div>
	            	<div class="html2pdf__page-break"></div>
	            </div>
            </div>
        </div>
    	<div class="row">
    		<!-- PEMANTAUAN -->
            <div class="col-12 mt-4">
                <div class="border-bottom">
                    <h3 class="pb-2">PEMANTAUAN PELAKSANAAN PELATIHAN</h3>
                </div>
            </div>
            <div class="col-12 mt-3">
                <table class="table table-borderless">
                    <tr>
                        <th>Nomor KTP</th>
                        <td class="id_number"></td>
                    </tr>
                    <tr>
                        <th>Nama Lengkap</th>
                        <td class="name"></td>
                    </tr>
                    <tr>
                        <th>Tempat, Tanggal Lahir</th>
                        <td class="ttl"></td>
                    </tr>
                    <tr>
                        <th>Nama Kegiatan</th>
                        <td id="training_title"></td>
                    </tr>
                    <tr>
                        <th>Kabupaten/ Kota Kegiatan</th>
                        <td id="training_disctricts_city"></td>
                    </tr>
                </table>
            </div>
            <div class="col-12 mt-3">
                <div class="card">
                    <div class="card-body">
                        <div>
                            <strong>Permasalahan yang dihadapi dalam pengembangan usaha</strong>
                            <p id="m1"></p>
                        </div>
                        <div class="mt-3">
                            <strong>Kebutuhan Diklat</strong>
                            <p id="m2">Jawab: </p>
                        </div>
                        <div class="mt-3">
                            <strong>Masukkan/Saran</strong>
                            <p id="m3">Jawab: </p>
                        </div>
                    </div>
                </div>
            </div>
    	</div>
    </div>

    @include('layouts.partials.script')
    <script type="text/javascript" src="{{ asset('assets/vendors/html2pdf.js') }}"></script>
    <script>var peserta_id = {{ $peserta_id }}</script>
    <script>
        $(document).ready(function() {

            $.ajax({
                url: `${api_url}participant/${peserta_id}/by_id`,
                type: 'GET',
                async: false,
                beforeSend: function(xhr) {
                    xhr.setRequestHeader("Authorization", "Bearer " + token)
                },
                success: function(result) {
                    let value = result.data
                    $.ajax({
                        url: `${api_url}training/${value.training_id}/by_id`,
                        type: 'GET',
                        async: false,
                        beforeSend: function(xhr) {
                            xhr.setRequestHeader("Authorization", "Bearer " + token)
                        },
                        success: function(result) {
                            let value = result.data
                            $('#training_title').html(value.training_title)
                            $('#training_disctricts_city').html(value.districts_city.districts_city)
                        }
                    })
                    // console.log(value)
                    $("#file-name").html(value.id_number + '-' + value.name)
                    $('img#profile_photo').attr('src', value.profile_photo)
                    $('.name').html(value.name)
                    $('.id_number').html(value.id_number)
                    $('#gender').html(value.gender)
                    $('.ttl').html(value.place_birth + ', ' + tanggal(value.date_birth))
                    $('#religion').html(value.religion_id.param)
                    $('#education').html(value.education.param)
                    $('#districts_city').html(value.districts_city.districts_city)
                    $('#phone_number').html(value.phone_number)
                    $('#email').html(value.email)
                    $('#business_status_id').html(value.business_status.param)
			        if (value.business_status.id == 14) {
			            $('#koperasi').remove()
			            $('#calon').remove()
			            $('#name_umkm').html(value.name_umkm)
			            $('#address_umkm').html(value.address_umkm)
			            $('#business_field_umkm_id').html(value.business_field_umkm.param)
			            $('#business_sector_id').html(value.business_sector.param)
			            $('#date_establishment_umkm').html(tanggal(value.date_establishment_umkm))
			            $('#npwp_umkm').html(value.npwp_umkm)
			            $('#nib_umkm').html(value.nib_umkm)
			            $('#asset_umkm').html(rupiah(value.asset_umkm))
			            $('#omset_umkm').html(rupiah(value.omset_umkm))
			            $('#number_employees').html(`${value.number_employees} Orang`)
			            $('#production_capacity').html(value.production_capacity)
			            $('#member_koperasi').html(value.member_koperasi == 'yes' ? 'Ya' : 'Tidak')
			            $('#banking_credit').html(value.banking_credit == 'yes' ? 'Ya' : 'Tidak')
			            $('#savings').html(value.savings == 'yes' ? 'Ya' : 'Tidak')
			            if (value.export != 'no') {
			                $('#export').html(`Ya, ${value.export}`)
			                $('#export_form').show()
			                $('#export_destination').html(value.export_destination)
			                $('#export_volume').html(value.export_volume)
			                $('#export_value').html(`Rp.${value.export_value}`)
			            } else {
			                $('#export').html('Tidak')
			            }
			            $('#product_supply').html(value.product_supply == 'yes' ? 'Ya' : 'Tidak')
			            $('#marketing_reach_umkm').html(value.marketing_reach.param)
			            if (value.marketing_reach.optional == 1) {
			                if (value.marketing_reach.id != 95) {
			                    $('#marketing_reach_umkm').append(`: ${value.marketing_reach_optional}`)
			                }
			            }
			        } else if (value.business_status.id == 15) {
			            $('#umkm').remove()
			            $('#calon').remove()
			            $('#pembiayaan_usaha').remove()
			            $('#rantai_pasok_ekspor').remove()
			            $('#registrasion_number_koperasi').html(value.registrasion_number_koperasi)
			            $('#name_koperasi').html(value.name_koperasi)
			            $('#legal_entity_number').html(value.legal_entity_number)
			            $('#date_establishment_koperasi').html(tanggal(value.date_establishment_koperasi))
			            $('#address_koperasi').html(value.address_koperasi)
			            $('#type_koperasi_id').html(value.type_koperasi.param)
			            $('#group_koperasi_id').html(value.group_koperasi.param)
			            $('#koperasi_sector_id').html(value.business_sector.param)
			            $('#fostered_koperasi_id').html(value.fostered_koperasi.param)
			            $('#position_koperasi_id').html(value.position_koperasi.param)
			            $('#npwp_koperasi').html(value.npwp_koperasi)
			            $('#last_rat').html(`Tahun ${value.last_rat}`)
			            $('#nik_status').html(value.nik_status)
			            $('#form_koperasi_id').html(value.form_koperasi.param)
			            $('#asset_koperasi').html(rupiah(value.asset_koperasi))
			            $('#omset_koperasi').html(rupiah(value.omset_koperasi))
			            $('#shu_koperasi').html(rupiah(value.shu_koperasi))
			            $('#total_member_koperasi_men').html(`${value.total_member_koperasi_men} Orang`)
			            $('#total_member_koperasi_women').html(`${value.total_member_koperasi_women} Orang`)
			            $('#total_employee_koperasi_men').html(`${value.total_employee_koperasi_men} Orang`)
			            $('#total_employee_koperasi_women').html(`${value.total_employee_koperasi_women} Orang`)
			            $('#marketing_reach_koperasi').html(value.marketing_reach.param)
			            if (value.marketing_reach.optional == 1) {
			                $('#marketing_reach_koperasi').append(`: ${value.marketing_reach_optional}`)
			            }
			        } else {
			            $('#umkm').remove()
			        	$('#koperasi').remove()
			            $('#digitalisasi_usaha').remove()
			            $('#pembiayaan_usaha').remove()
			            $('#transformasi_usaha').remove()
			            $('#rantai_pasok_ekspor').remove()
			            $('#kemitraan_usaha').remove()
			            $('#business_idea').html(value.business_idea)
			            $('#entrepreneur_address').html(value.entrepreneur_address)
			            $('#entrepreneur_sector_id').html(value.business_sector.param)
			            $('#business_field_entrepreneur_id').html(value.business_field_entrepreneur.param)
			            $('#accompaniment_entrepreneur_id').html(value.accompaniment_entrepreneur.param)
			        }
			        if (value.business_status.id == 14 || value.business_status.id == 15) {
			            $('#partnership').html(value.partnership == 'yes' ? 'Ya' : 'Tidak')
			            $('#business_email').html(value.business_email != null ? value.business_email : 'Tidak Ada')
			            $('#business_website').html(value.business_website != null ? value.business_website : 'Tidak Ada')
			            let length_social_media = value.social_media.length - 1
			            $.each(value.social_media, function(index, value) {
			            	if (index < length_social_media) {
				                append = `${value.choice.param}, `
			            	} else {
				                append = `${value.choice.param}`
			            	}
			                $('#social_media_id').append(append)
			            })
			            let length_marketplace = value.marketplace.length - 1
			            $.each(value.marketplace, function(index, value) {
			                if (index < length_marketplace) {
				                append = `${value.choice.param}, `
			            	} else {
				                append = `${value.choice.param}`
			            	}
			                $('#marketplace_id').append(append)
			            })
			            let length_business_licensing = value.business_licensing.length - 1
			            $.each(value.business_licensing, function(index, value) {
			                if (index < length_business_licensing) {
				                append = `${value.choice.param}, `
			            	} else {
				                append = `${value.choice.param}`
			            	}
			                $('#business_licensing_id').append(append)
			            })
			            let length_business_certificate = value.business_certificate.length - 1
			            $.each(value.business_certificate, function(index, value) {
			                if (index < length_business_certificate) {
				                append = `${value.choice.param}, `
			            	} else {
				                append = `${value.choice.param}`
			            	}
			                $('#business_certificate_id').append(append)
			            })
			        }
                    if (value.monitoring.other_m1 != null) {
                    	$('#m1').append(`Lainnya: ${value.monitoring.other_m1}`)
                    } else {
                    	$('#m1').append(`Jawab: ${value.monitoring.m1.param}`)
                    }
			        $('#m2').html(value.monitoring.m2.param)
			        $('#m3').html(value.monitoring.m3)
                }
            })
            let filename = $("#file-name").text() + '.pdf'
            let element = $('#content-profile-peserta').html()
            // console.log(element)
            let opt = {
                margin: [0.3, 0.3],
                filename: filename,
                image: { type: 'jpeg', quality: 0.98 },
                html2canvas: { scale: 2 },
                jsPDF: { unit: 'in', format: 'a4', orientation: 'portrait' }
            }
            html2pdf().from(element).set(opt).toPdf().get('pdf').then(function(pdf) {
                var totalPages = pdf.internal.getNumberOfPages()
                for (i = 1; i <= totalPages; i++) {
                    pdf.setPage(i)
                    pdf.setFontSize(9)
                    pdf.text(i + ' / ' + totalPages, (pdf.internal.pageSize.getWidth() - 0.8), (pdf.internal.pageSize.getHeight() - 0.8))
                }
            }).save()
            $(".preloader").fadeOut()
            $("#download-success").removeClass('d-none')
        })
    </script>
</body>
</html>