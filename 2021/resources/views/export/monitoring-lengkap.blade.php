<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Unduh Monitoring Pendamping Lengkap</title>
    <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap/css/bootstrap.min.css') }}">
    <style>
        .preloader {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background-color: #fff;
        }
        .preloader .loading {
            position: absolute;
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%);
            font: 14px arial;
        }
    </style>
</head>
<body>
    <div class="preloader">
        <div class="loading">
            <img src="{{ asset('assets/images/spiner.gif') }}" width="220">
            <p>Harap Tunggu File Sedang di Proses</p>
        </div>
    </div>
    <div class="d-flex justify-content-center" style="margin-top: 200px">
        <div id="download-success" class="d-none col-lg-7 text-center">
            <div class="alert alert-success py-5" role="alert">
                File berhasil di download
            </div>
        </div>
    </div>
    <div class="d-none">
        <table id="data">
        	<tr><th colspan="5">Data Monitoring Per Bulan Pendamping</th></tr>
        	<tr><th colspan="5" id="name"></th></tr>
        	<tr><th colspan="5" id="no_ktp"></th></tr>
        	<tr></tr>
            <tr>
                <th>No.</th>
                <th>Bulan</th>
                <th>Tanggal Report</th>
                <th>Detail Aktivitas</th>
                <th colspan="2">Output</th>
            </tr>
        </table>
    </div>
    @include('layouts.partials.script')
    <script>let user_id = {{$user_id}}</script>
    <script>let companion_id = {{$companion_id}}</script>
    <script type="text/javascript" src="{{ asset('assets/js/exportExcel.js') }}"></script>
    <script>
    $(document).ready(function() {
    	$.ajax({
		    url: `${api_url}companion`,
		    type: 'GET',
		    data: {
		    	user_id: user_id
		    },
		    beforeSend: function(xhr) {
		        xhr.setRequestHeader("Authorization", "Bearer " + token)
		    },
		    success: function(result) {
		    	let value = result.data
		        // console.log(value)
		        $('#name').html(value.name)
		        $('#no_ktp').html(value.no_ktp)
		        $.ajax({
		            url: `${api_url}monitoring_companion/fetch/${companion_id}`,
		            type: 'GET',
		            beforeSend: function(xhr) {
		                xhr.setRequestHeader("Authorization", "Bearer " + token)
		            },
		            success: function(result) {
		                console.log(result)
		                $.each(result.data, function(index, value) {
		                    append = `<tr>
		                        <td>${index + 1}</td>
		                        <td>${bulan(value.month)}</td>
		                        <td>${tanggal(value.date)}</td>
		                        <td>${value.activity_details}</td>
		                        <td>${value.output_koperasi} Koperasi</td>
		                        <td>${value.output_umkm} UMKM</td>
		                    </tr>`
			                $("#data").append(append)
		                })
				        exportTableToExcel('data', 'Monitoring Pendamping Lengkap')
				        $(".preloader").fadeOut();
				        $("#download-success").removeClass('d-none');
		            }
		        })
		    }
		})
    })
    </script>
</body>
</html>