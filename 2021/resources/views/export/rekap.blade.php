<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Unduh Rekap</title>
    <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap/css/bootstrap.min.css') }}">
    <style>
        .preloader {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background-color: #fff;
        }

        .preloader .loading {
            position: absolute;
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%);
            font: 14px arial;
        }
    </style>
</head>
<body>
    <div class="preloader">
        <div class="loading">
            <img src="{{ asset('assets/images/spiner.gif') }}" width="220">
            <p>Harap Tunggu File Sedang di Proses</p>
        </div>
    </div>
    <div class="d-flex justify-content-center" style="margin-top: 200px">
        <div id="download-success" class="d-none col-lg-7 text-center">
            <div class="alert alert-success py-5" role="alert">
                File anda berhasil di download
            </div>
        </div>
    </div>
    <div class="d-none">
        <table id="data">
        	<tr>
				<th>No.</th>
				<th>Provinsi</th>
				<th>Anggaran (Prov+Kab/Kota)</th>
				<th>Realisasi Anggaran (Prov+Kab/Kota)</th>
				<th>Peserta (Prov+Kab/Kota)</th>
				<th>Realisasi Peserta (Prov+Kab/Kota)</th>
				<th>Pendamping (Prov+Kab/Kota)</th>
				<th>Realiasi Pendamping (Prov+Kab/Kota)</th>
			</tr>
        </table>
    </div>

    @include('layouts.partials.script')
    <script type="text/javascript" src="{{ asset('assets/js/exportExcel.js') }}"></script>
    <script>
        $(document).ready(function() {
            $.ajax({
                url: `${api_url}budged_performance/budged_performance_by_province`,
                type: 'GET',
                beforeSend: function(xhr) {
                    xhr.setRequestHeader("Authorization", "Bearer " + token)
                },
                success: function(result) {
                	// console.log(result.data)
                    let pagu_anggaran = 0, realisasi_anggaran = 0
			        let target_peserta = 0, realisasi_peserta = 0
			        let target_pendamping = 0, realisasi_pendamping = 0
			        $.each(result.data, function(index, value) {
			            pagu_anggaran += parseInt(value.total_budged)
			            realisasi_anggaran += parseInt(value.total_budged_realization)
			            target_peserta += parseInt(value.total_target_participant)
			            realisasi_peserta += parseInt(value.total_participant_realization)
			            target_pendamping += parseInt(value.total_target_companion)
			            realisasi_pendamping += parseInt(value.total_companion_realization)
			            append = `<tr>
			        		<td>${convert(index + 1)}</td>
			        		<td>${value.province}</td>
			        		<td>${rupiah(value.total_budged)}</td>
			        		<td>${rupiah(value.total_budged_realization)}</td>
			        		<td>${convert(value.total_target_participant)}</td>
			        		<td>${convert(value.total_participant_realization)}</td>
			        		<td>${convert(value.total_target_companion)}</td>
			        		<td>${convert(value.total_companion_realization)}</td>
			        	</tr>`
			            $('#data').append(append)
			        })
			        let total = `<tr>
			    		<td class="text-center" colspan="2"><b>Total</b></td>
			    		<td><b>${rupiah(pagu_anggaran)}</b></td>
			    		<td><b>${rupiah(realisasi_anggaran)}</b></td>
			    		<td><b>${convert(target_peserta)}</b></td>
			    		<td><b>${convert(realisasi_peserta)}</b></td>
			    		<td><b>${convert(target_pendamping)}</b></td>
			    		<td><b>${convert(realisasi_pendamping)}</b></td>
			    	</tr>`
			        $('#data').append(total)
		            exportTableToExcel('data', 'Rekap per Provinsi')
		            $(".preloader").fadeOut();
		            $("#download-success").removeClass('d-none');
                }
            })
        })
    </script>
</body>
</html>