<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Peserta Lengkap</title>
    <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap/css/bootstrap.min.css') }}">
    <style>
        .preloader {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background-color: #fff;
        }

        .preloader .loading {
            position: absolute;
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%);
            font: 14px arial;
        }
    </style>
</head>
<body>
    <div class="preloader">
        <div class="loading">
            <img src="{{ asset('assets/images/spiner.gif') }}" width="220">
            <p>Harap Tunggu File Sedang di Proses</p>
        </div>
    </div>
    <div class="d-flex justify-content-center" style="margin-top: 200px">
        <div id="download-success" class="d-none col-lg-7 text-center">
            <div class="alert alert-success py-5" role="alert">
                File anda berhasil di download
            </div>
        </div>
    </div>
    <div class="d-none">
        <table id="data">
        </table>
    </div>

    @include('layouts.partials.script')
    <script type="text/javascript" src="{{ asset('assets/js/exportExcel.js') }}"></script>
    <script>var training_id = {{ $training_id }}</script>
    <script>
        $(document).ready(function() {
            $.ajax({
                url: `${api_url}training/${training_id}/by_id`,
                type: 'GET',
                async: false,
                beforeSend: function(xhr) {
                    xhr.setRequestHeader("Authorization", "Bearer " + token)
                },
                success: function(result) {
                    let data = result.data
                    append = `
                        <tr>
                            <th colspan="14">LAPORAN DATA PESERTA KEGIATAN</th>
                        </tr>
                        <tr>
                            <th colspan="14">DAK NONFISIK PK2UKM</th>
                        </tr>
                        <tr>
                            <th colspan="14">DEPUTI BIDANG PENGEMBANGAN SDM</th>
                        </tr>
                        <tr></tr><tr></tr>
                            <tr>
                                <th>Jenis Kegiatan</th>
                                <th>${data.training_type.param}</th>
                            </tr>
                            <tr>
                                <th>Judul Kegiatan</th>
                                <th>${data.training_title}</th>
                            </tr>
                            <tr>
                                <th>Provinsi</th>
                                <th>${data.province.province}</th>
                            </tr>
                            <tr>
                                <th>Kab/Kota</th>
                                <th>${data.districts_city.districts_city}</th>
                            </tr>
                            <tr>
                                <th>Jumlah Peserta</th>
                                <th>${data.total_participant}</th>
                            </tr>
                            <tr>
                                <th>Tempat Pelaksanaan</th>
                                <th>${data.place}</th>
                            </tr>
                            <tr>
                                <th>Tanggal Pelaksanaan</th>
                                <th>${tanggal(data.start_date)} s/d ${tanggal(data.finish_date)}</th>
                            </tr>
                            <tr></tr>
                        <tr>
                            <th>No</th>
                            <th>Nomor Identifikasi</th>
                            <th>Nama Lengkap</th>
                            <th>Nomor KTP</th>
                            <th>Jenis Kelamin</th>
                            <th>Tempat Lahir</th>
                            <th>Tanggal Lahir</th>
                            <th>Agama</th>
                            <th>Pendidikan Terakhir</th>
                            <th>Kab/ Kota </th>
                            <th>Telp/ HP</th>

                            <th>Status Usaha</th>

                            <th>Nama Usaha</th>
                            <th>Alamat Usaha</th>
                            <th>Sektor Usaha</th>
                            <th>Bidang Usaha</th>
                            <th>Tanggal Pendirian Usaha</th>
                            <th>NPWP Usaha</th>
                            <th>Nomor Induk Berusaha</th>
                            <th>Kekayaan Usaha (Asset) per Tahun</th>
                            <th>Volume Usaha (Omset) per Tahun</th>
                            <th>Jumlah Karyawan</th>
                            <th>Kapasitas Produksi per Tahun</th>
                            <th>Anggota Koperasi</th>

                            <th>Nomor Induk Koperasi</th>
                            <th>Nama Koperasi</th>
                            <th>Nomor Badan Hukum</th>
                            <th>Tanggal Pendirian Koperasi</th>
                            <th>Alamat Koperasi</th>
                            <th>Jenis Koperasi</th>
                            <th>Kelompok Koperasi</th>
                            <th>Sektor Usaha (Koperasi)</th>
                            <th>Koperasi Binaan</th>
                            <th>Jabatan di Koperasi</th>
                            <th>NPWP Koperasi</th>
                            <th>RAT Terakhir</th>
                            <th>Status NIK</th>
                            <th>Bentuk Koperasi</th>
                            <th>Asset Koperasi</th>
                            <th>Omzet Koperasi</th>
                            <th>SHU Koperasi</th>
                            <th>Jumlah Anggota Laki-Laki</th>
                            <th>Jumlah Anggota Perempuan</th>
                            <th>Jumlah Karyawan Laki-Laki</th>
                            <th>Jumlah Karyawan Perempuan</th>
                            <th>Jangkauan Pemasaran Produk/Layanan Koperasi</th>

                            <th>Ide Bisnis</th>
                            <th>Alamat Calon Usaha</th>
                            <th>Sektor Calon Usaha</th>
                            <th>Bidang Calon Usaha</th>
                            <th>Pendamping yang didapatkan</th>

                            <th>Email Usaha</th>
                            <th>Website Usaha</th>
                            <th>Media Sosial Usaha</th>
                            <th>Marketplace</th>

                            <th>Apakah pernah mengakses kredit perbankan/non-perbankan?</th>
                            <th>Apakah memiliki tabungan?</th>

                            <th>Perizinan Usaha</th>
                            <th>Sertifikasi Produk/Usaha</th>

                            <th>Jangkauan Pemasaran Produk/Layanan Usaha</th>
                            <th>Apakah Pernah Melakukan Ekspor?</th>
                            <th>Negara Tujuan</th>
                            <th>Volume Ekspor per Tahun</th>
                            <th>Nilai Ekspor</th>
                            <th>Apakah bapak/ibu mensuplai produk sebagai bahan baku pada usaha lain?</th>

                            <th>Apakah melakukan kemitraan dengan lembaga lain?</th>

                            <th>Permasalahan yang dihadapi dalam pengembangan usaha</th>
                            <th>Kebutuhan Diklat</th>
                            <th>Masukan/Saran</th>
                        </tr>
                    `
                    $.ajax({
                        url: `${api_url}participant/${training_id}/by_training`,
                        type: 'GET',
                        async: false,
                        data: {
                            'limit' : 'false'
                        },
                        beforeSend: function(xhr) {
                            xhr.setRequestHeader("Authorization", "Bearer " + token)
                        },
                        success: function(result) {
                            let no = 1
                            // console.log(result)
                            $.each(result.data, function(index, value) {
                            	sektor_usaha_umkm = ''
                            	sektor_usaha_koperasi = ''
                            	sektor_usaha_calon = ''
                            	marketing_reach_umkm = ''
                            	marketing_reach_koperasi = ''
                            	if (value.business_status.id == 14) {
                            		sektor_usaha_umkm = value.business_sector.param
                            		marketing_reach_umkm = value.marketing_reach.param
                            		if (value.marketing_reach.optional != null) {
		                        		if (value.marketing_reach.optional == 1) {
							                if (value.marketing_reach.id != 95) {
							                    marketing_reach_umkm += ': '+value.marketing_reach_optional
							                }
							            }
						            }
                            	}
                            	else if (value.business_status.id == 15) {
                            		sektor_usaha_koperasi = value.business_sector.param
                            		marketing_reach_koperasi = value.marketing_reach.param
                            		if (value.marketing_reach.optional != null) {
	                            		if (value.marketing_reach.optional == 1) {
						                    marketing_reach_koperasi += ': '+value.marketing_reach_optional
							            }
							        }
                            	}
                            	else if (value.business_status.id == 16) {
                            		sektor_usaha_calon = value.business_sector.param
                            	}
                            	business_email = ''
                            	business_website = ''
                            	social_media = ''
                            	marketplace = ''
                            	business_licensing = ''
                            	business_certificate = ''
                            	if (value.business_status.id == 14 || value.business_status.id == 15) {
                            		value.business_email != null ? business_email = value.business_email : 'Tidak Ada'
                            		value.business_website != null ? business_website = value.business_website : 'Tidak Ada'
	                            	length_social_media = value.social_media.length - 1
						            $.each(value.social_media, function(index, value) {
						            	if (index < length_social_media) {
							                social_media += value.choice.param+', '
						            	} else {
							                social_media += value.choice.param
						            	}
						            })
	                            	length_marketplace = value.marketplace.length - 1
						            $.each(value.marketplace, function(index, value) {
						            	if (index < length_marketplace) {
							                marketplace += value.choice.param+', '
						            	} else {
							                marketplace += value.choice.param
						            	}
						            })
	                            	length_business_licensing = value.business_licensing.length - 1
						            $.each(value.business_licensing, function(index, value) {
						            	if (index < length_business_licensing) {
							                business_licensing += value.choice.param+', '
						            	} else {
							                business_licensing += value.choice.param
						            	}
						            })
	                            	length_business_certificate = value.business_certificate.length - 1
						            $.each(value.business_certificate, function(index, value) {
						            	if (index < length_business_certificate) {
							                business_certificate += value.choice.param+', '
						            	} else {
							                business_certificate += value.choice.param
						            	}
						            })
						        }
                                append += `<tr>
                                    <td>${no++}</td>
                                    <td>${value.unique_code}</td>
                                    <td>${value.name}</td>
                                    <td>'${value.id_number}</td>
                                    <td>${value.gender == 'laki-laki' ? 'Laki-Laki' : 'Perempuan'}</td>
                                    <td>${value.place_birth}</td>
                                    <td>${tanggal(value.date_birth)}</td>
                                    <td>${value.religion_id.param}</td>
                                    <td>${value.education.param}</td>
                                    <td>${value.districts_city.districts_city}</td>
                                    <td>'${value.phone_number}</td>

                                    <td>${value.business_status.param}</td>

                                    <td>${value.name_umkm != null ? value.name_umkm : ''}</td>
                                    <td>${value.address_umkm != null ? value.address_umkm : ''}</td>
                                    <td>${sektor_usaha_umkm}</td>
                                    <td>${value.business_field_umkm != null ? value.business_field_umkm.param : ''}</td>
                                    <td>${value.date_establishment_umkm != null ? tanggal(value.date_establishment_umkm) : ''}</td>
                                    <td>${value.npwp_umkm != null ? value.npwp_umkm : ''}</td>
                                    <td>${value.nib_umkm != null ? "'"+value.nib_umkm : ''}</td>
                                    <td>${value.asset_umkm != null ? rupiah(value.asset_umkm) : ''}</td>
                                    <td>${value.omset_umkm != null ? rupiah(value.omset_umkm) : ''}</td>
                                    <td>${value.number_employees != null ? value.number_employees+' Orang' : ''}</td>
                                    <td>${value.production_capacity != null ? value.production_capacity : ''}</td>
                                    <td>${value.member_koperasi != null ? value.member_koperasi == 'yes' ? 'Ya' : 'Tidak' : ''}</td>

                                    <td>${value.registrasion_number_koperasi != null ? "'"+value.registrasion_number_koperasi : ''}</td>
                                    <td>${value.name_koperasi != null ? value.name_koperasi : ''}</td>
                                    <td>${value.legal_entity_number != null ? "'"+value.legal_entity_number : ''}</td>
                                    <td>${value.date_establishment_koperasi != null ? tanggal(value.date_establishment_koperasi) : ''}</td>
                                    <td>${value.address_koperasi != null ? value.address_koperasi : ''}</td>
                                    <td>${value.type_koperasi != null ? value.type_koperasi.param : ''}</td>
                                    <td>${value.group_koperasi != null ? value.group_koperasi.param : ''}</td>
                                    <td>${sektor_usaha_koperasi}</td>
                                    <td>${value.fostered_koperasi != null ? value.fostered_koperasi.param : ''}</td>
                                    <td>${value.position_koperasi != null ? value.position_koperasi.param : ''}</td>
                                    <td>${value.npwp_koperasi != null ? value.npwp_koperasi : ''}</td>
                                    <td>${value.last_rat != null ? 'Tahun '+value.last_rat : ''}</td>
                                    <td class="text-capitalize">${value.nik_status != null ? value.nik_status : ''}</td>
                                    <td>${value.form_koperasi != null ? value.form_koperasi.param : ''}</td>
                                    <td>${value.asset_koperasi != null ? rupiah(value.asset_koperasi) : ''}</td>
                                    <td>${value.omset_koperasi != null ? rupiah(value.omset_koperasi) : ''}</td>
                                    <td>${value.shu_koperasi != null ? rupiah(value.shu_koperasi) : ''}</td>
                                    <td>${value.total_member_koperasi_men != null ? value.total_member_koperasi_men+' Orang' : ''}</td>
                                    <td>${value.total_member_koperasi_women != null ? value.total_member_koperasi_women+' Orang' : ''}</td>
                                    <td>${value.total_employee_koperasi_men != null ? value.total_employee_koperasi_men+' Orang' : ''}</td>
                                    <td>${value.total_employee_koperasi_women != null ? value.total_employee_koperasi_women+' Orang' : ''}</td>
                                    <td>${marketing_reach_koperasi}</td>

                                    <td>${value.business_idea != null ? value.business_idea : ''}</td>
                                    <td>${value.entrepreneur_address != null ? value.entrepreneur_address : ''}</td>
                                    <td>${sektor_usaha_calon}</td>
                                    <td>${value.business_field_entrepreneur != null ? value.business_field_entrepreneur.param : ''}</td>
                                    <td>${value.accompaniment_entrepreneur != null ? value.accompaniment_entrepreneur.param : ''}</td>

                                    <td>${business_email}</td>
                                    <td>${business_website}</td>
                                    <td>${value.social_media.length != 0 ? social_media : ''}</td>
                                    <td>${value.marketplace.length != 0 ? marketplace : ''}</td>

                                    <td>${value.banking_credit != null ? value.banking_credit == 'yes' ? 'Ya' : 'Tidak' : ''}</td>
                                    <td>${value.savings != null ? value.savings == 'yes' ? 'Ya' : 'Tidak' : ''}</td>

                                    <td>${value.business_licensing.length != 0 ? business_licensing : ''}</td>
                                    <td>${value.business_certificate.length != 0 ? business_certificate : ''}</td>

                                    <td>${marketing_reach_umkm}</td>
                                    <td>${value.export != null ? value.export != 'no' ? 'Ya, '+value.export : 'Tidak' : ''}</td>
                                    <td>${value.export_destination != null ? value.export_destination : ''}</td>
                                    <td>${value.export_volume != null ? value.export_volume : ''}</td>
                                    <td>${value.export_value != null ? 'Rp.'+value.export_value : ''}</td>
                                    <td>${value.product_supply != null ? value.product_supply == 'yes' ? 'Ya' : 'Tidak' : ''}</td>

                                    <td>${value.partnership != null ? value.partnership == 'yes' ? 'Ya' : 'Tidak Ada' : ''}</td>

                                    <td>${value.monitoring.other_m1 == null ? value.monitoring.m1.param : value.monitoring.m1.param+': '+value.monitoring.other_m1}</td>
                                    <td>${value.monitoring.m2.param}</td>
                                    <td>${value.monitoring.m3}</td>
                                </tr>`
                            })
                        }
                    })
                }
            })
           
            $('#data').append(append)
            exportTableToExcel('data', 'Peserta Lengkap')
            $(".preloader").fadeOut();
            $("#download-success").removeClass('d-none');
        })
    </script>
</body>
</html>