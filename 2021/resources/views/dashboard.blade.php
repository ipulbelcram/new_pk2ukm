@extends('layouts/app')

@section('title','Dashboard')

@section('content')
	<div class="container">
		<div class="d-flex justify-content-between align-items-center mb-2">
			<h4>Kinerja DAK Nonfisik PK2UKM TA. 2021</h4>
		</div>
		<div class="row">
			<div class="col-md-6 col-xl-4 mb-4">
				<div class="card card-custom">
					<div class="card-body">
						<h6>Realisasi Anggaran</h6>
						<div class="d-flex justify-content-between align-items-center">
							<i class="mdi mdi-cash-multiple text-primary mdi-36px"></i>
							<h5 class="mb-0" id="realisasi_anggaran"></h5>
						</div>
						<h5 class="text-secondary font-weight-normal text-right" id="persentase_anggaran"></h5>
					</div>
				</div>
			</div>
			<div class="col-md-6 col-xl-4 mb-4">
				<div class="card card-custom">
					<div class="card-body">
						<h6>Realisasi Peserta</h6>
						<div class="d-flex justify-content-between align-items-center">
							<i class="mdi mdi-account-group-outline text-primary mdi-36px"></i>
							<h5 class="mb-0" id="realisasi_peserta"></h5>
						</div>
						<h5 class="text-secondary font-weight-normal text-right" id="persentase_peserta"></h5>
					</div>
				</div>
			</div>
			<div class="col-md-6 col-xl-4 mb-4">
				<div class="card card-custom">
					<div class="card-body">
						<h6>Realisasi Pendamping</h6>
						<div class="d-flex justify-content-between align-items-center">
							<i class="mdi mdi-account-supervisor-outline text-primary mdi-36px"></i>
							<h5 class="mb-0" id="realisasi_pendamping"></h5>
						</div>
						<h5 class="text-secondary font-weight-normal text-right" id="persentase_pendamping"></h5>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 mb-4">
				<div class="card card-custom">
					<div class="card-body">
						<h6>Rekapitulasi per Dinas</h6>
						<div class="row mb-xl-2 align-items-center">
							<div class="col-xl-6"><select class="custom-select mb-3 mb-xl-0" id="select-province"></select></div>
							<div class="col"><button class="btn btn-sm btn-block btn-primary cekRekapitulasiProvinsi" data-type="budget">Anggaran</button></div>
							<div class="col px-0"><button class="btn btn-sm btn-block btn-primary cekRekapitulasiProvinsi" data-type="participant">Peserta</button></div>
							<div class="col"><button class="btn btn-sm btn-block btn-primary cekRekapitulasiProvinsi" data-type="companion">Pendamping</button></div>
						</div>
						<div id="rekapitulasi-province-table" class="table-responsive d-none">
							<h6 class="pt-3 pb-2" id="total-rekapitulasi"></h6>
							<table class="table">
								<thead>
									<tr>
										<th class="text-truncate">No.</th>
										<th class="text-truncate">Provinsi</th>
										<th class="text-truncate">Dinas Prov/Kab/Kota</th>
		
										{{-- Anggaran --}}
										<th class="text-truncate th-budget">Pagu Anggaran</th>
										<th class="text-truncate th-budget">Realisasi Anggaran</th>
										<th class="text-truncate th-budget">Persentase</th>
		
										{{-- Peserta --}}
										<th class="text-truncate th-participant">Target Peserta</th>
										<th class="text-truncate th-participant">Realisasi Peserta</th>
										<th class="text-truncate th-participant">Persentase</th>
		
										{{-- Pendamping --}}
										<th class="text-truncate th-companion">Target Pendamping</th>
										<th class="text-truncate th-companion">Realiasi Pendamping</th>
										<th class="text-truncate th-companion">Persentase</th>
									</tr>
								</thead>
								<tbody id="rekapitulasi-provinsi"></tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12 mb-4">
				<div class="card card-custom">
					<div class="card-body">
						<div class="d-flex justify-content-between align-items-center mb-3">
							<h6 class="mb-0">Rekapitulasi per Provinsi</h6>
							<div class="ml-auto">
								<a href="{{url('unduh-rekap')}}" class="btn btn-sm btn-outline-primary" target="_blank">Unduh Rekap</a>
							</div>
						</div>
						<div class="table-responsive">
							<table class="table">
								<thead>
									<tr>
										<th class="text-truncate">No.</th>
										<th class="text-truncate">Provinsi</th>
										<th class="text-truncate">Anggaran</th>
										<th class="text-truncate">Realisasi Anggaran</th>
										<th class="text-truncate">Target Peserta</th>
										<th class="text-truncate">Realisasi Peserta</th>
										<th class="text-truncate">Target Pendamping</th>
										<th class="text-truncate">Realiasi Pendamping</th>
									</tr>
								</thead>
								<tbody id="peserta-provinsi"></tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6 mb-4">
				<div class="card card-custom">
					<div class="card-body">
						<h6>Peserta Berdasarkan Jenis Kelamin</h6>
						<canvas id="chartGender"></canvas>
					</div>
				</div>
			</div>
			<div class="col-md-6 mb-4">
				<div class="card card-custom">
					<div class="card-body">
						<h6>Peserta Berdasarkan Agama</h6>
						<canvas id="chartReligion"></canvas>
					</div>
				</div>
			</div>
			<div class="col-md-12 mb-4">
				<div class="card card-custom">
					<div class="card-body">
						<h6>Peserta Berdasarkan Jenis Pelatihan</h6>
						<canvas id="chartTraining"></canvas>
					</div>
				</div>
			</div>
			<div class="col-md-6 mb-4">
				<div class="card card-custom">
					<div class="card-body">
						<h6>Peserta Berdasarkan Pendidikan Terakhir</h6>
						<canvas id="chartEducation"></canvas>
					</div>
				</div>
			</div>
			<div class="col-md-6 mb-4">
				<div class="card card-custom">
					<div class="card-body">
						<h6>Peserta Berdasarkan Status Peserta</h6>
						<canvas id="chartStatus"></canvas>
					</div>
				</div>
			</div>
			<div class="col-md-6 mb-4">
				<div class="card card-custom">
					<div class="card-body">
						<h6>Kekayaan Usaha (Asset) per Tahun</h6>
						<canvas id="chartAsset"></canvas>
					</div>
				</div>
			</div>
			<div class="col-md-6 mb-4">
				<div class="card card-custom">
					<div class="card-body">
						<h6>Volume Usaha (Omset) per Tahun</h6>
						<canvas id="chartOmset"></canvas>
					</div>
				</div>
			</div>
			<div class="col-md-12 mb-4">
				<div class="card card-custom">
					<div class="card-body">
						<h6>Peserta Berdasarkan Permasalahan yang dihadapi</h6>
						<canvas id="chartProblem"></canvas>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('script')
	<!-- <script src="{{asset('assets/js/exportCsv.js')}}"></script> -->
	<script src="{{asset('assets/js/chart.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('api/dashboard.js')}}"></script>
@endsection