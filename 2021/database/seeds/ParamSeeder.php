<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ParamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Data Master
            DB::table('params')->insert([
                'parent_id' => NULL,
                'category_param' => 'education',
                'param' => 'SD',
                'active' => true,
                'order' => 1,
            ]);

            DB::table('params')->insert([
                'parent_id' => NULL,
                'category_param' => 'education',
                'param' => 'SLTP',
                'active' => true,
                'order' => 2,
            ]);

            DB::table('params')->insert([
                'parent_id' => NULL,
                'category_param' => 'education',
                'param' => 'SLTA',
                'active' => true,
                'order' => 3,
            ]);

            DB::table('params')->insert([
                'parent_id' => NULL,
                'category_param' => 'education',
                'param' => 'DIPLOMA',
                'active' => true,
                'order' => 4,
            ]);

            DB::table('params')->insert([
                'parent_id' => NULL,
                'category_param' => 'education',
                'param' => 'S1',
                'active' => true,
                'order' => 5,
            ]);

            DB::table('params')->insert([
                'parent_id' => NULL,
                'category_param' => 'education',
                'param' => 'S2',
                'active' => true,
                'order' => 6,
            ]);

            DB::table('params')->insert([
                'parent_id' => NULL,
                'category_param' => 'education',
                'param' => 'S3',
                'active' => true,
                'order' => 7,
            ]);

            DB::table('params')->insert([
                'parent_id' => NULL,
                'category_param' => 'religion',
                'param' => 'Islam',
                'active' => true,
                'order' => 1,
            ]);

            DB::table('params')->insert([
                'parent_id' => NULL,
                'category_param' => 'religion',
                'param' => 'Kristen',
                'active' => true,
                'order' => 2,
            ]);

            DB::table('params')->insert([
                'parent_id' => NULL,
                'category_param' => 'religion',
                'param' => 'Katolik',
                'active' => true,
                'order' => 3,
            ]);

            DB::table('params')->insert([
                'parent_id' => NULL,
                'category_param' => 'religion',
                'param' => 'Hindu',
                'active' => true,
                'order' => 4,
            ]);

            DB::table('params')->insert([
                'parent_id' => NULL,
                'category_param' => 'religion',
                'param' => 'Buddha',
                'active' => true,
                'order' => 5,
            ]);

            DB::table('params')->insert([
                'parent_id' => NULL,
                'category_param' => 'religion',
                'param' => 'Konghucu',
                'active' => true,
                'order' => 1,
            ]);

            DB::table('params')->insert([
                'parent_id' => NULL,
                'category_param' => 'business_status',
                'param' => 'UMKM',
                'active' => true,
                'order' => 1,
            ]);

            DB::table('params')->insert([
                'parent_id' => NULL,
                'category_param' => 'business_status',
                'param' => 'Koperasi',
                'active' => true,
                'order' => 2,
            ]);

            DB::table('params')->insert([
                'parent_id' => NULL,
                'category_param' => 'business_status',
                'param' => 'Calon Wirausaha',
                'active' => true,
                'order' => 3,
            ]);
        // End data Master

        // Busines Sector
            DB::table('params')->insert([
                'parent_id' => NULL,
                'category_param' => 'business_sector',
                'param' => '1. Pertanian, kehutanan dan perikanan',
                'active' => true,
                'order' =>  1,
            ]);

            DB::table('params')->insert([
                'parent_id' => NULL,
                'category_param' => 'business_sector',
                'param' => '2. Pertambangan dan penggalian',
                'active' => true,
                'order' =>  2,
            ]);

            DB::table('params')->insert([
                'parent_id' => NULL,
                'category_param' => 'business_sector',
                'param' => '3. Industri Pengolahan',
                'active' => true,
                'order' =>  3,
            ]);

            DB::table('params')->insert([
                'parent_id' => NULL,
                'category_param' => 'business_sector',
                'param' => '4. Pengadaan listrik, gas, uap/air panas dan udara dingin',
                'active' => true,
                'order' =>  4,
            ]);

            DB::table('params')->insert([
                'parent_id' => NULL,
                'category_param' => 'business_sector',
                'param' => '5. Treatment air, Treatment air limbah, treatment dan pemulihan material sampah, dan aktivitas remediasi',
                'active' => true,
                'order' =>  5,
            ]);

            DB::table('params')->insert([
                'parent_id' => NULL,
                'category_param' => 'business_sector',
                'param' => '6. Kontruksi',
                'active' => true,
                'order' =>  6,
            ]);

            DB::table('params')->insert([
                'parent_id' => NULL,
                'category_param' => 'business_sector',
                'param' => '7. Perdagangan besar dan eceran; reparasi dan perawatan mobil dan sepeda motor',
                'active' => true,
                'order' =>  7,
            ]);

            DB::table('params')->insert([
                'parent_id' => NULL,
                'category_param' => 'business_sector',
                'param' => '8. Pengangkutan dan pergudangan',
                'active' => true,
                'order' =>  8,
            ]);

            DB::table('params')->insert([
                'parent_id' => NULL,
                'category_param' => 'business_sector',
                'param' => '9. Penyediaan akomodasi dan penyediaan makan minum',
                'active' => true,
                'order' =>  9,
            ]);

            DB::table('params')->insert([
                'parent_id' => NULL,
                'category_param' => 'business_sector',
                'param' => '10. Informasi dan komunikasi',
                'active' => true,
                'order' =>  10,
            ]);

            DB::table('params')->insert([
                'parent_id' => NULL,
                'category_param' => 'business_sector',
                'param' => '11. Aktivitas keuangan dan asuransi',
                'active' => true,
                'order' =>  11,
            ]);

            DB::table('params')->insert([
                'parent_id' => NULL,
                'category_param' => 'business_sector',
                'param' => '12. Real estate',
                'active' => true,
                'order' =>  12,
            ]);

            DB::table('params')->insert([
                'parent_id' => NULL,
                'category_param' => 'business_sector',
                'param' => '13. Aktivitas profesional, ilmiah dan teknis',
                'active' => true,
                'order' =>  13,
            ]);

            DB::table('params')->insert([
                'parent_id' => NULL,
                'category_param' => 'business_sector',
                'param' => '14. Aktifitas penyewaan dan sewa guna usaha tanpa hak opsi, ketenagakerjaan, agen perjalanan, dan penunjang usaha lainnya',
                'active' => true,
                'order' =>  14,
            ]);

            DB::table('params')->insert([
                'parent_id' => NULL,
                'category_param' => 'business_sector',
                'param' => '15. Administrasi pemerintahan, pertahanan dan jaminan sosial wajib',
                'active' => true,
                'order' =>  15,
            ]);

            DB::table('params')->insert([
                'parent_id' => NULL,
                'category_param' => 'business_sector',
                'param' => '16. pendidikan',
                'active' => true,
                'order' =>  16,
            ]);

            DB::table('params')->insert([
                'parent_id' => NULL,
                'category_param' => 'business_sector',
                'param' => '17. Aktifitas kesehatan manusia dan aktivitas sosial',
                'active' => true,
                'order' =>  17,
            ]);

            DB::table('params')->insert([
                'parent_id' => NULL,
                'category_param' => 'business_sector',
                'param' => '18. Kesenian, hiburan dan rekreasi',
                'active' => true,
                'order' =>  18,
            ]);

            DB::table('params')->insert([
                'parent_id' => NULL,
                'category_param' => 'business_sector',
                'param' => '19. Aktivitas jasa lainnya',
                'active' => true,
                'order' =>  19,
            ]);

            DB::table('params')->insert([
                'parent_id' => NULL,
                'category_param' => 'business_sector',
                'param' => '20. Aktivitas rumah tangga sebagai pemberi kerja; aktifitas yang menghasilkan barang dan jasa oleh rumah tangga yang digunakan untuk memenuhi kebutuhan sekali',
                'active' => true,
                'order' =>  20,
            ]);

            DB::table('params')->insert([
                'parent_id' => NULL,
                'category_param' => 'business_sector',
                'param' => '21. Aktifitas badan internasional dan badan ekstra internasional lainnya',
                'active' => true,
                'order' =>  21,
            ]);
        // End Busines Sector

        // Business Field
            DB::table('params')->insert([
                'parent_id' => NULL,
                'category_param' => 'business_field',
                'param' => 'Makanan/Minuman',
                'active' => true,
                'order' =>  1,
            ]);

            DB::table('params')->insert([
                'parent_id' => NULL,
                'category_param' => 'business_field',
                'param' => 'Fashion',
                'active' => true,
                'order' => 2,
            ]);

            DB::table('params')->insert([
                'parent_id' => NULL,
                'category_param' => 'business_field',
                'param' => 'Furniture/Home Decor',
                'active' => true,
                'order' => 3,
            ]);

            DB::table('params')->insert([
                'parent_id' => NULL,
                'category_param' => 'business_field',
                'param' => 'Handicraft',
                'active' => true,
                'order' => 4,
            ]);

            DB::table('params')->insert([
                'parent_id' => NULL,
                'category_param' => 'business_field',
                'param' => 'Perdagangan',
                'active' => true,
                'order' => 5,
            ]);

            DB::table('params')->insert([
                'parent_id' => NULL,
                'category_param' => 'business_field',
                'param' => 'Pertanian/Perkebunan',
                'active' => true,
                'order' => 6,
            ]);

            DB::table('params')->insert([
                'parent_id' => NULL,
                'category_param' => 'business_field',
                'param' => 'Perikanan/Peternakan',
                'active' => true,
                'order' => 7,
            ]);

            DB::table('params')->insert([
                'parent_id' => NULL,
                'category_param' => 'business_field',
                'param' => 'Lainnya',
                'active' => true,
                'order' => 8,
            ]);
        // End Business Field

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'type_koperasi',
            'param' => 'Koperasi Jasa',
            'active' => true,
            'order' =>  1,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'type_koperasi',
            'param' => 'Koperasi Produsen',
            'active' => true,
            'order' =>  2,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'type_koperasi',
            'param' => 'Koperasi Konsumen',
            'active' => true,
            'order' =>  3,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'type_koperasi',
            'param' => 'Koperasi Simpan Pinjam',
            'active' => true,
            'order' =>  4,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'type_koperasi',
            'param' => 'Koperasi Pemasaran',
            'active' => true,
            'order' =>  4,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'type_koperasi',
            'param' => 'Lainnya',
            'active' => true,
            'order' =>  5,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'position_koperasi',
            'param' => 'Pengurus Koperasi',
            'active' => true,
            'order' =>  1,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'position_koperasi',
            'param' => 'Pengawas Koperasi',
            'active' => true,
            'order' =>  2,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'position_koperasi',
            'param' => 'Pengelola Koperasi',
            'active' => true,
            'order' =>  3,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'position_koperasi',
            'param' => 'Dewan Pengawas Koperasi',
            'active' => true,
            'order' =>  4,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'training_type',
            'param' => 'Pelatihan Perkoperasian',
            'active' => true,
            'order' =>  1,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'training_type',
            'param' => 'Kewirausahaan',
            'active' => true,
            'order' =>  2,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'training_type',
            'param' => 'Keterampilan Teknis (Vocational)',
            'active' => true,
            'order' =>  3,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'training_type',
            'param' => 'Teknologi Informasi',
            'active' => true,
            'order' =>  4,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'training_type',
            'param' => 'Kompetensi Berdasarkan Standar Kompetensi Kerja Nasional Indonesia',
            'active' => true,
            'order' =>  5,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'training_type',
            'param' => 'Manajerial',
            'active' => true,
            'order' =>  6,
        ]);


        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'expertise_companion',
            'param' => 'Bidang Kelembagaan',
            'active' => true,
            'order' =>  1,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'expertise_companion',
            'param' => 'Bidang Produksi',
            'active' => true,
            'order' =>  2,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'expertise_companion',
            'param' => 'Bidang Pembiayaan',
            'active' => true,
            'order' =>  3,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'expertise_companion',
            'param' => 'Bidang Pemasaran',
            'active' => true,
            'order' =>  4,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'expertise_companion',
            'param' => 'Bidang Pengembangan IT',
            'active' => true,
            'order' =>  5,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'expertise_companion',
            'param' => 'Bidang Pengembangan Jaringan Kerjasama',
            'active' => true,
            'order' =>  6,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'social_media',
            'param' => 'Instagram',
            'active' => true,
            'order' =>  1,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'social_media',
            'param' => 'Facebook',
            'active' => true,
            'order' =>  2,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'social_media',
            'param' => 'Lainnya',
            'active' => true,
            'order' =>  3,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'marketplace',
            'param' => 'Shopee',
            'active' => true,
            'order' =>  1,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'marketplace',
            'param' => 'Bukalapak',
            'active' => true,
            'order' =>  2,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'marketplace',
            'param' => 'Tokopedia',
            'active' => true,
            'order' =>  3,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'marketplace',
            'param' => 'Blibli',
            'active' => true,
            'order' =>  4,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'marketplace',
            'param' => 'Lazada',
            'active' => true,
            'order' =>  5,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'marketplace',
            'param' => 'Lainnya',
            'active' => true,
            'order' =>  6,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'business_licensing',
            'param' => 'NIB',
            'active' => true,
            'order' =>  1,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'business_licensing',
            'param' => 'SIUP',
            'active' => true,
            'order' =>  2,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'business_licensing',
            'param' => 'IMB',
            'active' => true,
            'order' =>  3,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'business_licensing',
            'param' => 'SKPD',
            'active' => true,
            'order' =>  4,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'business_licensing',
            'param' => 'SIUI',
            'active' => true,
            'order' =>  5,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'business_licensing',
            'param' => 'SIUJK',
            'active' => true,
            'order' =>  6,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'business_licensing',
            'param' => 'Lainnya',
            'active' => true,
            'order' =>  7,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'business_certificate',
            'param' => 'ISO',
            'active' => true,
            'order' =>  1,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'business_certificate',
            'param' => 'SNI',
            'active' => true,
            'order' =>  2,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'business_certificate',
            'param' => 'Halal',
            'active' => true,
            'order' =>  3,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'business_certificate',
            'param' => 'BPOM',
            'active' => true,
            'order' =>  4,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'business_certificate',
            'param' => 'PIRT',
            'active' => true,
            'order' =>  5,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'business_certificate',
            'param' => 'Izin DEPKES/ DINKES RI',
            'active' => true,
            'order' =>  6,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'business_certificate',
            'param' => 'Merk',
            'active' => true,
            'order' =>  7,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'business_certificate',
            'param' => 'Lainnya',
            'active' => true,
            'order' =>  8,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'marketing_reach',
            'param' => 'Lokal',
            'active' => true,
            'order' =>  1,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'marketing_reach',
            'param' => 'Lintas Kab/Kota',
            'active' => true,
            'order' =>  2,
            'optional' => true
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'marketing_reach',
            'param' => 'Lintas Provinsi',
            'active' => true,
            'order' =>  3,
            'optional' => true
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'marketing_reach',
            'param' => 'Ekspor',
            'active' => true,
            'order' =>  4,
            'optional' => true
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'accompaniment',
            'param' => 'Perbankan',
            'active' => true,
            'order' =>  1,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'accompaniment',
            'param' => 'BUKM (RKB, Telkom, Pertamina. dll)',
            'active' => true,
            'order' =>  2,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'accompaniment',
            'param' => 'Pemerintah Daerah',
            'active' => true,
            'order' =>  3,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'accompaniment',
            'param' => 'Komunitas/Asosiasi',
            'active' => true,
            'order' =>  4,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'accompaniment',
            'param' => 'Lainnya',
            'active' => true,
            'order' =>  5,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'training_needs',
            'param' => 'Perkoperasian',
            'active' => true,
            'order' =>  1,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'training_needs',
            'param' => 'Kewirausahaan',
            'active' => true,
            'order' =>  2,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'training_needs',
            'param' => 'Keterampilan Teknis',
            'active' => true,
            'order' =>  3,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'training_needs',
            'param' => 'Manajerial',
            'active' => true,
            'order' =>  4,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'training_needs',
            'param' => 'Kompetensi (SKKNI)',
            'active' => true,
            'order' =>  5,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'participant_problem',
            'param' => 'Permodalan',
            'active' => true,
            'order' =>  1,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'participant_problem',
            'param' => 'Kapasitas SDM',
            'active' => true,
            'order' =>  2,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'participant_problem',
            'param' => 'Perizinan Usaha',
            'active' => true,
            'order' =>  3,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'participant_problem',
            'param' => 'Kesulitan Distribusi Barang/produk',
            'active' => true,
            'order' =>  4,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'participant_problem',
            'param' => 'Pemasaran Secara Online',
            'active' => true,
            'order' =>  5,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'participant_problem',
            'param' => 'Akses Pasar',
            'active' => true,
            'order' =>  6,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'participant_problem',
            'param' => 'Lainnya',
            'active' => true,
            'order' =>  7,
            'optional' => true,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'form_koperasi',
            'param' => 'Primer Kab/Kota',
            'active' => true,
            'order' =>  1,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'form_koperasi',
            'param' => 'Primer Provinsi',
            'active' => true,
            'order' =>  2,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'form_koperasi',
            'param' => 'Primer Nasional',
            'active' => true,
            'order' =>  3,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'form_koperasi',
            'param' => 'Sekunder Kab/Kota',
            'active' => true,
            'order' =>  4,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'form_koperasi',
            'param' => 'Sekunder Provinsi',
            'active' => true,
            'order' =>  5,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'form_koperasi',
            'param' => 'Sekunder Nasional',
            'active' => true,
            'order' =>  6,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'group_koperasi',
            'param' => '1. KUD',
            'active' => true,
            'order' => 1,
        ]);
        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'group_koperasi',
            'param' => '2. Kop. Pertanian',
            'active' => true,
            'order' => 2,
        ]);
        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'group_koperasi',
            'param' => '3. Kop. Perkebunan',
            'active' => true,
            'order' => 3,
        ]);
        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'group_koperasi',
            'param' => '4. Kop. Peternakan',
            'active' => true,
            'order' => 4,
        ]);
        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'group_koperasi',
            'param' => '5. Kop. Nelayan',
            'active' => true,
            'order' => 5,
        ]);
        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'group_koperasi',
            'param' => '6. Kop. Kehutanan',
            'active' => true,
            'order' => 6,
        ]);
        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'group_koperasi',
            'param' => '7. Kopti',
            'active' => true,
            'order' => 7,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'group_koperasi',
            'param' => '8. Kopra',
            'active' => true,
            'order' => 8,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'group_koperasi',
            'param' => '9. Kopinkra',
            'active' => true,
            'order' => 9,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'group_koperasi',
            'param' => '10. Koppontren',
            'active' => true,
            'order' => 10,
        ]);
        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'group_koperasi',
            'param' => '11. Kopkar',
            'active' => true,
            'order' => 11,
        ]);
        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'group_koperasi',
            'param' => '12. Kop. Angkatan Darat',
            'active' => true,
            'order' => 12,
        ]);
        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'group_koperasi',
            'param' => '13. Kop. Angkatan Laut',
            'active' => true,
            'order' => 13,
        ]);
        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'group_koperasi',
            'param' => '14. Kop. Angkatan Udara',
            'active' => true,
            'order' => 14,
        ]);
        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'group_koperasi',
            'param' => '15. Kop. Kepolisian',
            'active' => true,
            'order' => 15,
        ]);
        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'group_koperasi',
            'param' => '16. Kop. Serba Usaha',
            'active' => true,
            'order' => 16,
        ]);
        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'group_koperasi',
            'param' => '17. Kop. Pasar',
            'active' => true,
            'order' => 17,
        ]);
        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'group_koperasi',
            'param' => '18. Kop. Simpan Pinjam',
            'active' => true,
            'order' => 18,
        ]);
        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'group_koperasi',
            'param' => '19. Kop. Angkutan Darat',
            'active' => true,
            'order' => 19,
        ]);
        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'group_koperasi',
            'param' => '20. Kop. Angkutan Laut',
            'active' => true,
            'order' => 20,
        ]);
        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'group_koperasi',
            'param' => '21. Kop. Angkutan Udara',
            'active' => true,
            'order' => 21,
        ]);
        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'group_koperasi',
            'param' => '22. Kop. Angkutan Sungai',
            'active' => true,
            'order' => 22,
        ]);
        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'group_koperasi',
            'param' => '23. Kop. Angkutan Penyeberangan',
            'active' => true,
            'order' => 23,
        ]);
        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'group_koperasi',
            'param' => '24. Kop. Wisata',
            'active' => true,
            'order' => 24,
        ]);
        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'group_koperasi',
            'param' => '25. Kop. Telkom',
            'active' => true,
            'order' => 25,
        ]);
        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'group_koperasi',
            'param' => '26. Kop. Perumahan',
            'active' => true,
            'order' => 26,
        ]);
        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'group_koperasi',
            'param' => '27. K.B.P.R',
            'active' => true,
            'order' => 27,
        ]);
        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'group_koperasi',
            'param' => '28. Kop. Pegawai Negeri (KPRI)',
            'active' => true,
            'order' => 28,
        ]);
        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'group_koperasi',
            'param' => '29. Kop. Listrik Pedesaan',
            'active' => true,
            'order' => 29,
        ]);
        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'group_koperasi',
            'param' => '30. Koperasi Asuransi Indonesia',
            'active' => true,
            'order' => 30,
        ]);
        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'group_koperasi',
            'param' => '31. Kop. Wanita',
            'active' => true,
            'order' => 31,
        ]);
        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'group_koperasi',
            'param' => '32. Kop. Profesi',
            'active' => true,
            'order' => 32,
        ]);
        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'group_koperasi',
            'param' => '33. Kop. Veteran',
            'active' => true,
            'order' => 33,
        ]);
        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'group_koperasi',
            'param' => '34. Kop. Wredatama',
            'active' => true,
            'order' => 34,
        ]);
        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'group_koperasi',
            'param' => '35. Kop. Pepabri',
            'active' => true,
            'order' => 35,
        ]);
        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'group_koperasi',
            'param' => '36. Kop. Mahasiswa',
            'active' => true,
            'order' => 36,
        ]);
        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'group_koperasi',
            'param' => '37. Kop. Pemuda',
            'active' => true,
            'order' => 37,
        ]);
        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'group_koperasi',
            'param' => '38. Kop. Pertambangan',
            'active' => true,
            'order' => 38,
        ]);
        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'group_koperasi',
            'param' => '39. Kop. Pedagang Kaki Lima',
            'active' => true,
            'order' => 39,
        ]);
        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'group_koperasi',
            'param' => '40. Kop. Jamu Gendong',
            'active' => true,
            'order' => 40,
        ]);
        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'group_koperasi',
            'param' => '41. Kop. Lainnya',
            'active' => true,
            'order' => 41,
        ]);
        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'group_koperasi',
            'param' => '42. Kop. Simpan Pinjam Pola Syariah (KSPPS)',
            'active' => true,
            'order' => 42,
        ]);
        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'group_koperasi',
            'param' => '43. Kop. Sekunder',
            'active' => true,
            'order' => 43,
        ]);
        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'group_koperasi',
            'param' => '44. Kop. Tenaga Kerja Bongkar Muat (TKBM)',
            'active' => true,
            'order' => 44,
        ]);
        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'group_koperasi',
            'param' => '45. Kop. Digital',
            'active' => true,
            'order' => 45,
        ]);

        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'fostered_koperasi',
            'param' => 'Koperasi Binaan Nasional',
            'active' => true,
            'order' => 1,
        ]);
        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'fostered_koperasi',
            'param' => 'Koperasi Binaan Provinsi',
            'active' => true,
            'order' => 2,
        ]);
        DB::table('params')->insert([
            'parent_id' => NULL,
            'category_param' => 'fostered_koperasi',
            'param' => 'Koperasi Binaan Kab/Kota',
            'active' => true,
            'order' => 3,
        ]);
    }

}