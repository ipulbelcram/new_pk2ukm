<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParticipantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participants', function (Blueprint $table) {
            $table->id();
            $table->foreignId('training_id')->constrained('trainings')->onDelete('cascade')->onUpdate('cascade');
            $table->string('name');
            $table->enum('gender', ['laki-laki','perempuan']);
            $table->foreignId('religion_id')->constrained('params')->onDelete('cascade')->onUpdate('cascade');
            $table->string('id_number');
            $table->string('place_birth');
            $table->date('date_birth');
            $table->foreignId('education_id')->constrained('params')->onDelete('cascade')->onUpdate('cascade');
            $table->string('phone_number');
            $table->string('email');
            $table->text('address_participant');
            $table->foreignId('province_id')->constrained('provinces')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('districts_city_id')->constrained('districts_cities')->onDelete('cascade')->onUpdate('cascade');
            $table->string('profile_photo');
            $table->bigInteger('unique_code')->unique();

            // busines status
            $table->foreignId('business_status_id')->constrained('params')->onDelete('cascade')->onUpdate('cascade');
            
            // data umkm
            $table->string('name_umkm')->nullable();
            $table->text('address_umkm')->nullable();
            $table->date('date_establishment_umkm')->nullable();
            $table->string('nib_umkm')->nullable();
            $table->integer('number_employees')->nullable();
            $table->bigInteger('asset_umkm')->nullable();
            $table->bigInteger('omset_umkm')->nullable();
            $table->string('npwp_umkm')->nullable();
            $table->foreignId('business_field_umkm_id')->nullable()->constrained('params')->onDelete('cascade')->onUpdate('cascade');
            $table->string('production_capacity')->nullable();
            $table->boolean('member_koperasi')->nullable();
            $table->boolean('banking_credit')->nullable();
            $table->boolean('savings')->nullable();
            $table->enum('export', ['pengiriman forwarder', 'pengiriman langsung'])->nullable();
            $table->string('export_destination')->nullable();
            $table->string('export_value')->nullable();
            $table->string('export_volume')->nullable();
            $table->boolean('product_supply')->nullable();

            // data koperasi
            $table->string('name_koperasi')->nullable();
            $table->text('address_koperasi')->nullable();
            $table->foreignId('type_koperasi_id')->nullable()->constrained('params')->onDelete('cascade')->onUpdate('cascade');
            $table->date('date_establishment_koperasi')->nullable();
            $table->string('registrasion_number_koperasi')->nullable();
            $table->foreignId('position_koperasi_id')->nullable()->constrained('params')->onDelete('cascade')->onUpdate('cascade');
            $table->string('npwp_koperasi')->nullable();
            $table->string('legal_entity_number')->nullable();
            $table->integer('last_rat')->nullable();
            $table->enum('nik_status', ['sudah bersertifikat', 'belum bersertifikat'])->nullable();
            $table->foreignId('form_koperasi_id')->nullable()->constrained('params')->onDelete('cascade')->onUpdate('cascade');
            $table->bigInteger('asset_koperasi')->nullable();
            $table->bigInteger('omset_koperasi')->nullable();
            $table->bigInteger('shu_koperasi')->nullable();
            $table->bigInteger('total_member_koperasi_men')->nullable();
            $table->bigInteger('total_member_koperasi_women')->nullable();
            $table->bigInteger('total_employee_koperasi_men')->nullable();
            $table->bigInteger('total_employee_koperasi_women')->nullable();
            $table->foreignId('group_koperasi_id')->nullable()->constrained('params')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('fostered_koperasi_id')->nullable()->constrained('params')->onDelete('cascade')->onUpdate('cascade');

            // ada di koperasi dan umkm
            $table->string('business_email')->nullable();
            $table->string('business_website')->nullable();
            $table->boolean('partnership')->nullable();
            $table->foreignId('marketing_reach_id')->nullable()->constrained('params')->onDelete('cascade')->onUpdate('cascade');
            $table->string('marketing_reach_optional')->nullable();

            // ada di koperasi, umkm, entrepreneur
            $table->foreignId('business_sector_id')->nullable()->constrained('params')->onDelete('cascade')->onUpdate('cascade');
            
            // entrepreneur
            $table->string('business_idea')->nullable();
            $table->string('entrepreneur_address')->nullable();
            $table->foreignId('business_field_entrepreneur_id')->nullable()->constrained('params')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('accompaniment_entrepreneur_id')->nullable()->constrained('params')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('participants');
    }
}
