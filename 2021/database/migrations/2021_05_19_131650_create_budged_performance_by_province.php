<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateBudgedPerformanceByProvince extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_budged_performance_by_province");
        DB::statement("
            CREATE VIEW vw_budged_performance_by_province as 
            SELECT 
                c.id as province_id, 
                c.province, 
                SUM(a.budged) as total_budged, 
                (SUM(a.budged_realization1) + SUM(a.budged_realization2)) as total_budged_realization,
                (((SUM(a.budged_realization1) + SUM(a.budged_realization2)) * 100) / SUM(a.budged)) as persentase_budged_realization,

                SUM(a.target_participant) as total_target_participant,
                (SUM(a.realization_participant1) + SUM(a.realization_participant2)) as total_participant_realization,
                (((SUM(a.realization_participant1) + SUM(a.realization_participant2) * 100)) / SUM(a.target_participant)) as persentase_participant_realization,

                SUM(a.target_companion) as total_target_companion,
                (SUM(a.realization_companion1) + SUM(a.realization_companion2)) as total_companion_realization,
                (((SUM(a.realization_companion1) + SUM(a.realization_companion2) * 100)) / SUM(a.target_companion)) AS persentase_companion_realization
            FROM vw_budged_performance_realization a
            LEFT JOIN users b ON b.id = a.user_id
            LEFT JOIN provinces c ON c.id = b.province_id
            GROUP by c.province
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('budged_performance_by_province');
    }
}
