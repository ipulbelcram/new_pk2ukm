<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class TotalByGender extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS laki_laki");
        DB::statement("
            CREATE VIEW laki_laki AS
            SELECT COUNT(*) as total_laki_laki FROM participants WHERE gender = 'laki-laki'
        ");
        
        DB::statement("DROP VIEW IF EXISTS perempuan");
        DB::statement("
            CREATE VIEW perempuan AS
            SELECT COUNT(*) as total_perempuan FROM participants WHERE gender = 'perempuan'
        ");
        
        DB::statement("DROP VIEW IF EXISTS vw_total_participant_by_gender");
        DB::statement("
            CREATE VIEW vw_total_participant_by_gender AS
            SELECT 
                laki_laki.total_laki_laki AS total_laki_laki,
                perempuan.total_perempuan AS total_perempuan
            FROM laki_laki, perempuan
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
