<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMonitoringCompanionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('monitoring_companions', function (Blueprint $table) {
            $table->id();
            $table->foreignId('companion_id')->constrained('companions')->onDelete('cascade')->onUpdate('cascade');
            $table->integer('month');
            $table->date('date');
            $table->text('activity_details');
            $table->string('output_koperasi');
            $table->string('output_umkm');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('monitoring_companions');
    }
}
