<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateTotalParticipantProblem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_total_participant_problem");
        DB::statement("
            CREATE VIEW vw_total_participant_problem AS
            SELECT a.param as participant_problem, COUNT(b.m1) AS total
            FROM params a
            LEFT JOIN monitorings b ON b.m1 = a.id
            WHERE a.category_param = 'participant_problem'
            GROUP BY a.id
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('total_participant_problem');
    }
}
