<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateParticipantDetailView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_participant_detail");
        DB::statement("
            CREATE VIEW vw_participant_detail AS
            SELECT
                a.id as training_id, 
                a.user_id,
                b.param as training_type,
                a.training_title,
                a.start_date,
                a.finish_date,
                a.place,
                c.districts_city,
                d.id as participant_id,
                d.unique_code,
                d.name,
                d.id_number,
                d.gender,
                d.place_birth,
                d.date_birth,
                e.param as religion,
                f.param as education,
                g.province as province_participant,
                h.districts_city as districts_cities_participant,
                d.phone_number,
                d.email,
                i.param as business_status,
                d.name_umkm,
                d.address_umkm,
                d.date_establishment_umkm,
                d.nib_umkm,
                d.omset_umkm,
                d.number_employees,
                j.param as business_sector,
                d.name_koperasi,
                d.address_koperasi,
                k.param as type_koperasi,
                d.date_establishment_koperasi,
                d.registrasion_number_koperasi,
                l.param as position_koperasi,
                d.omset_koperasi,
                d.business_idea,
                d.entrepreneur_address,
                m.param as business_field_entrepreneur,
                o.param as m1,
                o.optional as optional_m1,
                p.param as m2,
                n.m3,
                n.other_m1
                FROM trainings a
            JOIN params b ON a.training_type_id = b.id
            JOIN districts_cities c on a.districts_city_id = c.id
            LEFT JOIN participants d ON a.id = d.training_id
            LEFT JOIN params e ON d.religion_id = e.id
            LEFT JOIN params f ON d.education_id = f.id
            LEFT JOIN provinces g ON d.province_id = g.id
            LEFT JOIN districts_cities h ON d.districts_city_id = h.id
            LEFT JOIN params i ON d.business_status_id = i.id
            LEFT JOIN params j ON d.business_sector_id = j.id
            LEFT JOIN params k ON d.type_koperasi_id = k.id
            LEFT JOIN params l ON d.position_koperasi_id = l.id
            LEFT JOIN params m ON d.business_field_entrepreneur_id = m.id
            LEFT JOIN monitorings n ON d.id = n.participant_id
            LEFT JOIN params o ON n.m1 = o.id
            LEFT JOIN params p ON n.m2_id = p.id
            GROUP BY d.id
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('participant_detail_view');
    }
}
